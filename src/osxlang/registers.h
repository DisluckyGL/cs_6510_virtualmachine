#ifndef HERB_OSXLANG_REGISTERS_H_
#define HERB_OSXLANG_REGISTERS_H_

#include <arc/std/string.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief gets the encoded value of a register based on a given string
 *
 * @param registerString the register as a string
 *
 * @return encoded register value, or ~(uint8_t)0 with arc_errno set to ARC_ERRNO_DATA
*/
uint8_t HERB_OSXLangRegister_GetEncode(ARC_String *registerString);

/**
 * @brief gets the const c string value of a register based on a given encode
 *
 * @param registerEncode the register as a uint8_t encode
 *
 * @return char * register value, or NULL with arc_errno set to ARC_ERRNO_DATA
*/
const char *HERB_OSXLangRegister_GetConstCString(uint8_t registerEncode);

/**
 * @brief registers with their associated strings and encodes
*/
#define HERB_OSXLANG_REGISTER_STRING_R0 "R0"
#define HERB_OSXLANG_REGISTER_ENCODE_R0 0

#define HERB_OSXLANG_REGISTER_STRING_R1 "R1"
#define HERB_OSXLANG_REGISTER_ENCODE_R1 1

#define HERB_OSXLANG_REGISTER_STRING_R2 "R2"
#define HERB_OSXLANG_REGISTER_ENCODE_R2 2

#define HERB_OSXLANG_REGISTER_STRING_R3 "R3"
#define HERB_OSXLANG_REGISTER_ENCODE_R3 3

#define HERB_OSXLANG_REGISTER_STRING_R4 "R4"
#define HERB_OSXLANG_REGISTER_ENCODE_R4 4

#define HERB_OSXLANG_REGISTER_STRING_R5 "R5"
#define HERB_OSXLANG_REGISTER_ENCODE_R5 5

#define HERB_OSXLANG_REGISTER_STRING_R6 "R6"
#define HERB_OSXLANG_REGISTER_ENCODE_R6 6

#define HERB_OSXLANG_REGISTER_STRING_R7 "R7"
#define HERB_OSXLANG_REGISTER_ENCODE_R7 7

#define HERB_OSXLANG_REGISTER_STRING_R8 "R8"
#define HERB_OSXLANG_REGISTER_ENCODE_R8 8

#define HERB_OSXLANG_REGISTER_STRING_SP "SP"
#define HERB_OSXLANG_REGISTER_ENCODE_SP 9

#define HERB_OSXLANG_REGISTER_STRING_FP "FP"
#define HERB_OSXLANG_REGISTER_ENCODE_FP 10

#define HERB_OSXLANG_REGISTER_STRING_SL "SL"
#define HERB_OSXLANG_REGISTER_ENCODE_SL 11

#define HERB_OSXLANG_REGISTER_STRING_Z "Z"
#define HERB_OSXLANG_REGISTER_ENCODE_Z 12

#define HERB_OSXLANG_REGISTER_STRING_SB "SB"
#define HERB_OSXLANG_REGISTER_ENCODE_SB 13

#define HERB_OSXLANG_REGISTER_STRING_PC "PC"
#define HERB_OSXLANG_REGISTER_ENCODE_PC 14

/**
 * @brief defines the max rgister size
 *
 * @note can be used to create an array of registers
*/
#define HERB_OSXLANG_REGISTER_MAX_ENCODE 15

#ifdef __cplusplus
}
#endif

#endif // !HERB_OSXLANG_REGISTERS_H_