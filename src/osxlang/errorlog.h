#ifndef HERB_OSXLANG_ERROR_LOG_H_
#define HERB_OSXLANG_ERROR_LOG_H_

#include <arc/console/buffer.h>
#include <arc/std/string.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief a type that holds logs for a the osx lang
*/
typedef struct HERB_OSXLangErrorLog HERB_OSXLangErrorLog;

/**
 * @brief create a global errorlog to make using it easier
 *
 * @note when created this is set to null, you need to create it to use it
*/
extern HERB_OSXLangErrorLog *herb_errorlog;

/**
 * @brief creates HERB_OSXLangErrorLog type
 *
 * @param errorLog HERB_OSXLangErrorLog to create
*/
void HERB_OSXLangErrorLog_Create(HERB_OSXLangErrorLog **errorLog);

/**
 * @brief destroys HERB_OSXLangErrorLog type
 * 
 * @param errorLog errorLog that will be destroyed
*/
void HERB_OSXLangErrorLog_Destroy(HERB_OSXLangErrorLog *errorLog);

/**
 * @brief adds a ARC_String log to the error log
 *
 * @param errorLog HERB_OSXLangErrorLog to add log to
 * @param log      ARC_String log that will be added
*/
void HERB_OSXLangErrorLog_AddError(HERB_OSXLangErrorLog *errorLog, ARC_String *log);

/**
 * @brief adds a ARC_String log to the error log
 *
 * @param errorLog HERB_OSXLangErrorLog to add log to
 * @param log      c string log that will be added
 * @param logSize  size of c string log that will be added
*/
void HERB_OSXLangErrorLog_AddErrorCString(HERB_OSXLangErrorLog *errorLog, char *log, uint64_t logSize);

/**
 * @brief prints all logs stored within a HERB_OSXLangErrorLog type
 *
 * @param errorLog HERB_OSXLangErrorLog that holds logs to print
*/
void HERB_OSXLangErrorLog_Print(HERB_OSXLangErrorLog *errorLog);

/**
 * @brief prints all logs stored within a HERB_OSXLangErrorLog type
 *
 * @param errorLog HERB_OSXLangErrorLog that holds logs to print
 * @param buffer   the ARC_ConsoleBuffer to print to
*/
void HERB_OSXLangErrorLog_PrintToBuffer(HERB_OSXLangErrorLog *errorLog, ARC_ConsoleBuffer *buffer);

#ifdef __cplusplus
}
#endif

#endif // !HERB_OSXLANG_ERROR_LOG_H_