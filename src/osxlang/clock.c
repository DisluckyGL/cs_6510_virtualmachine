#include "clock.h"

#include <stdlib.h>

void HERB_OSXLangClock_Create(HERB_OSXLangClock **clock, HERB_OSXLangClock_TickFn tickFn){
    *clock = (HERB_OSXLangClock *)malloc(sizeof(HERB_OSXLangClock));

    (*clock)->cycle  = 0;
    (*clock)->tickFn = tickFn;
}

void HERB_OSXLangClock_Destroy(HERB_OSXLangClock *clock){
    free(clock);
}

void HERB_OSXLangClock_Tick(HERB_OSXLangClock *clock, void *userdata){
    if(clock->tickFn == NULL){
        clock->cycle++;
        return;
    }

    clock->tickFn(userdata);
    clock->cycle++;
}

uint64_t HERB_OSXLangClock_GetCycle(HERB_OSXLangClock *clock){
    return clock->cycle;
}

void HERB_OSXLangClock_SetTickFn(HERB_OSXLangClock *clock, HERB_OSXLangClock_TickFn tickFn){
    clock->tickFn = tickFn;
}