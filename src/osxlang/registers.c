#include "registers.h"
#include <arc/std/errno.h>
#include <string.h>

uint8_t HERB_OSXLangRegister_GetEncode(ARC_String *registerString){
    if(registerString == NULL){
        arc_errno = ARC_ERRNO_NULL;
        return ~(uint8_t)0;
    }

    if(ARC_String_EqualsCString(registerString, HERB_OSXLANG_REGISTER_STRING_R0, strlen(HERB_OSXLANG_REGISTER_STRING_R0))){
        return HERB_OSXLANG_REGISTER_ENCODE_R0;
    }

    if(ARC_String_EqualsCString(registerString, HERB_OSXLANG_REGISTER_STRING_R1, strlen(HERB_OSXLANG_REGISTER_STRING_R1))){
        return HERB_OSXLANG_REGISTER_ENCODE_R1;
    }

    if(ARC_String_EqualsCString(registerString, HERB_OSXLANG_REGISTER_STRING_R2, strlen(HERB_OSXLANG_REGISTER_STRING_R2))){
        return HERB_OSXLANG_REGISTER_ENCODE_R2;
    }

    if(ARC_String_EqualsCString(registerString, HERB_OSXLANG_REGISTER_STRING_R3, strlen(HERB_OSXLANG_REGISTER_STRING_R3))){
        return HERB_OSXLANG_REGISTER_ENCODE_R3;
    }

    if(ARC_String_EqualsCString(registerString, HERB_OSXLANG_REGISTER_STRING_R4, strlen(HERB_OSXLANG_REGISTER_STRING_R4))){
        return HERB_OSXLANG_REGISTER_ENCODE_R4;
    }

    if(ARC_String_EqualsCString(registerString, HERB_OSXLANG_REGISTER_STRING_R5, strlen(HERB_OSXLANG_REGISTER_STRING_R5))){
        return HERB_OSXLANG_REGISTER_ENCODE_R5;
    }

    if(ARC_String_EqualsCString(registerString, HERB_OSXLANG_REGISTER_STRING_R6, strlen(HERB_OSXLANG_REGISTER_STRING_R6))){
        return HERB_OSXLANG_REGISTER_ENCODE_R6;
    }

    if(ARC_String_EqualsCString(registerString, HERB_OSXLANG_REGISTER_STRING_R7, strlen(HERB_OSXLANG_REGISTER_STRING_R7))){
        return HERB_OSXLANG_REGISTER_ENCODE_R7;
    }

    if(ARC_String_EqualsCString(registerString, HERB_OSXLANG_REGISTER_STRING_R8, strlen(HERB_OSXLANG_REGISTER_STRING_R8))){
        return HERB_OSXLANG_REGISTER_ENCODE_R8;
    }

    if(ARC_String_EqualsCString(registerString, HERB_OSXLANG_REGISTER_STRING_SP, strlen(HERB_OSXLANG_REGISTER_STRING_SP))){
        return HERB_OSXLANG_REGISTER_ENCODE_SP;
    }

    if(ARC_String_EqualsCString(registerString, HERB_OSXLANG_REGISTER_STRING_FP, strlen(HERB_OSXLANG_REGISTER_STRING_FP))){
        return HERB_OSXLANG_REGISTER_ENCODE_FP;
    }

    if(ARC_String_EqualsCString(registerString, HERB_OSXLANG_REGISTER_STRING_SL, strlen(HERB_OSXLANG_REGISTER_STRING_SL))){
        return HERB_OSXLANG_REGISTER_ENCODE_SL;
    }

    if(ARC_String_EqualsCString(registerString, HERB_OSXLANG_REGISTER_STRING_Z, strlen(HERB_OSXLANG_REGISTER_STRING_Z))){
        return HERB_OSXLANG_REGISTER_ENCODE_Z;
    }

    if(ARC_String_EqualsCString(registerString, HERB_OSXLANG_REGISTER_STRING_SB, strlen(HERB_OSXLANG_REGISTER_STRING_SB))){
        return HERB_OSXLANG_REGISTER_ENCODE_SB;
    }

    if(ARC_String_EqualsCString(registerString, HERB_OSXLANG_REGISTER_STRING_PC, strlen(HERB_OSXLANG_REGISTER_STRING_PC))){
        return HERB_OSXLANG_REGISTER_ENCODE_PC;
    }

    arc_errno = ARC_ERRNO_DATA;
    return ~(uint8_t)0;
}

const char *HERB_OSXLangRegister_GetConstCString(uint8_t registerEncode){
    switch(registerEncode){
        case HERB_OSXLANG_REGISTER_ENCODE_R0:
            return HERB_OSXLANG_REGISTER_STRING_R0;

        case HERB_OSXLANG_REGISTER_ENCODE_R1:
            return HERB_OSXLANG_REGISTER_STRING_R1;

        case HERB_OSXLANG_REGISTER_ENCODE_R2:
            return HERB_OSXLANG_REGISTER_STRING_R2;

        case HERB_OSXLANG_REGISTER_ENCODE_R3:
            return HERB_OSXLANG_REGISTER_STRING_R3;

        case HERB_OSXLANG_REGISTER_ENCODE_R4:
            return HERB_OSXLANG_REGISTER_STRING_R4;

        case HERB_OSXLANG_REGISTER_ENCODE_R5:
            return HERB_OSXLANG_REGISTER_STRING_R5;

        case HERB_OSXLANG_REGISTER_ENCODE_R6:
            return HERB_OSXLANG_REGISTER_STRING_R6;

        case HERB_OSXLANG_REGISTER_ENCODE_R7:
            return HERB_OSXLANG_REGISTER_STRING_R7;

        case HERB_OSXLANG_REGISTER_ENCODE_R8:
            return HERB_OSXLANG_REGISTER_STRING_R8;

        case HERB_OSXLANG_REGISTER_ENCODE_SP:
            return HERB_OSXLANG_REGISTER_STRING_SP;

        case HERB_OSXLANG_REGISTER_ENCODE_FP:
            return HERB_OSXLANG_REGISTER_STRING_FP;

        case HERB_OSXLANG_REGISTER_ENCODE_SL:
            return HERB_OSXLANG_REGISTER_STRING_SL;

        case HERB_OSXLANG_REGISTER_ENCODE_Z:
            return HERB_OSXLANG_REGISTER_STRING_Z;

        case HERB_OSXLANG_REGISTER_ENCODE_SB:
            return HERB_OSXLANG_REGISTER_STRING_SB;

        case HERB_OSXLANG_REGISTER_ENCODE_PC:
            return HERB_OSXLANG_REGISTER_STRING_PC;

        default:
            arc_errno = ARC_ERRNO_DATA;
            return NULL;
    }
}