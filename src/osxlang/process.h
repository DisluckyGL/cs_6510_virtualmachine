#ifndef HERB_OSXLANG_PROCESS_H_
#define HERB_OSXLANG_PROCESS_H_

#include "memory.h"
#include "registers.h"
#include <stdint.h>
#include <arc/console/buffer.h>
#include <arc/std/bool.h>
#include <arc/std/queue.h>
#include <arc/std/string.h>
#include <arc/std/vector.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief the number of bursts to pull to determint a processes quantum
*/
#define HERB_OSXLANG_PROCESS_DEFUALT_PULL_CYCLES  5

/**
 * @brief the pull percent to determine if the process should change quantum
*/
#define HERB_OSXLANG_PROCESS_DEFUALT_PULL_PERCENT 0.80

/**
 * @brief osx lang process, is a volatile type that may change a lot as this vm changes
*/
typedef struct HERB_OSXLangProcess {
    uint32_t id;

    ARC_String *name;

    uint8_t mode;
    uint8_t completed;
    uint8_t quantum;

    uint32_t dataStartIndex;
    uint32_t dataSize;

    uint32_t registers[HERB_OSXLANG_REGISTER_MAX_ENCODE];

    ARC_Vector *memoryBlocks;

    ARC_ConsoleBuffer *buffer;
    ARC_Bool bufferConnected;
    ARC_Queue *bufferCharQueue;

    uint64_t burstsRun;
    uint8_t burstsFinished;

    uint32_t pollCycles;
    uint32_t pollCurrentCycle;
    uint32_t pollsOver;
    uint32_t pollsUnder;
} HERB_OSXLangProcess;

/**
 * @brief creates an HERB_OSXLangProcess type
 *
 * @param id             id for process
 * @param name           name for process
 * @param process        process to create
 * @param mode           the current mode of the process
 * @param dataStartIndex start of the program within the process
 * @param dataSize       size of the data within the process
 * @param programCounter current counter of process
*/
void HERB_OSXLangProcess_Create(HERB_OSXLangProcess **process, uint32_t id, ARC_String *name, uint8_t mode, uint32_t dataStartIndex, uint32_t dataSize, uint64_t programCounter);

/**
 * @brief creates an HERB_OSXLangProcess type from a file in memory
 *
 * @note will set arc_errno to ARC_ERRNO_NULL if file cannot be found in memory
 *
 * @param id             id for process
 * @param process            process to create
 * @param file               file name that is being created
 * @param softDrive          memory to read file from
 * @param randomAccessMemory memory to store process in
*/
void HERB_OSXLangProcess_CreateFromMemory(HERB_OSXLangProcess **process, uint32_t id, ARC_String *fileName, HERB_OSXLangMemory *softDrive, HERB_OSXLangMemory *randomAccessMemory);

/**
 * @brief destroys HERB_OSXLangMemory type
 * 
 * @param process process that will be destroyed
 */
void HERB_OSXLangProcess_Destroy(HERB_OSXLangProcess *process);

/**
 * @brief frees process from memory and destroys the process
 *
 * @param process            process that will be destroyed
 * @param randomAccessMemory the memory to free the alocated memory from
*/
void HERB_OSXLangProcess_DestroyAndFreeMemory(HERB_OSXLangProcess *process, HERB_OSXLangMemory *randomAccessMemory);

/**
 * @brief gets the number of blocks needed to create a process
 *
 * @param fileName           the name of the process to get number of blocks from
 * @param softDrive          the memory to get the size of the process from
 * @param randomAccessMemory the memory to get number of blocks from
*/
uint32_t HERB_OSXLangProcess_GetBlocksInMemoryNeededToCreateProcess(ARC_String *fileName, HERB_OSXLangMemory *softDrive, HERB_OSXLangMemory *randomAccessMemory);

#ifdef __cplusplus
}
#endif

#endif // !HERB_OSXLANG_PROCESS_H_