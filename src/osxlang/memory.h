#ifndef HERB_OSXLANG_MEMORY_H_
#define HERB_OSXLANG_MEMORY_H_

#include "binary.h"
#include <arc/console/buffer.h>
#include <arc/std/vector.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief HERB_OSXLang_Memory type creates an array of memory to store and run instructions
 *
 * @note data is broken up into blocks with the first byte signifying if this is allocated space and what type or not
*/
typedef struct HERB_OSXLangMemory {
    uint8_t *data;
    uint64_t dataSize;
    uint32_t blockSize;
} HERB_OSXLangMemory;

/**
 * @brief defines the size of a block, used to break up files in a simple way
*/
#define HERB_OSXLANG_MEMORY_BLOCK 256

/**
 * @brief defines the smallest size of a block
*/
#define HERB_OSXLANG_MEMORY_BLOCK_MIN_SIZE 16

/**
 * @brief types a block can be
*/
#define HERB_OSXLANG_MEMORY_BLOCK_NONE   0x00
#define HERB_OSXLANG_MEMORY_BLOCK_DATA   0x01
#define HERB_OSXLANG_MEMORY_BLOCK_FOLDER 0x02
#define HERB_OSXLANG_MEMORY_BLOCK_FILE   0x03
#define HERB_OSXLANG_MEMORY_BLOCK_SHARED 0x04

/**
 * @brief creates an HERB_OSXLangMemory type
 *
 * @param Memory memory to create
 * @param size   size of memory to create
*/
void HERB_OSXLangMemory_Create(HERB_OSXLangMemory **memory, uint64_t size);

/**
 * @brief destroys HERB_OSXLangMemory type
 * 
 * @param memory memory that will be destroyed
 */
void HERB_OSXLangMemory_Destroy(HERB_OSXLangMemory *memory);

/**
 * @brief loads a HERB_OSXLangBinary file into a HERB_OSXLangMemory type
 *
 * @param Memory memory that will store binary
 * @param binary a binary to load
*/
void HERB_OSXLangMemory_LoadBinaryFile(HERB_OSXLangMemory *memory, HERB_OSXLangBinary *binary, ARC_String *fileName);

/**
 * @brief prints contents of memory
 * 
 * @param memory       HERB_OSXLangMemory to print from
 * @param rowSize      size of row to print
 * @param groupedBytes grouping of bytes printed
*/
void HERB_OSXLangMemory_Print(HERB_OSXLangMemory *memory, uint8_t rowSize, uint8_t groupedBytes);

/**
 * @brief prints contents of memory to a buffer
 * 
 * @param memory       HERB_OSXLangMemory to print from
 * @param rowSize      size of row to print
 * @param groupedBytes grouping of bytes printed
 * @param buffer       the ARC_ConsoleBuffer to print to
*/
void HERB_OSXLangMemory_PrintToBuffer(HERB_OSXLangMemory *memory, uint8_t rowSize, uint8_t groupedBytes, ARC_ConsoleBuffer *buffer);

/**
 * @brief prints contents of memory from a given range
 * 
 * @param memory       HERB_OSXLangMemory to print from
 * @param startAddress memory location to start printing at
 * @param endAddress   memory location to end printing at
 * @param rowSize      size of row to print
 * @param groupedBytes grouping of bytes printed
*/
void HERB_OSXLangMemory_PrintRange(HERB_OSXLangMemory *memory, uint64_t startAddress, uint64_t endAddress, uint8_t rowSize, uint8_t groupedBytes);

/**
 * @brief prints contents of memory from a given range to a buffer
 * 
 * @param memory       HERB_OSXLangMemory to print from
 * @param startAddress memory location to start printing at
 * @param endAddress   memory location to end printing at
 * @param rowSize      size of row to print
 * @param groupedBytes grouping of bytes printed
 * @param buffer       the ARC_ConsoleBuffer to print to
*/
void HERB_OSXLangMemory_PrintRangeToBuffer(HERB_OSXLangMemory *memory, uint64_t startAddress, uint64_t endAddress, uint8_t rowSize, uint8_t groupedBytes, ARC_ConsoleBuffer *buffer);

/**
 * @brief prints files from memory
 * 
 * @param memory HERB_OSXLangMemory to print from
*/
void HERB_OSXLangMemory_PrintListFiles(HERB_OSXLangMemory *memory);

/**
 * @brief prints files from memory to a buffer
 * 
 * @param memory HERB_OSXLangMemory to print from
 * @param buffer the ARC_ConsoleBuffer to print to
*/
void HERB_OSXLangMemory_PrintListFilesToBuffer(HERB_OSXLangMemory *memory, ARC_ConsoleBuffer *buffer);

/**
 * @brief gets byte of data at index
 *
 * @note the parameter index will both be used an modified
 *
 * @param index  the location to get a byte from
 * @param memory memory to get byte from
 *
 * @return byte of at index
*/
uint8_t HERB_OSXLangMemory_GetByte(uint32_t *index, HERB_OSXLangMemory *memory);

/**
 * @brief gets a uint32_t of bytes starting at index
 *
 * @note the parameter index will both be used an modified
 *
 * @param index  the location to get a uint32_t of data from
 * @param memory memory to get uint32_t of data bytes from
 *
 * @return uint32_t of bytes at index
*/
uint32_t HERB_OSXLangMemory_GetBytesAsUint32(uint32_t *index, HERB_OSXLangMemory *memory);

/**
 * @brief gets byte of data at index or at next byte that is data
 *
 * @note the parameter index will both be used an modified
 *
 * @param index      the location to get a data byte from, will increment untill index is at a data byte
 * @param memory     memory to get data byte from
 * @param blocks     blocks of memory to get data byte from
 * @param blocksSize size of blocks to get data byte from
 *
 * @return byte of data at index (if data byte) or next data byte
*/
uint8_t HERB_OSXLangMemory_GetDataByteFromBlocks(uint32_t *index, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize);

/**
 * @brief gets a uint32_t of data bytes starting at index
 *
 * @note the parameter index will both be used an modified
 *
 * @param index      the location to get a data byte from, will increment untill index is at a data byte
 * @param memory     memory to get uint32_t of data bytes from
 * @param blocks     blocks of memory to get uint32_t of data bytes from
 * @param blocksSize size of blocks to get uint32_t of data bytes from
 *
 * @return uint32_t of data at index (if data byte) or next data byte
*/
uint32_t HERB_OSXLangMemory_GetDataBytesAsUint32FromBlocks(uint32_t *index, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize);

/**
 * @brief gets blocks from given start index
 *
 * @note pass in index as the starting data within a block, should be 0 unless there is a reason to start at another location
 * @note index will not be malloced, just both used an modified
 * @note blocks will be malloced, the caller of this function needs to free it
 * @note blocksSize will not be malloced, just assigned
 * @note pass in block with index as the blockStartIndex (where the block type byte is)
 *
 * @param index           the index of data within a block, should be 0 unless there is a reason to start at another location
 * @param blocks          the place to store blocks, will be NULL on errnr
 * @param blocksSize      the place to store the blocks size, will be 0 on error
 * @param memory          the memory to get blocks from
 * @param blockStartIndex the index where the blocks start
*/
void HERB_OSXLangMemory_GetBlocksFromIndex(uint32_t *index, uint32_t **blocks, uint32_t *blocksSize, HERB_OSXLangMemory *memory, uint32_t blockStartIndex);

/**
 * @brief gets the number of blocks within memory 
 *
 * @param memory the memory to get blocks from
 *
 * @return number of blocks in memory
*/
uint32_t HERB_OSXLangMemory_GetTotalBlockSize(HERB_OSXLangMemory *memory);

/**
 * @brief gets the number of available blocks within memory 
 *
 * @param memory the memory to get blocks from
 *
 * @return number of avalable blocks in memory
*/
uint32_t HERB_OSXLangMemory_GetAvailableBlockSize(HERB_OSXLangMemory *memory);

/**
 * @brief gets name of file
 *
 * @note fileName will be created, the caller of this function needs to destroy it
 * @note pass in block with index as the blockStartIndex (where the HERB_OSXLANG_MEMORY_BLOCK_FILE is)
 *
 * @param fileName        the fileName, will be assigned, or NULL on fail
 * @param memory          the memory to get file name from
 * @param blockStartIndex the index where the blocks start
*/
void HERB_OSXLangMemory_GetFileName(ARC_String **fileName, HERB_OSXLangMemory *memory, uint32_t blockStartIndex);

/**
 * @brief gets the start block index from a filename
 *
 * @param memory   the memory to get the file start block index from
 * @param fileName the file to get from memroy and return its starting block index
 *
 * @return the starting block index on success, ~(uint32_t)0 on fail
*/
uint32_t HERB_OSXLangMemory_GetStartBlockIndexFromFileName(HERB_OSXLangMemory *memory, ARC_String *fileName);

/**
 * @brief gets a vector full of file neames
 *
 * @note fileNames needs to be created before being passed in
 *
 * @param memory    HERB_OSXLangMemory to search for files
 * @param fileNames ARC_Vector that will store file names as an ARC_String
*/
void HERB_OSXLangMemory_GetVectorFileNames(HERB_OSXLangMemory *memory, ARC_Vector *fileNames);

/**
 * @brief sets byte of data at index
 *
 * @note the parameter index will both be used an modified
 *
 * @param index  the location to set a byte to
 * @param memory memory to set byte to
 * @param byte   the value of the byte to set
*/
void HERB_OSXLangMemory_SetByte(uint32_t *index, HERB_OSXLangMemory *memory, uint8_t byte);

/**
 * @brief sets a uint32_t of bytes starting at index
 *
 * @note the parameter index will both be used an modified
 *
 * @param index  the location to set a uint32_t of data to
 * @param memory memory to set uint32_t of data bytes to
 * @param uint32 the value of the uint32 to set
*/
void HERB_OSXLangMemory_SetBytesToUint32(uint32_t *index, HERB_OSXLangMemory *memory, uint32_t uint32);

/**
 * @brief sets byte of data at index or at next byte that is data
 *
 * @note the parameter index will both be used an modified
 *
 * @param index      the location to set a data byte to, will increment untill index is at a valid data byte location
 * @param memory     memory to set data byte to
 * @param blocks     blocks of memory to set data byte to
 * @param blocksSize size of blocks to set data byte to
 * @param byte       the value of the byte to set
 *
 * @return byte of data at index (if data byte) or next data byte
*/
void HERB_OSXLangMemory_SetDataByteToBlocks(uint32_t *index, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize, uint8_t byte);

/**
 * @brief gets a uint32_t of data bytes starting at index
 *
 * @note the parameter index will both be used an modified
 *
 * @param index      the location to set a data byte to, will increment untill index is at a valid data byte location
 * @param memory     memory to set uint32_t of data bytes to
 * @param blocks     blocks of memory to set uint32_t of data bytes to
 * @param blocksSize size of blocks to set uint32_t of data bytes to
 * @param uint32     the value of the uint32 to set
*/
void HERB_OSXLangMemory_SetDataBytesToUint32InBlocks(uint32_t *index, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize, uint32_t uint32);

/**
 * @brief gets blocks from given start index
 *
 * @note pass in block with index as the starting block index (where the block type byte is)
 *
 * @param index      the index of where the blocks start, will be incremented when run
 * @param memory     the memory to set the block header to
 * @param blocks     the blocks to store in the header
 * @param blocksSize the block size to store
*/
void HERB_OSXLangMemory_SetBlockHeaderAtIndex(uint32_t *index, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize);

#ifdef __cplusplus
}
#endif

#endif // !HERB_OSXLANG_MEMORY_H_
