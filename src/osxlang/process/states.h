#ifndef HERB_PROCESS_STATES_H_
#define HERB_PROCESS_STATES_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "../../state.h"
#include "../process.h"
#include "../machine.h"
#include <arc/console/buffer.h>
#include <arc/std/queue.h>
// #include <stdarg.h>

/**
 * @brief the number of states the HERB_OSXLangProcessState type holds
*/
#define HERB_OSXLANG_PROCESS_STATE_MAX_SIZE 5

/**
 * @brief the state ids for the states in a HERB_OSXLangProcessState type
*/
#define HERB_OSXLANG_PROCESS_STATE_NEW       0
#define HERB_OSXLANG_PROCESS_STATE_READY     1
#define HERB_OSXLANG_PROCESS_STATE_WAITING   2
#define HERB_OSXLANG_PROCESS_STATE_RUNNING   3
#define HERB_OSXLANG_PROCESS_STATE_TERMINATE 4

/**
 * @brief the HERB_OSXLangProcessState type, holds the states, ids, and machine to use when within a process state
*/
typedef struct HERB_OSXLangProcessState {
    HERB_State **states;
    uint32_t stateSize;

    uint32_t processSize;

    HERB_State *currentState;

    HERB_OSXLangMachine *machine;

    uint32_t currentId;

    ARC_Queue *availableIds;

}HERB_OSXLangProcessState;

/**
 * @brief creates the HERB_OSXLangProcessState and populates each state within the state machine
 *
 * @param processState the HERB_OSXLangProcessState to create
 * @param machine      the HERB_OSXLangMachine that will be used by the process state machines
*/
void HERB_OSXLangProcessState_Create(HERB_OSXLangProcessState **processState, HERB_OSXLangMachine *machine);

/**
 * @brief destroys the HERB_OSXLangProcessState type
 *
 * @param processState the HERB_OSXLangProcessState type to destory
*/
void HERB_OSXLangProcessState_Destroy(HERB_OSXLangProcessState *processState);

/**
 * @brief runs the current selected state withing the HERB_OSXLangProcessState type
 *
 * @param processState the HERB_OSXLangProcessState type to run the current state from
*/
void HERB_OSXLangProcessState_RunCurrentState(HERB_OSXLangProcessState *processState);

/**
 * @brief creates a new process from a filename and adds it to the new state wihtin a HERB_OSXLangProcessState type
 *
 * @param processState the HERB_OSXLangProcessState that holds the new state to add the new process to
 * @param fileName     the name of the file that will be turned into a process
*/
void HERB_OSXLangProcessState_CreateProcessWithFilenameAsNewState(HERB_OSXLangProcessState *processState, ARC_String *fileName);

/**
 * @brief prints a process with a state id
 *
 * @param process the process to print information from
 * @param stateId the id of the state the process is currently in
*/
void HERB_OSXLangProcessState_PrintProcessWithStateId(HERB_OSXLangProcess *process, uint32_t stateId);

/**
 * @brief prints a process with a state id to a buffer
 *
 * @param process the process to print information from
 * @param stateId the id of the state the process is currently in
 * @param buffer  the ARC_ConsoleBuffer to print to
*/
void HERB_OSXLangProcessState_PrintProcessWithStateIdToBuffer(HERB_OSXLangProcess *process, uint32_t stateId, ARC_ConsoleBuffer *buffer);

/**
 * @brief prints all of the processes within the HERB_OSXLangProcessState type
 *
 * @param processState the HERB_OSXLangProcessState to print the processes from
*/
void HERB_OSXLangProcessState_PrintProcesses(HERB_OSXLangProcessState *processState);

/**
 * @brief prints all of the processes within the HERB_OSXLangProcessState type to a buffer
 *
 * @param processState the HERB_OSXLangProcessState to print the processes from
 * @param buffer       the ARC_ConsoleBuffer to print to
*/
void HERB_OSXLangProcessState_PrintProcessesToBuffer(HERB_OSXLangProcessState *processState, ARC_ConsoleBuffer *buffer);

/**
 * @brief gets the number of processes stored within a HERB_OSXLangProcessState type
 *
 * @param processState the HERB_OSXLangProcessState to get the number of processes from
 *
 * @return the number of processesses within the HERB_OSXLangProcessState
*/
uint32_t HERB_OSXLangProcessState_GetProcessSize(HERB_OSXLangProcessState *processState);

/**
 * @brief gets a process from a HERB_OSXLangProcessState type
 *
 * @param processState the HERB_OSXLangProcessState to get the process from
 * @param id           the ide of the process to get
 *
 * @return the selected process or NULL
*/
HERB_OSXLangProcess *HERB_OSXLangProcessState_GetProcess(HERB_OSXLangProcessState *processState, uint32_t id);

/**
 * @brief sets the quantums
 *
 * @param processState the HERB_OSXLangProcessState to set the quantums to
 * @param quantumSize  number of quantums to create
 * @param quantums     the quantums provided by a uint64_t array
*/
void HERB_OSXLangProcessState_SetQuantums(HERB_OSXLangProcessState *processState, uint8_t quantumSize, uint64_t *quantums);

#ifdef __cplusplus
}
#endif

#endif // !HERB_PROCESS_STATES_H_
