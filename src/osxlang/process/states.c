#include "states.h"

#include "../machine.h"
#include "../process.h"
#include "../gantt.h"
#include "states/new.h"
#include "states/ready.h"
#include "states/running.h"
#include "states/terminate.h"
#include "states/waiting.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arc/console/buffer.h>
#include <arc/std/queue.h>

void HERB_OSXLangProcessState_Create(HERB_OSXLangProcessState **processState, HERB_OSXLangMachine *machine){
    *processState = (HERB_OSXLangProcessState *)malloc(sizeof(HERB_OSXLangProcessState));

    (*processState)->states = malloc(sizeof(HERB_State *) * HERB_OSXLANG_PROCESS_STATE_MAX_SIZE);
    (*processState)->stateSize = HERB_OSXLANG_PROCESS_STATE_MAX_SIZE;

    //NOTE: you need to add states here if you change the HERB_OSXLANG_PROCESS_STATE_NUMBER

    //new state
    HERB_State *newState;
    HERB_OSXLangProcessStateNew_Create(&newState, *processState);
    (*processState)->states[HERB_OSXLANG_PROCESS_STATE_NEW] = newState;

    //ready state
    HERB_State *readyState;
    uint64_t tempQuantums[3] = {4, 8, 64 };
    HERB_OSXLangGanttChart_SetQuantums(herb_ganttchart, 3);
    HERB_OSXLangProcessStateReady_Create(&readyState, *processState, 3, tempQuantums);
    (*processState)->states[HERB_OSXLANG_PROCESS_STATE_READY] = readyState;

    //wating state
    HERB_State *waitingState;
    HERB_OSXLangProcessStateWaiting_Create(&waitingState, *processState);
    (*processState)->states[HERB_OSXLANG_PROCESS_STATE_WAITING] = waitingState;

    //running state
    HERB_State *runningState;
    HERB_OSXLangProcessStateRunning_Create(&runningState, *processState);
    (*processState)->states[HERB_OSXLANG_PROCESS_STATE_RUNNING] = runningState;

    //terminate state
    HERB_State *terminateState;
    HERB_OSXLangProcessStateTerminate_Create(&terminateState, *processState);
    (*processState)->states[HERB_OSXLANG_PROCESS_STATE_TERMINATE] = terminateState;

    (*processState)->processSize = 0;

    (*processState)->currentState = (*processState)->states[HERB_OSXLANG_PROCESS_STATE_NEW];

    (*processState)->machine = machine;

    (*processState)->currentId = 0;

    ARC_Queue_Create(&((*processState)->availableIds));
}

void HERB_OSXLangProcessState_Destroy(HERB_OSXLangProcessState *processState){
    HERB_OSXLangProcessStateTerminate_Destroy(processState->states[HERB_OSXLANG_PROCESS_STATE_TERMINATE]);
    HERB_OSXLangProcessStateRunning_Destroy(processState->states[HERB_OSXLANG_PROCESS_STATE_RUNNING]);
    HERB_OSXLangProcessStateWaiting_Destroy(processState->states[HERB_OSXLANG_PROCESS_STATE_WAITING]);
    HERB_OSXLangProcessStateReady_Destroy(processState->states[HERB_OSXLANG_PROCESS_STATE_READY]);
    HERB_OSXLangProcessStateNew_Destroy(processState->states[HERB_OSXLANG_PROCESS_STATE_NEW]);

    uint32_t *id = NULL;
    while(ARC_Queue_Size(processState->availableIds)){
        id = ARC_Queue_Pop(processState->availableIds);

        if(id != NULL){
            free(id);
        }
    }

    ARC_Queue_Destroy(processState->availableIds);

    free(processState->states);
    free(processState);
}

void HERB_OSXLangProcessState_RunCurrentState(HERB_OSXLangProcessState *processState){
    processState->currentState->mainFn(processState->currentState->data);
}

void HERB_OSXLangProcessState_CreateProcessWithFilenameAsNewState(HERB_OSXLangProcessState *processState, ARC_String *fileName){
    //create temp process to load in when moving to the ready state
    HERB_OSXLangProcess *process;
    HERB_OSXLangProcess_Create(&process, ~(uint32_t)0, fileName, ~(uint8_t)0, ~(uint32_t)0, ~(uint32_t)0, ~(uint64_t)0);

    processState->processSize++;

    //add the temp process to the new state storage
    HERB_OSXLangProcessStateNew_AddProcess(processState->states[HERB_OSXLANG_PROCESS_STATE_NEW], process);
}

void HERB_OSXLangProcessState_PrintProcessWithStateId(HERB_OSXLangProcess *process, uint32_t stateId){
    printf("ID: %04x\tSTATE: %02x\tBLOCK SIZE:%04x\t\tQUANTUM:%01x\t\tNAME:%s\n", process->id, stateId, ARC_Vector_Size(process->memoryBlocks), process->quantum, process->name->data);
}

void HERB_OSXLangProcessState_PrintProcessWithStateIdToBuffer(HERB_OSXLangProcess *process, uint32_t stateId, ARC_ConsoleBuffer *buffer){
    //TODO: fix this number to be more dynamic
    char processCString[256];
    sprintf(processCString, "ID: %04x\tSTATE: %02x\tBLOCK SIZE:%04x\t\tQUANTUM:%01x\t\tNAME:%s\n", process->id, stateId, ARC_Vector_Size(process->memoryBlocks),process->quantum, process->name->data);
    ARC_ConsoleBuffer_AddCStringWithStrlen(buffer, processCString);
}

void HERB_OSXLangProcessState_PrintProcesses(HERB_OSXLangProcessState *processState){
    HERB_OSXLangProcessStateNew_PrintProcesses(processState->states[HERB_OSXLANG_PROCESS_STATE_NEW]);
    HERB_OSXLangProcessStateReady_PrintProcesses(processState->states[HERB_OSXLANG_PROCESS_STATE_READY]);
    HERB_OSXLangProcessStateRunning_PrintProcesses(processState->states[HERB_OSXLANG_PROCESS_STATE_RUNNING]);
    HERB_OSXLangProcessStateWaiting_PrintProcesses(processState->states[HERB_OSXLANG_PROCESS_STATE_WAITING]);
    HERB_OSXLangProcessStateTerminate_PrintProcesses(processState->states[HERB_OSXLANG_PROCESS_STATE_TERMINATE]);
}

void HERB_OSXLangProcessState_PrintProcessesToBuffer(HERB_OSXLangProcessState *processState, ARC_ConsoleBuffer *buffer){
    HERB_OSXLangProcessStateNew_PrintProcessesToBuffer(processState->states[HERB_OSXLANG_PROCESS_STATE_NEW], buffer);
    HERB_OSXLangProcessStateReady_PrintProcessesToBuffer(processState->states[HERB_OSXLANG_PROCESS_STATE_READY], buffer);
    HERB_OSXLangProcessStateRunning_PrintProcessesToBuffer(processState->states[HERB_OSXLANG_PROCESS_STATE_RUNNING], buffer);
    HERB_OSXLangProcessStateWaiting_PrintProcessesToBuffer(processState->states[HERB_OSXLANG_PROCESS_STATE_WAITING], buffer);
    HERB_OSXLangProcessStateTerminate_PrintProcessesToBuffer(processState->states[HERB_OSXLANG_PROCESS_STATE_TERMINATE], buffer);
}

uint32_t HERB_OSXLangProcessState_GetProcessSize(HERB_OSXLangProcessState *processState){
    return processState->processSize;
}

HERB_OSXLangProcess *HERB_OSXLangProcessState_GetProcess(HERB_OSXLangProcessState *processState, uint32_t id){
    HERB_OSXLangProcess *process = HERB_OSXLangProcessNew_GetProcess(processState->states[HERB_OSXLANG_PROCESS_STATE_NEW], id);
    if(process != NULL){
        return process;
    }

    process = HERB_OSXLangProcessReady_GetProcess(processState->states[HERB_OSXLANG_PROCESS_STATE_READY], id);
    if(process != NULL){
        return process;
    }

    process = HERB_OSXLangProcessRunning_GetProcess(processState->states[HERB_OSXLANG_PROCESS_STATE_RUNNING], id);
    if(process != NULL){
        return process;
    }

    process = HERB_OSXLangProcessWaiting_GetProcess(processState->states[HERB_OSXLANG_PROCESS_STATE_WAITING], id);
    if(process != NULL){
        return process;
    }

    process = HERB_OSXLangProcessTerminate_GetProcess(processState->states[HERB_OSXLANG_PROCESS_STATE_TERMINATE], id);
    if(process != NULL){
        return process;
    }

    return process;
}

void HERB_OSXLangProcessState_SetQuantums(HERB_OSXLangProcessState *processState, uint8_t quantumSize, uint64_t *quantums){
    HERB_OSXLangGanttChart_SetQuantums(herb_ganttchart, quantumSize);

    HERB_State *readyState = processState->states[HERB_OSXLANG_PROCESS_STATE_READY];
    HERB_OSXLangProcessStateReady_SetQuantums(readyState, quantumSize, quantums);
}
