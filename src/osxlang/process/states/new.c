#include "new.h"

#include "ready.h"
#include "../states.h"
#include "../../process.h"
#include <arc/console/buffer.h>
#include <arc/std/queue.h>
#include <stdlib.h>

typedef struct HERB_OSXLangProcessStateNewData {
    HERB_OSXLangProcessState *processState;
    ARC_Queue *queue;
} HERB_OSXLangProcessStateNewData;

void HERB_OSXLangProcessStateNew_Create(HERB_State **state, HERB_OSXLangProcessState *processState){
    //create the new state data
    *state = (HERB_State *)malloc(sizeof(HERB_State));
    (*state)->mainFn = HERB_OSXLangProcessStateNew_MainFn;
    (*state)->data = malloc(sizeof(HERB_OSXLangProcessStateNewData));

    //create the state data
    HERB_OSXLangProcessStateNewData *stateData = (*state)->data;
    stateData->processState = processState;
    ARC_Queue_Create(&(stateData->queue));
}

void HERB_OSXLangProcessStateNew_Destroy(HERB_State *state){
    HERB_OSXLangProcessStateNewData *stateData = (HERB_OSXLangProcessStateNewData *)state->data;

    //clear the queue
    while(ARC_Queue_Size(stateData->queue) != 0){
        HERB_OSXLangProcess *process = ARC_Queue_Pop(stateData->queue);
        HERB_OSXLangProcess_Destroy(process);
    }

    //destroy the queue
    ARC_Queue_Destroy(stateData->queue);

    //destroy the state data
    free(stateData);
}

void HERB_OSXLangProcessStateNew_MainFn(void *userdata){
    HERB_OSXLangProcessStateNewData *data = (HERB_OSXLangProcessStateNewData *)userdata;

    //if there are no processes to work idle for a bit
    if(data->processState->processSize == 0){
        return;
    }

    //if there are no processes to move to the ready queue, go to the ready state
    if(ARC_Queue_Size(data->queue) == 0){
        data->processState->currentState = data->processState->states[HERB_OSXLANG_PROCESS_STATE_READY];
        return;
    }

    //add to ready queue when space is available
    HERB_OSXLangProcess *process = (HERB_OSXLangProcess *)ARC_Queue_Pop(data->queue);
    uint32_t availableBlocks = HERB_OSXLangMemory_GetAvailableBlockSize(data->processState->machine->randomAccessMemory);
    uint32_t neededBlocks = HERB_OSXLangProcess_GetBlocksInMemoryNeededToCreateProcess(process->name, data->processState->machine->softDrive, data->processState->machine->randomAccessMemory);

    if(neededBlocks <= availableBlocks){
        uint32_t id = data->processState->currentId;
        data->processState->currentId++;

        if(ARC_Queue_Size(data->processState->availableIds) != 0){
            //reset the current id as we will us an available id instead
            data->processState->currentId--;

            //get avilable id and free it when done
            uint32_t *availableId = ARC_Queue_Pop(data->processState->availableIds);
            id = *availableId;
            free(availableId);
        }

        //create the active process and read it into memory
        HERB_OSXLangProcess *activeProcess;
        HERB_OSXLangProcess_CreateFromMemory(&activeProcess, id, process->name, data->processState->machine->softDrive, data->processState->machine->randomAccessMemory);
        HERB_OSXLangProcess_Destroy(process);
        process = NULL;

        //add the process to the ready state
        HERB_State *readyState = data->processState->states[HERB_OSXLANG_PROCESS_STATE_READY];
        HERB_OSXLangProcessStateReady_AddProcess(readyState, activeProcess);
    }

    //put the process back into the queue if it could not be created
    if(process != NULL){
        ARC_Queue_Push(data->queue, (void *)process);
    }

    //ste state to ready
    HERB_State *readyState = data->processState->states[HERB_OSXLANG_PROCESS_STATE_READY];
    data->processState->currentState = readyState;
}

void HERB_OSXLangProcessStateNew_AddProcess(HERB_State *state, HERB_OSXLangProcess *process){
    HERB_OSXLangProcessStateNewData *data = (HERB_OSXLangProcessStateNewData *)(state->data);
    ARC_Queue_Push(data->queue, (void *)process);
}

void HERB_OSXLangProcessStateNew_PrintProcesses(HERB_State *state){
    ARC_Queue *tempProcessQueue;
    ARC_Queue_Create(&tempProcessQueue);

    HERB_OSXLangProcessStateNewData *data = (HERB_OSXLangProcessStateNewData *)(state->data);
    while(ARC_Queue_Size(data->queue) != 0){
        HERB_OSXLangProcess *process = (HERB_OSXLangProcess *)ARC_Queue_Pop(data->queue);

        HERB_OSXLangProcessState_PrintProcessWithStateId(process, HERB_OSXLANG_PROCESS_STATE_NEW);

        ARC_Queue_Push(tempProcessQueue, (void *)process);
    }

    while(ARC_Queue_Size(tempProcessQueue) != 0){
        HERB_OSXLangProcess *process = ARC_Queue_Pop(tempProcessQueue);
        ARC_Queue_Push(data->queue, process);
    }

    ARC_Queue_Destroy(tempProcessQueue);
}

void HERB_OSXLangProcessStateNew_PrintProcessesToBuffer(HERB_State *state, ARC_ConsoleBuffer *buffer){
    ARC_Queue *tempProcessQueue;
    ARC_Queue_Create(&tempProcessQueue);

    HERB_OSXLangProcessStateNewData *data = (HERB_OSXLangProcessStateNewData *)(state->data);
    while(ARC_Queue_Size(data->queue) != 0){
        HERB_OSXLangProcess *process = (HERB_OSXLangProcess *)ARC_Queue_Pop(data->queue);

        HERB_OSXLangProcessState_PrintProcessWithStateIdToBuffer(process, HERB_OSXLANG_PROCESS_STATE_NEW, buffer);

        ARC_Queue_Push(tempProcessQueue, (void *)process);
    }

    while(ARC_Queue_Size(tempProcessQueue) != 0){
        HERB_OSXLangProcess *process = ARC_Queue_Pop(tempProcessQueue);
        ARC_Queue_Push(data->queue, process);
    }

    ARC_Queue_Destroy(tempProcessQueue);
}

//TODO: probs want to do this more efficiently
HERB_OSXLangProcess *HERB_OSXLangProcessNew_GetProcess(HERB_State *state, uint32_t id){
    HERB_OSXLangProcess *returnProcess = NULL;
    ARC_Queue *tempProcessQueue;
    ARC_Queue_Create(&tempProcessQueue);

    HERB_OSXLangProcessStateNewData *data = (HERB_OSXLangProcessStateNewData *)(state->data);
    while(ARC_Queue_Size(data->queue) != 0){
        HERB_OSXLangProcess *process = (HERB_OSXLangProcess *)ARC_Queue_Pop(data->queue);
        ARC_Queue_Push(tempProcessQueue, (void *)process);

        if(returnProcess != NULL){
            continue;
        }

        if(process->id == id){
            returnProcess = process;
        }
    }

    while(ARC_Queue_Size(tempProcessQueue) != 0){
        HERB_OSXLangProcess *process = ARC_Queue_Pop(tempProcessQueue);
        ARC_Queue_Push(data->queue, process);
    }

    ARC_Queue_Destroy(tempProcessQueue);

    return returnProcess;
}