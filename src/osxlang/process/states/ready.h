#ifndef HERB_PROCESS_STATE_READY_H_
#define HERB_PROCESS_STATE_READY_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "../states.h"
#include "../../../state.h"
#include <stdint.h>

/**
 * @brief creates a HERB_State for the osx lang ready process state
 *
 * @param state        the HERB_State to create 
 * @param processState the processes state to use within the main function (to switch states, add processes to other states, etc)
 * @param quantumSize  number of quantums to create
 * @param quantums     the quantums provided by a uint64_t array
*/
void HERB_OSXLangProcessStateReady_Create(HERB_State **state, HERB_OSXLangProcessState *processState, uint8_t quantumSize, uint64_t *quantums);

/**
 * @brief destroyes the HERB_State for the osx lang ready process state
 *
 * @param state the HERB_State to destroy
*/
void HERB_OSXLangProcessStateReady_Destroy(HERB_State *state);

/**
 * @brief the main HERB_State function
 *
 * @note this function will decide when to run each process
*/
void HERB_OSXLangProcessStateReady_MainFn(void *userdata);

/**
 * @brief adds a process to the osx lang ready state
 *
 * @param state   the osx lang ready state to add the process to
 * @param process the process to add to the state
*/
void HERB_OSXLangProcessStateReady_AddProcess(HERB_State *state, HERB_OSXLangProcess *process);

/**
 * @brief prints all of the processes in osx lang ready state
 *
 * @param state the osx lang ready state to print processes from
*/
void HERB_OSXLangProcessStateReady_PrintProcesses(HERB_State *state);

/**
 * @brief prints all of the processes in osx lang ready state to a buffer
 *
 * @param state  the osx lang ready state to print processes from
 * @param buffer the ARC_ConsoleBuffer to print to
*/
void HERB_OSXLangProcessStateReady_PrintProcessesToBuffer(HERB_State *state, ARC_ConsoleBuffer *buffer);

/**
 * @brief gets the number of processes in the osx lang ready state
 *
 * @param state the osx lang ready state to get the number of processes from
*/
uint32_t HERB_OSXLangProcessStateReady_ProcessesSize(HERB_State *state);

/**
 * @brief sets the quantums
 *
 * @param state       the state to set the quantums to
 * @param quantumSize number of quantums to create
 * @param quantums    the quantums provided by a uint64_t array
*/
void HERB_OSXLangProcessStateReady_SetQuantums(HERB_State *state, uint8_t quantumSize, uint64_t *quantums);

/**
 * @brief get the birst num from process
 *
 * @param state   the state to set the birst num from
 * @param process the process to get the burst num from its quantum
*/
uint64_t HERB_OSXLangProcessStateReady_GetBurstNum(HERB_State *state, HERB_OSXLangProcess *process);

/**
 * @brief gets an process from the ready state
 *
 * @param state  the osx lang ready state to get process from
 * @param id     the ARC_ConsoleBuffer to print to
 *
 * @return a HERB_OSXLangProcess if one matches the id or NULL
*/
HERB_OSXLangProcess *HERB_OSXLangProcessReady_GetProcess(HERB_State *state, uint32_t id);

#ifdef __cplusplus
}
#endif

#endif // !HERB_PROCESS_STATE_READY_H_