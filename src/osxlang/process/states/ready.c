#include "ready.h"

#include "running.h"
#include "../states.h"
#include "../../process.h"
#include <arc/console/buffer.h>
#include <arc/std/queue.h>
#include <arc/std/vector.h>
#include <stdlib.h>

typedef struct HERB_OSXLangProcessStateReadyQuantum {
    ARC_Queue *queue;
    uint64_t burst;
} HERB_OSXLangProcessStateReadyQuantum;

typedef struct HERB_OSXLangProcessStateReadyData {
    HERB_OSXLangProcessState *processState;
    ARC_Vector *quantums;

    uint32_t countedIterations;
} HERB_OSXLangProcessStateReadyData;

void HERB_OSXLangProcessStateReady_Create(HERB_State **state, HERB_OSXLangProcessState *processState, uint8_t quantumSize, uint64_t *quantums){
    //create the new state data
    *state = (HERB_State *)malloc(sizeof(HERB_State));
    (*state)->mainFn = HERB_OSXLangProcessStateReady_MainFn;
    (*state)->data = malloc(sizeof(HERB_OSXLangProcessStateReadyData));

    //create the state data
    HERB_OSXLangProcessStateReadyData *stateData = (*state)->data;
    stateData->processState = processState;
    ARC_Vector_Create(&(stateData->quantums));
    stateData->countedIterations = 0;

    HERB_OSXLangProcessStateReady_SetQuantums(*state, quantumSize, quantums);
}

void HERB_OSXLangProcessStateReady_Destroy(HERB_State *state){
    HERB_OSXLangProcessStateReadyData *stateData = (HERB_OSXLangProcessStateReadyData *)state->data;

    //clear the queue
    for(uint32_t i = 0; i < ARC_Vector_Size(stateData->quantums); i++){
        HERB_OSXLangProcessStateReadyQuantum *quantum = (HERB_OSXLangProcessStateReadyQuantum *)ARC_Vector_Get(stateData->quantums, i);

        //free data from quantum
        while(ARC_Queue_Size(quantum->queue) != 0){
            HERB_OSXLangProcess *process = (HERB_OSXLangProcess *)ARC_Queue_Pop(quantum->queue);
            HERB_OSXLangProcess_Destroy(process);
        }

        //free quantum
        free(quantum->queue);
        free(quantum);
    }

    //destroy the quantums
    ARC_Vector_Destroy(stateData->quantums);

    //destroy the state data
    free(stateData);
}

void HERB_OSXLangProcessStateReady_MainFn(void *userdata){
    HERB_OSXLangProcessStateReadyData *data = (HERB_OSXLangProcessStateReadyData *)userdata;

    //if the storage is empty move on to the running queue
    if(HERB_OSXLangProcessStateReady_ProcessesSize(data->processState->currentState) == 0){
        HERB_State *runningState = data->processState->states[HERB_OSXLANG_PROCESS_STATE_RUNNING];
        data->processState->currentState = runningState;
        return;
    }

    //get the next process to run from the quantums
    HERB_OSXLangProcessStateReadyQuantum *quantum = (HERB_OSXLangProcessStateReadyQuantum *)ARC_Vector_Get(data->quantums, data->countedIterations);

    data->countedIterations++;

    //reset the quantum to run from
    if(data->countedIterations >= ARC_Vector_Size(data->quantums)){
        data->countedIterations = 0;
    }

    if(ARC_Queue_Size(quantum->queue) == 0){
        return;
    }

    HERB_OSXLangProcess *process = (HERB_OSXLangProcess *)ARC_Queue_Pop(quantum->queue);
    HERB_State *runningState = data->processState->states[HERB_OSXLANG_PROCESS_STATE_RUNNING];
    HERB_OSXLangProcessStateRunning_AddProcess(runningState, process);

    //set state to running
    data->processState->currentState = runningState;
}

void HERB_OSXLangProcessStateReady_AddProcess(HERB_State *state, HERB_OSXLangProcess *process){
    HERB_OSXLangProcessStateReadyData *data = (HERB_OSXLangProcessStateReadyData *)(state->data);
    //if the process didn't run any bursts, place in the same queue it was in before
    if(process->burstsRun == 0){
        HERB_OSXLangProcessStateReadyQuantum *quantum = (HERB_OSXLangProcessStateReadyQuantum *)ARC_Vector_Get(data->quantums, process->quantum);
        ARC_Queue_Push(quantum->queue, process);
        return;
    }

    if(process->pollCurrentCycle < process->pollCycles){
        //place in the same quantum and update the values
        HERB_OSXLangProcessStateReadyQuantum *quantum = (HERB_OSXLangProcessStateReadyQuantum *)ARC_Vector_Get(data->quantums, process->quantum);
        ARC_Queue_Push(quantum->queue, process);

        HERB_OSXLangProcessStateReadyQuantum *previousQuantum = NULL;
        if(process->quantum > 0){
            previousQuantum = (HERB_OSXLangProcessStateReadyQuantum *)ARC_Vector_Get(data->quantums, process->quantum - 1);
        }

        process->pollCurrentCycle++;

        if(process->burstsFinished == 1){
            process->pollsOver++;
            return;
        }

        if(process->burstsRun > quantum->burst){
            process->pollsOver++;
            process->burstsRun = 0;
            return;
        }

        if(previousQuantum != NULL && process->burstsRun < previousQuantum->burst){
            process->pollsUnder++;
            process->burstsRun = 0;
            return;
        }

        process->burstsRun = 0;
        return;
    }

    process->pollCurrentCycle = 0;

    //check for percentage
    double overPercent = ((double)process->pollsOver / ((double)process->pollCycles));
    double underPercent = ((double)process->pollsUnder / ((double)process->pollCycles));

    process->burstsRun = 0;
    process->pollsOver = 0;

    if(overPercent >= HERB_OSXLANG_PROCESS_DEFUALT_PULL_PERCENT && process->quantum < ARC_Vector_Size(data->quantums) - 1){
        process->quantum++;
    }

    if(underPercent >= HERB_OSXLANG_PROCESS_DEFUALT_PULL_PERCENT && process->quantum > 0){
        process->quantum--;
    }

    HERB_OSXLangProcessStateReadyQuantum *quantum = (HERB_OSXLangProcessStateReadyQuantum *)ARC_Vector_Get(data->quantums, process->quantum);
    ARC_Queue_Push(quantum->queue, process);
}

void HERB_OSXLangProcessStateReady_PrintProcesses(HERB_State *state){
    HERB_OSXLangProcessStateReadyData *data = (HERB_OSXLangProcessStateReadyData *)(state->data);

    for(uint32_t i = 0; i < ARC_Vector_Size(data->quantums); i++){
        HERB_OSXLangProcessStateReadyQuantum *quantum = (HERB_OSXLangProcessStateReadyQuantum *)ARC_Vector_Get(data->quantums, i);

        ARC_Queue *tempProcessQueue;
        ARC_Queue_Create(&tempProcessQueue);

        while(ARC_Queue_Size(quantum->queue) != 0){
            HERB_OSXLangProcess *process = (HERB_OSXLangProcess *)ARC_Queue_Pop(quantum->queue);
            HERB_OSXLangProcessState_PrintProcessWithStateId(process, HERB_OSXLANG_PROCESS_STATE_READY);
            ARC_Queue_Push(tempProcessQueue, (void *)process);
        }

        while(ARC_Queue_Size(tempProcessQueue) != 0){
            HERB_OSXLangProcess *process = ARC_Queue_Pop(tempProcessQueue);
            ARC_Queue_Push(quantum->queue, process);
        }

        ARC_Queue_Destroy(tempProcessQueue);
    }
}

void HERB_OSXLangProcessStateReady_PrintProcessesToBuffer(HERB_State *state, ARC_ConsoleBuffer *buffer){
    HERB_OSXLangProcessStateReadyData *data = (HERB_OSXLangProcessStateReadyData *)(state->data);

    for(uint32_t i = 0; i < ARC_Vector_Size(data->quantums); i++){
        HERB_OSXLangProcessStateReadyQuantum *quantum = (HERB_OSXLangProcessStateReadyQuantum *)ARC_Vector_Get(data->quantums, i);

        ARC_Queue *tempProcessQueue;
        ARC_Queue_Create(&tempProcessQueue);

        while(ARC_Queue_Size(quantum->queue) != 0){
            HERB_OSXLangProcess *process = (HERB_OSXLangProcess *)ARC_Queue_Pop(quantum->queue);
            HERB_OSXLangProcessState_PrintProcessWithStateIdToBuffer(process, HERB_OSXLANG_PROCESS_STATE_READY, buffer);
            ARC_Queue_Push(tempProcessQueue, (void *)process);
        }

        while(ARC_Queue_Size(tempProcessQueue) != 0){
            HERB_OSXLangProcess *process = ARC_Queue_Pop(tempProcessQueue);
            ARC_Queue_Push(quantum->queue, process);
        }

        ARC_Queue_Destroy(tempProcessQueue);
    }
}

uint32_t HERB_OSXLangProcessStateReady_ProcessesSize(HERB_State *state){
    HERB_OSXLangProcessStateReadyData *data = (HERB_OSXLangProcessStateReadyData *)(state->data);

    uint32_t processesSize = 0;
    for(uint32_t i = 0; i < ARC_Vector_Size(data->quantums); i++){
        HERB_OSXLangProcessStateReadyQuantum *quantum = (HERB_OSXLangProcessStateReadyQuantum *)ARC_Vector_Get(data->quantums, i);
        processesSize += ARC_Queue_Size(quantum->queue);
    }

    return processesSize;
}

void HERB_OSXLangProcessStateReady_SetQuantums(HERB_State *state, uint8_t quantumSize, uint64_t *quantums){
    HERB_OSXLangProcessStateReadyData *stateData = (HERB_OSXLangProcessStateReadyData *)state->data;

    //setup queue to reset the current proceeses
    ARC_Queue *currentProcesses;
    ARC_Queue_Create(&currentProcesses);

    //clear the queue
    for(uint32_t i = 0; i < ARC_Vector_Size(stateData->quantums); i++){
        //get quantum at index 0
        HERB_OSXLangProcessStateReadyQuantum *quantum = (HERB_OSXLangProcessStateReadyQuantum *)ARC_Vector_Get(stateData->quantums, i);

        //free data from quantum
        while(ARC_Queue_Size(quantum->queue) != 0){
            HERB_OSXLangProcess *process = (HERB_OSXLangProcess *)ARC_Queue_Pop(quantum->queue);
            ARC_Queue_Push(currentProcesses, process);
        }

        //free quantum
        free(quantum->queue);
        free(quantum);
    }
    ARC_Vector_Destroy(stateData->quantums);
    ARC_Vector_Create(&(stateData->quantums));

    //create the new quantums
    for(uint8_t i = 0; i < quantumSize; i++){
        HERB_OSXLangProcessStateReadyQuantum *quantum = (HERB_OSXLangProcessStateReadyQuantum *)malloc(sizeof(HERB_OSXLangProcessStateReadyQuantum *));
        quantum->burst = quantums[i];
        ARC_Queue_Create(&(quantum->queue));

        //add the new quantum
        ARC_Vector_Add(stateData->quantums, quantum);
    }

    uint32_t tempIndex = 0;
    HERB_OSXLangProcessStateReadyQuantum *quantum = (HERB_OSXLangProcessStateReadyQuantum *)ARC_Vector_Get(stateData->quantums, tempIndex);
    while(ARC_Queue_Size(currentProcesses) != 0){
        //add processes to the first quantum
        ARC_Queue_Push(quantum->queue, (HERB_OSXLangProcess *)ARC_Queue_Pop(currentProcesses));
    }
    ARC_Queue_Destroy(currentProcesses);
}

uint64_t HERB_OSXLangProcessStateReady_GetBurstNum(HERB_State *state, HERB_OSXLangProcess *process){
    HERB_OSXLangProcessStateReadyData *stateData = (HERB_OSXLangProcessStateReadyData *)state->data;

    uint32_t quantumIndex = (uint32_t)process->quantum;
    HERB_OSXLangProcessStateReadyQuantum *quantum = (HERB_OSXLangProcessStateReadyQuantum *)ARC_Vector_Get(stateData->quantums, quantumIndex);

    return quantum->burst;
}

HERB_OSXLangProcess *HERB_OSXLangProcessReady_GetProcess(HERB_State *state, uint32_t id){
    HERB_OSXLangProcessStateReadyData *data = (HERB_OSXLangProcessStateReadyData *)(state->data);
    HERB_OSXLangProcess *returnProcess = NULL;

    for(uint32_t i = 0; i < ARC_Vector_Size(data->quantums); i++){
        HERB_OSXLangProcessStateReadyQuantum *quantum = (HERB_OSXLangProcessStateReadyQuantum *)ARC_Vector_Get(data->quantums, i);

        ARC_Queue *tempProcessQueue;
        ARC_Queue_Create(&tempProcessQueue);

        while(ARC_Queue_Size(quantum->queue) != 0){
            HERB_OSXLangProcess *process = (HERB_OSXLangProcess *)ARC_Queue_Pop(quantum->queue);
            ARC_Queue_Push(tempProcessQueue, (void *)process);
            
            if(returnProcess != NULL){
                continue;
            }

            if(process->id == id){
                returnProcess = process;
            }
        }

        while(ARC_Queue_Size(tempProcessQueue) != 0){
            HERB_OSXLangProcess *process = ARC_Queue_Pop(tempProcessQueue);
            ARC_Queue_Push(quantum->queue, process);
        }

        ARC_Queue_Destroy(tempProcessQueue);
    }

    return returnProcess;
}