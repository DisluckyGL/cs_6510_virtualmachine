#include "running.h"

#include "ready.h"
#include "terminate.h"
#include "waiting.h"
#include "../states.h"
#include "../../clock.h"
#include "../../gantt.h"
#include "../../process.h"
#include <arc/console/buffer.h>
#include <arc/std/queue.h>
#include <stdlib.h>

typedef struct HERB_OSXLangProcessStateRunningData {
    HERB_OSXLangProcessState *processState;
    ARC_Queue *queue;

    uint32_t countedIterations;
} HERB_OSXLangProcessStateRunningData;

void HERB_OSXLangProcessStateRunning_Create(HERB_State **state, HERB_OSXLangProcessState *processState){
    //create the new state data
    *state = (HERB_State *)malloc(sizeof(HERB_State));
    (*state)->mainFn = HERB_OSXLangProcessStateRunning_MainFn;
    (*state)->data = malloc(sizeof(HERB_OSXLangProcessStateRunningData));

    //create the state data
    HERB_OSXLangProcessStateRunningData *stateData = (*state)->data;
    stateData->processState = processState;
    ARC_Queue_Create(&(stateData->queue));
}

void HERB_OSXLangProcessStateRunning_Destroy(HERB_State *state){
    HERB_OSXLangProcessStateRunningData *stateData = (HERB_OSXLangProcessStateRunningData *)state->data;

    //clear the queue
    while(ARC_Queue_Size(stateData->queue) != 0){
        HERB_OSXLangProcess *process = ARC_Queue_Pop(stateData->queue);
        HERB_OSXLangProcess_Destroy(process);
    }

    //destroy the queue
    ARC_Queue_Destroy(stateData->queue);

    //destroy the state data
    free(stateData);
}

void HERB_OSXLangProcessStateRunning_MainFn(void *userdata){
    HERB_OSXLangProcessStateRunningData *data = (HERB_OSXLangProcessStateRunningData *)userdata;

    HERB_OSXLangProcess *process = NULL;
    while(ARC_Queue_Size(data->queue) != 0){
        process = (HERB_OSXLangProcess *)ARC_Queue_Pop(data->queue);

        HERB_State *readyState = data->processState->states[HERB_OSXLANG_PROCESS_STATE_READY];
        uint32_t burst      = HERB_OSXLangProcessStateReady_GetBurstNum(readyState, process);
        uint32_t burstsLeft = burst;
        HERB_OSXLangBinaryExecutor_ExecuteBurst(data->processState->machine->binaryExecutor, data->processState->machine->randomAccessMemory, process, &burstsLeft);
        process->burstsRun += burst - burstsLeft;

        //tick the clock once to indicate a cycle
        HERB_OSXLangClock_Tick(data->processState->machine->clock, NULL);

        if(herb_useGanttchart){
        //add to the gantt chart
            for(uint32_t i = 0; i < process->burstsRun; i++){
                HERB_OSXLangGanttChart_AddId(herb_ganttchart, process->id, process->quantum);
            }
        }

        //add completed process to terminate state
        if(process->completed){
            HERB_State *terminateState = data->processState->states[HERB_OSXLANG_PROCESS_STATE_TERMINATE];
            HERB_OSXLangProcessStateTerminate_AddProcess(terminateState, process);
            continue;
        }

        //mode bit is activated put on the wating queue
        if(process->mode == 1){
            HERB_State *waitingState = data->processState->states[HERB_OSXLANG_PROCESS_STATE_WAITING];
            HERB_OSXLangProcessStateWaiting_AddProcess(waitingState, process);
            continue;
        }

        //put process back on the ready queue
        HERB_OSXLangProcessStateReady_AddProcess(readyState, process);
    }

    //update the counted iterations number
    data->countedIterations++;

    //TODO: set number for checking the wating queue
    HERB_State *watingState = data->processState->states[HERB_OSXLANG_PROCESS_STATE_WAITING];
    if(data->countedIterations == 4 && HERB_OSXLangProcessStateWaiting_ProcessesSize(watingState) != 0){
        data->processState->currentState = watingState;
        return;
    }

    //TODO: set number for checking the terminate queue, use fallthrough so end state will allways be terminate
    HERB_State *readyState = data->processState->states[HERB_OSXLANG_PROCESS_STATE_READY];
    if(data->countedIterations < 8 && HERB_OSXLangProcessStateReady_ProcessesSize(readyState) != 0){
        data->processState->currentState = readyState;
        return;
    }

    //the fallthrough state for if the other states are empty
    HERB_State *terminateState = data->processState->states[HERB_OSXLANG_PROCESS_STATE_TERMINATE];
    data->processState->currentState = terminateState;

    //reset the counted iterations
    if(data->countedIterations > 32){
        data->countedIterations = 0;
    }
}

void HERB_OSXLangProcessStateRunning_AddProcess(HERB_State *state, HERB_OSXLangProcess *process){
    HERB_OSXLangProcessStateRunningData *data = (HERB_OSXLangProcessStateRunningData *)(state->data);
    ARC_Queue_Push(data->queue, (void *)process);
}

void HERB_OSXLangProcessStateRunning_PrintProcesses(HERB_State *state){
    ARC_Queue *tempProcessQueue;
    ARC_Queue_Create(&tempProcessQueue);

    HERB_OSXLangProcessStateRunningData *data = (HERB_OSXLangProcessStateRunningData *)(state->data);
    while(ARC_Queue_Size(data->queue) != 0){
        HERB_OSXLangProcess *process = (HERB_OSXLangProcess *)ARC_Queue_Pop(data->queue);

        HERB_OSXLangProcessState_PrintProcessWithStateId(process, HERB_OSXLANG_PROCESS_STATE_RUNNING);

        ARC_Queue_Push(tempProcessQueue, (void *)process);
    }

    while(ARC_Queue_Size(tempProcessQueue) != 0){
        HERB_OSXLangProcess *process = ARC_Queue_Pop(tempProcessQueue);
        ARC_Queue_Push(data->queue, process);
    }

    ARC_Queue_Destroy(tempProcessQueue);
}

void HERB_OSXLangProcessStateRunning_PrintProcessesToBuffer(HERB_State *state, ARC_ConsoleBuffer *buffer){
    ARC_Queue *tempProcessQueue;
    ARC_Queue_Create(&tempProcessQueue);

    HERB_OSXLangProcessStateRunningData *data = (HERB_OSXLangProcessStateRunningData *)(state->data);
    while(ARC_Queue_Size(data->queue) != 0){
        HERB_OSXLangProcess *process = (HERB_OSXLangProcess *)ARC_Queue_Pop(data->queue);

        HERB_OSXLangProcessState_PrintProcessWithStateIdToBuffer(process, HERB_OSXLANG_PROCESS_STATE_RUNNING, buffer);

        ARC_Queue_Push(tempProcessQueue, (void *)process);
    }

    while(ARC_Queue_Size(tempProcessQueue) != 0){
        HERB_OSXLangProcess *process = ARC_Queue_Pop(tempProcessQueue);
        ARC_Queue_Push(data->queue, process);
    }

    ARC_Queue_Destroy(tempProcessQueue);
}

HERB_OSXLangProcess *HERB_OSXLangProcessRunning_GetProcess(HERB_State *state, uint32_t id){
    HERB_OSXLangProcess *returnProcess = NULL;
    ARC_Queue *tempProcessQueue;
    ARC_Queue_Create(&tempProcessQueue);

    HERB_OSXLangProcessStateRunningData *data = (HERB_OSXLangProcessStateRunningData *)(state->data);
    while(ARC_Queue_Size(data->queue) != 0){
        HERB_OSXLangProcess *process = (HERB_OSXLangProcess *)ARC_Queue_Pop(data->queue);
        ARC_Queue_Push(tempProcessQueue, (void *)process);

        if(returnProcess != NULL){
            continue;
        }

        if(process->id == id){
            returnProcess = process;
        }
    }

    while(ARC_Queue_Size(tempProcessQueue) != 0){
        HERB_OSXLangProcess *process = ARC_Queue_Pop(tempProcessQueue);
        ARC_Queue_Push(data->queue, process);
    }

    ARC_Queue_Destroy(tempProcessQueue);

    return returnProcess;
}