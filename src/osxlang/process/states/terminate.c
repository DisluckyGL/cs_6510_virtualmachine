#include "terminate.h"

#include "../states.h"
#include "../../process.h"
#include <arc/console/buffer.h>
#include <arc/std/queue.h>
#include <stdlib.h>

typedef struct HERB_OSXLangProcessStateTerminateData {
    HERB_OSXLangProcessState *processState;
    ARC_Queue *queue;
} HERB_OSXLangProcessStateTerminateData;

void HERB_OSXLangProcessStateTerminate_Create(HERB_State **state, HERB_OSXLangProcessState *processState){
    //create the new state data
    *state = (HERB_State *)malloc(sizeof(HERB_State));
    (*state)->mainFn = HERB_OSXLangProcessStateTerminate_MainFn;
    (*state)->data = malloc(sizeof(HERB_OSXLangProcessStateTerminateData));

    //create the state data
    HERB_OSXLangProcessStateTerminateData *stateData = (*state)->data;
    stateData->processState = processState;
    ARC_Queue_Create(&(stateData->queue));
}

void HERB_OSXLangProcessStateTerminate_Destroy(HERB_State *state){
    HERB_OSXLangProcessStateTerminateData *stateData = (HERB_OSXLangProcessStateTerminateData *)state->data;

    //clear the queue
    while(ARC_Queue_Size(stateData->queue) != 0){
        HERB_OSXLangProcess *process = ARC_Queue_Pop(stateData->queue);
        HERB_OSXLangProcess_Destroy(process);
    }

    //destroy the queue
    ARC_Queue_Destroy(stateData->queue);

    //destroy the state data
    free(stateData);
}

void HERB_OSXLangProcessStateTerminate_MainFn(void *userdata){
    HERB_OSXLangProcessStateTerminateData *data = (HERB_OSXLangProcessStateTerminateData *)userdata;

    ARC_Queue *tempProcessQueue;
    ARC_Queue_Create(&tempProcessQueue);

    HERB_OSXLangProcess *process = NULL;
    while(ARC_Queue_Size(data->queue) != 0){
        process = (HERB_OSXLangProcess *)ARC_Queue_Pop(data->queue);

        //the shell is still connectd, it needs to disconnect before it can be removed
        if(process->bufferConnected){
            ARC_Queue_Push(tempProcessQueue, (void *)process);
            continue;
        }

        data->processState->processSize--;

        //create id to store in availableIds
        uint32_t *availableId = (uint32_t *)malloc(sizeof(uint32_t));
        *availableId = process->id;

        //clean up the process and remove it from memory
        HERB_OSXLangProcess_DestroyAndFreeMemory(process, data->processState->machine->randomAccessMemory);

        ARC_Queue_Push(data->processState->availableIds, availableId);
    }

    //readd processes that are still connected to a shell
    while(ARC_Queue_Size(tempProcessQueue) != 0){
        HERB_OSXLangProcess *process = ARC_Queue_Pop(tempProcessQueue);
        ARC_Queue_Push(data->queue, process);
    }
    ARC_Queue_Destroy(tempProcessQueue);

    //go back to the initial state
    HERB_State *newState = data->processState->states[HERB_OSXLANG_PROCESS_STATE_NEW];
    data->processState->currentState = newState;
}

void HERB_OSXLangProcessStateTerminate_AddProcess(HERB_State *state, HERB_OSXLangProcess *process){
    HERB_OSXLangProcessStateTerminateData *data = (HERB_OSXLangProcessStateTerminateData *)(state->data);
    ARC_Queue_Push(data->queue, (void *)process);
}

void HERB_OSXLangProcessStateTerminate_PrintProcesses(HERB_State *state){
    ARC_Queue *tempProcessQueue;
    ARC_Queue_Create(&tempProcessQueue);

    HERB_OSXLangProcessStateTerminateData *data = (HERB_OSXLangProcessStateTerminateData *)(state->data);
    while(ARC_Queue_Size(data->queue) != 0){
        HERB_OSXLangProcess *process = (HERB_OSXLangProcess *)ARC_Queue_Pop(data->queue);

        HERB_OSXLangProcessState_PrintProcessWithStateId(process, HERB_OSXLANG_PROCESS_STATE_TERMINATE);

        ARC_Queue_Push(tempProcessQueue, (void *)process);
    }

    while(ARC_Queue_Size(tempProcessQueue) != 0){
        HERB_OSXLangProcess *process = ARC_Queue_Pop(tempProcessQueue);
        ARC_Queue_Push(data->queue, process);
    }

    ARC_Queue_Destroy(tempProcessQueue);
}

void HERB_OSXLangProcessStateTerminate_PrintProcessesToBuffer(HERB_State *state, ARC_ConsoleBuffer *buffer){
    ARC_Queue *tempProcessQueue;
    ARC_Queue_Create(&tempProcessQueue);

    HERB_OSXLangProcessStateTerminateData *data = (HERB_OSXLangProcessStateTerminateData *)(state->data);
    while(ARC_Queue_Size(data->queue) != 0){
        HERB_OSXLangProcess *process = (HERB_OSXLangProcess *)ARC_Queue_Pop(data->queue);

        HERB_OSXLangProcessState_PrintProcessWithStateIdToBuffer(process, HERB_OSXLANG_PROCESS_STATE_TERMINATE, buffer);

        ARC_Queue_Push(tempProcessQueue, (void *)process);
    }

    while(ARC_Queue_Size(tempProcessQueue) != 0){
        HERB_OSXLangProcess *process = ARC_Queue_Pop(tempProcessQueue);
        ARC_Queue_Push(data->queue, process);
    }

    ARC_Queue_Destroy(tempProcessQueue);
}

HERB_OSXLangProcess *HERB_OSXLangProcessTerminate_GetProcess(HERB_State *state, uint32_t id){
    HERB_OSXLangProcess *returnProcess = NULL;
    ARC_Queue *tempProcessQueue;
    ARC_Queue_Create(&tempProcessQueue);

    HERB_OSXLangProcessStateTerminateData *data = (HERB_OSXLangProcessStateTerminateData *)(state->data);
    while(ARC_Queue_Size(data->queue) != 0){
        HERB_OSXLangProcess *process = (HERB_OSXLangProcess *)ARC_Queue_Pop(data->queue);
        ARC_Queue_Push(tempProcessQueue, (void *)process);

        if(returnProcess != NULL){
            continue;
        }

        if(process->id == id){
            returnProcess = process;
        }
    }

    while(ARC_Queue_Size(tempProcessQueue) != 0){
        HERB_OSXLangProcess *process = ARC_Queue_Pop(tempProcessQueue);
        ARC_Queue_Push(data->queue, process);
    }

    ARC_Queue_Destroy(tempProcessQueue);

    return returnProcess;
}