#include "waiting.h"

#include "ready.h"
#include "terminate.h"
#include "../states.h"
#include "../../process.h"
#include "../../registers.h"
#include "../../machine.h"
#include "../../memory.h"
#include "../../sharedmemory.h"
#include <arc/console/buffer.h>
#include <arc/std/bool.h>
#include <arc/std/queue.h>
#include <stdlib.h>
#include <stdio.h>

typedef struct HERB_OSXLangProcessStateWaitingData {
    HERB_OSXLangProcessState *processState;
    ARC_Queue *queue;
} HERB_OSXLangProcessStateWaitingData;

void HERB_OSXLangProcessStateWaiting_Create(HERB_State **state, HERB_OSXLangProcessState *processState){
    //create the new state data
    *state = (HERB_State *)malloc(sizeof(HERB_State));
    (*state)->mainFn = HERB_OSXLangProcessStateWaiting_MainFn;
    (*state)->data = malloc(sizeof(HERB_OSXLangProcessStateWaitingData));

    //create the state data
    HERB_OSXLangProcessStateWaitingData *stateData = (*state)->data;
    stateData->processState = processState;
    ARC_Queue_Create(&(stateData->queue));
}

void HERB_OSXLangProcessStateWaiting_Destroy(HERB_State *state){
    HERB_OSXLangProcessStateWaitingData *stateData = (HERB_OSXLangProcessStateWaitingData *)state->data;

    //clear the queue
    while(ARC_Queue_Size(stateData->queue) != 0){
        HERB_OSXLangProcess *process = ARC_Queue_Pop(stateData->queue);
        HERB_OSXLangProcess_Destroy(process);
    }

    //destroy the queue
    ARC_Queue_Destroy(stateData->queue);

    //destroy the state data
    free(stateData);
}

void HERB_OSXLangProcessStateWaiting_MainFn(void *userdata){
    HERB_OSXLangProcessStateWaitingData *data = (HERB_OSXLangProcessStateWaitingData *)userdata;

    HERB_State *readyState     = data->processState->states[HERB_OSXLANG_PROCESS_STATE_READY];
    HERB_State *waitingState   = data->processState->states[HERB_OSXLANG_PROCESS_STATE_WAITING];
    HERB_State *terminateState = data->processState->states[HERB_OSXLANG_PROCESS_STATE_TERMINATE];

    //the waiting state is empty, go back to the ready state
    if(ARC_Queue_Size(data->queue) == 0){
        data->processState->currentState = readyState;
        return;
    }

    //TODO: check for if io event occured, and move process to ready state if it has
    HERB_OSXLangProcess *process = (HERB_OSXLangProcess *)ARC_Queue_Pop(data->queue);

    process->mode = 0;

    //get process blocks in a format that can be used by HERB_OSXLangMemory
    uint32_t blocksSize = ARC_Vector_Size(process->memoryBlocks);
    uint32_t blocks[blocksSize];
    for(uint32_t i = 0; i < blocksSize; i++){
        blocks[i] = *(uint32_t *)ARC_Vector_Get(process->memoryBlocks, i);
    }

    //read in instruction
    uint32_t index = process->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] + ((process->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] / data->processState->machine->randomAccessMemory->dataSize) + 1);
    HERB_OSXLangInstruction *instruction;
    HERB_OSXLangInstruction_Create(&instruction, HERB_OSXLANG_INSTRUCTION_SIZE);

    for(uint32_t i = 0; i < HERB_OSXLANG_INSTRUCTION_SIZE; i++){
        instruction->bytes[i] = HERB_OSXLangMemory_GetDataByteFromBlocks(&index, data->processState->machine->randomAccessMemory, blocks, blocksSize);
    }

    uint32_t swiInstruction = 0;
    swiInstruction |= (0xff000000 & instruction->bytes[1] << 24); 
    swiInstruction |= (0x00ff0000 & instruction->bytes[2] << 16);
    swiInstruction |= (0x0000ff00 & instruction->bytes[3] <<  8);
    swiInstruction |= (0x000000ff & instruction->bytes[4] <<  0);

    uint32_t memoryIndex = process->registers[HERB_OSXLANG_REGISTER_ENCODE_R0] + ((process->registers[HERB_OSXLANG_REGISTER_ENCODE_R0] / data->processState->machine->randomAccessMemory->dataSize) + 1);
    uint8_t character;
    uint32_t *sharedBlocks = NULL;
    uint32_t sharedBlocksSize = 0;
    uint32_t sharedIndex = 0;
    uint32_t lockId = 0;
    switch(swiInstruction){
        case HERB_OSXLANG_INSTRUCTION_SWI_COMMAND_OUTPUT_BYTE:
            character = HERB_OSXLangMemory_GetDataByteFromBlocks(&memoryIndex, data->processState->machine->randomAccessMemory, blocks, blocksSize);
            ARC_ConsoleBuffer_AddChar(process->buffer, character);

            //update the program counter
            process->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] += HERB_OSXLANG_INSTRUCTION_SIZE;

            //add the process to the ready state
            HERB_OSXLangProcessStateReady_AddProcess(readyState, process);
            break;

        case HERB_OSXLANG_INSTRUCTION_SWI_COMMAND_OUTPUT_STRING:
            character = HERB_OSXLangMemory_GetDataByteFromBlocks(&memoryIndex, data->processState->machine->randomAccessMemory, blocks, blocksSize);
            while(character != (uint8_t)'\0'){
                ARC_ConsoleBuffer_AddChar(process->buffer, character);
                character = HERB_OSXLangMemory_GetDataByteFromBlocks(&memoryIndex, data->processState->machine->randomAccessMemory, blocks, blocksSize);
            }

            //update the program counter
            process->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] += HERB_OSXLANG_INSTRUCTION_SIZE;

            //add the process to the ready state
            HERB_OSXLangProcessStateReady_AddProcess(readyState, process);
            break;

        case HERB_OSXLANG_INSTRUCTION_SWI_COMMAND_INPUT_BYTE:
            if(!process->bufferConnected || ARC_Queue_Size(process->bufferCharQueue) == 0){
                HERB_State *waitingState = data->processState->states[HERB_OSXLANG_PROCESS_STATE_WAITING];
                HERB_OSXLangProcessStateWaiting_AddProcess(waitingState, process);
                break;
            }

            uint8_t *characterPtr = ARC_Queue_Pop(process->bufferCharQueue);
            ARC_ConsoleBuffer_AddChar(process->buffer, *characterPtr);
            free(characterPtr);

            //update the program counter
            process->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] += HERB_OSXLANG_INSTRUCTION_SIZE;

            //add the process to the ready state
            HERB_OSXLangProcessStateReady_AddProcess(readyState, process);
            break;

        case HERB_OSXLANG_INSTRUCTION_SWI_COMMAND_INPUT_STRING:
            //TODO: write this

            //update the program counter
            HERB_OSXLangProcessStateReady_AddProcess(readyState, process);

            //add the process to the ready state
            HERB_OSXLangProcessStateReady_AddProcess(readyState, process);
            break;

        case HERB_OSXLANG_INSTRUCTION_SWI_COMMAND_SHARED_MEMORY_CREATE:
            //TODO: might need to decrement the program counter on fail
            if(!HERB_OSXLangMemory_ReserveSharedSection(data->processState->machine->randomAccessMemory, process->registers[HERB_OSXLANG_REGISTER_ENCODE_R0], process->registers[HERB_OSXLANG_REGISTER_ENCODE_R1])){
                HERB_OSXLangProcessStateWaiting_AddProcess(waitingState, process);
                process->mode = 1;
                break;
            }

            //was able to reseve the memory so increemnt the program counter
            process->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] += HERB_OSXLANG_INSTRUCTION_SIZE;
            HERB_OSXLangProcessStateReady_AddProcess(readyState, process);
            break;

        case HERB_OSXLANG_INSTRUCTION_SWI_COMMAND_SHARED_MEMORY_DESTROY:
            HERB_OSXLangMemory_ClearSharedSection(data->processState->machine->randomAccessMemory, process->registers[HERB_OSXLANG_REGISTER_ENCODE_R0]);
            process->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] += HERB_OSXLANG_INSTRUCTION_SIZE;
            HERB_OSXLangProcessStateReady_AddProcess(readyState, process);
            break;

        case HERB_OSXLANG_INSTRUCTION_SWI_COMMAND_SHARED_MEMORY_SET_BYTE:
            HERB_OSXLangMemory_GetSharedBlocksFromId(process->registers[HERB_OSXLANG_REGISTER_ENCODE_R0], &sharedBlocks, &sharedBlocksSize, data->processState->machine->randomAccessMemory, &sharedIndex);
            //the shared blocks don't exist, so ignore till they do?
            if(sharedBlocksSize == 0){
                HERB_OSXLangProcessStateWaiting_AddProcess(waitingState, process);
                process->mode = 1;
                break;
            }

            sharedIndex += process->registers[HERB_OSXLANG_REGISTER_ENCODE_R2];
            HERB_OSXLangMemory_SetDataByteToBlocks(&sharedIndex, data->processState->machine->randomAccessMemory, sharedBlocks, sharedBlocksSize, process->registers[HERB_OSXLANG_REGISTER_ENCODE_R1]);

            free(sharedBlocks);

            process->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] += HERB_OSXLANG_INSTRUCTION_SIZE;
            HERB_OSXLangProcessStateReady_AddProcess(readyState, process);
            break;

        case HERB_OSXLANG_INSTRUCTION_SWI_COMMAND_SHARED_MEMORY_GET_BYTE:
            HERB_OSXLangMemory_GetSharedBlocksFromId(process->registers[HERB_OSXLANG_REGISTER_ENCODE_R0], &sharedBlocks, &sharedBlocksSize, data->processState->machine->randomAccessMemory, &sharedIndex);
            //the shared blocks don't exist, so ignore till they do?
            if(sharedBlocks == NULL){
                HERB_OSXLangProcessStateWaiting_AddProcess(waitingState, process);
                process->mode = 1;
                break;
            }

            sharedIndex += process->registers[HERB_OSXLANG_REGISTER_ENCODE_R2];
            process->registers[HERB_OSXLANG_REGISTER_ENCODE_R1] = HERB_OSXLangMemory_GetDataByteFromBlocks(&sharedIndex, data->processState->machine->randomAccessMemory, sharedBlocks, sharedBlocksSize);

            free(sharedBlocks);

            process->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] += HERB_OSXLANG_INSTRUCTION_SIZE;
            HERB_OSXLangProcessStateReady_AddProcess(readyState, process);
            break;

        case HERB_OSXLANG_INSTRUCTION_SWI_COMMAND_GET_SEMIPHORE:
            lockId = process->registers[HERB_OSXLANG_REGISTER_ENCODE_R0];
            if(lockId >= HERB_OSXLANG_MACHINE_LOCK_NUM){
                HERB_OSXLangProcessStateTerminate_AddProcess(terminateState, process);
                break;
            }

            //get the lock
            process->registers[HERB_OSXLANG_REGISTER_ENCODE_R1] = data->processState->machine->locks[lockId];

            //increment the program counter
            process->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] += HERB_OSXLANG_INSTRUCTION_SIZE;
            HERB_OSXLangProcessStateReady_AddProcess(readyState, process);
            break;

        case HERB_OSXLANG_INSTRUCTION_SWI_COMMAND_SET_SEMIPHORE:
            lockId = process->registers[HERB_OSXLANG_REGISTER_ENCODE_R0];
            if(lockId >= HERB_OSXLANG_MACHINE_LOCK_NUM){
                HERB_OSXLangProcessStateTerminate_AddProcess(terminateState, process);
                break;
            }

            //set the lock
            data->processState->machine->locks[lockId] = process->registers[HERB_OSXLANG_REGISTER_ENCODE_R1];

            //increment the program counter
            process->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] += HERB_OSXLANG_INSTRUCTION_SIZE;
            HERB_OSXLangProcessStateReady_AddProcess(readyState, process);
            break;
    }

    //set state to ready
    data->processState->currentState = readyState;
}

void HERB_OSXLangProcessStateWaiting_AddProcess(HERB_State *state, HERB_OSXLangProcess *process){
    HERB_OSXLangProcessStateWaitingData *data = (HERB_OSXLangProcessStateWaitingData *)(state->data);
    ARC_Queue_Push(data->queue, (void *)process);
}

void HERB_OSXLangProcessStateWaiting_PrintProcesses(HERB_State *state){
    ARC_Queue *tempProcessQueue;
    ARC_Queue_Create(&tempProcessQueue);

    HERB_OSXLangProcessStateWaitingData *data = (HERB_OSXLangProcessStateWaitingData *)(state->data);
    while(ARC_Queue_Size(data->queue) != 0){
        HERB_OSXLangProcess *process = (HERB_OSXLangProcess *)ARC_Queue_Pop(data->queue);

        HERB_OSXLangProcessState_PrintProcessWithStateId(process, HERB_OSXLANG_PROCESS_STATE_WAITING);

        ARC_Queue_Push(tempProcessQueue, (void *)process);
    }

    while(ARC_Queue_Size(tempProcessQueue) != 0){
        HERB_OSXLangProcess *process = ARC_Queue_Pop(tempProcessQueue);
        ARC_Queue_Push(data->queue, process);
    }

    ARC_Queue_Destroy(tempProcessQueue);
}

void HERB_OSXLangProcessStateWaiting_PrintProcessesToBuffer(HERB_State *state, ARC_ConsoleBuffer *buffer){
    ARC_Queue *tempProcessQueue;
    ARC_Queue_Create(&tempProcessQueue);

    HERB_OSXLangProcessStateWaitingData *data = (HERB_OSXLangProcessStateWaitingData *)(state->data);
    while(ARC_Queue_Size(data->queue) != 0){
        HERB_OSXLangProcess *process = (HERB_OSXLangProcess *)ARC_Queue_Pop(data->queue);

        HERB_OSXLangProcessState_PrintProcessWithStateIdToBuffer(process, HERB_OSXLANG_PROCESS_STATE_WAITING, buffer);

        ARC_Queue_Push(tempProcessQueue, (void *)process);
    }

    while(ARC_Queue_Size(tempProcessQueue) != 0){
        HERB_OSXLangProcess *process = ARC_Queue_Pop(tempProcessQueue);
        ARC_Queue_Push(data->queue, process);
    }

    ARC_Queue_Destroy(tempProcessQueue);
}

uint32_t HERB_OSXLangProcessStateWaiting_ProcessesSize(HERB_State *state){
    HERB_OSXLangProcessStateWaitingData *data = (HERB_OSXLangProcessStateWaitingData *)(state->data);
    return ARC_Queue_Size(data->queue);
}

HERB_OSXLangProcess *HERB_OSXLangProcessWaiting_GetProcess(HERB_State *state, uint32_t id){
    HERB_OSXLangProcess *returnProcess = NULL;
    ARC_Queue *tempProcessQueue;
    ARC_Queue_Create(&tempProcessQueue);

    HERB_OSXLangProcessStateWaitingData *data = (HERB_OSXLangProcessStateWaitingData *)(state->data);
    while(ARC_Queue_Size(data->queue) != 0){
        HERB_OSXLangProcess *process = (HERB_OSXLangProcess *)ARC_Queue_Pop(data->queue);
        ARC_Queue_Push(tempProcessQueue, (void *)process);

        if(returnProcess != NULL){
            continue;
        }

        if(process != NULL && process->id == id){
            returnProcess = process;
        }
    }

    while(ARC_Queue_Size(tempProcessQueue) != 0){
        HERB_OSXLangProcess *process = ARC_Queue_Pop(tempProcessQueue);
        ARC_Queue_Push(data->queue, process);
    }

    ARC_Queue_Destroy(tempProcessQueue);

    return returnProcess;
}
