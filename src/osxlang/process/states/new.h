#ifndef HERB_PROCESS_STATE_NEW_H_
#define HERB_PROCESS_STATE_NEW_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "../states.h"
#include "../../../state.h"
#include <arc/console/buffer.h>

/**
 * @brief creates a HERB_State for the osx lang new process state
 *
 * @param state        the HERB_State to create 
 * @param processState the processes state to use within the main function (to switch states, add processes to other states, etc)
*/
void HERB_OSXLangProcessStateNew_Create(HERB_State **state, HERB_OSXLangProcessState *processState);

/**
 * @brief destroyes the HERB_State for the osx lang new process state
 *
 * @param state the HERB_State to destroy
*/
void HERB_OSXLangProcessStateNew_Destroy(HERB_State *state);

/**
 * @brief the main HERB_State function
 *
 * @note this function will create processes if memory is available and add them to the ready state
*/
void HERB_OSXLangProcessStateNew_MainFn(void *userdata);

/**
 * @brief adds a process to the osx lang new state
 *
 * @param state   the osx lang new state to add the process to
 * @param process the process to add to the state
*/
void HERB_OSXLangProcessStateNew_AddProcess(HERB_State *state, HERB_OSXLangProcess *process);

/**
 * @brief prints all of the processes in osx lang new state
 *
 * @param state the osx lang new state to print processes from
*/
void HERB_OSXLangProcessStateNew_PrintProcesses(HERB_State *state);

/**
 * @brief prints all of the processes in osx lang new state to a buffer
 *
 * @param state  the osx lang new state to print processes from
 * @param buffer the ARC_ConsoleBuffer to print to
*/
void HERB_OSXLangProcessStateNew_PrintProcessesToBuffer(HERB_State *state, ARC_ConsoleBuffer *buffer);

/**
 * @brief gets an process from the new state
 *
 * @param state  the osx lang new state to get process from
 * @param id     the ARC_ConsoleBuffer to print to
 *
 * @return a HERB_OSXLangProcess if one matches the id or NULL
*/
HERB_OSXLangProcess *HERB_OSXLangProcessNew_GetProcess(HERB_State *state, uint32_t id);

#ifdef __cplusplus
}
#endif

#endif // !HERB_PROCESS_STATE_NEW_H_