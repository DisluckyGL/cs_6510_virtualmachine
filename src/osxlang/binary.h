#ifndef HERB_OSXLANG_BINARY_H_
#define HERB_OSXLANG_BINARY_H_

#include <stdint.h>
#include <arc/console/buffer.h>
#include <arc/std/string.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief a HERB_OSXLangBinary type, store in data specified in the osxlang documentation, might change later
*/
typedef struct HERB_OSXLangBinary {
    uint8_t mode;

    uint32_t programCounter;
    uint32_t loaderAddress;

    uint8_t  *data;
    uint32_t  dataSize;
} HERB_OSXLangBinary;

/**
 * @brief creates HERB_OSXLangBinary type
 *
 * @param binary         HERB_OSXLangBinary to create
 * @param mode           this is a byte for user or kernel mode
 * @param programCounter start of instructions within a binary
 * @param loaderAddress  location to load address into, might be changed later
 * @param data           bytes of data stored within the binary
 * @param dataSize       size of bytes to store within the binary
*/
void HERB_OSXLangBinary_Create(HERB_OSXLangBinary **binary, uint8_t mode, uint32_t programCounter, uint32_t loaderAddress, uint8_t *data, uint32_t dataSize);

/**
 * @brief creates a HERB_OSXLangBinary from a file
 *
 * @param binary HERB_OSXLangBinary to create
 * @param path   path to file to read into a HERB_OSXLangBinary
 * @param mode   mode to load the file in as (can be user or kernel)
*/
void HERB_OSXLangBinary_CreateFromFile(HERB_OSXLangBinary **binary, ARC_String *path, uint8_t mode);

/**
 * @brief destroys HERB_OSXLangBinary type
 * 
 * @param binary binary that will be destroyed
*/
void HERB_OSXLangBinary_Destroy(HERB_OSXLangBinary *binary);

/**
 * @brief prints the contents of a binary split into rows with byte groupings
 *
 * @param binary       HERB_OSXLangBinary to print
 * @param rowSize      number of bytes per row to print
 * @param groupedBytes number of bytes to group together
*/
void HERB_OSXLangBinary_Print(HERB_OSXLangBinary *binary, uint8_t rowSize, uint8_t groupedBytes);

/**
 * @brief prints the contents of a binary split into rows with byte groupings to a buffer
 *
 * @param binary       HERB_OSXLangBinary to print
 * @param rowSize      number of bytes per row to print
 * @param groupedBytes number of bytes to group together
 * @param buffer       the ARC_ConsoleBuffer to print to
*/
void HERB_OSXLangBinary_PrintToBuffer(HERB_OSXLangBinary *binary, uint8_t rowSize, uint8_t groupedBytes, ARC_ConsoleBuffer *buffer);

#ifdef __cplusplus
}
#endif

#endif // !HERB_OSXLANG_BINARY_H_