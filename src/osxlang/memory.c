#include "memory.h"

#include "errorlog.h"
#include <arc/console/buffer.h>
#include <arc/std/errno.h>
#include <stdlib.h>
#include <stdio.h>

void HERB_OSXLangMemory_Create(HERB_OSXLangMemory **memory, uint64_t size){
    *memory = (HERB_OSXLangMemory *)malloc(sizeof(HERB_OSXLangMemory));

    (*memory)->data     = (uint8_t *)malloc(sizeof(uint8_t) * size);
    (*memory)->dataSize = size;
    (*memory)->blockSize = HERB_OSXLANG_MEMORY_BLOCK;
}

void HERB_OSXLangMemory_Destroy(HERB_OSXLangMemory *memory){
    free(memory->data);
    free(memory);
}

void HERB_OSXLangMemory_LoadBinaryFile(HERB_OSXLangMemory *memory, HERB_OSXLangBinary *binary, ARC_String *fileName){
    //get total size of what is being stored, start with one byte for the block type
    uint32_t storageSize = 1;
    //add size of blockSize
    storageSize += 4;

    //add filename and 0x00 to end
    storageSize += fileName->length + 1;

    //add mode byte to header
    storageSize++;

    storageSize += binary->dataSize;

    //add osx headers to the size
    storageSize += 12;

    //get number of blocks needed
    uint32_t blocksSize = storageSize / memory->blockSize;

    //add block num and each block to the header minus the initial block along with an extra byte for the block's header
    storageSize += (blocksSize - 1) * (4 + 1);

    //get new block size
    uint32_t newBlocksSize = storageSize / memory->blockSize;

    //add remainding blocks (if any)
    storageSize += (newBlocksSize - blocksSize) * (4 + 1);

    //set block size to the new block size
    blocksSize = newBlocksSize;

    //add extra block if needed
    if(storageSize % memory->blockSize != 0){
        blocksSize++;
        //this will never create a new block because memory->blockSize has to be bigger than 5
        storageSize += 4 + 1;
    }

    //create blocks and fill with max value for error checks
    uint32_t blocks[blocksSize];
    for(uint32_t i = 0; i < blocksSize; i++){
        blocks[i] = ~(uint32_t)0;
    }

    //find empty blocks
    uint32_t blockIndex = 0;
    for(uint32_t i = 0; i < memory->dataSize / memory->blockSize; i++){
        if(((uint8_t *)memory->data)[i * memory->blockSize] == HERB_OSXLANG_MEMORY_BLOCK_NONE){
            blocks[blockIndex] = i * memory->blockSize;
            blockIndex++;
            if(blockIndex >= blocksSize){
                break;
            }
        }
    }

    //throw an error if not all of the blocks were found
    if(blockIndex != blocksSize){
        arc_errno = ARC_ERRNO_DATA;
        ARC_DEBUG_ERR("HERB_OSXLangMemory_LoadBinaryFile(memory, binary), overflowed when loading binary");

        ARC_String *errorLogString;
        ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] overflowed when loading binary");

        HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
        ARC_String_Destroy(errorLogString);

        return;
    }

    //get the starting index
    uint32_t dataIndex = 0;

    //set block type header
    memory->data[blocks[dataIndex]] = HERB_OSXLANG_MEMORY_BLOCK_FILE;

    //set blocks into header
    HERB_OSXLangMemory_SetBlockHeaderAtIndex(&dataIndex, memory, blocks, blocksSize);

    //read in name 
    for(uint32_t i = 0; i < fileName->length; i++){
        HERB_OSXLangMemory_SetDataByteToBlocks(&dataIndex, memory, blocks, blocksSize, fileName->data[i]);
    }

    //end name string with null
    HERB_OSXLangMemory_SetDataByteToBlocks(&dataIndex, memory, blocks, blocksSize, '\0');

    //add mode byte to header
    HERB_OSXLangMemory_SetDataByteToBlocks(&dataIndex, memory, blocks, blocksSize, binary->mode);

    //add the data size to the header
    HERB_OSXLangMemory_SetDataBytesToUint32InBlocks(&dataIndex, memory, blocks, blocksSize, binary->dataSize);

    //add the program counter to the header
    HERB_OSXLangMemory_SetDataBytesToUint32InBlocks(&dataIndex, memory, blocks, blocksSize, binary->programCounter);

    for(uint32_t i = 0; i < binary->dataSize; i++){
        HERB_OSXLangMemory_SetDataByteToBlocks(&dataIndex, memory, blocks, blocksSize, binary->data[i]);

        if(arc_errno){
            return;
        }
    }
}

void HERB_OSXLangMemory_Print(HERB_OSXLangMemory *memory, uint8_t rowSize, uint8_t groupedBytes){
    HERB_OSXLangMemory_PrintRange(memory, 0, memory->dataSize, rowSize, groupedBytes);
}

void HERB_OSXLangMemory_PrintToBuffer(HERB_OSXLangMemory *memory, uint8_t rowSize, uint8_t groupedBytes, ARC_ConsoleBuffer *buffer){
    HERB_OSXLangMemory_PrintRangeToBuffer(memory, 0, memory->dataSize, rowSize, groupedBytes, buffer);
}

void HERB_OSXLangMemory_PrintRange(HERB_OSXLangMemory *memory, uint64_t startAddress, uint64_t endAddress, uint8_t rowSize, uint8_t groupedBytes){
    if(startAddress > endAddress){
        arc_errno = ARC_ERRNO_DATA;
        ARC_DEBUG_ERR("HERB_OSXLangMemory_PrintRange(memory, start, end), start address is bigger then end address");

        ARC_String *errorLogString;
        ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] start address is bigger then end address");

        HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
        ARC_String_Destroy(errorLogString);

        return;
    }

    if(startAddress > memory->dataSize || endAddress > memory->dataSize){
        arc_errno = ARC_ERRNO_DATA;
        ARC_DEBUG_ERR("HERB_OSXLangMemory_PrintRange(memory, start, end), start or end address are bigger then the size of the memory");

        ARC_String *errorLogString;
        ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] start or end address are bigger then the size of the memory");

        HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
        ARC_String_Destroy(errorLogString);

        return;
    }

    //default row size is the same as xxd 8 grouped by two bytes
    if(rowSize == 0){
        rowSize = 8;
    }

    if(groupedBytes == 0){
        groupedBytes = 2;
    }

    for(uint32_t i = 0; i + startAddress < endAddress; i++){
        if(i % (rowSize * groupedBytes) == 0){
            if(i != 0){
                printf("  ");
                for(uint32_t j = 0; j < (rowSize * groupedBytes); j++){
                    if(memory->data[(j + i + startAddress) - (rowSize * groupedBytes)] < ' '){
                        printf(".");
                        continue;
                    }
                    if(memory->data[(j + i + startAddress) - (rowSize * groupedBytes)] > '~'){
                        printf(".");
                        continue;
                    }
                    printf("%c", memory->data[(j + i + startAddress) - (rowSize * groupedBytes)]);
                }
            }
            printf("\n");
            printf("%08x: ", (uint32_t)(i + startAddress));
        }

        if(i % groupedBytes == 0){
            printf(" ");
        }

        printf("%02x", memory->data[i + startAddress]);
    }
    //TODO output spaces and char values of last line

    printf("\n\n");
}

void HERB_OSXLangMemory_PrintRangeToBuffer(HERB_OSXLangMemory *memory, uint64_t startAddress, uint64_t endAddress, uint8_t rowSize, uint8_t groupedBytes, ARC_ConsoleBuffer *buffer){
    if(startAddress > endAddress){
        arc_errno = ARC_ERRNO_DATA;
        //TODO: add this to buffer
        // ARC_DEBUG_ERR("HERB_OSXLangMemory_PrintRange(memory, start, end), start address is bigger then end address");

        ARC_String *errorLogString;
        ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] start address is bigger then end address");

        HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
        ARC_String_Destroy(errorLogString);

        return;
    }

    if(startAddress > memory->dataSize || endAddress > memory->dataSize){
        arc_errno = ARC_ERRNO_DATA;
        //TODO: add this to buffer
        // ARC_DEBUG_ERR("HERB_OSXLangMemory_PrintRange(memory, start, end), start or end address are bigger then the size of the memory");

        ARC_String *errorLogString;
        ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] start or end address are bigger then the size of the memory");

        HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
        ARC_String_Destroy(errorLogString);

        return;
    }

    //default row size is the same as xxd 8 grouped by two bytes
    if(rowSize == 0){
        rowSize = 8;
    }

    if(groupedBytes == 0){
        groupedBytes = 2;
    }

    for(uint32_t i = 0; i + startAddress < endAddress; i++){
        if(i % (rowSize * groupedBytes) == 0){
            if(i != 0){
                ARC_ConsoleBuffer_AddCStringWithStrlen(buffer, "  ");
                for(uint32_t j = 0; j < (rowSize * groupedBytes); j++){
                    if(memory->data[(j + i + startAddress) - (rowSize * groupedBytes)] < ' '){
                        ARC_ConsoleBuffer_AddChar(buffer, '.');
                        continue;
                    }
                    if(memory->data[(j + i + startAddress) - (rowSize * groupedBytes)] > '~'){
                        ARC_ConsoleBuffer_AddChar(buffer, '.');
                        continue;
                    }
                    ARC_ConsoleBuffer_AddChar(buffer, memory->data[(j + i + startAddress) - (rowSize * groupedBytes)]);
                }
            }
            ARC_ConsoleBuffer_AddChar(buffer, '\n');
            char lineNumCString[11];
            sprintf(lineNumCString, "%08x: ", (uint32_t)(i + startAddress));
            ARC_ConsoleBuffer_AddCStringWithStrlen(buffer, lineNumCString);
        }

        if(i % groupedBytes == 0){
            ARC_ConsoleBuffer_AddChar(buffer, ' ');
        }

        char byteCString[3];
        sprintf(byteCString,"%02x", memory->data[i + startAddress]);
        ARC_ConsoleBuffer_AddCStringWithStrlen(buffer, byteCString);
    }

    //TODO output spaces and char values of last line
    ARC_ConsoleBuffer_AddChar(buffer, '\n');
    ARC_ConsoleBuffer_AddChar(buffer, '\n');
}

void HERB_OSXLangMemory_PrintListFiles(HERB_OSXLangMemory *memory){
    ARC_Vector *fileNames;
    ARC_Vector_Create(&fileNames);

    HERB_OSXLangMemory_GetVectorFileNames(memory, fileNames);

    for(uint32_t i = 0; i < ARC_Vector_Size(fileNames); i++){
        ARC_String *fileName = (ARC_String *)ARC_Vector_Get(fileNames, i);
        printf("%s ", fileName->data);
        ARC_String_Destroy(fileName);
    }

    ARC_Vector_Destroy(fileNames);
    printf("\n\n");
}

void HERB_OSXLangMemory_PrintListFilesToBuffer(HERB_OSXLangMemory *memory, ARC_ConsoleBuffer *buffer){
    ARC_Vector *fileNames;
    ARC_Vector_Create(&fileNames);

    HERB_OSXLangMemory_GetVectorFileNames(memory, fileNames);

    for(uint32_t i = 0; i < ARC_Vector_Size(fileNames); i++){
        ARC_String *fileName = (ARC_String *)ARC_Vector_Get(fileNames, i);
        ARC_ConsoleBuffer_AddString(buffer, fileName);
        ARC_ConsoleBuffer_AddChar(buffer, ' ');
        ARC_String_Destroy(fileName);
    }

    ARC_Vector_Destroy(fileNames);
    ARC_ConsoleBuffer_AddChar(buffer, '\n');
    ARC_ConsoleBuffer_AddChar(buffer, '\n');
}

uint8_t HERB_OSXLangMemory_GetByte(uint32_t *index, HERB_OSXLangMemory *memory){
    if(*index >= memory->dataSize){
        arc_errno = ARC_ERRNO_OVERFLOW;
        ARC_DEBUG_ERR("HERB_OSXLangMemory_GetByte(index, memory), index is outside of memory");

        ARC_String *errorLogString;
        ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] index is outside of memory");

        HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
        ARC_String_Destroy(errorLogString);

        return 0;
    }

    uint8_t data = memory->data[*index];
    ++*index;
    return data;
}

uint32_t HERB_OSXLangMemory_GetBytesAsUint32(uint32_t *index, HERB_OSXLangMemory *memory){
    uint32_t data = 0;
    data |= (0xff000000 & (HERB_OSXLangMemory_GetByte(index, memory) << 24));
    data |= (0x00ff0000 & (HERB_OSXLangMemory_GetByte(index, memory) << 16));
    data |= (0x0000ff00 & (HERB_OSXLangMemory_GetByte(index, memory) <<  8));
    data |= (0x000000ff & (HERB_OSXLangMemory_GetByte(index, memory) <<  0));

    return data;
}

uint8_t HERB_OSXLangMemory_GetDataByteFromBlocks(uint32_t *index, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize){
    //check if block has enough space
    if(*index >= blocksSize * memory->blockSize){
        arc_errno = ARC_ERRNO_OVERFLOW;
        ARC_DEBUG_ERR("HERB_OSXLangMemory_GetDataByteFromBlocks(index, memory, blocks, blocksSize), block index is outside of blocks the file has");

        ARC_String *errorLogString;
        ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] block index is outside of blocks the file has");

        HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
        ARC_String_Destroy(errorLogString);

        return 0;
    }

    //if not a new block return the value
    if(*index % memory->blockSize != 0){
        //get data and move to next index
        uint8_t data = memory->data[blocks[*index / memory->blockSize] + (*index % memory->blockSize)];
        ++*index;
        return data;
    }

    //check if valid memory block
    if(memory->data[blocks[*index / memory->blockSize]] != HERB_OSXLANG_MEMORY_BLOCK_DATA){
        arc_errno = ARC_ERRNO_DATA;
        ARC_DEBUG_ERR("HERB_OSXLangMemory_GetDataByteFromBlocks(index, memory, blocks, blocksSize), file block pointed to a non data block");

        ARC_String *errorLogString;
        ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] file block pointed to a non data block");

        HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
        ARC_String_Destroy(errorLogString);

        return 0;
    }

    //move past the block byte
    ++*index;

    //get data and move to next index
    uint8_t data = memory->data[blocks[*index / memory->blockSize] + (*index % memory->blockSize)];
    ++*index;
    return data;
}

uint32_t HERB_OSXLangMemory_GetDataBytesAsUint32FromBlocks(uint32_t *index, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize){
    uint32_t bytes = 0;
    bytes |= (0xff000000 & (HERB_OSXLangMemory_GetDataByteFromBlocks(index, memory, blocks, blocksSize) << 24));
    if(arc_errno){
        return 0;
    }

    bytes |= (0x00ff0000 & (HERB_OSXLangMemory_GetDataByteFromBlocks(index, memory, blocks, blocksSize) << 16));
    if(arc_errno){
        return 0;
    }

    bytes |= (0x0000ff00 & (HERB_OSXLangMemory_GetDataByteFromBlocks(index, memory, blocks, blocksSize) <<  8));
    if(arc_errno){
        return 0;
    }

    bytes |= (0x000000ff & (HERB_OSXLangMemory_GetDataByteFromBlocks(index, memory, blocks, blocksSize) <<  0));
    if(arc_errno){
        return 0;
    }

    return bytes;
}

void HERB_OSXLangMemory_GetBlocksFromIndex(uint32_t *index, uint32_t **blocks, uint32_t *blocksSize, HERB_OSXLangMemory *memory, uint32_t blockStartIndex){
    //move past the block header type
    ++*index;

    //get the block size
    uint32_t tempBlockStartIndex = blockStartIndex + 1;
    *blocksSize = HERB_OSXLangMemory_GetBytesAsUint32(&tempBlockStartIndex, memory);
    *index += 4;

    //initialize the blocks
    *blocks = malloc(sizeof(uint32_t) * *blocksSize);

    //set the start block index
    (*blocks)[0] = blockStartIndex;

    //get block value
    for(uint32_t blockIndex = 1; blockIndex < *blocksSize; blockIndex++){
        (*blocks)[blockIndex] = HERB_OSXLangMemory_GetDataBytesAsUint32FromBlocks(index, memory, *blocks, *blocksSize);
    }
}

uint32_t HERB_OSXLangMemory_GetTotalBlockSize(HERB_OSXLangMemory *memory){
    return memory->dataSize / memory->blockSize;
}

uint32_t HERB_OSXLangMemory_GetAvailableBlockSize(HERB_OSXLangMemory *memory){
    uint32_t availableBlocks = 0;

    for(uint32_t blockIndex = 0; blockIndex < memory->dataSize / memory->blockSize; blockIndex++){
        if(((uint8_t *)(memory->data))[blockIndex * memory->blockSize] == HERB_OSXLANG_MEMORY_BLOCK_NONE){
            availableBlocks++;
        }
    }

    return availableBlocks;
}

void HERB_OSXLangMemory_GetFileName(ARC_String **fileName, HERB_OSXLangMemory *memory, uint32_t blockStartIndex){
    if(HERB_OSXLangMemory_GetByte(&blockStartIndex, memory) != HERB_OSXLANG_MEMORY_BLOCK_FILE){
        arc_errno = ARC_ERRNO_DATA;
        ARC_DEBUG_ERR("HERB_OSXLangMemory_GetFileName(index, fileName, memory), block was not a file, cannot get file name");

        ARC_String *errorLogString;
        ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] block was not a file, cannot get file name");

        HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
        ARC_String_Destroy(errorLogString);

        return;
    }

    //reset index back to he start of the block
    blockStartIndex--;

    //start the data index at 0
    uint32_t index = 0;

    //get the blocks
    uint32_t *blocks;
    uint32_t blocksSize;
    HERB_OSXLangMemory_GetBlocksFromIndex(&index, &blocks, &blocksSize, memory, blockStartIndex);

    //TODO: add error checks here?

    //get filename
    uint8_t fileNameChar = HERB_OSXLangMemory_GetDataByteFromBlocks(&index, memory, blocks, blocksSize);
    ARC_String_Create(fileName, (char *)&fileNameChar, 1);

    //read in the rest of the string
    fileNameChar = HERB_OSXLangMemory_GetDataByteFromBlocks(&index, memory, blocks, blocksSize);
    while(fileNameChar != '\0'){
        ARC_String *fileNameCharString;
        ARC_String_Create(&fileNameCharString, (char *)&fileNameChar, 1);

        ARC_String *tempFileName = *fileName;
        ARC_String_Merge(fileName, tempFileName, fileNameCharString);

        ARC_String_Destroy(tempFileName);
        ARC_String_Destroy(fileNameCharString);

        fileNameChar = HERB_OSXLangMemory_GetDataByteFromBlocks(&index, memory, blocks, blocksSize);
    }

    if(blocks){
        free(blocks);
    }
}

uint32_t HERB_OSXLangMemory_GetStartBlockIndexFromFileName(HERB_OSXLangMemory *memory, ARC_String *fileName){
    //find file blocks
    for(uint32_t blockIndex = 0; blockIndex < memory->dataSize / memory->blockSize; blockIndex++){
        if(((uint8_t *)(memory->data))[blockIndex * memory->blockSize] != HERB_OSXLANG_MEMORY_BLOCK_FILE){
            continue;
        }

        //get filename
        ARC_String *memoryFileName = NULL;
        uint32_t index = blockIndex * memory->blockSize;
        HERB_OSXLangMemory_GetFileName(&memoryFileName, memory, index);

        //if filename is null there is nothing to check so continue
        if(memoryFileName == NULL){
            continue;
        }

        //if filename is found return the index
        if(ARC_String_Equals(fileName, memoryFileName)){
            ARC_String_Destroy(memoryFileName);
            return blockIndex * memory->blockSize;
        }

        //cleanup
        ARC_String_Destroy(memoryFileName);
    }

    //file wasn't found, return max uint32_t value as an error
    arc_errno = ARC_ERRNO_NULL;
    ARC_DEBUG_LOG(arc_errno, "HERB_OSXLangMemory_GetStartBlockIndexFromFileName(memory, filename), \"%s\" not found in memory", fileName->data);

    ARC_String *errorLogString;
    ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] file not found in memory");

    HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
    ARC_String_Destroy(errorLogString);

    return ~(uint32_t)0;
}

void HERB_OSXLangMemory_GetVectorFileNames(HERB_OSXLangMemory *memory, ARC_Vector *fileNames){
    //find file blocks
    for(uint32_t blockIndex = 0; blockIndex < memory->dataSize / memory->blockSize; blockIndex++){
        if(((uint8_t *)(memory->data))[blockIndex * memory->blockSize] != HERB_OSXLANG_MEMORY_BLOCK_FILE){
            continue;
        }

        //get filename
        ARC_String *fileName = NULL;
        uint32_t index = blockIndex * memory->blockSize;
        HERB_OSXLangMemory_GetFileName(&fileName, memory, index);

        //if filename isn't null, add it
        if(fileName != NULL){
            ARC_Vector_Add(fileNames, (void *)fileName);
        }
    }
}

void HERB_OSXLangMemory_SetByte(uint32_t *index, HERB_OSXLangMemory *memory, uint8_t byte){
    if(*index >= memory->dataSize){
        arc_errno = ARC_ERRNO_OVERFLOW;
        ARC_DEBUG_ERR("HERB_OSXLangMemory_SetByte(index, memory, memory), index is outside of memory");

        ARC_String *errorLogString;
        ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] index is outside of memory");

        HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
        ARC_String_Destroy(errorLogString);

        return;
    }

    memory->data[*index] = byte;
    ++*index;
}

void HERB_OSXLangMemory_SetBytesToUint32(uint32_t *index, HERB_OSXLangMemory *memory, uint32_t uint32){
    HERB_OSXLangMemory_SetByte(index, memory, 0xff & (uint32 >> 24));
    HERB_OSXLangMemory_SetByte(index, memory, 0xff & (uint32 >> 16));
    HERB_OSXLangMemory_SetByte(index, memory, 0xff & (uint32 >>  8));
    HERB_OSXLangMemory_SetByte(index, memory, 0xff & (uint32 >>  0));
}

void HERB_OSXLangMemory_SetDataByteToBlocks(uint32_t *index, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize, uint8_t byte){
    //check if block has enough space
    if(*index >= blocksSize * memory->blockSize){
        arc_errno = ARC_ERRNO_OVERFLOW;
        ARC_DEBUG_ERR("HERB_OSXLangMemory_SetDataByteToBlocks(index, memory, blocks, blocksSize, byte), block index is outside of blocks the file has");

        ARC_String *errorLogString;
        ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] block index is outside of blocks the file has");

        HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
        ARC_String_Destroy(errorLogString);

        return;
    }

    //if not a new block return the value
    if(*index % memory->blockSize != 0){
        //get data and move to next index
        memory->data[blocks[*index / memory->blockSize] + (*index % memory->blockSize)] = byte;
        ++*index;
        return;
    }

    //check if valid memory block
    if(memory->data[blocks[*index / memory->blockSize]] != HERB_OSXLANG_MEMORY_BLOCK_NONE && memory->data[blocks[*index / memory->blockSize]] != HERB_OSXLANG_MEMORY_BLOCK_DATA){
        arc_errno = ARC_ERRNO_DATA;
        ARC_DEBUG_ERR("HERB_OSXLangMemory_SetDataByteToBlocks(index, memory, blocks, blocksSize, byte), file block pointed to a non null and non data block");

        ARC_String *errorLogString;
        ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] file block pointed to a non null and non data block");

        HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
        ARC_String_Destroy(errorLogString);

        return;
    }

    //set block byte to data in case it was set to null
    memory->data[blocks[*index / memory->blockSize] + (*index % memory->blockSize)] = HERB_OSXLANG_MEMORY_BLOCK_DATA;

    //move past the block byte
    ++*index;

    //set data and move to next index
    memory->data[blocks[*index / memory->blockSize] + (*index % memory->blockSize)] = byte;
    ++*index;
}

void HERB_OSXLangMemory_SetDataBytesToUint32InBlocks(uint32_t *index, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize, uint32_t uint32){
    HERB_OSXLangMemory_SetDataByteToBlocks(index, memory, blocks, blocksSize, 0xff & (uint32 >> 24));
    HERB_OSXLangMemory_SetDataByteToBlocks(index, memory, blocks, blocksSize, 0xff & (uint32 >> 16));
    HERB_OSXLangMemory_SetDataByteToBlocks(index, memory, blocks, blocksSize, 0xff & (uint32 >>  8));
    HERB_OSXLangMemory_SetDataByteToBlocks(index, memory, blocks, blocksSize, 0xff & (uint32 >>  0));
}

void HERB_OSXLangMemory_SetBlockHeaderAtIndex(uint32_t *index, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize){
    uint32_t startBlockIndex = blocks[*index];
    uint8_t memoryBlockTypeByte = HERB_OSXLangMemory_GetByte(&startBlockIndex, memory);
    if(memoryBlockTypeByte != HERB_OSXLANG_MEMORY_BLOCK_FILE && memoryBlockTypeByte != HERB_OSXLANG_MEMORY_BLOCK_FOLDER && memoryBlockTypeByte != HERB_OSXLANG_MEMORY_BLOCK_SHARED){
        arc_errno = ARC_ERRNO_DATA;
        ARC_DEBUG_ERR("HERB_OSXLangMemory_SetBlockHeaderAtIndex(index, memory, blocks, blocksSize), cannot set block header as block type is not file, folder, or shared");

        ARC_String *errorLogString;
        ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] cannot set block header as block type is not file, folder, or shared");

        HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
        ARC_String_Destroy(errorLogString);

        return;
    }

    //move past the block type byte
    ++*index;

    //set the blocks size into the header
    HERB_OSXLangMemory_SetDataBytesToUint32InBlocks(index, memory, blocks, blocksSize, blocksSize);

    //set each block except the first block into the header
    for(uint32_t i = 1; i < blocksSize; i++){
        HERB_OSXLangMemory_SetDataBytesToUint32InBlocks(index, memory, blocks, blocksSize, blocks[i]);
    }
}