#ifndef HERB_OSXLANG_MACHINE_H_
#define HERB_OSXLANG_MACHINE_H_

#include "binary/executor.h"
#include "memory.h"
#include "clock.h"
#include <stdint.h>
#include <arc/console/buffer.h>
#include <arc/std/bool.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief the number of locks to have
*/
#define HERB_OSXLANG_MACHINE_LOCK_NUM 6

/**
 * @brief osx lang machine type that has a ram, softdrive, and binary executor
*/
typedef struct HERB_OSXLangMachine {
    HERB_OSXLangMemory *randomAccessMemory;
    HERB_OSXLangMemory *softDrive;

    HERB_OSXLangBinaryExecutor *binaryExecutor;

    HERB_OSXLangClock *clock;

    ARC_Bool locks[HERB_OSXLANG_MACHINE_LOCK_NUM];
} HERB_OSXLangMachine;

/**
 * @brief creates HERB_OSXLangMachine type
 *
 * @param machine                HERB_OSXLangMachine to create
 * @param randomAccessMemorySize number of bytes to give to the machine's random access memory
 * @param softDriveSize          number of bytes to give to the machine's soft drive
*/
void HERB_OSXLangMachine_Create(HERB_OSXLangMachine **machine, uint64_t randomAccessMemorySize, uint64_t softDriveSize);

/**
 * @brief destroys HERB_OSXLangMachine type
 * 
 * @param machine machine that will be destroyed
*/
void HERB_OSXLangMachine_Destroy(HERB_OSXLangMachine *machine);

/**
 * @brief prints contents of machine
 * 
 * @param machine      HERB_OSXLangMachine to print from
 * @param rowSize      size of row to print
 * @param groupedBytes grouping of bytes printed
*/
void HERB_OSXLangMachine_Print(HERB_OSXLangMachine *machine, uint8_t rowSize, uint8_t groupedBytes);

/**
 * @brief prints contents of machine to a buffer
 * 
 * @param machine      HERB_OSXLangMachine to print from
 * @param rowSize      size of row to print
 * @param groupedBytes grouping of bytes printed
 * @param buffer       the ARC_ConsoleBuffer to print to
*/
void HERB_OSXLangMachine_PrintToBuffer(HERB_OSXLangMachine *machine, uint8_t rowSize, uint8_t groupedBytes, ARC_ConsoleBuffer *buffer);

#ifdef __cplusplus
}
#endif

#endif // !HERB_OSXLANG_MACHINE_H_