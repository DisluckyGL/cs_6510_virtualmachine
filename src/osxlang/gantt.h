#ifndef HERB_OSXLANG_GANTT_CHART_H_
#define HERB_OSXLANG_GANTT_CHART_H_

#include <arc/console/buffer.h>
#include <arc/std/string.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief a type that holds gant chart data for a the osx lang
*/
typedef struct HERB_OSXLangGanttChart HERB_OSXLangGanttChart;

/**
 * @brief create a global ganttchart to make using it easier
 *
 * @note when created this is set to null, you need to create it to use it
*/
extern HERB_OSXLangGanttChart *herb_ganttchart;

/**
 * @brief create a global bool to make using a gantt chart easier to remove
 *
 * @note when created this is set to ARC_False
*/
extern ARC_Bool herb_useGanttchart;

/**
 * @brief creates HERB_OSXLangGanttChart type
 *
 * @param ganttchart HERB_OSXLangGanttChart to create
 * @param quantums   the number of quantums to display in the gantt chart
*/
void HERB_OSXLangGanttChart_Create(HERB_OSXLangGanttChart **ganttchart, uint32_t quantums);

/**
 * @brief destroys HERB_OSXLangGanttChart type
 * 
 * @param ganttchart ganttchart that will be destroyed
*/
void HERB_OSXLangGanttChart_Destroy(HERB_OSXLangGanttChart *ganttchart);

/**
 * @brief clears HERB_OSXLangGanttChart type
 * 
 * @param ganttchart ganttchart that will be cleared 
*/
void HERB_OSXLangGanttChart_Clear(HERB_OSXLangGanttChart *ganttchart);

/**
 * @brief adds an id to the gantt chart
 *
 * @param ganttchart HERB_OSXLangGanttChart to add id to
 * @param id         the id to add
 * @param quantum    the quantum being used by the id
*/
void HERB_OSXLangGanttChart_AddId(HERB_OSXLangGanttChart *ganttchart, uint32_t id, uint32_t quantum);

/**
 * @brief prints a HERB_OSXLangErrorLog type
 *
 * @param ganttchart HERB_OSXLangGanttChart to print
*/
void HERB_OSXLangGanttChart_Print(HERB_OSXLangGanttChart *ganttchart);

/**
 * @brief prints a HERB_OSXLangErrorLog type to a buffer
 *
 * @param ganttchart HERB_OSXLangGanttChart to print to buffer
 * @param buffer     ARC_ConsoleBuffer to print to
*/
void HERB_OSXLangGanttChart_PrintToBuffer(HERB_OSXLangGanttChart *ganttchart, ARC_ConsoleBuffer *buffer);

/**
 * @brief prints a HERB_OSXLangErrorLog type to a file
 *
 * @param ganttchart HERB_OSXLangGanttChart to print to file
 * @param path       ARC_String to open file and write to
*/
void HERB_OSXLangGanttChart_PrintToFile(HERB_OSXLangGanttChart *ganttchart, ARC_String *path);

/**
 * @brief sets the number of quantums in a HERB_OSXLangGanttChart type
 *
 * @note using this function will clear the gantt chart
 *
 * @param ganttchart HERB_OSXLangGanttChart to set the number of quantums
 * @param quantums   the number of quantums to display in the gantt chart
*/
void HERB_OSXLangGanttChart_SetQuantums(HERB_OSXLangGanttChart *ganttchart, uint32_t quantums);

#ifdef __cplusplus
}
#endif

#endif // !HERB_OSXLANG_GANTT_CHART_LOG_H_