#ifndef HERB_OSXLANG_SHELL_STORAGE_H_
#define HERB_OSXLANG_SHELL_STORAGE_H_

#include "../machine.h"
#include "../process/states.h"
#include <arc/console/shell.h>
#include <pthread.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief the shell storage type, used when running a shell
*/
typedef struct HERB_OSXLangShellStorage {
    uint8_t running;

    HERB_OSXLangMachine *machine;

    HERB_OSXLangProcessState *processState;

    uint32_t *shellsRunning;

    ARC_ConsoleShell *shell;

    pthread_mutex_t *machineMutex;
    pthread_mutex_t *shellsRunningMutex;
} HERB_OSXLangShellStorage;

/**
 * @brief creates HERB_OSXLangShellStorage type
 *
 * @param storage HERB_OSXLangShellStorage to create
*/
void HERB_OSXLangShellStorage_Create(HERB_OSXLangShellStorage **storage);

/**
 * @brief destroys HERB_OSXLangShellStorage type
 * 
 * @param storage storage that will be destroyed
*/
void HERB_OSXLangShellStorage_Destroy(HERB_OSXLangShellStorage *storage);

#ifdef __cplusplus
}
#endif

#endif // !HERB_OSXLANG_SHELL_STORAGE_H_