#ifndef HERB_OSXLANG_RUNNER_SHELL_H_
#define HERB_OSXLANG_RUNNER_SHELL_H_

#include "../../runner.h"
#include "../machine.h"
#include "../process/states.h"
#include <stdint.h>
#include <pthread.h>
#include <arc/console/view.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief parameters to run an osx shell, used for multithreading
*/
typedef struct HERB_OSXLangRunnerShellParams {
    HERB_OSXLangMachine *machine;

    HERB_OSXLangProcessState *processState;

    ARC_ConsoleView *consoleView;

    char *prompt;

    uint32_t *shellsRunning;

    pthread_mutex_t *machineMutex;
    pthread_mutex_t *shellsRunningMutex;
} HERB_OSXLangRunnerShellParams;

/**
 * @brief creates storege for HERB_Runner used by HERB_OSXLangRunnerShell
 *
 * @param storage the storate the HERB_OSXLangRunnerShell will use
*/
void HERB_OSXLangRunnerShell_InitStorage(void **storage);

/**
 * @brief destroys storege for HERB_Runner used by HERB_OSXLangRunnerShell
 *
 * @param storage the storate the HERB_OSXLangRunnerShell will destroy
*/
void HERB_OSXLangRunnerShell_DeinitStorage(void *storage);

/**
 * @brief compares instructions used in storage, passed as a callback to HERB_Runner
 *
 * @note this is the same as ARC_Hashtable_KeyCompare, just rewritten here to make it easier on the user
 *
 * @note TODO: create ARC_Bool and start using it instead of returning int8_t
 *             also reformat hashtable to only require instructions and not their sizes for comparison
 *
 * @param instruction1     data for instruction1
 * @param instruction1size size of instruction1
 * @param instruction2     data for instruction2
 * @param instruction2size size of instruction2
 *
 * @return 0 when keys match
*/
int8_t HERB_OSXLangRunnerShell_InstructionComp(void *instruction1, size_t *instruction1size, void *instruction2, size_t *instruction2size);

/**
 * @brief used as a callback for HERB_Runner_Run
 *
 * @param runner   current runnter that is running the program, can be used to get the storage
 * @param data     this void * is a char * substring
 * @param dataSize this is the size of the substring
*/
void HERB_OSXLangRunnerShell_CommandRun(HERB_Runner *runner, void *data, size_t dataSize);

/**
 * @brief starts the HERB_OSXLangRunnerShell
 *
 * @param params HERB_OSXLangRunnerShellParams cast as a void *
 *
 * @return nothing, this is needed though to be a pthread
*/
void *HERB_OSXLangRunnerShell_Start(void *params);

#ifdef __cplusplus
}
#endif

#endif // !HERB_OSXLANG_RUNNER_SHELL_H_