#include "storage.h"

#include <stdlib.h>

void HERB_OSXLangShellStorage_Create(HERB_OSXLangShellStorage **storage){
    *storage = (HERB_OSXLangShellStorage *)malloc(sizeof(HERB_OSXLangShellStorage));

    (*storage)->running = 1;

    //will copy the values in src/osxlang/shell/runner.c before running
    (*storage)->machine            = NULL;
    (*storage)->processState       = NULL;
    (*storage)->shellsRunning      = NULL;
    (*storage)->shell              = NULL;
    (*storage)->machineMutex       = NULL;
    (*storage)->shellsRunningMutex = NULL;
}

void HERB_OSXLangShellStorage_Destroy(HERB_OSXLangShellStorage *storage){
    free(storage);
}