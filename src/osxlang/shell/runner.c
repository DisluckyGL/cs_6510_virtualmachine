#include "runner.h"

#include "commands.h"
#include "storage.h"
#include <arc/console/shell.h>
#include <arc/std/bool.h>
#include <arc/std/errno.h>
#include <stdlib.h>
#include <string.h>

ARC_Bool Temp_ConsoleView_OverrideCharInputFn(ARC_ConsoleKey *key, char *inputCStr, uint32_t *inputSize, uint32_t maxInputSize, void *userdata){
    ARC_ConsoleShell *shell = (ARC_ConsoleShell *)userdata;
    if(ARC_ConsoleKey_EqualsPointer(key, ARC_KEY_UP)){
        shell->historyIndex++;
        if(shell->historyIndex > ARC_Vector_Size(shell->history)){
            shell->historyIndex = ARC_Vector_Size(shell->history);
            return ARC_True;
        }

        ARC_String *temp = ARC_ConsoleShell_GetHistoryAt(shell, shell->historyIndex - 1);
        *inputSize = temp->length;
        for(uint64_t i = 0; i < temp->length; i++){
            inputCStr[i] = temp->data[i];

            if(i >= maxInputSize - 1){
                *inputSize = maxInputSize;
                break;
            }
        }

        return ARC_True;
    }

    if(ARC_ConsoleKey_EqualsPointer(key, ARC_KEY_DOWN)){
        if(shell->historyIndex != 0){
            shell->historyIndex--;
        }

        if(shell->historyIndex == 0){
            *inputSize = 0;
            shell->historyIndex = 0;
            return ARC_True;
        }


        ARC_String *temp = ARC_ConsoleShell_GetHistoryAt(shell, shell->historyIndex - 1);
        *inputSize = temp->length;
        for(uint64_t i = 0; i < temp->length; i++){
            inputCStr[i] = temp->data[i];

            if(i >= maxInputSize - 1){
                *inputSize = maxInputSize;
                break;
            }
        }

        return ARC_True;
    }

    return ARC_False;
}

void HERB_OSXLangRunnerShell_InitStorage(void **storage){
    HERB_OSXLangShellStorage_Create((HERB_OSXLangShellStorage **)storage);
}

void HERB_OSXLangRunnerShell_DeinitStorage(void *storage){
    HERB_OSXLangShellStorage_Destroy((HERB_OSXLangShellStorage *)storage);
}

int8_t HERB_OSXLangRunnerShell_InstructionComp(void *instruction1, size_t *instruction1size, void *instruction2, size_t *instruction2size){
    //if sizes are the same return false (-1)
    if(*instruction1size - *instruction2size){
        return -1;
    }

    //return a string comparison of the data
    return strncmp((const char *)instruction1, (const char *)instruction2, *instruction1size);
}

void HERB_OSXLangRunnerShell_CommandRun(HERB_Runner *runner, void *data, size_t dataSize){
    if(arc_errno){
        //there is an unresolved error break
        return;
    }

    //copy data into a string removing whitespace so we can string maniuplate
    ARC_String *dataString;
    ARC_String_Create(&dataString, data, dataSize);
    ARC_String *tempDataString = dataString;
    ARC_String_StripEndsWhitespace(&dataString, tempDataString);
    ARC_String_Destroy(tempDataString);

    //check for space
    uint64_t commandSize = ARC_String_FindCString(dataString, " ", 1);
    if(arc_errno){
        ARC_String_Destroy(dataString);
        return;
    }

    //if command size wasn't found, set size back to 1, so we can remove the last index for if it does exist
    if(commandSize == ~(uint64_t)0){
        commandSize = 1;
    }

    //remove the space or set commandSize to 0
    commandSize--;

    //if space wasn't found, check for newline
    if(commandSize == 0){
        commandSize = ARC_String_FindCString(dataString, "\n", 1);
        if(arc_errno){
            ARC_String_Destroy(dataString);
            return;
        }

        //if command wasn't found, set size back to 1, so we can remove the last index for if it does exist
        if(commandSize == ~(uint64_t)0){
            commandSize = 1;
        }

        //remove the space or set commandSize to 0
        commandSize--;
    }

    //if command still wasn't found, set command size to length of dataString
    if(commandSize == 0){
        commandSize = dataString->length;
    }

    //get command
    ARC_String *command;
    ARC_String_CopySubstring(&command, dataString, 0, commandSize);
    HERB_Runner_InstructionFn instructionFn = HERB_Runner_GetInstructionFn(runner, command->data, command->length);
    ARC_String_Destroy(command);

    //if instructionFn is not found, run ERROR command
    if(instructionFn == NULL){
        instructionFn = HERB_Runner_GetInstructionFn(runner, "error", 5);
    }

    HERB_OSXLangShellStorage *storage = HERB_Runner_GetStorage(runner);
    instructionFn(storage, dataString, 0);

    //cleanup
    ARC_String_Destroy(dataString);
}

void HERB_OSXLangRunnerShell_UpdateConsoleShellAndGetInput(ARC_String **input, ARC_ConsoleShell *shell, char *promptCString){
    ARC_String *prompt;
    ARC_String_CreateWithStrlen(&prompt, promptCString);

    ARC_Point currentPos = (ARC_Point){ 0, 0 };
    ARC_Rect viewBounds = ARC_ConsoleView_GetBounds(shell->view);

    currentPos.y = ARC_ConsoleBuffer_GetLineNumbers(shell->buffer) - 1;
    if(currentPos.y >= viewBounds.h - 1){
        currentPos.y = viewBounds.h - 2;
    }

    ARC_ConsoleView_RenderStringAt(shell->view, prompt, currentPos);

    ARC_ConsoleView_OverrideCharInputFn overrideFn = Temp_ConsoleView_OverrideCharInputFn;
    *input = ARC_ConsoleView_GetStringInput(shell->view, (ARC_Point){ currentPos.x + prompt->length, currentPos.y }, &overrideFn, (void *)shell);
    currentPos.y++;

    shell->historyIndex = 0;

    if(*input == NULL){
        return;
    }

    ARC_String *promptCopy;
    ARC_String_Copy(&promptCopy, prompt);
    ARC_ConsoleBuffer_AddString(shell->buffer, promptCopy);
    ARC_ConsoleBuffer_AddString(shell->buffer, *input);
    ARC_ConsoleBuffer_AddChar(shell->buffer, '\n');

    ARC_String *lastHistory = ARC_ConsoleShell_GetHistoryAt(shell, 0);
    if(lastHistory == NULL || !ARC_String_Equals(lastHistory, *input)){
        ARC_String *inputCopy;
        ARC_String_Copy(&inputCopy, *input);
        ARC_ConsoleShell_AddHistory(shell, inputCopy);
    }

    //render buffer
    uint32_t startIndex = 0;
    uint32_t bufferLines = ARC_ConsoleBuffer_GetLineNumbers(shell->buffer);
    if(bufferLines >= viewBounds.h - 1){
        startIndex = bufferLines - (viewBounds.h - 1);
        ARC_ConsoleView_Clear(shell->view);
    }
    ARC_ConsoleBuffer_RenderSection(shell->buffer, shell->view, startIndex, viewBounds.h - 1);

    ARC_String_Destroy(prompt);
}

void HERB_OSXLangRunnerShell_RenderConsoleShell(ARC_ConsoleShell *shell){
    ARC_Rect viewBounds = ARC_ConsoleView_GetBounds(shell->view);

    //render buffer
    uint32_t startIndex = 0;
    uint32_t bufferLines = ARC_ConsoleBuffer_GetLineNumbers(shell->buffer);
    if(bufferLines >= viewBounds.h - 1){
        startIndex = bufferLines - (viewBounds.h - 1);
        ARC_ConsoleView_Clear(shell->view);
    }
    ARC_ConsoleBuffer_RenderSection(shell->buffer, shell->view, startIndex, viewBounds.h - 1);
}

void *HERB_OSXLangRunnerShell_Start(void *params){
    //init runner
    HERB_Runner *runner;
    HERB_Runner_Create(&runner, HERB_OSXLangRunnerShell_InitStorage, HERB_OSXLangRunnerShell_DeinitStorage, HERB_OSXLangRunnerShell_InstructionComp);

    //recast params to HERB_OSXLangRunnerShellParams
    HERB_OSXLangRunnerShellParams *shellParams = (HERB_OSXLangRunnerShellParams *)params;

    //set storage references
    HERB_OSXLangShellStorage *storage = HERB_Runner_GetStorage(runner);
    storage->machine            = shellParams->machine;
    storage->processState       = shellParams->processState;
    storage->shellsRunning      = shellParams->shellsRunning;
    storage->machineMutex       = shellParams->machineMutex;
    storage->shellsRunningMutex = shellParams->shellsRunningMutex;

    //create shell attached to the machine consoleView
    ARC_ConsoleShell_Create(&(storage->shell), shellParams->consoleView, NULL);

    //set instructions for runner
    HERB_OSXLang_RegisterShellCommandsToHERBRunner(runner);

    //define seperator
    char *separator = " ";
    uint64_t separatorSize = 1;

    //start shell main loop
    while(storage->running){
        //get the input from the connected shell
        ARC_String *lineString;
        HERB_OSXLangRunnerShell_UpdateConsoleShellAndGetInput(&lineString, storage->shell, shellParams->prompt);

        //run the command
        HERB_Runner_RunSingle(runner, lineString->data, lineString->length, separator, &separatorSize, HERB_OSXLangRunnerShell_CommandRun);

        //render the shell
        HERB_OSXLangRunnerShell_RenderConsoleShell(storage->shell);

        //cleanup
        ARC_String_Destroy(lineString);
    }

    //cleanup
    ARC_ConsoleShell_Destroy(storage->shell);
    HERB_Runner_Destroy(runner);

    //decrement the number of shells running on end
    pthread_mutex_lock(storage->shellsRunningMutex);
    --*shellParams->shellsRunning;
    pthread_mutex_unlock(storage->shellsRunningMutex);

    return NULL;
}