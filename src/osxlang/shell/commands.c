#include "commands.h"

#include "../binary.h"
#include "../errorlog.h"
#include "../gantt.h"
#include "../../bufferpoll.h"
#include "osxlang/machine.h"
#include "storage.h"
#include <arc/console/buffer.h>
#include <arc/console/key.h>
#include <arc/std/errno.h>
#include <arc/std/string.h>
#include <arc/std/vector.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>

void HERB_OSXLang_RegisterShellCommandsToHERBRunner(HERB_Runner *runner){
    HERB_Runner_RegisterInstruction(runner, HERB_OSXLANG_SHELL_STRING_ERROR          , strlen(HERB_OSXLANG_SHELL_STRING_ERROR          ), HERB_OSXLang_ShellERROR          );
    HERB_Runner_RegisterInstruction(runner, HERB_OSXLANG_SHELL_STRING_EXIT           , strlen(HERB_OSXLANG_SHELL_STRING_EXIT           ), HERB_OSXLang_ShellEXIT           );
    HERB_Runner_RegisterInstruction(runner, HERB_OSXLANG_SHELL_STRING_HELP           , strlen(HERB_OSXLANG_SHELL_STRING_HELP           ), HERB_OSXLang_ShellHELP           );
    HERB_Runner_RegisterInstruction(runner, HERB_OSXLANG_SHELL_STRING_LOAD           , strlen(HERB_OSXLANG_SHELL_STRING_LOAD           ), HERB_OSXLang_ShellLOAD           );
    HERB_Runner_RegisterInstruction(runner, HERB_OSXLANG_SHELL_STRING_LS             , strlen(HERB_OSXLANG_SHELL_STRING_LS             ), HERB_OSXLang_ShellLS             );
    HERB_Runner_RegisterInstruction(runner, HERB_OSXLANG_SHELL_STRING_RUNLOCAL       , strlen(HERB_OSXLANG_SHELL_STRING_RUNLOCAL       ), HERB_OSXLang_ShellRUNLOCAL       );
    HERB_Runner_RegisterInstruction(runner, HERB_OSXLANG_SHELL_STRING_COREDUMP       , strlen(HERB_OSXLANG_SHELL_STRING_COREDUMP       ), HERB_OSXLang_ShellCOREDUMP       );
    HERB_Runner_RegisterInstruction(runner, HERB_OSXLANG_SHELL_STRING_ERRORDUMP      , strlen(HERB_OSXLANG_SHELL_STRING_ERRORDUMP      ), HERB_OSXLang_ShellERRORDUMP      );
    // HERB_Runner_RegisterInstruction(runner, HERB_OSXLANG_SHELL_STRING_SHELL          , strlen(HERB_OSXLANG_SHELL_STRING_SHELL          ), HERB_OSXLang_ShellSHELL          );
    HERB_Runner_RegisterInstruction(runner, HERB_OSXLANG_SHELL_STRING_PROCESS_VIEWER , strlen(HERB_OSXLANG_SHELL_STRING_PROCESS_VIEWER ), HERB_OSXLang_ShellPROCESS_VIEWER );
    HERB_Runner_RegisterInstruction(runner, HERB_OSXLANG_SHELL_STRING_CONNECT_PROCESS, strlen(HERB_OSXLANG_SHELL_STRING_CONNECT_PROCESS), HERB_OSXLang_ShellCONNECT_PROCESS);
    HERB_Runner_RegisterInstruction(runner, HERB_OSXLANG_SHELL_STRING_GANTT          , strlen(HERB_OSXLANG_SHELL_STRING_GANTT          ), HERB_OSXLang_ShellGANTT          );
    HERB_Runner_RegisterInstruction(runner, HERB_OSXLANG_SHELL_STRING_SET_QUANTUM    , strlen(HERB_OSXLANG_SHELL_STRING_SET_QUANTUM    ), HERB_OSXLang_ShellSET_QUANTUM    );
    HERB_Runner_RegisterInstruction(runner, HERB_OSXLANG_SHELL_STRING_PAGE           , strlen(HERB_OSXLANG_SHELL_STRING_PAGE           ), HERB_OSXLang_ShellPAGE           );
}

void HERB_OSXLang_ShellERROR(void *storage, void *instructionString, uint64_t instructionSize){
    ARC_String *errorString;
    ARC_String_CreateWithStrlen(&errorString, "SHELL: command not found: ");

    ARC_String *tempErrorString = errorString;
    ARC_String_Merge(&errorString, tempErrorString, (ARC_String *)instructionString);
    ARC_String_Destroy(tempErrorString);

    //add the output to the shell buffer
    ARC_ConsoleBuffer_AddString(((HERB_OSXLangShellStorage *)storage)->shell->buffer, errorString);
    ARC_ConsoleBuffer_AddChar(((HERB_OSXLangShellStorage *)storage)->shell->buffer, '\n');
    ARC_ConsoleBuffer_AddChar(((HERB_OSXLangShellStorage *)storage)->shell->buffer, '\n');

    //add to error log
    ARC_String *errorLogString;
    ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] SHELL: command not found: \"");

    ARC_String *tempErrorLogString = errorLogString;
    ARC_String_Merge(&errorLogString, tempErrorLogString, (ARC_String *)instructionString);
    ARC_String_Destroy(tempErrorLogString);

    ARC_String *tempErrorLogStringNext;
    tempErrorLogString = errorLogString;
    ARC_String_CreateWithStrlen(&tempErrorLogStringNext, "\"");
    ARC_String_Merge(&errorLogString, tempErrorLogString, tempErrorLogStringNext);
    ARC_String_Destroy(tempErrorLogString);
    ARC_String_Destroy(tempErrorLogStringNext);

    HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
    ARC_String_Destroy(errorLogString);
}

void HERB_OSXLang_ShellEXIT(void *storage, void *instructionString, uint64_t instructionSize){
    ((HERB_OSXLangShellStorage *)storage)->running = 0;
}

void HERB_OSXLang_ShellHELP(void *storage, void *instructionString, uint64_t instructionSize){
    ARC_ConsoleBuffer_AddCStringWithStrlen(((HERB_OSXLangShellStorage *)storage)->shell->buffer, "commands:\n");

    ARC_ConsoleBuffer_AddCStringWithStrlen(((HERB_OSXLangShellStorage *)storage)->shell->buffer, "\t" HERB_OSXLANG_SHELL_STRING_EXIT            "\t\t\t\t- closes the shell\n");
    ARC_ConsoleBuffer_AddCStringWithStrlen(((HERB_OSXLangShellStorage *)storage)->shell->buffer, "\t" HERB_OSXLANG_SHELL_STRING_HELP            "\t\t\t\t- lists this menu\n");
    ARC_ConsoleBuffer_AddCStringWithStrlen(((HERB_OSXLangShellStorage *)storage)->shell->buffer, "\t" HERB_OSXLANG_SHELL_STRING_LOAD            " <file> { -v }\t\t- loads an osx file into the soft drive\n");
    ARC_ConsoleBuffer_AddCStringWithStrlen(((HERB_OSXLangShellStorage *)storage)->shell->buffer, "\t" HERB_OSXLANG_SHELL_STRING_LS              "\t\t\t\t- lists loaded files in the soft drive\n");
    ARC_ConsoleBuffer_AddCStringWithStrlen(((HERB_OSXLangShellStorage *)storage)->shell->buffer, "\t" HERB_OSXLANG_SHELL_STRING_RUNLOCAL        " <file>\t\t\t- loads and runs a selected file from the soft drive\n");
    ARC_ConsoleBuffer_AddCStringWithStrlen(((HERB_OSXLangShellStorage *)storage)->shell->buffer, "\t" HERB_OSXLANG_SHELL_STRING_COREDUMP        "\t\t\t- outputs machine's data (memory, registers)\n");
    ARC_ConsoleBuffer_AddCStringWithStrlen(((HERB_OSXLangShellStorage *)storage)->shell->buffer, "\t" HERB_OSXLANG_SHELL_STRING_ERRORDUMP       "\t\t\t- outputs a list of logged errors thrown\n");
    // ARC_ConsoleBuffer_AddCStringWithStrlen(((HERB_OSXLangShellStorage *)storage)->shell->buffer, "\t" HERB_OSXLANG_SHELL_STRING_SHELL           "\t\t\t- runs a nested shell\n");
    ARC_ConsoleBuffer_AddCStringWithStrlen(((HERB_OSXLangShellStorage *)storage)->shell->buffer, "\t" HERB_OSXLANG_SHELL_STRING_PROCESS_VIEWER  "\t\t\t\t- views running processes (press esc to close)\n");
    ARC_ConsoleBuffer_AddCStringWithStrlen(((HERB_OSXLangShellStorage *)storage)->shell->buffer, "\t" HERB_OSXLANG_SHELL_STRING_CONNECT_PROCESS " <id>\t\t\t- connects the shell to a process of the selected id\n");
    ARC_ConsoleBuffer_AddCStringWithStrlen(((HERB_OSXLangShellStorage *)storage)->shell->buffer, "\t" HERB_OSXLANG_SHELL_STRING_GANTT           " { -o, -c, -s, -e }\t- views gantt chart (press esc to close)\n");
    ARC_ConsoleBuffer_AddCStringWithStrlen(((HERB_OSXLangShellStorage *)storage)->shell->buffer, "\t" HERB_OSXLANG_SHELL_STRING_SET_QUANTUM     " <num quantum> ...\t- sets the quantums, the first number is the number of quantums\n");
    ARC_ConsoleBuffer_AddCStringWithStrlen(((HERB_OSXLangShellStorage *)storage)->shell->buffer, "\t" HERB_OSXLANG_SHELL_STRING_PAGE            " { -g, -s, -d, -r }\t\t- gets or sets the page number\n");
    ARC_ConsoleBuffer_AddChar(((HERB_OSXLangShellStorage *)storage)->shell->buffer, '\n');
}

void HERB_OSXLang_ShellLOAD(void *storage, void *instructionString, uint64_t instructionSize){
    //create temporary string data to manipulate
    ARC_String *substring;
    ARC_String_Copy(&substring, (ARC_String *)instructionString);

    //check command
    ARC_String *command;
    ARC_String_CopySubstring(&command, instructionString, 0, strlen(HERB_OSXLANG_SHELL_STRING_LOAD));
    if(!ARC_String_EqualsCString(command, HERB_OSXLANG_SHELL_STRING_LOAD, strlen(HERB_OSXLANG_SHELL_STRING_LOAD))){
        arc_errno = ARC_ERRNO_DATA;
        //TODO: add this to buffer
        // ARC_DEBUG_LOG(arc_errno, "HERB_OSXLang_ShellLOAD(storage, instructionString, instructionSize), command \"%s\" does not match load command \"%s\"", command->data, HERB_OSXLANG_SHELL_STRING_LOAD);

        ARC_String *errorLogString;
        ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] command did not match " HERB_OSXLANG_SHELL_STRING_LOAD);

        HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
        ARC_String_Destroy(errorLogString);

        ARC_String_Destroy(command);
        ARC_String_Destroy(substring);
        return;
    }
    ARC_String_Destroy(command);

    //strip command
    ARC_String *tempDataString = substring;
    ARC_String_CopySubstring(&substring, tempDataString, strlen(HERB_OSXLANG_SHELL_STRING_LOAD), tempDataString->length - strlen(HERB_OSXLANG_SHELL_STRING_LOAD));

    if(substring == NULL){
        ARC_ConsoleBuffer_AddCStringWithStrlen(((HERB_OSXLangShellStorage *)storage)->shell->buffer, HERB_OSXLANG_SHELL_STRING_LOAD " had no parameters, check \"" HERB_OSXLANG_SHELL_STRING_LOAD " -h\" for details on how to use " HERB_OSXLANG_SHELL_STRING_LOAD);
        ARC_ConsoleBuffer_AddChar(((HERB_OSXLangShellStorage *)storage)->shell->buffer, '\n');
        return;
    }

    //get file path
    tempDataString = substring;
    ARC_String_StripEndsWhitespace(&substring, tempDataString);
    ARC_String_Destroy(tempDataString);

    //check if verbose flag
    uint8_t verboseFlag = HERB_OSXLang_ShellCheckAndStripFlag(&substring, "-v", 2);
    if(arc_errno){
        arc_errno = 0;
        ARC_ConsoleBuffer_AddCStringWithStrlen(((HERB_OSXLangShellStorage *)storage)->shell->buffer, HERB_OSXLANG_SHELL_STRING_LOAD " found -v flag but no file specified, check \"" HERB_OSXLANG_SHELL_STRING_LOAD " -h\" for details on how to use " HERB_OSXLANG_SHELL_STRING_LOAD);
        ARC_ConsoleBuffer_AddChar(((HERB_OSXLangShellStorage *)storage)->shell->buffer, '\n');
    }

    //TODO: set usermode based on shell
    HERB_OSXLangBinary *binary;
    HERB_OSXLangBinary_CreateFromFile(&binary, substring, 0);

    //remove path but not file name
    uint64_t endSlash = ARC_String_FindBackCString(substring, "/", 1);
    if(endSlash != ~(uint64_t)0){
        tempDataString = substring;
        ARC_String_CopySubstring(&substring, tempDataString, endSlash, tempDataString->length - endSlash);
        ARC_String_Destroy(tempDataString);
    }

    //check if file name already exists
    ARC_Vector *fileNames;
    ARC_Vector_Create(&fileNames);

    pthread_mutex_lock(((HERB_OSXLangShellStorage *)storage)->machineMutex);
    HERB_OSXLangMemory_GetVectorFileNames(((HERB_OSXLangShellStorage *)storage)->machine->softDrive, fileNames);
    pthread_mutex_unlock(((HERB_OSXLangShellStorage *)storage)->machineMutex);

    uint8_t foundFile = 0;
    for(uint32_t i = 0; i < ARC_Vector_Size(fileNames); i++){
        ARC_String *fileName = (ARC_String *)ARC_Vector_Get(fileNames, i);
        if(!foundFile){
            foundFile = ARC_String_Equals(substring, fileName);
        }
        ARC_String_Destroy(fileName);
    }
    ARC_Vector_Destroy(fileNames);

    if(foundFile){
        ARC_ConsoleBuffer_AddCStringWithStrlen(((HERB_OSXLangShellStorage *)storage)->shell->buffer, "could not load osx file, file with name already exists: \"");
        ARC_ConsoleBuffer_AddString(((HERB_OSXLangShellStorage *)storage)->shell->buffer, substring);
        ARC_ConsoleBuffer_AddChar(((HERB_OSXLangShellStorage *)storage)->shell->buffer, '"');
        ARC_ConsoleBuffer_AddChar(((HERB_OSXLangShellStorage *)storage)->shell->buffer, '\n');
        ARC_ConsoleBuffer_AddChar(((HERB_OSXLangShellStorage *)storage)->shell->buffer, '\n');

        //add to error log
        ARC_String *errorLogString;
        ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] SHELL: could not load file with same name as already is in memory: \"");

        ARC_String *tempErrorLogString = errorLogString;
        ARC_String_Merge(&errorLogString, tempErrorLogString, substring);
        ARC_String_Destroy(tempErrorLogString);

        ARC_String *tempErrorLogStringNext;
        tempErrorLogString = errorLogString;
        ARC_String_CreateWithStrlen(&tempErrorLogStringNext, "\"");
        ARC_String_Merge(&errorLogString, tempErrorLogString, tempErrorLogStringNext);
        ARC_String_Destroy(tempErrorLogString);
        ARC_String_Destroy(tempErrorLogStringNext);

        HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
        ARC_String_Destroy(errorLogString);
        ARC_String_Destroy(substring);
        return;
    }

    if(binary == NULL){
        arc_errno = 0;
        ARC_ConsoleBuffer_AddCStringWithStrlen(((HERB_OSXLangShellStorage *)storage)->shell->buffer, "could not load osx file: \"");
        ARC_ConsoleBuffer_AddString(((HERB_OSXLangShellStorage *)storage)->shell->buffer, substring);
        ARC_ConsoleBuffer_AddChar(((HERB_OSXLangShellStorage *)storage)->shell->buffer, '"');
        ARC_ConsoleBuffer_AddChar(((HERB_OSXLangShellStorage *)storage)->shell->buffer, '\n');
        ARC_ConsoleBuffer_AddChar(((HERB_OSXLangShellStorage *)storage)->shell->buffer, '\n');

        ARC_String_Destroy(substring);
        return;
    }

    //print the read in data verbosly
    if(verboseFlag){
        printf("Loaded: \"%s\"\n", substring->data);
        ARC_ConsoleBuffer_AddCStringWithStrlen(((HERB_OSXLangShellStorage *)storage)->shell->buffer, "Loaded: \"");
        ARC_ConsoleBuffer_AddString(((HERB_OSXLangShellStorage *)storage)->shell->buffer, substring);
        ARC_ConsoleBuffer_AddChar(((HERB_OSXLangShellStorage *)storage)->shell->buffer, '"');
        ARC_ConsoleBuffer_AddChar(((HERB_OSXLangShellStorage *)storage)->shell->buffer, '\n');

        //TODO: print to buffer
        HERB_OSXLangBinary_PrintToBuffer(binary, 0, 0, ((HERB_OSXLangShellStorage *)storage)->shell->buffer);
    }

    //load into soft drive
    pthread_mutex_lock(((HERB_OSXLangShellStorage *)storage)->machineMutex);
    HERB_OSXLangMemory_LoadBinaryFile(((HERB_OSXLangShellStorage *)storage)->machine->softDrive, binary, substring);
    pthread_mutex_unlock(((HERB_OSXLangShellStorage *)storage)->machineMutex);

    if(verboseFlag){
        ARC_ConsoleBuffer_AddCStringWithStrlen(((HERB_OSXLangShellStorage *)storage)->shell->buffer, "\nSoft Drive Memory:");

        pthread_mutex_lock(((HERB_OSXLangShellStorage *)storage)->machineMutex);
        HERB_OSXLangMemory_PrintToBuffer(((HERB_OSXLangShellStorage *)storage)->machine->softDrive, 0, 0, ((HERB_OSXLangShellStorage *)storage)->shell->buffer);
        pthread_mutex_unlock(((HERB_OSXLangShellStorage *)storage)->machineMutex);
    }
    ARC_ConsoleBuffer_AddChar(((HERB_OSXLangShellStorage *)storage)->shell->buffer, '\n');

    //cleanup
    ARC_String_Destroy(substring);

    //TODO: probs fix this
    if(arc_errno){
        arc_errno = 0;
    }
}

void HERB_OSXLang_ShellLS(void *storage, void *instructionString, uint64_t instructionSize){
    //TODO: print to buffer
    pthread_mutex_lock(((HERB_OSXLangShellStorage *)storage)->machineMutex);
    HERB_OSXLangMemory_PrintListFilesToBuffer(((HERB_OSXLangShellStorage *)storage)->machine->softDrive, ((HERB_OSXLangShellStorage *)storage)->shell->buffer);
    pthread_mutex_unlock(((HERB_OSXLangShellStorage *)storage)->machineMutex);
}

void HERB_OSXLang_ShellRUNLOCAL(void *storage, void *instructionString, uint64_t instructionSize){
    //create temporary string data to manipulate
    ARC_String *substring;
    ARC_String_Copy(&substring, (ARC_String *)instructionString);

    //check command
    ARC_String *command;
    ARC_String_CopySubstring(&command, instructionString, 0, strlen(HERB_OSXLANG_SHELL_STRING_RUNLOCAL));
    if(!ARC_String_EqualsCString(command, HERB_OSXLANG_SHELL_STRING_RUNLOCAL, strlen(HERB_OSXLANG_SHELL_STRING_RUNLOCAL))){
        arc_errno = ARC_ERRNO_DATA;
        //TODO: add this to buffer
        // ARC_DEBUG_LOG(arc_errno, "HERB_OSXLang_ShellLOAD(storage, instructionString, instructionSize), command \"%s\" does not match load command \"%s\"", command->data, HERB_OSXLANG_SHELL_STRING_RUNLOCAL);

        ARC_String *errorLogString;
        ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] command did not match " HERB_OSXLANG_SHELL_STRING_RUNLOCAL);

        HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
        ARC_String_Destroy(errorLogString);

        ARC_String_Destroy(command);
        ARC_String_Destroy(substring);

        //TODO: probs fix this
        if(arc_errno){
            arc_errno = 0;
        }

        return;
    }
    ARC_String_Destroy(command);

    //strip command
    ARC_String *tempDataString = substring;
    ARC_String_CopySubstring(&substring, tempDataString, strlen(HERB_OSXLANG_SHELL_STRING_RUNLOCAL), tempDataString->length - strlen(HERB_OSXLANG_SHELL_STRING_RUNLOCAL));

    //strip whitespace
    tempDataString = substring;
    ARC_String_StripEndsWhitespace(&substring, tempDataString);
    ARC_String_Destroy(tempDataString);

    ARC_Vector *loadFileNames;
    ARC_Vector_Create(&loadFileNames);

    //read in all the file names to load
    uint64_t spaceIndex = ARC_String_FindCString(substring, " ", 1);
    while(spaceIndex != ~(uint64_t)0){
        ARC_String *loadFile;
        ARC_String_CopySubstring(&loadFile, substring, 0, spaceIndex - 1);

        ARC_Vector_Add(loadFileNames, (void *)loadFile);

        tempDataString = substring;
        ARC_String_CopySubstring(&substring, tempDataString, spaceIndex, tempDataString->length - spaceIndex);
        ARC_String_Destroy(tempDataString);

        spaceIndex = ARC_String_FindCString(substring, " ", 1);
    }

    ARC_String *loadFile;
    ARC_String_Copy(&loadFile, substring);
    ARC_Vector_Add(loadFileNames, (void *)loadFile);
    ARC_String_Destroy(substring);

    //check if file name already exists
    ARC_Vector *fileNames;
    ARC_Vector_Create(&fileNames);

    pthread_mutex_lock(((HERB_OSXLangShellStorage *)storage)->machineMutex);
    HERB_OSXLangMemory_GetVectorFileNames(((HERB_OSXLangShellStorage *)storage)->machine->softDrive, fileNames);
    pthread_mutex_unlock(((HERB_OSXLangShellStorage *)storage)->machineMutex);

    for(uint32_t loadFileIndex = 0; loadFileIndex < ARC_Vector_Size(loadFileNames); loadFileIndex++){
        ARC_String *loadFile = (ARC_String *)ARC_Vector_Get(loadFileNames, loadFileIndex);

        uint8_t foundFile = 0;
        for(uint32_t i = 0; i < ARC_Vector_Size(fileNames); i++){
            ARC_String *fileName = (ARC_String *)ARC_Vector_Get(fileNames, i);
            if(!foundFile){
                foundFile = ARC_String_Equals(loadFile, fileName);
            }
        }

        if(!foundFile){
            ARC_ConsoleBuffer_AddCStringWithStrlen(((HERB_OSXLangShellStorage *)storage)->shell->buffer, "could not find osx file \"");
            ARC_ConsoleBuffer_AddString(((HERB_OSXLangShellStorage *)storage)->shell->buffer, substring);
            ARC_ConsoleBuffer_AddCStringWithStrlen(((HERB_OSXLangShellStorage *)storage)->shell->buffer, "\" on soft drive, you may need to load it to run it\n\n");

            //filenames cleanup
            for(uint32_t i = 0; i < ARC_Vector_Size(fileNames); i++){
                ARC_String *fileName = (ARC_String *)ARC_Vector_Get(fileNames, i);
                ARC_String_Destroy(fileName);
            }
            ARC_Vector_Destroy(fileNames);

            //loadfilenames cleanup
            for(uint32_t i = 0; i < ARC_Vector_Size(loadFileNames); i++){
                ARC_String *loadFile = (ARC_String *)ARC_Vector_Get(loadFileNames, i);
                ARC_String_Destroy(loadFile);
            }
            ARC_Vector_Destroy(loadFileNames);

            //TODO: probs fix this
            if(arc_errno){
                arc_errno = 0;
            }
            return;
        }
    }

    //filenames cleanup
    for(uint32_t i = 0; i < ARC_Vector_Size(fileNames); i++){
        ARC_String *fileName = (ARC_String *)ARC_Vector_Get(fileNames, i);
        ARC_String_Destroy(fileName);
    }
    ARC_Vector_Destroy(fileNames);

    //add process state to the new queue to run
    pthread_mutex_lock(((HERB_OSXLangShellStorage *)storage)->machineMutex);
    for(uint32_t i = 0; i < ARC_Vector_Size(loadFileNames); i++){
        ARC_String *loadFile = (ARC_String *)ARC_Vector_Get(loadFileNames, i);
        HERB_OSXLangProcessState_CreateProcessWithFilenameAsNewState(((HERB_OSXLangShellStorage *)storage)->processState, loadFile);
    }
    pthread_mutex_unlock(((HERB_OSXLangShellStorage *)storage)->machineMutex);

    //cleanup
    for(uint32_t i = 0; i < ARC_Vector_Size(loadFileNames); i++){
        ARC_String *loadFile = (ARC_String *)ARC_Vector_Get(loadFileNames, i);
        ARC_String_Destroy(loadFile);
    }
    ARC_Vector_Destroy(loadFileNames);

    ARC_ConsoleBuffer_AddChar(((HERB_OSXLangShellStorage *)storage)->shell->buffer, '\n');

    //TODO: probs fix this
    if(arc_errno){
        arc_errno = 0;
    }
}

typedef struct HERB_OSXLang_ShellCOREDUMPPollData {
    pthread_mutex_t *machineMutex;
    ARC_ConsoleBuffer *buffer;
    HERB_OSXLangMachine *machine;
} HERB_OSXLang_ShellCOREDUMPPollData;

void HERB_OSXLang_ShellCOREDUMPPollFn(void *userdata){
    HERB_OSXLang_ShellCOREDUMPPollData *pollData = (HERB_OSXLang_ShellCOREDUMPPollData *)userdata;

    pthread_mutex_lock(pollData->machineMutex);
    ARC_ConsoleBuffer_Clear(pollData->buffer);
    HERB_OSXLangMachine_PrintToBuffer(pollData->machine, 0, 0, pollData->buffer);
    pthread_mutex_unlock(pollData->machineMutex);
}

void HERB_OSXLang_ShellCOREDUMP(void *storage, void *instructionString, uint64_t instructionSize){
    ARC_ConsoleBuffer *processViewerBuffer;
    ARC_ConsoleBuffer_Create(&processViewerBuffer);

    uint32_t pollTime = 5;
    HERB_ConsoleBufferPollFn pollFn = HERB_OSXLang_ShellCOREDUMPPollFn;

    HERB_OSXLang_ShellCOREDUMPPollData pollData = {
        ((HERB_OSXLangShellStorage *)storage)->machineMutex,
        processViewerBuffer,
        ((HERB_OSXLangShellStorage *)storage)->machine
    };
    ARC_ConsoleView_Clear(((HERB_OSXLangShellStorage *)storage)->shell->view);
    HERB_ConsoleBuffer_PollAndRunScroll(((HERB_OSXLangShellStorage *)storage)->shell->view, processViewerBuffer, &pollTime, &pollFn, (void *)&pollData);

    ARC_ConsoleView_Clear(((HERB_OSXLangShellStorage *)storage)->shell->view);
    ARC_ConsoleBuffer_Destroy(processViewerBuffer);

    ARC_ConsoleBuffer_AddChar(((HERB_OSXLangShellStorage *)storage)->shell->buffer, '\n');
}

void HERB_OSXLang_ShellERRORDUMP(void *storage, void *instructionString, uint64_t instructionSize){
    HERB_OSXLangErrorLog_PrintToBuffer(herb_errorlog, ((HERB_OSXLangShellStorage *)storage)->shell->buffer);
    ARC_ConsoleBuffer_AddChar(((HERB_OSXLangShellStorage *)storage)->shell->buffer, '\n');
}

void HERB_OSXLang_ShellSHELL(void *storage, void *instructionString, uint64_t instructionSize){
    //TODO: Fix this
    // HERB_OSXLangRunnerShell_Start("NESTED SHELL > ");
}

typedef struct HERB_OSXLang_ShellPROCESS_VIEWERPollData {
    pthread_mutex_t *machineMutex;
    ARC_ConsoleBuffer *buffer;
    HERB_OSXLangProcessState *processState;
} HERB_OSXLang_ShellPROCESS_VIEWERPollData;

void HERB_OSXLang_ShellPROCESS_VIEWERPollFn(void *userdata){
    HERB_OSXLang_ShellPROCESS_VIEWERPollData *pollData = (HERB_OSXLang_ShellPROCESS_VIEWERPollData *)userdata;

    pthread_mutex_lock(pollData->machineMutex);
    ARC_ConsoleBuffer_Clear(pollData->buffer);
    HERB_OSXLangProcessState_PrintProcessesToBuffer(pollData->processState, pollData->buffer);
    pthread_mutex_unlock(pollData->machineMutex);
}

void HERB_OSXLang_ShellPROCESS_VIEWER(void *storage, void *instructionString, uint64_t instructionSize){
    ARC_ConsoleBuffer *processViewerBuffer;
    ARC_ConsoleBuffer_Create(&processViewerBuffer);

    uint32_t pollTime = 1;
    HERB_ConsoleBufferPollFn pollFn = HERB_OSXLang_ShellPROCESS_VIEWERPollFn;

    HERB_OSXLang_ShellPROCESS_VIEWERPollData pollData = {
        ((HERB_OSXLangShellStorage *)storage)->machineMutex,
        processViewerBuffer,
        ((HERB_OSXLangShellStorage *)storage)->processState
    };
    ARC_ConsoleView_Clear(((HERB_OSXLangShellStorage *)storage)->shell->view);
    HERB_ConsoleBuffer_PollAndRunScroll(((HERB_OSXLangShellStorage *)storage)->shell->view, processViewerBuffer, &pollTime, &pollFn, (void *)&pollData);

    ARC_ConsoleView_Clear(((HERB_OSXLangShellStorage *)storage)->shell->view);
    ARC_ConsoleBuffer_Destroy(processViewerBuffer);

    ARC_ConsoleBuffer_AddChar(((HERB_OSXLangShellStorage *)storage)->shell->buffer, '\n');
}

typedef struct HERB_OSXLang_ShellCONNECT_PROCESSInputData {
    HERB_OSXLangProcess *process;
    ARC_Point cursorPos;
} HERB_OSXLang_ShellCONNECT_PROCESSInputData;

ARC_Bool HERB_OSXLang_ShellCONNECT_PROCESSInputFn(ARC_ConsoleKey *consoleKey, void *userdata){
    HERB_OSXLang_ShellCONNECT_PROCESSInputData *inputData = (HERB_OSXLang_ShellCONNECT_PROCESSInputData *)userdata;
    if(inputData->process->completed){
        return ARC_False;
    }

    if(ARC_ConsoleKey_EqualsPointer(consoleKey, ARC_KEY_ESC)){
        return ARC_False;
    }

    uint8_t *keyChar = (uint8_t *)malloc(sizeof(uint8_t));
    *keyChar = ARC_ConsoleKey_GetCharFromKey(consoleKey);
    ARC_Queue_Push(inputData->process->bufferCharQueue, keyChar);

    return ARC_True;
}

void HERB_OSXLang_ShellCONNECT_PROCESS(void *storage, void *instructionString, uint64_t instructionSize){
    HERB_OSXLangShellStorage *shellStorage = (HERB_OSXLangShellStorage *)storage;

    //create temporary string data to manipulate
    ARC_String *substring;
    ARC_String_Copy(&substring, (ARC_String *)instructionString);

    //check command
    ARC_String *command;
    ARC_String_CopySubstring(&command, instructionString, 0, strlen(HERB_OSXLANG_SHELL_STRING_CONNECT_PROCESS));
    if(!ARC_String_EqualsCString(command, HERB_OSXLANG_SHELL_STRING_CONNECT_PROCESS, strlen(HERB_OSXLANG_SHELL_STRING_CONNECT_PROCESS))){
        arc_errno = ARC_ERRNO_DATA;
        //TODO: add this to buffer
        // ARC_DEBUG_LOG(arc_errno, "HERB_OSXLang_ShellLOAD(storage, instructionString, instructionSize), command \"%s\" does not match load command \"%s\"", command->data, HERB_OSXLANG_SHELL_STRING_RUNLOCAL);

        ARC_String *errorLogString;
        ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] command did not match " HERB_OSXLANG_SHELL_STRING_RUNLOCAL);

        HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
        ARC_String_Destroy(errorLogString);

        ARC_String_Destroy(command);
        ARC_String_Destroy(substring);

        //TODO: probs fix this
        if(arc_errno){
            arc_errno = 0;
        }

        return;
    }
    ARC_String_Destroy(command);

    //strip command
    ARC_String *tempDataString = substring;
    ARC_String_CopySubstring(&substring, tempDataString, strlen(HERB_OSXLANG_SHELL_STRING_RUNLOCAL), tempDataString->length - strlen(HERB_OSXLANG_SHELL_STRING_RUNLOCAL));
    ARC_String_Destroy(tempDataString);
    uint32_t id = (uint32_t)ARC_String_ToUint64_t(substring);

    uint32_t pollTime = 1;
    HERB_OSXLangProcess *process = HERB_OSXLangProcessState_GetProcess(shellStorage->processState, id);
    HERB_OSXLang_ShellCONNECT_PROCESSInputData inputData = {
        process,
        (ARC_Point){ 0, 0 }
    };

    process->bufferConnected = ARC_True;
    HERB_ConsoleBuffer_Poll(shellStorage->shell->view, process->buffer, &(inputData.cursorPos), HERB_OSXLang_ShellCONNECT_PROCESSInputFn, &pollTime, NULL, (void *)&inputData);

    if(process){
        process->bufferConnected = ARC_False;
    }

    ARC_ConsoleBuffer_AddChar(((HERB_OSXLangShellStorage *)storage)->shell->buffer, '\n');
}

typedef struct HERB_OSXLang_ShellGANTTPollData {
    pthread_mutex_t *machineMutex;
    ARC_ConsoleBuffer *buffer;
    HERB_OSXLangProcessState *processState;
} HERB_OSXLang_ShellGANTTPollData;

void HERB_OSXLang_ShellGANTTPollFn(void *userdata){
    HERB_OSXLang_ShellGANTTPollData *pollData = (HERB_OSXLang_ShellGANTTPollData *)userdata;

    pthread_mutex_lock(pollData->machineMutex);
    ARC_ConsoleBuffer_Clear(pollData->buffer);
    HERB_OSXLangGanttChart_PrintToBuffer(herb_ganttchart, pollData->buffer);
    pthread_mutex_unlock(pollData->machineMutex);
}

void HERB_OSXLang_ShellGANTT(void *storage, void *instructionString, uint64_t instructionSize){
    //create temporary string data to manipulate
    ARC_String *substring;
    ARC_String_Copy(&substring, (ARC_String *)instructionString);

    //check if output flag
    uint8_t outputFlag = HERB_OSXLang_ShellCheckAndStripFlag(&substring, "-o", 2);
    if(arc_errno){
        arc_errno = 0;
        ARC_ConsoleBuffer_AddCStringWithStrlen(((HERB_OSXLangShellStorage *)storage)->shell->buffer, HERB_OSXLANG_SHELL_STRING_GANTT " found -o flag but no file specified, check \"" HERB_OSXLANG_SHELL_STRING_GANTT " -h\" for details on how to use " HERB_OSXLANG_SHELL_STRING_GANTT);
        ARC_ConsoleBuffer_AddChar(((HERB_OSXLangShellStorage *)storage)->shell->buffer, '\n');
    }

    //if output flag was included, output to file then go back to the shell
    if(outputFlag){
        ARC_String *path;
        ARC_String_CreateWithStrlen(&path, "gantt.txt");
        HERB_OSXLangGanttChart_PrintToFile(herb_ganttchart, path);
        ARC_String_Destroy(path);

        //add a new line chart to the shell buffer
        ARC_ConsoleBuffer_AddChar(((HERB_OSXLangShellStorage *)storage)->shell->buffer, '\n');
        return;
    }

    //check if output flag
    uint8_t clearFlag = HERB_OSXLang_ShellCheckAndStripFlag(&substring, "-c", 2);
    if(arc_errno){
        arc_errno = 0;
    }

    //if output flag was included, clear the chart then go back to the shell
    if(clearFlag){
        HERB_OSXLangGanttChart_Clear(herb_ganttchart);

        //add a new line chart to the shell buffer
        ARC_ConsoleBuffer_AddChar(((HERB_OSXLangShellStorage *)storage)->shell->buffer, '\n');
        return;
    }

    //check if start flag
    uint8_t startFlag = HERB_OSXLang_ShellCheckAndStripFlag(&substring, "-s", 2);
    if(arc_errno){
        arc_errno = 0;
    }

    //if start flag was included, set use gantt chart to true then go back to the shell
    if(startFlag){
        herb_useGanttchart = ARC_True;

        //add a new line chart to the shell buffer
        ARC_ConsoleBuffer_AddChar(((HERB_OSXLangShellStorage *)storage)->shell->buffer, '\n');
        return;
    }

    //check if output flag
    uint8_t endFlag = HERB_OSXLang_ShellCheckAndStripFlag(&substring, "-e", 2);
    if(arc_errno){
        arc_errno = 0;
    }

    //if end flag was included, set use gantt chart to false then go back to the shell
    if(endFlag){
        herb_useGanttchart = ARC_False;

        //add a new line chart to the shell buffer
        ARC_ConsoleBuffer_AddChar(((HERB_OSXLangShellStorage *)storage)->shell->buffer, '\n');
        return;
    }

    //create a buffer to show the gantt chart
    ARC_ConsoleBuffer *ganttBuffer;
    ARC_ConsoleBuffer_Create(&ganttBuffer);

    //set the poll time and poll function
    uint32_t pollTime = 1;
    HERB_ConsoleBufferPollFn pollFn = HERB_OSXLang_ShellGANTTPollFn;

    HERB_OSXLang_ShellPROCESS_VIEWERPollData pollData = {
        ((HERB_OSXLangShellStorage *)storage)->machineMutex,
        ganttBuffer,
        ((HERB_OSXLangShellStorage *)storage)->processState
    };
    ARC_ConsoleView_Clear(((HERB_OSXLangShellStorage *)storage)->shell->view);
    HERB_ConsoleBuffer_PollAndRunScroll(((HERB_OSXLangShellStorage *)storage)->shell->view, ganttBuffer, &pollTime, &pollFn, (void *)&pollData);

    //clear the console buffer
    ARC_ConsoleView_Clear(((HERB_OSXLangShellStorage *)storage)->shell->view);
    ARC_ConsoleBuffer_Destroy(ganttBuffer);

    //add a new line chart to the shell buffer
    ARC_ConsoleBuffer_AddChar(((HERB_OSXLangShellStorage *)storage)->shell->buffer, '\n');
}

void HERB_OSXLang_ShellSET_QUANTUM(void *storage, void *instructionString, uint64_t instructionSize){
    HERB_OSXLangShellStorage *shellStorage = (HERB_OSXLangShellStorage *)storage;

    //create temporary string data to manipulate
    ARC_String *substring;
    ARC_String_Copy(&substring, (ARC_String *)instructionString);

    //strip command
    ARC_String *tempDataString = substring;
    ARC_String_CopySubstring(&substring, tempDataString, strlen(HERB_OSXLANG_SHELL_STRING_SET_QUANTUM) + 1, tempDataString->length - (strlen(HERB_OSXLANG_SHELL_STRING_SET_QUANTUM) + 1));
    ARC_String_Destroy(tempDataString);

    uint64_t spaceIndex = ARC_String_FindCString(substring, " ", 1);
    if(spaceIndex == ~(uint64_t)0){
        //TODO print error;
        ARC_String_Destroy(substring);
        return;
    }

    ARC_String_CopySubstring(&tempDataString, substring, 0, spaceIndex - 1);
    uint8_t quantumSize = (uint8_t)ARC_String_ToUint64_t(tempDataString);
    ARC_String_Destroy(tempDataString);

    tempDataString = substring;
    ARC_String_CopySubstring(&substring, tempDataString, spaceIndex, tempDataString->length - spaceIndex);
    ARC_String_Destroy(tempDataString);

    uint64_t quantums[quantumSize];
    for(uint32_t i = 0; i < quantumSize - 1; i++){
        spaceIndex = ARC_String_FindCString(substring, " ", 1);
        ARC_String_CopySubstring(&tempDataString, substring, 0, spaceIndex - 1);
        quantums[i] = ARC_String_ToUint64_t(tempDataString);
        ARC_String_Destroy(tempDataString);

        tempDataString = substring;
        ARC_String_CopySubstring(&substring, tempDataString, spaceIndex, tempDataString->length - spaceIndex);
        ARC_String_Destroy(tempDataString);
    }

    quantums[quantumSize - 1] = ARC_String_ToUint64_t(substring);
    ARC_String_Destroy(substring);

    HERB_OSXLangProcessState_SetQuantums(shellStorage->processState, quantumSize, quantums);
}

void HERB_OSXLang_ShellPAGE(void *storage, void *instructionString, uint64_t instructionSize){
    //get storage back into its original type
    HERB_OSXLangShellStorage *shellStorage = (HERB_OSXLangShellStorage *)storage;

    //create temporary string data to manipulate
    ARC_String *substring;
    ARC_String_Copy(&substring, (ARC_String *)instructionString);

    //strip command
    ARC_String *tempDataString = substring;
    ARC_String_CopySubstring(&substring, tempDataString, strlen(HERB_OSXLANG_SHELL_STRING_PAGE) + 1, tempDataString->length - (strlen(HERB_OSXLANG_SHELL_STRING_PAGE) + 1));
    ARC_String_Destroy(tempDataString);

    //check if set flag
    uint8_t setFlag = HERB_OSXLang_ShellCheckAndStripFlag(&substring, "-s", 2);
    if(arc_errno){
        arc_errno = 0;
    }

    //check if get flag
    uint8_t getFlag = HERB_OSXLang_ShellCheckAndStripFlag(&substring, "-g", 2);
    if(arc_errno){
        arc_errno = 0;
    }

    //check if ram flag
    uint8_t ramFlag = HERB_OSXLang_ShellCheckAndStripFlag(&substring, "-r", 2);
    if(arc_errno){
        arc_errno = 0;
    }

    //check if drive flag
    uint8_t driveFlag = HERB_OSXLang_ShellCheckAndStripFlag(&substring, "-d", 2);
    if(arc_errno){
        arc_errno = 0;
    }

    if(setFlag && getFlag){
        //TODO: error stuff
        return;
    }

    //output ram when output and ram flag are both toggled
    if(getFlag && (ramFlag || (!ramFlag && !driveFlag))){
        ARC_ConsoleBuffer_AddCStringWithStrlen(shellStorage->shell->buffer, "Random Access Memory Page Size: ");
        pthread_mutex_lock(shellStorage->machineMutex);

        //I'm too burn't out to put a better value here, 256 is overkill
        char pageSizeCStr[256];
        sprintf(pageSizeCStr, "%u", shellStorage->machine->randomAccessMemory->blockSize);
        ARC_ConsoleBuffer_AddCStringWithStrlen(shellStorage->shell->buffer, pageSizeCStr);

        pthread_mutex_unlock(shellStorage->machineMutex);
        ARC_ConsoleBuffer_AddChar(shellStorage->shell->buffer, '\n');
    }

    //output drive when output and ram flag are both toggled
    if(getFlag && (driveFlag || (!ramFlag && !driveFlag))){
        ARC_ConsoleBuffer_AddCStringWithStrlen(shellStorage->shell->buffer, "Soft Drive Page Size: ");
        pthread_mutex_lock(shellStorage->machineMutex);

        //I'm too burn't out to put a better value here, 256 is overkill
        char pageSizeCStr[256];
        sprintf(pageSizeCStr, "%u", shellStorage->machine->softDrive->blockSize);
        ARC_ConsoleBuffer_AddCStringWithStrlen(shellStorage->shell->buffer, pageSizeCStr);

        pthread_mutex_unlock(shellStorage->machineMutex);
        ARC_ConsoleBuffer_AddChar(shellStorage->shell->buffer, '\n');
    }

    if(substring->length < 1){
        return;
    }

    //get the page size
    uint32_t pageSize = ARC_String_ToUint64_t(substring);
    ARC_String_Destroy(substring);
    if(arc_errno){
        arc_errno = 0;
        return;
    }

    //set the ram
    if(setFlag && (ramFlag || (!ramFlag && !driveFlag))){
        shellStorage->machine->randomAccessMemory->blockSize = pageSize;
    }

    //set the drive
    if(setFlag && (driveFlag || (!ramFlag && !driveFlag))){
        shellStorage->machine->softDrive->blockSize = pageSize;
    }

    ARC_ConsoleBuffer_AddChar(shellStorage->shell->buffer, '\n');
}

uint8_t HERB_OSXLang_ShellCheckAndStripFlag(ARC_String **command, char *flag, uint64_t flagSize){
    if(ARC_String_EqualsCString(*command, flag, 2)){
        arc_errno = ARC_ERRNO_DATA;
        return 1;
    }

    uint64_t checkFlagIndex = ARC_String_FindCString(*command, flag, 2);
    ARC_String *tempDataString = NULL;
    if(checkFlagIndex == ~(uint64_t)0){
        return 0;
    }

    //strip if at the end
    if(checkFlagIndex == (*command)->length - 1){
        tempDataString = *command;
        ARC_String_CopySubstring(command, tempDataString, 0, tempDataString->length - 2);
        ARC_String_Destroy(tempDataString);

        //strip whitespace
        tempDataString = *command;
        ARC_String_StripEndsWhitespace(command, tempDataString);
        ARC_String_Destroy(tempDataString);
        return 1;
    }

    //strip if at front
    if(checkFlagIndex == 1){
        tempDataString = *command;
        ARC_String_CopySubstring(command, tempDataString, 2, tempDataString->length - 2);
        ARC_String_Destroy(tempDataString);

        //strip whitespace
        tempDataString = *command;
        ARC_String_StripEndsWhitespace(command, tempDataString);
        ARC_String_Destroy(tempDataString);
        return 1;
    }

    //strip if in center TODO: check if this works if there are more flags
    ARC_String *tempDataStringFront;
    ARC_String_CopySubstring(&tempDataStringFront, *command, 0, checkFlagIndex - 1);

    ARC_String *tempDataStringBack;
    ARC_String_CopySubstring(&tempDataStringBack, *command, checkFlagIndex + flagSize, (*command)->length - (checkFlagIndex + flagSize));

    ARC_String_Merge(command, tempDataStringFront, tempDataStringBack);

    ARC_String_Destroy(tempDataStringFront);
    ARC_String_Destroy(tempDataStringBack);

    return 1;
}
