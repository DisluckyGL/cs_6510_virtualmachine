#ifndef HERB_OSXLANG_COMMANDS_H_
#define HERB_OSXLANG_COMMANDS_H_

#include "../../runner.h"
#include <arc/std/string.h>
#include <arc/std/vector.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief defines commands for OSX shell so they can be changed easily
*/
#define HERB_OSXLANG_SHELL_STRING_ERROR           "error"
#define HERB_OSXLANG_SHELL_STRING_EXIT            "exit"
#define HERB_OSXLANG_SHELL_STRING_HELP            "help"
#define HERB_OSXLANG_SHELL_STRING_LOAD            "load"
#define HERB_OSXLANG_SHELL_STRING_LS              "ls"
#define HERB_OSXLANG_SHELL_STRING_RUNLOCAL        "runlocal"
#define HERB_OSXLANG_SHELL_STRING_COREDUMP        "coredump"
#define HERB_OSXLANG_SHELL_STRING_ERRORDUMP       "errordump"
#define HERB_OSXLANG_SHELL_STRING_SHELL           "shell"
#define HERB_OSXLANG_SHELL_STRING_PROCESS_VIEWER  "pview"
#define HERB_OSXLANG_SHELL_STRING_CONNECT_PROCESS "connect"
#define HERB_OSXLANG_SHELL_STRING_GANTT           "gantt"
#define HERB_OSXLANG_SHELL_STRING_SET_QUANTUM     "setquantum"
#define HERB_OSXLANG_SHELL_STRING_PAGE            "page"

/**
 * @brief adds OSX Lang shell commands to a HERB_Runner
 *
 * @param runner HERB_Runner to add OSX Lang assembly instructions to
*/
void HERB_OSXLang_RegisterShellCommandsToHERBRunner(HERB_Runner *runner);

/**
 * @brief the ERROR command for the OSX Lang shell
 *
 * @note this is used as a HERB_Runner_InstructionFn, this doxygen will mention types used instead of void *
 *
 * @param storage       HERB_OSXLangShellStorage to store commands to. will do nothing
 * @param commandString string that holds the command from an osx lang assembly string
 * @param commandSize   this will not be used as commandString holds the string length, this parameter is needed for HERB_Runner
*/
void HERB_OSXLang_ShellERROR(void *storage, void *instructionString, uint64_t instructionSize);

/**
 * @brief the EXIT command for the OSX Lang shell
 *
 * @note this is used as a HERB_Runner_InstructionFn, this doxygen will mention types used instead of void *
 *
 * @param storage       HERB_OSXLangShellStorage to store commands to. will set running to false on success
 * @param commandString string that holds the command from an osx lang assembly string
 * @param commandSize   this will not be used as commandString holds the string length, this parameter is needed for HERB_Runner
*/
void HERB_OSXLang_ShellEXIT(void *storage, void *instructionString, uint64_t instructionSize);

/**
 * @brief the HELP command for the OSX Lang shell
 *
 * @note this is used as a HERB_Runner_InstructionFn, this doxygen will mention types used instead of void *
 *
 * @param storage       HERB_OSXLangShellStorage to store commands to. will do nothing
 * @param commandString string that holds the command from an osx lang assembly string
 * @param commandSize   this will not be used as commandString holds the string length, this parameter is needed for HERB_Runner
*/
void HERB_OSXLang_ShellHELP(void *storage, void *instructionString, uint64_t instructionSize);

/**
 * @brief the LOAD command for the OSX Lang shell
 *
 * @note this is used as a HERB_Runner_InstructionFn, this doxygen will mention types used instead of void *
 *
 * @param storage       HERB_OSXLangShellStorage to store commands to. will load to machine random access memory
 * @param commandString string that holds the command from an osx lang assembly string
 * @param commandSize   this will not be used as commandString holds the string length, this parameter is needed for HERB_Runner
*/
void HERB_OSXLang_ShellLOAD(void *storage, void *instructionString, uint64_t instructionSize);

/**
 * @brief the LS command for the OSX Lang shell
 *
 * @note this is used as a HERB_Runner_InstructionFn, this doxygen will mention types used instead of void *
 *
 * @param storage       HERB_OSXLangShellStorage to list files from
 * @param commandString string that holds the command from an osx lang assembly string
 * @param commandSize   this will not be used as commandString holds the string length, this parameter is needed for HERB_Runner
*/
void HERB_OSXLang_ShellLS(void *storage, void *instructionString, uint64_t instructionSize);

/**
 * @brief the RUNLOCAL command for the OSX Lang shell
 *
 * @note this is used as a HERB_Runner_InstructionFn, this doxygen will mention types used instead of void *
 *
 * @param storage       HERB_OSXLangShellStorage to run specified file from
 * @param commandString string that holds the command from an osx lang assembly string
 * @param commandSize   this will not be used as commandString holds the string length, this parameter is needed for HERB_Runner
*/
void HERB_OSXLang_ShellRUNLOCAL(void *storage, void *instructionString, uint64_t instructionSize);

/**
 * @brief the COREDUMP command for the OSX Lang shell
 *
 * @note this is used as a HERB_Runner_InstructionFn, this doxygen will mention types used instead of void *
 *
 * @param storage       HERB_OSXLangShellStorage to print data from
 * @param commandString string that holds the command from an osx lang assembly string
 * @param commandSize   this will not be used as commandString holds the string length, this parameter is needed for HERB_Runner
*/
void HERB_OSXLang_ShellCOREDUMP(void *storage, void *instructionString, uint64_t instructionSize);

/**
 * @brief the COREDUMP command for the OSX Lang shell
 *
 * @note this is used as a HERB_Runner_InstructionFn, this doxygen will mention types used instead of void *
 *
 * @param storage       HERB_OSXLangShellStorage to store commands to, will do nothing
 * @param commandString string that holds the command from an osx lang assembly string
 * @param commandSize   this will not be used as commandString holds the string length, this parameter is needed for HERB_Runner
*/
void HERB_OSXLang_ShellERRORDUMP(void *storage, void *instructionString, uint64_t instructionSize);

/**
 * @brief the SHELL command for the OSX Lang shell
 *
 * @note this is used as a HERB_Runner_InstructionFn, this doxygen will mention types used instead of void *
 *
 * @param storage       HERB_OSXLangShellStorage to store commands to. will do nothing
 * @param commandString string that holds the command from an osx lang assembly string
 * @param commandSize   this will not be used as commandString holds the string length, this parameter is needed for HERB_Runner
*/
void HERB_OSXLang_ShellSHELL(void *storage, void *instructionString, uint64_t instructionSize);

/**
 * @brief the PROCESS_VIEWER command for the OSX Lang shell
 *
 * @note this is used as a HERB_Runner_InstructionFn, this doxygen will mention types used instead of void *
 *
 * @param storage       HERB_OSXLangShellStorage to list porcesses from
 * @param commandString string that holds the command from an osx lang assembly string
 * @param commandSize   this will not be used as commandString holds the string length, this parameter is needed for HERB_Runner
*/
void HERB_OSXLang_ShellPROCESS_VIEWER(void *storage, void *instructionString, uint64_t instructionSize);

/**
 * @brief the CONNECT_PROCESS command for the OSX Lang shell
 *
 * @note this is used as a HERB_Runner_InstructionFn, this doxygen will mention types used instead of void *
 *
 * @param storage       HERB_OSXLangShellStorage to connect console view to porcesses
 * @param commandString string that holds the command from an osx lang assembly string
 * @param commandSize   this will not be used as commandString holds the string length, this parameter is needed for HERB_Runner
*/
void HERB_OSXLang_ShellCONNECT_PROCESS(void *storage, void *instructionString, uint64_t instructionSize);

/**
 * @brief the GANTT command for the OSX Lang shell
 *
 * @note this is used as a HERB_Runner_InstructionFn, this doxygen will mention types used instead of void *
 *
 * @param storage       HERB_OSXLangShellStorage to connect console view to gantt chart
 * @param commandString string that holds the command from an osx lang assembly string
 * @param commandSize   this will not be used as commandString holds the string length, this parameter is needed for HERB_Runner
*/
void HERB_OSXLang_ShellGANTT(void *storage, void *instructionString, uint64_t instructionSize);

/**
 * @brief the SET_QUANTUM command for the OSX Lang shell
 *
 * @note this is used as a HERB_Runner_InstructionFn, this doxygen will mention types used instead of void *
 *
 * @param storage       HERB_OSXLangShellStorage to set the quantum
 * @param commandString string that holds the command from an osx lang assembly string
 * @param commandSize   this will not be used as commandString holds the string length, this parameter is needed for HERB_Runner
*/
void HERB_OSXLang_ShellSET_QUANTUM(void *storage, void *instructionString, uint64_t instructionSize);

/**
 * @brief the PAGE command for the OSX Lang shell
 *
 * @note this is used as a HERB_Runner_InstructionFn, this doxygen will mention types used instead of void *
 *
 * @param storage       HERB_OSXLangShellStorage to set or get the memory page
 * @param commandString string that holds the command from an osx lang assembly string
 * @param commandSize   this will not be used as commandString holds the string length, this parameter is needed for HERB_Runner
*/
void HERB_OSXLang_ShellPAGE(void *storage, void *instructionString, uint64_t instructionSize);

/**
 * @brief checks for a flag and strips it from the string
 *
 * @note command will destroy the given ARC_String and replace it with a stripped version
 *
 * @param command  string to check for flag and strip
 * @param flag     cstring of flag to check
 * @param flagSize size of cstring flag to check
*/
uint8_t HERB_OSXLang_ShellCheckAndStripFlag(ARC_String **command, char *flag, uint64_t flagSize);

#ifdef __cplusplus
}
#endif

#endif // !HERB_OSXLANG_ASSEMBLER_H_
