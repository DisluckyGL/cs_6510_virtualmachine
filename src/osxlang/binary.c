#include "binary.h"

#include "errorlog.h"
#include <arc/console/buffer.h>
#include <arc/std/errno.h>
#include <arc/std/io.h>
#include <arc/std/string.h>
#include <stdlib.h>

void HERB_OSXLangBinary_Create(HERB_OSXLangBinary **binary, uint8_t mode, uint32_t programCounter, uint32_t loaderAddress, uint8_t *data, uint32_t dataSize){
    *binary = (HERB_OSXLangBinary *)malloc(sizeof(HERB_OSXLangBinary));

    (*binary)->mode           = mode;
    (*binary)->programCounter = programCounter;
    (*binary)->loaderAddress  = loaderAddress;

    (*binary)->data     = malloc(sizeof(uint8_t) * dataSize);
    (*binary)->dataSize = dataSize;

    for(uint32_t i = 0; i < dataSize; i++){
        (*binary)->data[i] = data[i];
    }
}

void HERB_OSXLangBinary_CreateFromFile(HERB_OSXLangBinary **binary, ARC_String *path, uint8_t mode){
    //read in file
    uint8_t *fileData;
    uint64_t fileDataSize;
    ARC_IO_ReadFileToUint8t(path, &fileData, &fileDataSize);
    if(arc_errno){
        *binary = NULL;
        return;
    }

    if(fileDataSize < 12){
        arc_errno = ARC_ERRNO_DATA;
        ARC_DEBUG_LOG(arc_errno, "HERB_OSXLangBinary_CreateFromFile(binary, path, mode), file \"%s\" had a size \"%lu\" which is smaller than the needed header", path->data, path->length);

        ARC_String *errorLogString;
        ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] file \"");

        ARC_String *tempErrorLogString = errorLogString;
        ARC_String_Merge(&errorLogString, tempErrorLogString, path);
        ARC_String_Destroy(tempErrorLogString);

        ARC_String *tempErrorLogStringNext;
        tempErrorLogString = errorLogString;
        ARC_String_CreateWithStrlen(&tempErrorLogStringNext, "\" did not have enough data for headers");
        ARC_String_Merge(&errorLogString, tempErrorLogString, tempErrorLogStringNext);
        ARC_String_Destroy(tempErrorLogString);
        ARC_String_Destroy(tempErrorLogStringNext);

        HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
        ARC_String_Destroy(errorLogString);

        return;
    }

    //get size of the data from the header
    uint32_t dataSize = 0;
    dataSize |= (0xff000000 & (fileData[0] << 24));
    dataSize |= (0x00ff0000 & (fileData[1] << 16));
    dataSize |= (0x0000ff00 & (fileData[2] <<  8));
    dataSize |= (0x000000ff & (fileData[3] <<  0));

    //get the program counter from the header
    uint32_t programCounter = 0;
    programCounter |= (0xff000000 & (fileData[4] << 24));
    programCounter |= (0x00ff0000 & (fileData[5] << 16));
    programCounter |= (0x0000ff00 & (fileData[6] <<  8));
    programCounter |= (0x000000ff & (fileData[7] <<  0));

    //get the loaderAddress from the header
    uint32_t loaderAddress = 0;
    loaderAddress |= (0xff000000 & (fileData[ 8] << 24));
    loaderAddress |= (0x00ff0000 & (fileData[ 9] << 16));
    loaderAddress |= (0x0000ff00 & (fileData[10] <<  8));
    loaderAddress |= (0x000000ff & (fileData[11] <<  0));

    //create the binary
    HERB_OSXLangBinary_Create(binary, mode, programCounter, loaderAddress, fileData + 12, dataSize);

    //cleanup
    free(fileData);
}

void HERB_OSXLangBinary_Destroy(HERB_OSXLangBinary *binary){
    free(binary->data);
    free(binary);
}

void HERB_OSXLangBinary_Print(HERB_OSXLangBinary *binary, uint8_t rowSize, uint8_t groupedBytes){
    //default row size is the same as xxd 8 grouped by two bytes
    if(rowSize == 0){
        rowSize = 8;
    }

    if(groupedBytes == 0){
        groupedBytes = 2;
    }

    printf("Mode: %u\n", binary->mode);
    printf("Program Counter: %u\n", binary->programCounter);
    printf("Loader Address: %u\n", binary->loaderAddress);

    for(uint32_t i = 0; i < binary->dataSize; i++){
        if(i % (rowSize * groupedBytes) == 0){
            printf("\n");
            printf("%08x:", i);
        }

        if(i % groupedBytes == 0){
            printf(" ");
        }

        printf("%02x", binary->data[i]);
    }

    printf("\n\n");
}

void HERB_OSXLangBinary_PrintToBuffer(HERB_OSXLangBinary *binary, uint8_t rowSize, uint8_t groupedBytes, ARC_ConsoleBuffer *buffer){
    //default row size is the same as xxd 8 grouped by two bytes
    if(rowSize == 0){
        rowSize = 8;
    }

    if(groupedBytes == 0){
        groupedBytes = 2;
    }

    char modeCString[11];
    sprintf(modeCString, "Mode: %u\n", binary->mode);
    ARC_ConsoleBuffer_AddCStringWithStrlen(buffer, modeCString);

    char programCounterCString[20];
    sprintf(programCounterCString, "Program Counter: %u\n", binary->programCounter);
    ARC_ConsoleBuffer_AddCStringWithStrlen(buffer, programCounterCString);

    char loaderAddressCString[19];
    sprintf(loaderAddressCString, "Loader Address: %u\n", binary->loaderAddress);
    ARC_ConsoleBuffer_AddCStringWithStrlen(buffer, loaderAddressCString);

    for(uint32_t i = 0; i < binary->dataSize; i++){
        if(i % (rowSize * groupedBytes) == 0){
            ARC_ConsoleBuffer_AddChar(buffer, '\n');

            char lineNumCString[11];
            sprintf(lineNumCString, "%08x:", i);
            ARC_ConsoleBuffer_AddCStringWithStrlen(buffer, lineNumCString);
        }

        if(i % groupedBytes == 0){
            ARC_ConsoleBuffer_AddChar(buffer, ' ');
        }

        char byteCString[3];
        sprintf(byteCString,"%02x", binary->data[i]);
        ARC_ConsoleBuffer_AddCStringWithStrlen(buffer, byteCString);
    }

    ARC_ConsoleBuffer_AddChar(buffer, '\n');
    ARC_ConsoleBuffer_AddChar(buffer, '\n');
}