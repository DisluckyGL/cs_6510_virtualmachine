#include "instruction.h"

#include <stdlib.h>
#include <stdio.h>

void HERB_OSXLangInstruction_Create(HERB_OSXLangInstruction **instruction, uint64_t byteSize){
    *instruction = (HERB_OSXLangInstruction *)malloc(sizeof(HERB_OSXLangInstruction));

    (*instruction)->bytes = (uint8_t *)malloc(sizeof(uint8_t) * (byteSize - 1));
    (*instruction)->bytesSize = byteSize;
}

void HERB_OSXLangInstruction_Destroy(HERB_OSXLangInstruction *instruction){
    free(instruction->bytes);
    free(instruction);
}

void HERB_OSXLangInstruction_Print(HERB_OSXLangInstruction *instruction){
    for(uint32_t byteIndex = 0; byteIndex < instruction->bytesSize; byteIndex++){
        printf("%02x ", instruction->bytes[byteIndex] & 0xff);
    }
}

void HERB_OSXLangInstruction_PrintWithNewLine(HERB_OSXLangInstruction *instruction){
    HERB_OSXLangInstruction_Print(instruction);
    printf("\n");
}