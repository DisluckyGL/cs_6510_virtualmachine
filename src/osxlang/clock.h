#ifndef HERB_OSXLANG_CLOCK_H_
#define HERB_OSXLANG_CLOCK_H_

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief a callback that runs when a clock is ticked
*/
typedef void (* HERB_OSXLangClock_TickFn)(void *userdata);

/**
 * @brief osx lang clock, cycle contains current cycle run, will loop back to 0 if ~(uint64_t)0 is hit
*/
typedef struct HERB_OSXLangClock {
    uint64_t cycle;

    HERB_OSXLangClock_TickFn tickFn;
} HERB_OSXLangClock;

/**
 * @brief creates a HERB_OSXLangClock type
 *
 * @param clock  clock to create
 * @param tickFn the function to run on tick, can be NULL
*/
void HERB_OSXLangClock_Create(HERB_OSXLangClock **clock, HERB_OSXLangClock_TickFn tickFn);

/**
 * @brief destroys HERB_OSXLangClock type
 * 
 * @param clock clock that will be destroyed
 */
void HERB_OSXLangClock_Destroy(HERB_OSXLangClock *clock);

/**
 * @brief runs a clock tick
 *
 * @param clock    the clock to run a tick on
 * @param userdata the userdata to pass to the TickFn, can be NULL
*/
void HERB_OSXLangClock_Tick(HERB_OSXLangClock *clock, void *userdata);

/**
 * @brief gets a clock cycle
 *
 * @param clock the clock to get the cycle from
 *
 * @return the clocks cycle
*/
uint64_t HERB_OSXLangClock_GetCycle(HERB_OSXLangClock *clock);

/**
 * @brief sets the function run for clock tick
 *
 * @param clock  the clock to set tickFn
 * @param tickFn the function to run on tick, can be NULL
*/
void HERB_OSXLangClock_SetTickFn(HERB_OSXLangClock *clock, HERB_OSXLangClock_TickFn tickFn);

#ifdef __cplusplus
}
#endif

#endif // !HERB_OSXLANG_CLOCK_H_