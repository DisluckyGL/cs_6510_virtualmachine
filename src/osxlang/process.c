#include "process.h"

#include "errorlog.h"
#include "memory.h"
#include "registers.h"
#include <arc/console/buffer.h>
#include <arc/std/bool.h>
#include <arc/std/errno.h>
#include <arc/std/queue.h>
#include <arc/std/vector.h>
#include <stdlib.h>
#include <stdint.h>

void HERB_OSXLangProcess_Create(HERB_OSXLangProcess **process, uint32_t id, ARC_String *name, uint8_t mode, uint32_t dataStartIndex, uint32_t dataSize, uint64_t programCounter){
    *process = (HERB_OSXLangProcess *)malloc(sizeof(HERB_OSXLangProcess));

    (*process)->id = id;

    ARC_String_Copy(&((*process)->name), name);

    (*process)->mode           = mode;
    (*process)->completed      = 0;
    (*process)->quantum        = 0;
    (*process)->dataStartIndex = dataStartIndex;
    (*process)->dataSize       = dataSize;
    (*process)->burstsRun      = 0;
    (*process)->burstsFinished = 0;

    //set registers to 0
    for(uint32_t i = 0; i < HERB_OSXLANG_REGISTER_MAX_ENCODE; i++){
        (*process)->registers[i] = 0;
    }

    (*process)->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] = programCounter + dataStartIndex;

    ARC_Vector_Create(&((*process)->memoryBlocks));
    ARC_ConsoleBuffer_Create(&((*process)->buffer));
    (*process)->bufferConnected = ARC_False;
    ARC_Queue_Create(&((*process)->bufferCharQueue));

    (*process)->pollCycles       = HERB_OSXLANG_PROCESS_DEFUALT_PULL_CYCLES;
    (*process)->pollCurrentCycle = 0;
    (*process)->pollsOver        = 0;
    (*process)->pollsUnder       = 0;
}

void HERB_OSXLangProcess_CreateFromMemory(HERB_OSXLangProcess **process, uint32_t id, ARC_String *fileName, HERB_OSXLangMemory *softDrive, HERB_OSXLangMemory *randomAccessMemory){
    uint32_t startBlockIndex = HERB_OSXLangMemory_GetStartBlockIndexFromFileName(softDrive, fileName);
    if(arc_errno){
        return;
    }

    uint32_t index = 0;

    //get blocks
    uint32_t *blocks = NULL;
    uint32_t  blocksSize = 0;
    HERB_OSXLangMemory_GetBlocksFromIndex(&index, &blocks, &blocksSize, softDrive, startBlockIndex);

    //iterate past the string and ending \0
    index += fileName->length + 1;

    //get mode byte from header
    uint8_t mode = HERB_OSXLangMemory_GetDataByteFromBlocks(&index, softDrive, blocks, blocksSize);

    //get the data size from the header
    uint32_t dataSize = HERB_OSXLangMemory_GetDataBytesAsUint32FromBlocks(&index, softDrive, blocks, blocksSize);

    //get the program counter from the header
    uint32_t programCounter = HERB_OSXLangMemory_GetDataBytesAsUint32FromBlocks(&index, softDrive, blocks, blocksSize);

    //create the process, and start after the initial block type
    HERB_OSXLangProcess_Create(process, id, fileName, mode, 0, dataSize, programCounter);

    //get total size of what is being stored
    uint32_t storageSize = 0;

    //add the data size to size
    storageSize += dataSize;

    //get number of blocks needed
    uint32_t processBlocksSize = storageSize / randomAccessMemory->blockSize;

    //add one byte for each the block header
    storageSize += blocksSize;

    //get new block size
    uint32_t newProcessBlocksSize = storageSize / randomAccessMemory->blockSize;

    //add remainding blocks (if any)
    storageSize += newProcessBlocksSize - processBlocksSize;

    //set block size to the new block size
    processBlocksSize = newProcessBlocksSize;

    //add extra block if needed
    if(storageSize % randomAccessMemory->blockSize != 0){
        processBlocksSize++;
        //this will never create a new block because randomAccessMemory->blockSize has to be bigger than 5
        storageSize += 1;
    }

    //create blocks and fill with max value for error checks
    for(uint32_t i = 0; i < processBlocksSize; i++){
        uint32_t *memoryBlock = (uint32_t *)malloc(sizeof(uint32_t));
        *memoryBlock = ~(uint32_t)0;
        ARC_Vector_Add((*process)->memoryBlocks, memoryBlock);
    }

    //find empty blocks in ram TODO: clear block bytes on error
    uint32_t processBlockIndex = 0;
    for(uint32_t i = 0; i < randomAccessMemory->dataSize / randomAccessMemory->blockSize; i++){
        if(((uint8_t *)randomAccessMemory->data)[i * randomAccessMemory->blockSize] == HERB_OSXLANG_MEMORY_BLOCK_NONE){
            uint32_t *memoryBlock = ARC_Vector_Get((*process)->memoryBlocks, processBlockIndex);
            *memoryBlock = i * randomAccessMemory->blockSize;
            processBlockIndex++;
            if(processBlockIndex >= processBlocksSize){
                break;
            }
        }
    }

    //throw an error if not all of the blocks were found
    if(processBlockIndex != processBlocksSize){
        arc_errno = ARC_ERRNO_DATA;
        ARC_DEBUG_ERR("HERB_OSXLangProcess_CreateFromMemory(process, id, fileName, softDrive, randomAccessMemory), overflowed when loading process");

        ARC_String *errorLogString;
        ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] overflowed when loading process");

        HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
        ARC_String_Destroy(errorLogString);

        return;
    }

    //get the starting index
    uint32_t dataIndex = 0;

    //set block type header
    uint32_t *memoryBlock = ARC_Vector_Get((*process)->memoryBlocks, dataIndex);
    randomAccessMemory->data[*memoryBlock] = HERB_OSXLANG_MEMORY_BLOCK_DATA;

    //get process blocks in a format that can be used by HERB_OSXLangMemory
    processBlocksSize = ARC_Vector_Size((*process)->memoryBlocks);
    uint32_t processBlocks[processBlocksSize];
    for(uint32_t i = 0; i < processBlocksSize; i++){
        processBlocks[i] = *(uint32_t *)ARC_Vector_Get((*process)->memoryBlocks, i);
    }

    //read in process data into random access memory
    for(uint32_t i = 0; i < dataSize; i++){
        uint8_t dataByte = HERB_OSXLangMemory_GetDataByteFromBlocks(&index, softDrive, blocks, blocksSize);
        HERB_OSXLangMemory_SetDataByteToBlocks(&dataIndex, randomAccessMemory, processBlocks, processBlocksSize, dataByte);

        if(arc_errno){
            return;
        }
    }

    //clenup
    free(blocks);
}

void HERB_OSXLangProcess_Destroy(HERB_OSXLangProcess *process){
    while(ARC_Queue_Size(process->bufferCharQueue) != 0){
        uint8_t *character = ARC_Queue_Pop(process->bufferCharQueue);
        free(character);
    }
    ARC_Queue_Destroy(process->bufferCharQueue);

    ARC_ConsoleBuffer_Destroy(process->buffer);

    for(uint32_t i = 0; i < ARC_Vector_Size(process->memoryBlocks); i++){
        void *data = ARC_Vector_Get(process->memoryBlocks, i);

        if(data != NULL){
            free(data);
        }
    }

    ARC_Vector_Destroy(process->memoryBlocks);

    ARC_String_Destroy(process->name);

    free(process);
}

void HERB_OSXLangProcess_DestroyAndFreeMemory(HERB_OSXLangProcess *process, HERB_OSXLangMemory *randomAccessMemory){
    for(uint32_t i = 0; i < ARC_Vector_Size(process->memoryBlocks); i++){
        uint32_t block = *(uint32_t *)ARC_Vector_Get(process->memoryBlocks, i);
        randomAccessMemory->data[block] = HERB_OSXLANG_MEMORY_BLOCK_NONE;
    }

    HERB_OSXLangProcess_Destroy(process);
}

uint32_t HERB_OSXLangProcess_GetBlocksInMemoryNeededToCreateProcess(ARC_String *fileName, HERB_OSXLangMemory *softDrive, HERB_OSXLangMemory *randomAccessMemory){
    uint32_t startBlockIndex = HERB_OSXLangMemory_GetStartBlockIndexFromFileName(softDrive, fileName);
    if(arc_errno){
        return 0;
    }

    uint32_t index = 0;

    //get blocks
    uint32_t *blocks = NULL;
    uint32_t  blocksSize = 0;
    HERB_OSXLangMemory_GetBlocksFromIndex(&index, &blocks, &blocksSize, softDrive, startBlockIndex);

    //iterate past the string and ending \0
    index += fileName->length + 1;

    //skip mode byte from header
    index++;

    //get the data size from the header
    uint32_t dataSize = HERB_OSXLangMemory_GetDataBytesAsUint32FromBlocks(&index, softDrive, blocks, blocksSize);

    //skip the program counter from the header
    index += 4;

    //get total size of what is being stored
    uint32_t storageSize = 0;

    //add the data size to size
    storageSize += dataSize;

    //get number of blocks needed
    uint32_t processBlocksSize = storageSize / randomAccessMemory->blockSize;

    //add one byte for each the block header
    storageSize += blocksSize;

    //get new block size
    uint32_t newProcessBlocksSize = storageSize / randomAccessMemory->blockSize;

    //add remainding blocks (if any)
    storageSize += newProcessBlocksSize - processBlocksSize;

    //set block size to the new block size
    processBlocksSize = newProcessBlocksSize;

    //add extra block if needed
    if(storageSize % softDrive->blockSize != 0){
        processBlocksSize++;
        //this will never create a new block because softDrive->blockSize has to be bigger than 5
        storageSize += 1;
    }

    free(blocks);

    return processBlocksSize;
}