#ifndef HERB_OSXLANG_BINARY_EXECUTOR_H_
#define HERB_OSXLANG_BINARY_EXECUTOR_H_

#include "../instruction.h"
#include "../memory.h"
#include "../registers.h"
#include "../process.h"
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief predefines the HERB_OSXLangBinaryExecutor type so that HERB_OSXLangBinaryExecutor_InstructionFn can pass it in params
*/
typedef struct HERB_OSXLangBinaryExecutor HERB_OSXLangBinaryExecutor;

/**
 * @brief a function used when regestring instructions in a HERB_OSXLangBinaryExecutor
*/
typedef void (* HERB_OSXLangBinaryExecutor_InstructionFn)(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize);

/**
 * @brief the HERB_OSXLangBinaryExecutor type, has registers and instructions for running
*/
struct HERB_OSXLangBinaryExecutor {
    HERB_OSXLangProcess *currentProcess;

    uint32_t registers[HERB_OSXLANG_REGISTER_MAX_ENCODE];

    HERB_OSXLangBinaryExecutor_InstructionFn instructions[HERB_OSXLANG_INSTRUCTION_MAX_ENCODE];
};

/**
 * @brief creates HERB_OSXLangBinaryExecutor type
 *
 * @param binaryExecutor HERB_OSXLangBinaryExecutor to create
*/
void HERB_OSXLangBinaryExecutor_Create(HERB_OSXLangBinaryExecutor **binaryExecutor);

/**
 * @brief destroys HERB_OSXLangBinaryExecutor type
 * 
 * @param binaryExecutor binaryExecutor that will be destroyed
*/
void HERB_OSXLangBinaryExecutor_Destroy(HERB_OSXLangBinaryExecutor *binaryExecutor);

/**
 * @brief executes a process from memory within using a binary executor
 *
 * @param binaryExecutor binary executor to run process and memory on
 * @param memory         memory to use when executing in a binary executor
 * @param process        process to run with binary executor
*/
void HERB_OSXLangBinaryExecutor_Execute(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, HERB_OSXLangProcess *process);

/**
 * @brief executes a process a burst number of times from memory within using a binary executor
 *
 * @note burstNum will be decremented as it is run, this is helpful for if burst ends early due to a software interrupt
 *
 * @param binaryExecutor binary executor to run process and memory on
 * @param memory         memory to use when executing in a binary executor
 * @param process        process to run with binary executor
 * @param burstNum       number of times to run the burst, will be decrimented
*/
void HERB_OSXLangBinaryExecutor_ExecuteBurst(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, HERB_OSXLangProcess *process, uint32_t *burstNum);

/**
 * @brief the instruction function that runs if no instruction is found
 *
 * @note this is used as a HERB_Runner_InstructionFn, this doxygen will mention types used instead of void *
 *
 * @param binaryExecutor binaryExecutor that holds registers and instructions, registers will be used but most likely not instructions
 * @param memory         the memory to preform an operation on
 * @param blocks         the blocks of memory that can be used
 * @param blocksSize     the size of the blocks of memory allowed
*/
void HERB_OSXLangBinaryExecutor_ExecuteNoInstructionFn(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize);

/**
 * @brief the ADR instruction for the OSX Lang
 *
 * @note [<label>] ADR <reg1> <label> ; Get address of label
 *       Encode: 1 byte op code | 1 byte register | 4 byte address
 *       ADR = 0
 *       Suggested Usage: <reg1>  address(<label>)
 *
 * @param binaryExecutor binaryExecutor that holds registers and instructions, registers will be used but most likely not instructions
 * @param memory         the memory to preform an adr operation on
 * @param blocks         the blocks of memory that can be used
 * @param blocksSize     the size of the blocks of memory allowed
*/
void HERB_OSXLangBinaryExecutor_ExecuteADR(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize);

/**
 * @brief the MOV instruction for the OSX Lang
 *
 * @note [<label>] MOV <reg1> < reg2> ; Load data from register
 *       Encode: 1 byte op code | 1 byte register | 1 byte register | 3 bytes unused
 *       MOV = 1
 *       Suggested Usage:  <reg1>  < reg2>
 *
 * @param binaryExecutor binaryExecutor that holds registers and instructions, registers will be used but most likely not instructions
 * @param memory         the memory to preform an mov operation on
 * @param blocks         the blocks of memory that can be used
 * @param blocksSize     the size of the blocks of memory allowed
*/
void HERB_OSXLangBinaryExecutor_ExecuteMOV(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize);

/**
 * @brief the STR instruction for the OSX Lang
 *
 * @note [<label>] STR <reg1> <reg2> ; Store word (int) using register indirect addressing
 *       Encode: 1 byte op code | 1 byte register | 1 byte register | 3 bytes unused
 *       STR=2
 *       Suggested Usage: memory[<reg2>]  <reg1>
 *
 * @param binaryExecutor binaryExecutor that holds registers and instructions, registers will be used but most likely not instructions
 * @param memory         the memory to preform an str operation on
 * @param blocks         the blocks of memory that can be used
 * @param blocksSize     the size of the blocks of memory allowed
*/
void HERB_OSXLangBinaryExecutor_ExecuteSTR(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize);

/**
 * @brief the STRB instruction for the OSX Lang
 *
 * @note [<label>] STRB <reg1> <reg2>  ; Store byte using register indirect addressing
 *       Encode: 1 byte op code | 1 byte register | 1 byte register | 3 bytes unused
 *       STRB=3
 *       Suggested Usage: memory[<reg2>]  byte(memory[<reg1>])
 *
 * @param binaryExecutor binaryExecutor that holds registers and instructions, registers will be used but most likely not instructions
 * @param memory         the memory to preform an strb operation on
 * @param blocks         the blocks of memory that can be used
 * @param blocksSize     the size of the blocks of memory allowed
*/
void HERB_OSXLangBinaryExecutor_ExecuteSTRB(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize);

/**
 * @brief the LDR instruction for the OSX Lang
 *
 * @note this is used as a HERB_OSXLangBinaryExecutor_InstructionFn
 *
 * @note [<label>] LDR <reg1> <reg2>  ; Load word (int) using register indirect addressing
 *       Encode: 1 byte op code | 1 byte register | 1 byte register | 3 bytes unused
 *       LDR=4
 *       Suggested Usage: <reg1>  memory[<reg2>]
 *
 * @param binaryExecutor binaryExecutor that holds registers and instructions, registers will be used but most likely not instructions
 * @param memory         the memory to preform an ldr operation on
 * @param blocks         the blocks of memory that can be used
 * @param blocksSize     the size of the blocks of memory allowed
*/
void HERB_OSXLangBinaryExecutor_ExecuteLDR(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize);

/**
 * @brief the LDRB instruction for the OSX Lang
 *
 * @note this is used as a HERB_OSXLangBinaryExecutor_InstructionFn
 *
 * @note [<label>] LDRB <reg1> <reg2>  ; Load byte using register indirect addressing
 *       Encode: 1 byte op code | 1 byte register | 1 byte register | 3 bytes unused
 *       LDRB=5
 *       Suggested Usage: <reg1>  byte(memory[<reg2>])
 *
 * @param binaryExecutor binaryExecutor that holds registers and instructions, registers will be used but most likely not instructions
 * @param memory         the memory to preform a ldrb operation on
 * @param blocks         the blocks of memory that can be used
 * @param blocksSize     the size of the blocks of memory allowed
*/
void HERB_OSXLangBinaryExecutor_ExecuteLDRB(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize);

/**
 * @brief the BX instruction for the OSX Lang
 *
 * @note this is used as a HERB_OSXLangBinaryExecutor_InstructionFn
 *
 * @note [<label>] BX <reg> ; Jump to address in register
 *       Encode: 1 byte op code | 1 byte register | 4 bytes unused
 *       BX = 6
 *       Suggested Usage: PC  <reg>
 *
 * @param binaryExecutor binaryExecutor that holds registers and instructions, registers will be used but most likely not instructions
 * @param memory         the memory to preform a bx operation on
 * @param blocks         the blocks of memory that can be used
 * @param blocksSize     the size of the blocks of memory allowed
*/
void HERB_OSXLangBinaryExecutor_ExecuteBX(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize);

/**
 * @brief the B instruction for the OSX Lang
 *
 * @note this is used as a HERB_OSXLangBinaryExecutor_InstructionFn
 *
 * @note [<label>] B <label> ;  Jump to label
 *       Encode: 1 byte op code | 4 byte address | 1 byte unused
 *       B = 7
 *       Suggested Usage: PC  address(<label>)
 *
 * @param binaryExecutor binaryExecutor that holds registers and instructions, registers will be used but most likely not instructions
 * @param memory         the memory to preform a b operation on
 * @param blocks         the blocks of memory that can be used
 * @param blocksSize     the size of the blocks of memory allowed
*/
void HERB_OSXLangBinaryExecutor_ExecuteB(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize);

/**
 * @brief the BNE instruction for the OSX Lang
 *
 * @note this is used as a HERB_OSXLangBinaryExecutor_InstructionFn
 *
 * @note [<label>] BNE <label> ; Jump to label if Z register is not zero
 *       Encode: 1 byte op code | 4 byte address | 1 byte unused
 *       BNE = 8
 *       Suggested Usage: PC  address(<label>) if Z != 0
 *
 * @param binaryExecutor binaryExecutor that holds registers and instructions, registers will be used but most likely not instructions
 * @param memory         the memory to preform a bne operation on
 * @param blocks         the blocks of memory that can be used
 * @param blocksSize     the size of the blocks of memory allowed
*/
void HERB_OSXLangBinaryExecutor_ExecuteBNE(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize);

/**
 * @brief the BGT instruction for the OSX Lang
 *
 * @note this is used as a HERB_OSXLangBinaryExecutor_InstructionFn
 *
 * @note [<label>] BGT <label> ; Jump to label if Z register is greater than zero
 *       Encode: 1 byte op code | 4 byte address | 1 byte unused
 *       BGT = 9
 *       Suggested Usage: PC  address(<label>) if Z > 0
 *
 * @param binaryExecutor binaryExecutor that holds registers and instructions, registers will be used but most likely not instructions
 * @param memory         the memory to preform a bgt operation on
 * @param blocks         the blocks of memory that can be used
 * @param blocksSize     the size of the blocks of memory allowed
*/
void HERB_OSXLangBinaryExecutor_ExecuteBGT(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize);

/**
 * @brief the BLT instruction for the OSX Lang
 *
 * @note this is used as a HERB_OSXLangBinaryExecutor_InstructionFn
 *
 * @note [<label>] BLT <label> ; Jump to label if Z register is less than zero
 *       Encode: 1 byte op code | 4 byte address | 1 byte unused
 *       BLT = 10
 *       Suggested Usage: PC  address(<label>) if Z < 0
 *
 * @param binaryExecutor binaryExecutor that holds registers and instructions, registers will be used but most likely not instructions
 * @param memory         the memory to preform a blt operation on
 * @param blocks         the blocks of memory that can be used
 * @param blocksSize     the size of the blocks of memory allowed
*/
void HERB_OSXLangBinaryExecutor_ExecuteBLT(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize);

/**
 * @brief the BEQ instruction for the OSX Lang
 *
 * @note this is used as a HERB_OSXLangBinaryExecutor_InstructionFn
 *
 * @note [<label>] BEQ <label> ; Jump to label if Z register is zero
 *       Encode: 1 byte op code | 4 byte address | 1 byte unused
 *       BEQ = 11
 *       Suggested Usage: PC  address(<label>) if Z == 0
 *
 * @param binaryExecutor binaryExecutor that holds registers and instructions, registers will be used but most likely not instructions
 * @param memory         the memory to preform a beq operation on
 * @param blocks         the blocks of memory that can be used
 * @param blocksSize     the size of the blocks of memory allowed
*/
void HERB_OSXLangBinaryExecutor_ExecuteBEQ(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize);

/**
 * @brief the CMP instruction for the OSX Lang
 *
 * @note this is used as a HERB_OSXLangBinaryExecutor_InstructionFn
 *
 * @note [<label>] CMP <reg1> <reg2> ; Compare <reg1> and <reg2> place result in Z register
 *       Encode: 1 byte op code | 1 byte register | 1 byte register | 3 bytes unused
 *       CMP = 12
 *       Suggested Usage: Z  <reg1> - <reg2>
 *
 * @param binaryExecutor binaryExecutor that holds registers and instructions, registers will be used but most likely not instructions
 * @param memory         the memory to preform a cmp operation on
 * @param blocks         the blocks of memory that can be used
 * @param blocksSize     the size of the blocks of memory allowed
*/
void HERB_OSXLangBinaryExecutor_ExecuteCMP(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize);

/**
 * @brief the AND instruction for the OSX Lang
 *
 * @note this is used as a HERB_OSXLangBinaryExecutor_InstructionFn
 *
 * @note [<label>] AND <reg1> <reg2> ; Perform an AND operation on <reg1> and <reg2> result in Z register
 *       Encode: 1 byte op code | 1 byte register | 1 byte register | 3 bytes unused
 *       AND = 13
 *
 * @note The AND operation can be logical or bitwise either will work
 *       Suggested Usage: Z  <reg1> & <reg2> bitwise AND operation
 *       Suggested Usage: Z  <reg1> && <reg2> logical AND operation
 *
 * @param binaryExecutor binaryExecutor that holds registers and instructions, registers will be used but most likely not instructions
 * @param memory         the memory to preform an and operation on
 * @param blocks         the blocks of memory that can be used
 * @param blocksSize     the size of the blocks of memory allowed
*/
void HERB_OSXLangBinaryExecutor_ExecuteAND(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize);

/**
 * @brief the ORR instruction for the OSX Lang
 *
 * @note this is used as a HERB_OSXLangBinaryExecutor_InstructionFn
 *
 * @note [<label>] ORR <reg1> <reg2> ; Perform an OR operation on <reg1> and <reg2> result in Z register
 *       Encode: 1 byte op code | 1 byte register | 1 byte register | 3 bytes unused
 *       ORR = 14
 *
 * @note The ORR operation can be logical or bitwise either will work
 *       Suggested Usage: Z  <reg1> | <reg2> bitwise OR operation
 *       Suggested Usage: Z  <reg1> || <reg2> logical OR operation
 *
 * @param binaryExecutor binaryExecutor that holds registers and instructions, registers will be used but most likely not instructions
 * @param memory         the memory to preform an orr operation on
 * @param blocks         the blocks of memory that can be used
 * @param blocksSize     the size of the blocks of memory allowed
*/
void HERB_OSXLangBinaryExecutor_ExecuteORR(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize);

/**
 * @brief the EOR instruction for the OSX Lang
 *
 * @note this is used as a HERB_OSXLangBinaryExecutor_InstructionFn
 *
 * @note [<label>] EOR <reg1> <reg2> ; Exclusive OR operation on <reg1> and <reg2> result in Z register
 *       Encode: 1 byte op code | 1 byte register | 1 byte register | 3 bytes unused
 *       EOR = 15
 *
 * @note Note: The EOR operation can be logical or bitwise, there is little actual need for this instruction
 *       Suggested Usage: Z  <reg1> ^ <reg2> bitwise Exclusive OR operation
 *       Suggested Usage: Z  (<reg1> ||<reg2>) && !(<reg1> && <reg2>)
 *
 * @param binaryExecutor binaryExecutor that holds registers and instructions, registers will be used but most likely not instructions
 * @param memory         the memory to preform an eor operation on
 * @param blocks         the blocks of memory that can be used
 * @param blocksSize     the size of the blocks of memory allowed
*/
void HERB_OSXLangBinaryExecutor_ExecuteEOR(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize);

/**
 * @brief runs an ADD OSX Lang instruction
 *
 * @note this is used as a HERB_OSXLangBinaryExecutor_InstructionFn
 *
 * @note [<label>] ADD <reg1> <reg2> <reg3> ; Add
 *       Encode: 1 byte op code | 1 byte register | 1 byte register | 1 byte register | 2 bytes unused
 *       ADD = 16
 *       Suggested Usage: <reg1>  <reg2> + <reg3> 
 *
 * @param binaryExecutor binaryExecutor that holds registers and instructions, registers will be used but most likely not instructions
 * @param memory         the memory to preform an add operation on
 * @param blocks         the blocks of memory that can be used
 * @param blocksSize     the size of the blocks of memory allowed
*/
void HERB_OSXLangBinaryExecutor_ExecuteADD(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize);

/**
 * @brief the SUB instruction for the OSX Lang
 *
 * @note this is used as a HERB_OSXLangBinaryExecutor_InstructionFn
 *
 * @note [<label>] SUB <reg1> <reg2> <reg3> ; Subtract
 *       Encode: 1 byte op code | 1 byte register | 1 byte register | 1 byte register | 2 bytes unused
 *       SUB = 17
 *       Suggested Usage:  <reg1>  <reg2> - <reg3>
 *
 * @param binaryExecutor binaryExecutor that holds registers and instructions, registers will be used but most likely not instructions
 * @param memory         the memory to preform an sub operation on
 * @param blocks         the blocks of memory that can be used
 * @param blocksSize     the size of the blocks of memory allowed
*/
void HERB_OSXLangBinaryExecutor_ExecuteSUB(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize);

/**
 * @brief the MUL instruction for the OSX Lang
 *
 * @note this is used as a HERB_OSXLangBinaryExecutor_InstructionFn
 *
 * @note [<label>] MUL <reg1> <reg2> <reg3> ; Multiple
 *       Encode: 1 byte op code | 1 byte register | 1 byte register | 1 byte register | 2 bytes unused
 *       MUL = 18
 *       Suggested Usage: <reg1>  <reg2> * <reg3>
 *
 * @param binaryExecutor binaryExecutor that holds registers and instructions, registers will be used but most likely not instructions
 * @param memory         the memory to preform an mul operation on
 * @param blocks         the blocks of memory that can be used
 * @param blocksSize     the size of the blocks of memory allowed
*/
void HERB_OSXLangBinaryExecutor_ExecuteMUL(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize);

/**
 * @brief the DIV instruction for the OSX Lang
 *
 * @note this is used as a HERB_OSXLangBinaryExecutor_InstructionFn
 *
 * @note [<label>] DIV <reg1> <reg2> <reg3> ; Divide
 *       Encode: 1 byte op code | 1 byte register | 1 byte register | 1 byte register | 2 bytes unused
 *       DIV = 19
 *       Suggested Usage: <reg1>  <reg2> / <reg3>
 *       Move Data
 *
 * @param binaryExecutor binaryExecutor that holds registers and instructions, registers will be used but most likely not instructions
 * @param memory         the memory to preform an div operation on
 * @param blocks         the blocks of memory that can be used
 * @param blocksSize     the size of the blocks of memory allowed
*/
void HERB_OSXLangBinaryExecutor_ExecuteDIV(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize);

/**
 * @brief the SWI instruction for the OSX Lang
 *
 * @note this is used as a HERB_OSXLangBinaryExecutor_InstructionFn
 *
 * @note SWI <imm> ; Software Interrupt
 *       Encode: 1 byte op code | 4 byte immediate | 1 byte unused
 *       SWI = 20
 *       Suggested Usage: Execute interrupt <imm>
 *
 * @param binaryExecutor binaryExecutor that holds registers and instructions, registers will be used but most likely not instructions
 * @param memory         the memory to preform an swi operation on
 * @param blocks         the blocks of memory that can be used
 * @param blocksSize     the size of the blocks of memory allowed
*/
void HERB_OSXLangBinaryExecutor_ExecuteSWI(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize);

/**
 * @brief the BL instruction for the OSX Lang
 *
 * @note this is used as a HERB_OSXLangBinaryExecutor_InstructionFn
 *
 * @note [<label>] BL <label> ;  Jump to label and link (place the PC of the next instruction into a register)
 *       Encode: 1 byte op code | 4 byte address | 1 byte unused
 *       BL = 21
 *       Suggested Usage: PC  address(<label>)  R5  PC+6
 * 
 * @note for the weebs reading this code, this bl does not stand for boys love...
 *
 * @param binaryExecutor binaryExecutor that holds registers and instructions, registers will be used but most likely not instructions
 * @param memory         the memory to preform an bl operation on
 * @param blocks         the blocks of memory that can be used
 * @param blocksSize     the size of the blocks of memory allowed
*/
void HERB_OSXLangBinaryExecutor_ExecuteBL(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize);

/**
 * @brief the MVI instruction for the OSX Lang
 *
 * @note this is used as a HERB_OSXLangBinaryExecutor_InstructionFn
 *
 * @note [<label>] MVI <reg1> <imm> ; Load register with immediate value
 *       Encode: 1 byte op code | 1 byte register | 4 byte immediate
 *       MVI = 22
 *       Suggested Usage: <reg1>  <imm>
 *
 * @param binaryExecutor binaryExecutor that holds registers and instructions, registers will be used but most likely not instructions
 * @param memory         the memory to preform a mvi operation on
 * @param blocks         the blocks of memory that can be used
 * @param blocksSize     the size of the blocks of memory allowed
*/
void HERB_OSXLangBinaryExecutor_ExecuteMVI(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize);

#ifdef __cplusplus
}
#endif

#endif // !HERB_OSXLANG_BINARY_EXECUTOR_H_