#include "executor.h"

#include "../errorlog.h"
#include "../registers.h"
#include <arc/std/errno.h>
#include <stdlib.h>
#include <stdio.h>

void HERB_OSXLangBinaryExecutor_Create(HERB_OSXLangBinaryExecutor **binaryExecutor){
    *binaryExecutor = (HERB_OSXLangBinaryExecutor *)malloc(sizeof(HERB_OSXLangBinaryExecutor));

    (*binaryExecutor)->currentProcess = NULL;

    //set registers to 0
    for(uint32_t i = 0; i < HERB_OSXLANG_REGISTER_MAX_ENCODE; i++){
        (*binaryExecutor)->registers[i] = 0;
    }

    //set instructions to fallback for if instructionFn doesn't exist
    for(uint32_t i = 0; i < HERB_OSXLANG_INSTRUCTION_MAX_ENCODE; i++){
        (*binaryExecutor)->instructions[i] = HERB_OSXLangBinaryExecutor_ExecuteNoInstructionFn;
    }

    //register instruction functions
    (*binaryExecutor)->instructions[HERB_OSXLANG_INSTRUCTION_ENCODE_ADR]  = HERB_OSXLangBinaryExecutor_ExecuteADR;
    (*binaryExecutor)->instructions[HERB_OSXLANG_INSTRUCTION_ENCODE_MOV]  = HERB_OSXLangBinaryExecutor_ExecuteMOV;
    (*binaryExecutor)->instructions[HERB_OSXLANG_INSTRUCTION_ENCODE_STR]  = HERB_OSXLangBinaryExecutor_ExecuteSTR;
    (*binaryExecutor)->instructions[HERB_OSXLANG_INSTRUCTION_ENCODE_STRB] = HERB_OSXLangBinaryExecutor_ExecuteSTRB;
    (*binaryExecutor)->instructions[HERB_OSXLANG_INSTRUCTION_ENCODE_LDR]  = HERB_OSXLangBinaryExecutor_ExecuteLDR;
    (*binaryExecutor)->instructions[HERB_OSXLANG_INSTRUCTION_ENCODE_LDRB] = HERB_OSXLangBinaryExecutor_ExecuteLDRB;
    (*binaryExecutor)->instructions[HERB_OSXLANG_INSTRUCTION_ENCODE_BX]   = HERB_OSXLangBinaryExecutor_ExecuteBX;
    (*binaryExecutor)->instructions[HERB_OSXLANG_INSTRUCTION_ENCODE_B]    = HERB_OSXLangBinaryExecutor_ExecuteB;
    (*binaryExecutor)->instructions[HERB_OSXLANG_INSTRUCTION_ENCODE_BNE]  = HERB_OSXLangBinaryExecutor_ExecuteBNE;
    (*binaryExecutor)->instructions[HERB_OSXLANG_INSTRUCTION_ENCODE_BGT]  = HERB_OSXLangBinaryExecutor_ExecuteBGT;
    (*binaryExecutor)->instructions[HERB_OSXLANG_INSTRUCTION_ENCODE_BLT]  = HERB_OSXLangBinaryExecutor_ExecuteBLT;
    (*binaryExecutor)->instructions[HERB_OSXLANG_INSTRUCTION_ENCODE_BEQ]  = HERB_OSXLangBinaryExecutor_ExecuteBEQ;
    (*binaryExecutor)->instructions[HERB_OSXLANG_INSTRUCTION_ENCODE_CMP]  = HERB_OSXLangBinaryExecutor_ExecuteCMP;
    (*binaryExecutor)->instructions[HERB_OSXLANG_INSTRUCTION_ENCODE_AND]  = HERB_OSXLangBinaryExecutor_ExecuteAND;
    (*binaryExecutor)->instructions[HERB_OSXLANG_INSTRUCTION_ENCODE_ORR]  = HERB_OSXLangBinaryExecutor_ExecuteORR;
    (*binaryExecutor)->instructions[HERB_OSXLANG_INSTRUCTION_ENCODE_EOR]  = HERB_OSXLangBinaryExecutor_ExecuteEOR;
    (*binaryExecutor)->instructions[HERB_OSXLANG_INSTRUCTION_ENCODE_ADD]  = HERB_OSXLangBinaryExecutor_ExecuteADD;
    (*binaryExecutor)->instructions[HERB_OSXLANG_INSTRUCTION_ENCODE_SUB]  = HERB_OSXLangBinaryExecutor_ExecuteSUB;
    (*binaryExecutor)->instructions[HERB_OSXLANG_INSTRUCTION_ENCODE_MUL]  = HERB_OSXLangBinaryExecutor_ExecuteMUL;
    (*binaryExecutor)->instructions[HERB_OSXLANG_INSTRUCTION_ENCODE_DIV]  = HERB_OSXLangBinaryExecutor_ExecuteDIV;
    (*binaryExecutor)->instructions[HERB_OSXLANG_INSTRUCTION_ENCODE_SWI]  = HERB_OSXLangBinaryExecutor_ExecuteSWI;
    (*binaryExecutor)->instructions[HERB_OSXLANG_INSTRUCTION_ENCODE_BL]   = HERB_OSXLangBinaryExecutor_ExecuteBL;
    (*binaryExecutor)->instructions[HERB_OSXLANG_INSTRUCTION_ENCODE_MVI]  = HERB_OSXLangBinaryExecutor_ExecuteMVI;
}

void HERB_OSXLangBinaryExecutor_Destroy(HERB_OSXLangBinaryExecutor *binaryExecutor){
    free(binaryExecutor);
}

void HERB_OSXLangBinaryExecutor_Execute(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, HERB_OSXLangProcess *process){
    //set the current process
    binaryExecutor->currentProcess = process;

    //copy contents of registers to the binary executor from the registers
    for(uint32_t i = 0; i < HERB_OSXLANG_REGISTER_MAX_ENCODE; i++){
        binaryExecutor->registers[i] = process->registers[i];
    }

    //get process blocks in a format that can be used by HERB_OSXLangMemory
    uint32_t blocksSize = ARC_Vector_Size(process->memoryBlocks);
    uint32_t blocks[blocksSize];
    for(uint32_t i = 0; i < blocksSize; i++){
        blocks[i] = *(uint32_t *)ARC_Vector_Get(process->memoryBlocks, i);
    }

    //run untill program is complete
    uint32_t index = binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] + ((binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] / memory->blockSize) + 1);
    uint8_t instructionByte = HERB_OSXLangMemory_GetDataByteFromBlocks(&index, memory, blocks, blocksSize);
    while(binaryExecutor->currentProcess->completed == 0){
        binaryExecutor->instructions[instructionByte](binaryExecutor, memory, blocks, blocksSize);

        //kill the program if there is an error TODO: might want to reset arc_errno to 0
        if(arc_errno){
            binaryExecutor->currentProcess->completed = 1;
        }

        index = binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] + ((binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] / memory->blockSize) + 1);
        instructionByte = HERB_OSXLangMemory_GetDataByteFromBlocks(&index, memory, blocks, blocksSize);
    }

    //copy contents of register back to process
    for(uint32_t i = 0; i < HERB_OSXLANG_REGISTER_MAX_ENCODE; i++){
        process->registers[i] = binaryExecutor->registers[i];
    }

    //unset the current process
    binaryExecutor->currentProcess = process;
}

void HERB_OSXLangBinaryExecutor_ExecuteBurst(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, HERB_OSXLangProcess *process, uint32_t *burstNum){
    //set the current process
    binaryExecutor->currentProcess = process;

    //copy contents of registers to the binary executor from the registers
    for(uint32_t i = 0; i < HERB_OSXLANG_REGISTER_MAX_ENCODE; i++){
        binaryExecutor->registers[i] = process->registers[i];
    }

    //get process blocks in a format that can be used by HERB_OSXLangMemory
    uint32_t blocksSize = ARC_Vector_Size(process->memoryBlocks);
    uint32_t blocks[blocksSize];
    for(uint32_t i = 0; i < blocksSize; i++){
        blocks[i] = *(uint32_t *)ARC_Vector_Get(process->memoryBlocks, i);
    }

    //run untill program is complete
    uint32_t index = binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] + ((binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] / memory->blockSize) + 1);
    uint8_t instructionByte = HERB_OSXLangMemory_GetDataByteFromBlocks(&index, memory, blocks, blocksSize);
    while(binaryExecutor->currentProcess->completed == 0 && *burstNum != 0 && binaryExecutor->currentProcess->mode != 1){
        binaryExecutor->instructions[instructionByte](binaryExecutor, memory, blocks, blocksSize);

        //kill the program if there is an error TODO: might want to reset arc_errno to 0
        if(arc_errno){
            binaryExecutor->currentProcess->completed = 1;
        }

        index = binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] + ((binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] / memory->blockSize) + 1);
        instructionByte = HERB_OSXLangMemory_GetDataByteFromBlocks(&index, memory, blocks, blocksSize);

        --*burstNum;
    }

    //copy contents of register back to process
    for(uint32_t i = 0; i < HERB_OSXLANG_REGISTER_MAX_ENCODE; i++){
        process->registers[i] = binaryExecutor->registers[i];
    }

    //unset the current process
    binaryExecutor->currentProcess = process;
    process->burstsFinished = 0;
    if(*burstNum == 0){
        process->burstsFinished = 1;
    }
}

void HERB_OSXLangBinaryExecutor_ExecuteNoInstructionFn(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize){
    uint32_t index = binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] + ((binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] / memory->blockSize) + 1);
    printf("INSTRUCTION %2u NOT WRITTEN\n", HERB_OSXLangMemory_GetDataByteFromBlocks(&index, memory, blocks, blocksSize));

    ARC_String *errorLogString;
    ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] instruction called doesn't have a matching function");

    HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
    ARC_String_Destroy(errorLogString);

    //end the program
    binaryExecutor->currentProcess->completed = 1;
}

void HERB_OSXLangBinaryExecutor_ExecuteADR(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize){
    //read in instruction
    uint32_t index = binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] + ((binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] / memory->blockSize) + 1);
    HERB_OSXLangInstruction *instruction;
    HERB_OSXLangInstruction_Create(&instruction, HERB_OSXLANG_INSTRUCTION_SIZE);

    for(uint32_t i = 0; i < HERB_OSXLANG_INSTRUCTION_SIZE; i++){
        instruction->bytes[i] = HERB_OSXLangMemory_GetDataByteFromBlocks(&index, memory, blocks, blocksSize);
    }

    //check if instruction is ADR
    if(instruction->bytes[0] != HERB_OSXLANG_INSTRUCTION_ENCODE_ADR){
        arc_errno = ARC_ERRNO_DATA;
        ARC_DEBUG_LOG(arc_errno, "HERB_OSXLangBinaryExecutor_ExecuteADR(binaryExecutor, memory), adr instruction is %d, instruction received was %u", HERB_OSXLANG_INSTRUCTION_ENCODE_ADR, instruction->bytes[0]);

        ARC_String *errorLogString;
        ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] adr instruction and instruction received were different");

        HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
        ARC_String_Destroy(errorLogString);

        HERB_OSXLangInstruction_Destroy(instruction);

        binaryExecutor->currentProcess->completed = 1;

        return;
    }

    //copy one register value into another register
    binaryExecutor->registers[instruction->bytes[1]] = 0;
    binaryExecutor->registers[instruction->bytes[1]] |= (0xff000000 & instruction->bytes[2] << 24);
    binaryExecutor->registers[instruction->bytes[1]] |= (0x00ff0000 & instruction->bytes[3] << 16);
    binaryExecutor->registers[instruction->bytes[1]] |= (0x0000ff00 & instruction->bytes[4] <<  8);
    binaryExecutor->registers[instruction->bytes[1]] |= (0x000000ff & instruction->bytes[5] <<  0);

    //update program counter
    binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] += HERB_OSXLANG_INSTRUCTION_SIZE;

    //clean up
    HERB_OSXLangInstruction_Destroy(instruction);
}

void HERB_OSXLangBinaryExecutor_ExecuteMOV(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize){
    //read in instruction
    uint32_t index = binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] + ((binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] / memory->blockSize) + 1);
    HERB_OSXLangInstruction *instruction;
    HERB_OSXLangInstruction_Create(&instruction, HERB_OSXLANG_INSTRUCTION_SIZE);

    for(uint32_t i = 0; i < HERB_OSXLANG_INSTRUCTION_SIZE; i++){
        instruction->bytes[i] = HERB_OSXLangMemory_GetDataByteFromBlocks(&index, memory, blocks, blocksSize);
    }

    //check if instruction is MOV
    if(instruction->bytes[0] != HERB_OSXLANG_INSTRUCTION_ENCODE_MOV){
        arc_errno = ARC_ERRNO_DATA;
        ARC_DEBUG_LOG(arc_errno, "HERB_OSXLangBinaryExecutor_ExecuteMOV(binaryExecutor, memory), mov instruction is %d, instruction received was %u", HERB_OSXLANG_INSTRUCTION_ENCODE_MOV, instruction->bytes[0]);

        ARC_String *errorLogString;
        ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] mov instruction and instruction received were different");

        HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
        ARC_String_Destroy(errorLogString);

        HERB_OSXLangInstruction_Destroy(instruction);

        binaryExecutor->currentProcess->completed = 1;

        return;
    }

    //copy one register value into another register
    binaryExecutor->registers[instruction->bytes[1]] = binaryExecutor->registers[instruction->bytes[2]];

    //update program counter
    binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] += HERB_OSXLANG_INSTRUCTION_SIZE;

    //clean up
    HERB_OSXLangInstruction_Destroy(instruction);
}

void HERB_OSXLangBinaryExecutor_ExecuteSTR(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize){
    //read in instruction
    uint32_t index = binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] + ((binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] / memory->blockSize) + 1);
    HERB_OSXLangInstruction *instruction;
    HERB_OSXLangInstruction_Create(&instruction, HERB_OSXLANG_INSTRUCTION_SIZE);

    for(uint32_t i = 0; i < HERB_OSXLANG_INSTRUCTION_SIZE; i++){
        instruction->bytes[i] = HERB_OSXLangMemory_GetDataByteFromBlocks(&index, memory, blocks, blocksSize);
    }

    //check if instruction is STR
    if(instruction->bytes[0] != HERB_OSXLANG_INSTRUCTION_ENCODE_STR){
        arc_errno = ARC_ERRNO_DATA;
        ARC_DEBUG_LOG(arc_errno, "HERB_OSXLangBinaryExecutor_ExecuteSTR(binaryExecutor, memory), str instruction is %d, instruction received was %u", HERB_OSXLANG_INSTRUCTION_ENCODE_STR, instruction->bytes[0]);

        ARC_String *errorLogString;
        ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] str instruction and instruction received were different");

        HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
        ARC_String_Destroy(errorLogString);

        HERB_OSXLangInstruction_Destroy(instruction);

        binaryExecutor->currentProcess->completed = 1;

        return;
    }

    //load four bytes memory from register1 address into memory at register2 address
    index = binaryExecutor->registers[instruction->bytes[1]];
    uint32_t data = HERB_OSXLangMemory_GetDataBytesAsUint32FromBlocks(&index, memory, blocks, blocksSize);

    index = binaryExecutor->registers[instruction->bytes[2]];
    HERB_OSXLangMemory_SetDataBytesToUint32InBlocks(&index, memory, blocks, blocksSize, data);

    //update program counter
    binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] += HERB_OSXLANG_INSTRUCTION_SIZE;

    //clean up
    HERB_OSXLangInstruction_Destroy(instruction);
}

void HERB_OSXLangBinaryExecutor_ExecuteSTRB(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize){
    //read in instruction
    uint32_t index = binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] + ((binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] / memory->blockSize) + 1);
    HERB_OSXLangInstruction *instruction;
    HERB_OSXLangInstruction_Create(&instruction, HERB_OSXLANG_INSTRUCTION_SIZE);

    for(uint32_t i = 0; i < HERB_OSXLANG_INSTRUCTION_SIZE; i++){
        instruction->bytes[i] = HERB_OSXLangMemory_GetDataByteFromBlocks(&index, memory, blocks, blocksSize);
    }

    //check if instruction is STRB
    if(instruction->bytes[0] != HERB_OSXLANG_INSTRUCTION_ENCODE_STRB){
        arc_errno = ARC_ERRNO_DATA;
        ARC_DEBUG_LOG(arc_errno, "HERB_OSXLangBinaryExecutor_ExecuteSTRB(binaryExecutor, memory), strb instruction is %d, instruction received was %u", HERB_OSXLANG_INSTRUCTION_ENCODE_STRB, instruction->bytes[0]);

        ARC_String *errorLogString;
        ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] strb instruction and instruction received were different");

        HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
        ARC_String_Destroy(errorLogString);

        HERB_OSXLangInstruction_Destroy(instruction);

        binaryExecutor->currentProcess->completed = 1;

        return;
    }

    //load one byte memory from register1 address into memory at register2 address
    index = binaryExecutor->registers[instruction->bytes[1]];
    uint8_t data = HERB_OSXLangMemory_GetDataByteFromBlocks(&index, memory, blocks, blocksSize);

    index = binaryExecutor->registers[instruction->bytes[2]];
    HERB_OSXLangMemory_SetDataByteToBlocks(&index, memory, blocks, blocksSize, data);

    //update program counter
    binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] += HERB_OSXLANG_INSTRUCTION_SIZE;

    //clean up
    HERB_OSXLangInstruction_Destroy(instruction);
}

void HERB_OSXLangBinaryExecutor_ExecuteLDR(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize){
    //read in instruction
    uint32_t index = binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] + ((binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] / memory->blockSize) + 1);
    HERB_OSXLangInstruction *instruction;
    HERB_OSXLangInstruction_Create(&instruction, HERB_OSXLANG_INSTRUCTION_SIZE);

    for(uint32_t i = 0; i < HERB_OSXLANG_INSTRUCTION_SIZE; i++){
        instruction->bytes[i] = HERB_OSXLangMemory_GetDataByteFromBlocks(&index, memory, blocks, blocksSize);
    }

    //check if instruction is LDR
    if(instruction->bytes[0] != HERB_OSXLANG_INSTRUCTION_ENCODE_LDR){
        arc_errno = ARC_ERRNO_DATA;
        ARC_DEBUG_LOG(arc_errno, "HERB_OSXLangBinaryExecutor_ExecuteLDR(binaryExecutor, memory), ldr instruction is %d, instruction received was %u", HERB_OSXLANG_INSTRUCTION_ENCODE_LDR, instruction->bytes[0]);

        ARC_String *errorLogString;
        ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] ldr instruction and instruction received were different");

        HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
        ARC_String_Destroy(errorLogString);

        HERB_OSXLangInstruction_Destroy(instruction);

        binaryExecutor->currentProcess->completed = 1;

        return;
    }

    //loads in 4 bytes to register from memory address stored in another register
    index = binaryExecutor->registers[instruction->bytes[2]];
    binaryExecutor->registers[instruction->bytes[1]] = HERB_OSXLangMemory_GetDataBytesAsUint32FromBlocks(&index, memory, blocks, blocksSize);

    //update program counter
    binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] += HERB_OSXLANG_INSTRUCTION_SIZE;

    //clean up
    HERB_OSXLangInstruction_Destroy(instruction);
}

void HERB_OSXLangBinaryExecutor_ExecuteLDRB(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize){
    //read in instruction
    uint32_t index = binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] + ((binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] / memory->blockSize) + 1);
    HERB_OSXLangInstruction *instruction;
    HERB_OSXLangInstruction_Create(&instruction, HERB_OSXLANG_INSTRUCTION_SIZE);

    for(uint32_t i = 0; i < HERB_OSXLANG_INSTRUCTION_SIZE; i++){
        instruction->bytes[i] = HERB_OSXLangMemory_GetDataByteFromBlocks(&index, memory, blocks, blocksSize);
    }

    //check if instruction is LDRB
    if(instruction->bytes[0] != HERB_OSXLANG_INSTRUCTION_ENCODE_LDRB){
        arc_errno = ARC_ERRNO_DATA;
        ARC_DEBUG_LOG(arc_errno, "HERB_OSXLangBinaryExecutor_ExecuteLDRB(binaryExecutor, memory), ldrb instruction is %d, instruction received was %u", HERB_OSXLANG_INSTRUCTION_ENCODE_LDRB, instruction->bytes[0]);

        ARC_String *errorLogString;
        ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] ldrb instruction and instruction received were different");

        HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
        ARC_String_Destroy(errorLogString);

        HERB_OSXLangInstruction_Destroy(instruction);

        binaryExecutor->currentProcess->completed = 1;

        return;
    }

    //loads in byte to register from memory address stored in another register
    index = binaryExecutor->registers[instruction->bytes[2]];
    binaryExecutor->registers[instruction->bytes[1]] = HERB_OSXLangMemory_GetDataByteFromBlocks(&index, memory, blocks, blocksSize);

    //update program counter
    binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] += HERB_OSXLANG_INSTRUCTION_SIZE;

    //clean up
    HERB_OSXLangInstruction_Destroy(instruction);
}

void HERB_OSXLangBinaryExecutor_ExecuteBX(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize){
    //read in instruction
    uint32_t index = binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] + ((binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] / memory->blockSize) + 1);
    HERB_OSXLangInstruction *instruction;
    HERB_OSXLangInstruction_Create(&instruction, HERB_OSXLANG_INSTRUCTION_SIZE);

    for(uint32_t i = 0; i < HERB_OSXLANG_INSTRUCTION_SIZE; i++){
        instruction->bytes[i] = HERB_OSXLangMemory_GetDataByteFromBlocks(&index, memory, blocks, blocksSize);
    }

    //check if instruction is BX
    if(instruction->bytes[0] != HERB_OSXLANG_INSTRUCTION_ENCODE_BX){
        arc_errno = ARC_ERRNO_DATA;
        ARC_DEBUG_LOG(arc_errno, "HERB_OSXLangBinaryExecutor_ExecuteBX(binaryExecutor, memory), bx instruction is %d, instruction received was %u", HERB_OSXLANG_INSTRUCTION_ENCODE_BX, instruction->bytes[0]);

        ARC_String *errorLogString;
        ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] bx instruction and instruction received were different");

        HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
        ARC_String_Destroy(errorLogString);

        HERB_OSXLangInstruction_Destroy(instruction);

        binaryExecutor->currentProcess->completed = 1;

        return;
    }

    //branch to location stored in address. Set program counter to location stored in address 
    binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] = binaryExecutor->registers[instruction->bytes[1]];

    //clean up
    HERB_OSXLangInstruction_Destroy(instruction);
}

void HERB_OSXLangBinaryExecutor_ExecuteB(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize){
    //read in instruction
    uint32_t index = binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] + ((binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] / memory->blockSize) + 1);
    HERB_OSXLangInstruction *instruction;
    HERB_OSXLangInstruction_Create(&instruction, HERB_OSXLANG_INSTRUCTION_SIZE);

    for(uint32_t i = 0; i < HERB_OSXLANG_INSTRUCTION_SIZE; i++){
        instruction->bytes[i] = HERB_OSXLangMemory_GetDataByteFromBlocks(&index, memory, blocks, blocksSize);
    }

    //check if instruction is B
    if(instruction->bytes[0] != HERB_OSXLANG_INSTRUCTION_ENCODE_B){
        arc_errno = ARC_ERRNO_DATA;
        ARC_DEBUG_LOG(arc_errno, "HERB_OSXLangBinaryExecutor_ExecuteB(binaryExecutor, memory), b instruction is %d, instruction received was %u", HERB_OSXLANG_INSTRUCTION_ENCODE_B, instruction->bytes[0]);

        ARC_String *errorLogString;
        ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] b instruction and instruction received were different");

        HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
        ARC_String_Destroy(errorLogString);

        HERB_OSXLangInstruction_Destroy(instruction);

        binaryExecutor->currentProcess->completed = 1;

        return;
    }

    //branch to label. Set program counter to label
    binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] = 0;
    binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] |= (0xff000000 & instruction->bytes[1] << 24);
    binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] |= (0x00ff0000 & instruction->bytes[2] << 16);
    binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] |= (0x0000ff00 & instruction->bytes[3] <<  8);
    binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] |= (0x000000ff & instruction->bytes[4] <<  0);

    //clean up
    HERB_OSXLangInstruction_Destroy(instruction);
}

void HERB_OSXLangBinaryExecutor_ExecuteBNE(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize){
    //read in instruction
    uint32_t index = binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] + ((binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] / memory->blockSize) + 1);
    HERB_OSXLangInstruction *instruction;
    HERB_OSXLangInstruction_Create(&instruction, HERB_OSXLANG_INSTRUCTION_SIZE);

    for(uint32_t i = 0; i < HERB_OSXLANG_INSTRUCTION_SIZE; i++){
        instruction->bytes[i] = HERB_OSXLangMemory_GetDataByteFromBlocks(&index, memory, blocks, blocksSize);
    }

    //check if instruction is BNE
    if(instruction->bytes[0] != HERB_OSXLANG_INSTRUCTION_ENCODE_BNE){
        arc_errno = ARC_ERRNO_DATA;
        ARC_DEBUG_LOG(arc_errno, "HERB_OSXLangBinaryExecutor_ExecuteBNE(binaryExecutor, memory), bne instruction is %d, instruction received was %u", HERB_OSXLANG_INSTRUCTION_ENCODE_BNE, instruction->bytes[0]);

        ARC_String *errorLogString;
        ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] bne instruction and instruction received were different");

        HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
        ARC_String_Destroy(errorLogString);

        HERB_OSXLangInstruction_Destroy(instruction);

        binaryExecutor->currentProcess->completed = 1;

        return;
    }

    //set program counter to label if z is not 0
    if(binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_Z] != 0){
        //branch to label. Set program counter to label
        binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] = 0;
        binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] |= (0xff000000 & instruction->bytes[1] << 24);
        binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] |= (0x00ff0000 & instruction->bytes[2] << 16);
        binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] |= (0x0000ff00 & instruction->bytes[3] <<  8);
        binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] |= (0x000000ff & instruction->bytes[4] <<  0);

        HERB_OSXLangInstruction_Destroy(instruction);
        return;
    }

    //go to next instruction if jump didn't happen
    binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] += HERB_OSXLANG_INSTRUCTION_SIZE;

    //clean up
    HERB_OSXLangInstruction_Destroy(instruction);
}

void HERB_OSXLangBinaryExecutor_ExecuteBGT(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize){
    //read in instruction
    uint32_t index = binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] + ((binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] / memory->blockSize) + 1);
    HERB_OSXLangInstruction *instruction;
    HERB_OSXLangInstruction_Create(&instruction, HERB_OSXLANG_INSTRUCTION_SIZE);

    for(uint32_t i = 0; i < HERB_OSXLANG_INSTRUCTION_SIZE; i++){
        instruction->bytes[i] = HERB_OSXLangMemory_GetDataByteFromBlocks(&index, memory, blocks, blocksSize);
    }

    //check if instruction is BGT
    if(instruction->bytes[0] != HERB_OSXLANG_INSTRUCTION_ENCODE_BGT){
        arc_errno = ARC_ERRNO_DATA;
        ARC_DEBUG_LOG(arc_errno, "HERB_OSXLangBinaryExecutor_ExecuteBGT(binaryExecutor, memory), bgt instruction is %d, instruction received was %u", HERB_OSXLANG_INSTRUCTION_ENCODE_BGT, instruction->bytes[0]);

        ARC_String *errorLogString;
        ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] bgt instruction and instruction received were different");

        HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
        ARC_String_Destroy(errorLogString);

        HERB_OSXLangInstruction_Destroy(instruction);

        binaryExecutor->currentProcess->completed = 1;

        return;
    }

    //set program counter to label if z is greater than to 0
    if(binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_Z] > 0){
        //branch to label. Set program counter to label
        binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] = 0;
        binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] |= (0xff000000 & instruction->bytes[1] << 24);
        binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] |= (0x00ff0000 & instruction->bytes[2] << 16);
        binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] |= (0x0000ff00 & instruction->bytes[3] <<  8);
        binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] |= (0x000000ff & instruction->bytes[4] <<  0);

        HERB_OSXLangInstruction_Destroy(instruction);
        return;
    }

    //go to next instruction if jump didn't happen
    binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] += HERB_OSXLANG_INSTRUCTION_SIZE;

    //clean up
    HERB_OSXLangInstruction_Destroy(instruction);
}

void HERB_OSXLangBinaryExecutor_ExecuteBLT(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize){
    //read in instruction
    uint32_t index = binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] + ((binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] / memory->blockSize) + 1);
    HERB_OSXLangInstruction *instruction;
    HERB_OSXLangInstruction_Create(&instruction, HERB_OSXLANG_INSTRUCTION_SIZE);

    for(uint32_t i = 0; i < HERB_OSXLANG_INSTRUCTION_SIZE; i++){
        instruction->bytes[i] = HERB_OSXLangMemory_GetDataByteFromBlocks(&index, memory, blocks, blocksSize);
    }

    //check if instruction is BLT
    if(instruction->bytes[0] != HERB_OSXLANG_INSTRUCTION_ENCODE_BLT){
        arc_errno = ARC_ERRNO_DATA;
        ARC_DEBUG_LOG(arc_errno, "HERB_OSXLangBinaryExecutor_ExecuteBLT(binaryExecutor, memory), blt instruction is %d, instruction received was %u", HERB_OSXLANG_INSTRUCTION_ENCODE_BLT, instruction->bytes[0]);

        ARC_String *errorLogString;
        ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] blt instruction and instruction received were different");

        HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
        ARC_String_Destroy(errorLogString);

        HERB_OSXLangInstruction_Destroy(instruction);

        binaryExecutor->currentProcess->completed = 1;

        return;
    }

    //set program counter to label if z is less than to 0
    if(binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_Z] < 0){
        //branch to label. Set program counter to label
        binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] = 0;
        binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] |= (0xff000000 & instruction->bytes[1] << 24);
        binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] |= (0x00ff0000 & instruction->bytes[2] << 16);
        binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] |= (0x0000ff00 & instruction->bytes[3] <<  8);
        binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] |= (0x000000ff & instruction->bytes[4] <<  0);

        HERB_OSXLangInstruction_Destroy(instruction);
        return;
    }

    //go to next instruction if jump didn't happen
    binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] += HERB_OSXLANG_INSTRUCTION_SIZE;

    //clean up
    HERB_OSXLangInstruction_Destroy(instruction);
}

void HERB_OSXLangBinaryExecutor_ExecuteBEQ(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize){
    //read in instruction
    uint32_t index = binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] + ((binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] / memory->blockSize) + 1);
    HERB_OSXLangInstruction *instruction;
    HERB_OSXLangInstruction_Create(&instruction, HERB_OSXLANG_INSTRUCTION_SIZE);

    for(uint32_t i = 0; i < HERB_OSXLANG_INSTRUCTION_SIZE; i++){
        instruction->bytes[i] = HERB_OSXLangMemory_GetDataByteFromBlocks(&index, memory, blocks, blocksSize);
    }

    //check if instruction is BEQ
    if(instruction->bytes[0] != HERB_OSXLANG_INSTRUCTION_ENCODE_BEQ){
        arc_errno = ARC_ERRNO_DATA;
        ARC_DEBUG_LOG(arc_errno, "HERB_OSXLangBinaryExecutor_ExecuteBEQ(binaryExecutor, memory), beq instruction is %d, instruction received was %u", HERB_OSXLANG_INSTRUCTION_ENCODE_BEQ, instruction->bytes[0]);

        ARC_String *errorLogString;
        ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] beq instruction and instruction received were different");

        HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
        ARC_String_Destroy(errorLogString);

        HERB_OSXLangInstruction_Destroy(instruction);

        binaryExecutor->currentProcess->completed = 1;

        return;
    }

    //set program counter to label if z is equal to 0
    if(binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_Z] == 0){
        //branch to label. Set program counter to label
        binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] = 0;
        binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] |= (0xff000000 & instruction->bytes[1] << 24);
        binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] |= (0x00ff0000 & instruction->bytes[2] << 16);
        binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] |= (0x0000ff00 & instruction->bytes[3] <<  8);
        binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] |= (0x000000ff & instruction->bytes[4] <<  0);

        HERB_OSXLangInstruction_Destroy(instruction);
        return;
    }

    //go to next instruction if jump didn't happen
    binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] += HERB_OSXLANG_INSTRUCTION_SIZE;

    //clean up
    HERB_OSXLangInstruction_Destroy(instruction);
}

void HERB_OSXLangBinaryExecutor_ExecuteCMP(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize){
    //read in instruction
    uint32_t index = binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] + ((binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] / memory->blockSize) + 1);
    HERB_OSXLangInstruction *instruction;
    HERB_OSXLangInstruction_Create(&instruction, HERB_OSXLANG_INSTRUCTION_SIZE);

    for(uint32_t i = 0; i < HERB_OSXLANG_INSTRUCTION_SIZE; i++){
        instruction->bytes[i] = HERB_OSXLangMemory_GetDataByteFromBlocks(&index, memory, blocks, blocksSize);
    }

    //check if instruction is CMP
    if(instruction->bytes[0] != HERB_OSXLANG_INSTRUCTION_ENCODE_CMP){
        arc_errno = ARC_ERRNO_DATA;
        ARC_DEBUG_LOG(arc_errno, "HERB_OSXLangBinaryExecutor_ExecuteCMP(binaryExecutor, memory), cmp instruction is %d, instruction received was %u", HERB_OSXLANG_INSTRUCTION_ENCODE_CMP, instruction->bytes[0]);

        ARC_String *errorLogString;
        ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] cmp instruction and instruction received were different");

        HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
        ARC_String_Destroy(errorLogString);

        HERB_OSXLangInstruction_Destroy(instruction);

        binaryExecutor->currentProcess->completed = 1;

        return;
    }

    //add Z = <reg1> - <reg2>
    binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_Z] = binaryExecutor->registers[instruction->bytes[1]] - binaryExecutor->registers[instruction->bytes[2]];

    binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] += HERB_OSXLANG_INSTRUCTION_SIZE;

    //clean up
    HERB_OSXLangInstruction_Destroy(instruction);
}

void HERB_OSXLangBinaryExecutor_ExecuteAND(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize){
    //read in instruction
    uint32_t index = binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] + ((binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] / memory->blockSize) + 1);
    HERB_OSXLangInstruction *instruction;
    HERB_OSXLangInstruction_Create(&instruction, HERB_OSXLANG_INSTRUCTION_SIZE);

    for(uint32_t i = 0; i < HERB_OSXLANG_INSTRUCTION_SIZE; i++){
        instruction->bytes[i] = HERB_OSXLangMemory_GetDataByteFromBlocks(&index, memory, blocks, blocksSize);
    }

    //check if instruction is AND
    if(instruction->bytes[0] != HERB_OSXLANG_INSTRUCTION_ENCODE_AND){
        arc_errno = ARC_ERRNO_DATA;
        ARC_DEBUG_LOG(arc_errno, "HERB_OSXLangBinaryExecutor_ExecuteAND(binaryExecutor, memory), and instruction is %d, instruction received was %u", HERB_OSXLANG_INSTRUCTION_ENCODE_AND, instruction->bytes[0]);

        ARC_String *errorLogString;
        ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] and instruction and instruction received were different");

        HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
        ARC_String_Destroy(errorLogString);

        HERB_OSXLangInstruction_Destroy(instruction);

        binaryExecutor->currentProcess->completed = 1;

        return;
    }

    //add Z = <reg1> & <reg2>
    binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_Z] = binaryExecutor->registers[instruction->bytes[1]] & binaryExecutor->registers[instruction->bytes[2]];

    binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] += HERB_OSXLANG_INSTRUCTION_SIZE;

    //clean up
    HERB_OSXLangInstruction_Destroy(instruction);
}

void HERB_OSXLangBinaryExecutor_ExecuteORR(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize){
    //read in instruction
    uint32_t index = binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] + ((binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] / memory->blockSize) + 1);
    HERB_OSXLangInstruction *instruction;
    HERB_OSXLangInstruction_Create(&instruction, HERB_OSXLANG_INSTRUCTION_SIZE);

    for(uint32_t i = 0; i < HERB_OSXLANG_INSTRUCTION_SIZE; i++){
        instruction->bytes[i] = HERB_OSXLangMemory_GetDataByteFromBlocks(&index, memory, blocks, blocksSize);
    }

    //check if instruction is ORR
    if(instruction->bytes[0] != HERB_OSXLANG_INSTRUCTION_ENCODE_ORR){
        arc_errno = ARC_ERRNO_DATA;
        ARC_DEBUG_LOG(arc_errno, "HERB_OSXLangBinaryExecutor_ExecuteORR(binaryExecutor, memory), orr instruction is %d, instruction received was %u", HERB_OSXLANG_INSTRUCTION_ENCODE_ORR, instruction->bytes[0]);

        ARC_String *errorLogString;
        ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] orr instruction and instruction received were different");

        HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
        ARC_String_Destroy(errorLogString);

        HERB_OSXLangInstruction_Destroy(instruction);

        binaryExecutor->currentProcess->completed = 1;

        return;
    }

    //add Z = <reg1> | <reg2>
    binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_Z] = binaryExecutor->registers[instruction->bytes[1]] | binaryExecutor->registers[instruction->bytes[2]];

    binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] += HERB_OSXLANG_INSTRUCTION_SIZE;

    //clean up
    HERB_OSXLangInstruction_Destroy(instruction);
}

void HERB_OSXLangBinaryExecutor_ExecuteEOR(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize){
    //read in instruction
    uint32_t index = binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] + ((binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] / memory->blockSize) + 1);
    HERB_OSXLangInstruction *instruction;
    HERB_OSXLangInstruction_Create(&instruction, HERB_OSXLANG_INSTRUCTION_SIZE);

    for(uint32_t i = 0; i < HERB_OSXLANG_INSTRUCTION_SIZE; i++){
        instruction->bytes[i] = HERB_OSXLangMemory_GetDataByteFromBlocks(&index, memory, blocks, blocksSize);
    }

    //check if instruction is EOR
    if(instruction->bytes[0] != HERB_OSXLANG_INSTRUCTION_ENCODE_EOR){
        arc_errno = ARC_ERRNO_DATA;
        ARC_DEBUG_LOG(arc_errno, "HERB_OSXLangBinaryExecutor_ExecuteEOR(binaryExecutor, memory), eor instruction is %d, instruction received was %u", HERB_OSXLANG_INSTRUCTION_ENCODE_EOR, instruction->bytes[0]);

        ARC_String *errorLogString;
        ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] eor instruction and instruction received were different");

        HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
        ARC_String_Destroy(errorLogString);

        HERB_OSXLangInstruction_Destroy(instruction);

        binaryExecutor->currentProcess->completed = 1;

        return;
    }

    //add Z = <reg1> ^ <reg2>
    binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_Z] = binaryExecutor->registers[instruction->bytes[1]] ^ binaryExecutor->registers[instruction->bytes[2]];

    binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] += HERB_OSXLANG_INSTRUCTION_SIZE;

    //clean up
    HERB_OSXLangInstruction_Destroy(instruction);
}

void HERB_OSXLangBinaryExecutor_ExecuteADD(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize){
    //read in instruction
    uint32_t index = binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] + ((binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] / memory->blockSize) + 1);
    HERB_OSXLangInstruction *instruction;
    HERB_OSXLangInstruction_Create(&instruction, HERB_OSXLANG_INSTRUCTION_SIZE);

    for(uint32_t i = 0; i < HERB_OSXLANG_INSTRUCTION_SIZE; i++){
        instruction->bytes[i] = HERB_OSXLangMemory_GetDataByteFromBlocks(&index, memory, blocks, blocksSize);
    }

    //check if instruction is ADD
    if(instruction->bytes[0] != HERB_OSXLANG_INSTRUCTION_ENCODE_ADD){
        arc_errno = ARC_ERRNO_DATA;
        ARC_DEBUG_LOG(arc_errno, "HERB_OSXLangBinaryExecutor_ExecuteADD(binaryExecutor, memory), add instruction is %d, instruction received was %u", HERB_OSXLANG_INSTRUCTION_ENCODE_ADD, instruction->bytes[0]);

        ARC_String *errorLogString;
        ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] add instruction and instruction received were different");

        HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
        ARC_String_Destroy(errorLogString);

        HERB_OSXLangInstruction_Destroy(instruction);

        binaryExecutor->currentProcess->completed = 1;

        return;
    }

    //add <reg1> = <reg2> + <reg3>
    binaryExecutor->registers[instruction->bytes[1]] = binaryExecutor->registers[instruction->bytes[2]] + binaryExecutor->registers[instruction->bytes[3]];

    //update program counter
    binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] += HERB_OSXLANG_INSTRUCTION_SIZE;

    //clean up
    HERB_OSXLangInstruction_Destroy(instruction);
}

void HERB_OSXLangBinaryExecutor_ExecuteSUB(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize){
    //read in instruction
    uint32_t index = binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] + ((binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] / memory->blockSize) + 1);
    HERB_OSXLangInstruction *instruction;
    HERB_OSXLangInstruction_Create(&instruction, HERB_OSXLANG_INSTRUCTION_SIZE);

    for(uint32_t i = 0; i < HERB_OSXLANG_INSTRUCTION_SIZE; i++){
        instruction->bytes[i] = HERB_OSXLangMemory_GetDataByteFromBlocks(&index, memory, blocks, blocksSize);
    }

    //check if instruction is SUB
    if(instruction->bytes[0] != HERB_OSXLANG_INSTRUCTION_ENCODE_SUB){
        arc_errno = ARC_ERRNO_DATA;
        ARC_DEBUG_LOG(arc_errno, "HERB_OSXLangBinaryExecutor_ExecuteSUB(binaryExecutor, memory), sub instruction is %d, instruction received was %u", HERB_OSXLANG_INSTRUCTION_ENCODE_SUB, instruction->bytes[0]);

        ARC_String *errorLogString;
        ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] sub instruction and instruction received were different");

        HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
        ARC_String_Destroy(errorLogString);

        HERB_OSXLangInstruction_Destroy(instruction);

        binaryExecutor->currentProcess->completed = 1;

        return;
    }

    //sub <reg1> = <reg2> - <reg3>
    binaryExecutor->registers[instruction->bytes[1]] = binaryExecutor->registers[instruction->bytes[2]] - binaryExecutor->registers[instruction->bytes[3]];

    //update program counter
    binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] += HERB_OSXLANG_INSTRUCTION_SIZE;

    //clean up
    HERB_OSXLangInstruction_Destroy(instruction);
}

void HERB_OSXLangBinaryExecutor_ExecuteMUL(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize){
    //read in instruction
    uint32_t index = binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] + ((binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] / memory->blockSize) + 1);
    HERB_OSXLangInstruction *instruction;
    HERB_OSXLangInstruction_Create(&instruction, HERB_OSXLANG_INSTRUCTION_SIZE);

    for(uint32_t i = 0; i < HERB_OSXLANG_INSTRUCTION_SIZE; i++){
        instruction->bytes[i] = HERB_OSXLangMemory_GetDataByteFromBlocks(&index, memory, blocks, blocksSize);
    }

    //check if instruction is MUL
    if(instruction->bytes[0] != HERB_OSXLANG_INSTRUCTION_ENCODE_MUL){
        arc_errno = ARC_ERRNO_DATA;
        ARC_DEBUG_LOG(arc_errno, "HERB_OSXLangBinaryExecutor_ExecuteMUL(binaryExecutor, memory), mul instruction is %d, instruction received was %u", HERB_OSXLANG_INSTRUCTION_ENCODE_MUL, instruction->bytes[0]);

        ARC_String *errorLogString;
        ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] mul instruction and instruction received were different");

        HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
        ARC_String_Destroy(errorLogString);

        HERB_OSXLangInstruction_Destroy(instruction);

        binaryExecutor->currentProcess->completed = 1;

        return;
    }

    //mul <reg1> = <reg2> * <reg3>
    binaryExecutor->registers[instruction->bytes[1]] = binaryExecutor->registers[instruction->bytes[2]] * binaryExecutor->registers[instruction->bytes[3]];

    //update program counter
    binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] += HERB_OSXLANG_INSTRUCTION_SIZE;

    //clean up
    HERB_OSXLangInstruction_Destroy(instruction);
}

void HERB_OSXLangBinaryExecutor_ExecuteDIV(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize){
    //read in instruction
    uint32_t index = binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] + ((binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] / memory->blockSize) + 1);
    HERB_OSXLangInstruction *instruction;
    HERB_OSXLangInstruction_Create(&instruction, HERB_OSXLANG_INSTRUCTION_SIZE);

    for(uint32_t i = 0; i < HERB_OSXLANG_INSTRUCTION_SIZE; i++){
        instruction->bytes[i] = HERB_OSXLangMemory_GetDataByteFromBlocks(&index, memory, blocks, blocksSize);
    }

    //check if instruction is DIV
    if(instruction->bytes[0] != HERB_OSXLANG_INSTRUCTION_ENCODE_DIV){
        arc_errno = ARC_ERRNO_DATA;
        ARC_DEBUG_LOG(arc_errno, "HERB_OSXLangBinaryExecutor_ExecuteDIV(binaryExecutor, memory), div instruction is %d, instruction received was %u", HERB_OSXLANG_INSTRUCTION_ENCODE_DIV, instruction->bytes[0]);

        ARC_String *errorLogString;
        ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] div instruction and instruction received were different");

        HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
        ARC_String_Destroy(errorLogString);

        HERB_OSXLangInstruction_Destroy(instruction);

        binaryExecutor->currentProcess->completed = 1;

        return;
    }

    //throw error if tyring to divide by 0
    if(binaryExecutor->registers[instruction->bytes[3]] == 0){
        arc_errno = ARC_ERRNO_DATA;
        ARC_DEBUG_ERR("HERB_OSXLangBinaryExecutor_ExecuteMUL(binaryExecutor, memory), cannot divide by zero");

        ARC_String *errorLogString;
        ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] cannot divide by 0");

        HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
        ARC_String_Destroy(errorLogString);

        HERB_OSXLangInstruction_Destroy(instruction);
        return;
    }

    //div <reg1> = <reg2> / <reg3>
    binaryExecutor->registers[instruction->bytes[1]] = binaryExecutor->registers[instruction->bytes[2]] / binaryExecutor->registers[instruction->bytes[3]];

    //update program counter
    binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] += HERB_OSXLANG_INSTRUCTION_SIZE;

    //clean up
    HERB_OSXLangInstruction_Destroy(instruction);
}

void HERB_OSXLangBinaryExecutor_ExecuteSWI(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize){
    //read in instruction
    uint32_t index = binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] + ((binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] / memory->blockSize) + 1);
    HERB_OSXLangInstruction *instruction;
    HERB_OSXLangInstruction_Create(&instruction, HERB_OSXLANG_INSTRUCTION_SIZE);

    for(uint32_t i = 0; i < HERB_OSXLANG_INSTRUCTION_SIZE; i++){
        instruction->bytes[i] = HERB_OSXLangMemory_GetDataByteFromBlocks(&index, memory, blocks, blocksSize);
    }

    //check if instruction is SWI
    if(instruction->bytes[0] != HERB_OSXLANG_INSTRUCTION_ENCODE_SWI){
        arc_errno = ARC_ERRNO_DATA;
        ARC_DEBUG_LOG(arc_errno, "HERB_OSXLangBinaryExecutor_ExecuteSWI(binaryExecutor, memory), swi instruction is %d, instruction received was %u", HERB_OSXLANG_INSTRUCTION_ENCODE_SWI, instruction->bytes[0]);

        ARC_String *errorLogString;
        ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] swi instruction and instruction received were different");

        HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
        ARC_String_Destroy(errorLogString);

        HERB_OSXLangInstruction_Destroy(instruction);

        binaryExecutor->currentProcess->completed = 1;

        return;
    }

    uint32_t command = 0;
    command |= (0xff000000 & instruction->bytes[1] << 24);
    command |= (0x00ff0000 & instruction->bytes[2] << 16);
    command |= (0x0000ff00 & instruction->bytes[3] <<  8);
    command |= (0x000000ff & instruction->bytes[4] <<  0);

    switch(command){
        case HERB_OSXLANG_INSTRUCTION_SWI_COMMAND_EXIT:
            binaryExecutor->currentProcess->completed = 1;
            break;

        case HERB_OSXLANG_INSTRUCTION_SWI_COMMAND_INPUT_BYTE:
        case HERB_OSXLANG_INSTRUCTION_SWI_COMMAND_INPUT_STRING:
        case HERB_OSXLANG_INSTRUCTION_SWI_COMMAND_OUTPUT_BYTE:
        case HERB_OSXLANG_INSTRUCTION_SWI_COMMAND_OUTPUT_STRING:
        case HERB_OSXLANG_INSTRUCTION_SWI_COMMAND_SHARED_MEMORY_CREATE:
        case HERB_OSXLANG_INSTRUCTION_SWI_COMMAND_SHARED_MEMORY_DESTROY:
        case HERB_OSXLANG_INSTRUCTION_SWI_COMMAND_SHARED_MEMORY_SET_BYTE:
        case HERB_OSXLANG_INSTRUCTION_SWI_COMMAND_SHARED_MEMORY_GET_BYTE:
            binaryExecutor->currentProcess->mode = 1;
            break;

        //the default option is to error because the command was not found
        default:
            arc_errno = ARC_ERRNO_DATA;
            ARC_DEBUG_LOG(arc_errno, "HERB_OSXLangBinaryExecutor_ExecuteSWI(binaryExecutor, memory), swi does not recognize command: %u", command);

            ARC_String *errorLogString;
            ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] swi command not recognized");

            HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
            ARC_String_Destroy(errorLogString);

            HERB_OSXLangInstruction_Destroy(instruction);
    }

    //NOTE: don't update program counter so that we can pull the command easy when in the waiting queue

    //clean up
    HERB_OSXLangInstruction_Destroy(instruction);
}

void HERB_OSXLangBinaryExecutor_ExecuteBL(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize){
    //read in instruction
    uint32_t index = binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] + ((binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] / memory->blockSize) + 1);
    HERB_OSXLangInstruction *instruction;
    HERB_OSXLangInstruction_Create(&instruction, HERB_OSXLANG_INSTRUCTION_SIZE);

    for(uint32_t i = 0; i < HERB_OSXLANG_INSTRUCTION_SIZE; i++){
        instruction->bytes[i] = HERB_OSXLangMemory_GetDataByteFromBlocks(&index, memory, blocks, blocksSize);
    }

    //check if instruction is BL
    if(instruction->bytes[0] != HERB_OSXLANG_INSTRUCTION_ENCODE_BL){
        arc_errno = ARC_ERRNO_DATA;
        ARC_DEBUG_LOG(arc_errno, "HERB_OSXLangBinaryExecutor_ExecuteBL(binaryExecutor, memory), bl instruction is %d, instruction received was %u", HERB_OSXLANG_INSTRUCTION_ENCODE_BL, instruction->bytes[0]);

        ARC_String *errorLogString;
        ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] bl instruction and instruction received were different");

        HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
        ARC_String_Destroy(errorLogString);

        HERB_OSXLangInstruction_Destroy(instruction);

        binaryExecutor->currentProcess->completed = 1;

        return;
    }

    //set R5 to program counter + 6
    binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_R5] = binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] + 6;
    
    //branch to label. Set program counter to label
    binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] = 0;
    binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] |= (0xff000000 & instruction->bytes[1] << 24);
    binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] |= (0x00ff0000 & instruction->bytes[2] << 16);
    binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] |= (0x0000ff00 & instruction->bytes[3] <<  8);
    binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] |= (0x000000ff & instruction->bytes[4] <<  0);

    //clean up
    HERB_OSXLangInstruction_Destroy(instruction);
}

void HERB_OSXLangBinaryExecutor_ExecuteMVI(HERB_OSXLangBinaryExecutor *binaryExecutor, HERB_OSXLangMemory *memory, uint32_t *blocks, uint32_t blocksSize){
    //read in instruction
    uint32_t index = binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] + ((binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] / memory->blockSize) + 1);
    HERB_OSXLangInstruction *instruction;
    HERB_OSXLangInstruction_Create(&instruction, HERB_OSXLANG_INSTRUCTION_SIZE);

    for(uint32_t i = 0; i < HERB_OSXLANG_INSTRUCTION_SIZE; i++){
        instruction->bytes[i] = HERB_OSXLangMemory_GetDataByteFromBlocks(&index, memory, blocks, blocksSize);
    }

    //check if instruction is MVI
    if(instruction->bytes[0] != HERB_OSXLANG_INSTRUCTION_ENCODE_MVI){
        arc_errno = ARC_ERRNO_DATA;
        ARC_DEBUG_LOG(arc_errno, "HERB_OSXLangBinaryExecutor_ExecuteMVI(binaryExecutor, memory), mvi instruction is %d, instruction received was %u", HERB_OSXLANG_INSTRUCTION_ENCODE_ADD, instruction->bytes[0]);

        ARC_String *errorLogString;
        ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] mvi instruction and instruction received were different");

        HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
        ARC_String_Destroy(errorLogString);

        HERB_OSXLangInstruction_Destroy(instruction);

        binaryExecutor->currentProcess->completed = 1;

        return;
    }

    //mvi reg = immediage_value
    binaryExecutor->registers[instruction->bytes[1]] = 0;
    binaryExecutor->registers[instruction->bytes[1]] |= (0xff000000 & instruction->bytes[2] << 24);
    binaryExecutor->registers[instruction->bytes[1]] |= (0x00ff0000 & instruction->bytes[3] << 16);
    binaryExecutor->registers[instruction->bytes[1]] |= (0x0000ff00 & instruction->bytes[4] <<  8);
    binaryExecutor->registers[instruction->bytes[1]] |= (0x000000ff & instruction->bytes[5] <<  0);

    //update program counter
    binaryExecutor->registers[HERB_OSXLANG_REGISTER_ENCODE_PC] += HERB_OSXLANG_INSTRUCTION_SIZE;

    //clean up
    HERB_OSXLangInstruction_Destroy(instruction);
}