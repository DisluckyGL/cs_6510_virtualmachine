#ifndef HERB_OSXLANG_SHARED_MEMORY_H_
#define HERB_OSXLANG_SHARED_MEMORY_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <arc/std/bool.h>
#include "memory.h"

/**
 * @brief reserves a section of HERB_OSXLangMemory for shared memory
 *
 * @param memory memory to try to reserve
 * @param id     the id of the shared memory
 * @param size   size of shared memory to reserve
 *
 * @return ARC_True if creation was successful, ARC_False on fail
*/
ARC_Bool HERB_OSXLangMemory_ReserveSharedSection(HERB_OSXLangMemory *memory, uint32_t id, uint64_t size);

/**
 * @brief clears a reserved section of HERB_OSXLangMemory
 *
 * @param memory memory to clear
 * @param size   id of reserved memory to clear
*/
void HERB_OSXLangMemory_ClearSharedSection(HERB_OSXLangMemory *memory, uint32_t id);

/**
 * @brief gets blocks from memory shared memory with id
 *
 * @note blocks will be NULL on fail, and blocksSize will be 0 on fail
 *
 * @param id         the id of shared memory to get blcoks from
 * @param blocks     the variable to store blocks in
 * @param blocksSize the variable to store the size of blocks into
 * @param memory     memory to get the blocks from
 * @param index      the index at where to get the shared blocks from, should usually be 0, will be incremented when run
*/
void HERB_OSXLangMemory_GetSharedBlocksFromId(uint32_t id, uint32_t **blocks, uint32_t *blocksSize, HERB_OSXLangMemory *memory, uint32_t *index);

#ifdef __cplusplus
}
#endif

#endif // !HERB_OSXLANG_SHARED_MEMORY_H_
