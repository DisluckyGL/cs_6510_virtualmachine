#ifndef HERB_OSXLANG_INSTRUCTION_H_
#define HERB_OSXLANG_INSTRUCTION_H_

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief the size in bytes of an instruction within the osx lang
*/
#define HERB_OSXLANG_INSTRUCTION_SIZE 6

/**
 * @brief HERB_OSXLang_Instruction type creates an array of the size of each instruction
*/
typedef struct HERB_OSXLangInstruction {
    uint8_t *bytes;
    uint64_t bytesSize;
} HERB_OSXLangInstruction;

/**
 * @brief creates HERB_OSXLangInstruction type
 *
 * @param instruction HERB_OSXLangInstruction to create
 * @param bypeSize    number of bytes within an instruction
*/
void HERB_OSXLangInstruction_Create(HERB_OSXLangInstruction **instruction, uint64_t byteSize);

/**
 * @brief destroys HERB_OSXLangInstruction type
 * 
 * @param instruction instruction that will be destroyed
*/
void HERB_OSXLangInstruction_Destroy(HERB_OSXLangInstruction *instruction);

/**
 * @brief prints contents of an instruction
 * 
 * @param instruction HERB_OSXLangInstruction to print
*/
void HERB_OSXLangInstruction_Print(HERB_OSXLangInstruction *instruction);

/**
 * @brief prints contents of an instruction with a new line
 * 
 * @param instruction HERB_OSXLangInstruction to print
*/
void HERB_OSXLangInstruction_PrintWithNewLine(HERB_OSXLangInstruction *instruction);

/**
 * @brief defines instructions for OSX so they can be changed easily
*/
#define HERB_OSXLANG_INSTRUCTION_STRING_ADR "ADR"
#define HERB_OSXLANG_INSTRUCTION_ENCODE_ADR 0

#define HERB_OSXLANG_INSTRUCTION_STRING_MOV "MOV"
#define HERB_OSXLANG_INSTRUCTION_ENCODE_MOV 1

#define HERB_OSXLANG_INSTRUCTION_STRING_STR "STR"
#define HERB_OSXLANG_INSTRUCTION_ENCODE_STR 2

#define HERB_OSXLANG_INSTRUCTION_STRING_STRB "STRB"
#define HERB_OSXLANG_INSTRUCTION_ENCODE_STRB 3

#define HERB_OSXLANG_INSTRUCTION_STRING_LDR "LDR"
#define HERB_OSXLANG_INSTRUCTION_ENCODE_LDR 4

#define HERB_OSXLANG_INSTRUCTION_STRING_LDRB "LDRB"
#define HERB_OSXLANG_INSTRUCTION_ENCODE_LDRB 5

#define HERB_OSXLANG_INSTRUCTION_STRING_BX "BX"
#define HERB_OSXLANG_INSTRUCTION_ENCODE_BX 6

#define HERB_OSXLANG_INSTRUCTION_STRING_B "B"
#define HERB_OSXLANG_INSTRUCTION_ENCODE_B 7

#define HERB_OSXLANG_INSTRUCTION_STRING_BNE "BNE"
#define HERB_OSXLANG_INSTRUCTION_ENCODE_BNE 8

#define HERB_OSXLANG_INSTRUCTION_STRING_BGT "BGT"
#define HERB_OSXLANG_INSTRUCTION_ENCODE_BGT 9

#define HERB_OSXLANG_INSTRUCTION_STRING_BLT "BLT"
#define HERB_OSXLANG_INSTRUCTION_ENCODE_BLT 10

#define HERB_OSXLANG_INSTRUCTION_STRING_BEQ "BEQ"
#define HERB_OSXLANG_INSTRUCTION_ENCODE_BEQ 11

#define HERB_OSXLANG_INSTRUCTION_STRING_CMP "CMP"
#define HERB_OSXLANG_INSTRUCTION_ENCODE_CMP 12

#define HERB_OSXLANG_INSTRUCTION_STRING_AND "AND"
#define HERB_OSXLANG_INSTRUCTION_ENCODE_AND 13

#define HERB_OSXLANG_INSTRUCTION_STRING_ORR "ORR"
#define HERB_OSXLANG_INSTRUCTION_ENCODE_ORR 14

#define HERB_OSXLANG_INSTRUCTION_STRING_EOR "EOR"
#define HERB_OSXLANG_INSTRUCTION_ENCODE_EOR 15

#define HERB_OSXLANG_INSTRUCTION_STRING_ADD "ADD"
#define HERB_OSXLANG_INSTRUCTION_ENCODE_ADD 16

#define HERB_OSXLANG_INSTRUCTION_STRING_SUB "SUB"
#define HERB_OSXLANG_INSTRUCTION_ENCODE_SUB 17

#define HERB_OSXLANG_INSTRUCTION_STRING_MUL "MUL"
#define HERB_OSXLANG_INSTRUCTION_ENCODE_MUL 18

#define HERB_OSXLANG_INSTRUCTION_STRING_DIV "DIV"
#define HERB_OSXLANG_INSTRUCTION_ENCODE_DIV 19

#define HERB_OSXLANG_INSTRUCTION_STRING_SWI "SWI"
#define HERB_OSXLANG_INSTRUCTION_ENCODE_SWI 20

#define HERB_OSXLANG_INSTRUCTION_STRING_BL "BL"
#define HERB_OSXLANG_INSTRUCTION_ENCODE_BL 21

#define HERB_OSXLANG_INSTRUCTION_STRING_MVI "MVI"
#define HERB_OSXLANG_INSTRUCTION_ENCODE_MVI 22

/**
 * @brief defines the max rgister size
 *
 * @note can be used to create an array of registers
*/
#define HERB_OSXLANG_INSTRUCTION_MAX_ENCODE 23

/**
 * @brief defines directives for OSX so they can be changed easily
*/
#define HERB_OSXLANG_DIRECTIVE_STRING_WORD   ".WORD"
#define HERB_OSXLANG_DIRECTIVE_STRING_BYTE   ".BYTE"
#define HERB_OSXLANG_DIRECTIVE_STRING_SPACE  ".SPACE"
#define HERB_OSXLANG_DIRECTIVE_STRING_STRING ".STRING"

/**
 * @brief defines the commands that swi does
*/
#define HERB_OSXLANG_INSTRUCTION_SWI_COMMAND_EXIT                   0
#define HERB_OSXLANG_INSTRUCTION_SWI_COMMAND_OUTPUT_BYTE            1
#define HERB_OSXLANG_INSTRUCTION_SWI_COMMAND_OUTPUT_STRING          2
#define HERB_OSXLANG_INSTRUCTION_SWI_COMMAND_INPUT_BYTE             3
#define HERB_OSXLANG_INSTRUCTION_SWI_COMMAND_INPUT_STRING           4
#define HERB_OSXLANG_INSTRUCTION_SWI_COMMAND_SHARED_MEMORY_CREATE   5
#define HERB_OSXLANG_INSTRUCTION_SWI_COMMAND_SHARED_MEMORY_DESTROY  6
#define HERB_OSXLANG_INSTRUCTION_SWI_COMMAND_SHARED_MEMORY_SET_BYTE 7
#define HERB_OSXLANG_INSTRUCTION_SWI_COMMAND_SHARED_MEMORY_GET_BYTE 8
#define HERB_OSXLANG_INSTRUCTION_SWI_COMMAND_GET_SEMIPHORE          9
#define HERB_OSXLANG_INSTRUCTION_SWI_COMMAND_SET_SEMIPHORE          10

#ifdef __cplusplus
}
#endif

#endif // !HERB_OSXLANG_INSTRUCTION_H_
