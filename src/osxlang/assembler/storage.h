#ifndef HERB_OSXLANG_ASSEMBLER_STORAGE_H_
#define HERB_OSXLANG_ASSEMBLER_STORAGE_H_

#include "../instruction.h"
#include <arc/std/hashtable.h>
#include <arc/std/string.h>
#include <arc/std/vector.h>

#ifdef __cplusplus
extern "C" {
#endif

#define HERB_OSXLANG_ASSEMBLER_STORAGE_BUCKET_SIZE 0x20

/**
 * @brief
*/
typedef struct HERB_OSXLangAssemblerStorage HERB_OSXLangAssemblerStorage;

/**
 * @brief creates HERB_OSXLangAssemblerStorage type
 *
 * @param storage HERB_OSXLangAssemblerStorage to create
*/
void HERB_OSXLangAssemblerStorage_Create(HERB_OSXLangAssemblerStorage **storage);

/**
 * @brief destroys HERB_OSXLangAssemblerStorage type
 * 
 * @param storage storage that will be destroyed
*/
void HERB_OSXLangAssemblerStorage_Destroy(HERB_OSXLangAssemblerStorage *storage);

/**
 * @brief writes the Assembler storage to a file
 *
 * @param storage storage that will be written to a file
 * @param path    path of file to write to
*/
void HERB_OSXLangAssemblerStorage_WriteToFile(HERB_OSXLangAssemblerStorage *storage, ARC_String *path);

/**
 * @brief adds an instruction to a HERB_OSXLangAssemblerStorage type
 * 
 * @param storage     storage to add instrcution to
 * @param instruction instruction to add to storage
*/
void HERB_OSXLangAssemblerStorage_AddInstruction(HERB_OSXLangAssemblerStorage *storage, HERB_OSXLangInstruction *instruction);

/**
 * @brief prints the instructions stored in a HERB_OSXLangAssemblerStorage
 *
 * @param storage storage to print from
*/
void HERB_OSXLangAssemblerStorage_PrintInstructions(HERB_OSXLangAssemblerStorage *storage);

/**
 * @brief adds a label to the HERB_OSXLangAssemblerStorage
 *
 * @param storage storage to add label to
 * @param label   label to add to storage
 * @param line    location where the label is written to
*/
void HERB_OSXLangAssemblerStorage_AddLabel(HERB_OSXLangAssemblerStorage *storage, ARC_String *label, uint64_t line);

/**
 * @brief gets a label address from a HERB_OSXLangAssemblerStorage
 *
 * @param storage storage to get label address from
 * @param label   label to get address from
 *
 * @return the address of the label, will be NULL if not found
*/
uint32_t *HERB_OSXLangAssemblerStorage_GetLabelAddress(HERB_OSXLangAssemblerStorage *storage, ARC_String *label);

/**
 * @brief gets current line from storage
 *
 * @param storage storage to get current line from
 *
 * @return the current line
*/
uint64_t HERB_OSXLangAssemblerStorage_GetLine(HERB_OSXLangAssemblerStorage *storage);

/**
 * @brief sets the current line in storage
 *
 * @param storage storage to set address
 * @param line    the new current line
*/
void HERB_OSXLangAssemblerStorage_SetLine(HERB_OSXLangAssemblerStorage *storage, uint32_t line);

/**
 * @brief gets the program counter
 *
 * @param storage storage to get program counter from
 *
 * @return the current program counter
*/
uint32_t HERB_OSXLangAssemblerStorage_GetProgramCounter(HERB_OSXLangAssemblerStorage *storage);

/**
 * @brief sets the program counter
 *
 * @param storage        storage to set the program counter
 * @param programCounter the new program counter
*/
void HERB_OSXLangAssemblerStorage_SetProgramCounter(HERB_OSXLangAssemblerStorage *storage, uint32_t programCounter);

/**
 * @brief sets the loader address
 *
 * @param storage       storage to set the loader address
 * @param loaderAddress the new loaderAddress
*/
void HERB_OSXLangAssemblerStorage_SetLoaderAddress(HERB_OSXLangAssemblerStorage *storage, uint32_t loaderAddress);

#ifdef __cplusplus
}
#endif

#endif // !HERB_OSXLANG_ASSEMBLER_STORAGE_H_