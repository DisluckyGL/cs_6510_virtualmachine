#include "assembler.h"

#include "../../runner.h"
#include "../errorlog.h"
#include "../registers.h"
#include <arc/std/errno.h>
#include <stdlib.h>
#include <string.h>

uint32_t HERB_OSXLang_GetInstructionSize(ARC_String *instruction){
    //check if instruction is a WORD directive and if so return four bytes
    if(ARC_String_SubstringEqualsCString(instruction, 0, HERB_OSXLANG_DIRECTIVE_STRING_WORD " ", strlen(HERB_OSXLANG_DIRECTIVE_STRING_WORD " "))){
        return 4;
    }

    //check if instruction is a BYTE directive and if so return one byte
    if(ARC_String_SubstringEqualsCString(instruction, 0, HERB_OSXLANG_DIRECTIVE_STRING_BYTE " ", strlen(HERB_OSXLANG_DIRECTIVE_STRING_BYTE " "))){
        return 1;
    }

    //check if instruction is a SPACE directive and if so return the number of bytes specified by space
    if(ARC_String_SubstringEqualsCString(instruction, 0, HERB_OSXLANG_DIRECTIVE_STRING_SPACE " ", strlen(HERB_OSXLANG_DIRECTIVE_STRING_SPACE " "))){
        //strip instruction
        ARC_String *substring;
        ARC_String_CopySubstring(&substring, instruction, strlen(HERB_OSXLANG_DIRECTIVE_STRING_SPACE " "), instruction->length - strlen(HERB_OSXLANG_DIRECTIVE_STRING_SPACE " "));

        //strip whitespace
        ARC_String *tempSubstring = substring;
        ARC_String_StripEndsWhitespace(&substring, tempSubstring);
        ARC_String_Destroy(tempSubstring);

        //get size
        uint32_t size = ARC_String_ToUint64_t(substring);
        ARC_String_Destroy(substring);
        if(arc_errno){
            return 0;
        }

        //if size is not within bounds throw an error
        if(size == 0 || size > 1024){
            arc_errno = ARC_ERRNO_DATA;
            ARC_DEBUG_LOG(arc_errno, "HERB_OSXLang_GetInstructionSize(instruction), SPACE value \"%u\" is out of bounds", size);

            ARC_String *errorLogString;
            ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] SPACE is out of bounds");

            HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
            ARC_String_Destroy(errorLogString);

            return 0;
        }

        //return size
        return (uint64_t)size;
    }

    //check if instruction is a STRING directive and if so return the number of bytes for the given string
    if(ARC_String_SubstringEqualsCString(instruction, 0, HERB_OSXLANG_DIRECTIVE_STRING_STRING " ", strlen(HERB_OSXLANG_DIRECTIVE_STRING_STRING " "))){
        //strip instruction
        ARC_String *substring;
        ARC_String_CopySubstring(&substring, instruction, strlen(HERB_OSXLANG_DIRECTIVE_STRING_SPACE " "), instruction->length - strlen(HERB_OSXLANG_DIRECTIVE_STRING_SPACE " "));

        //strip whitespace
        ARC_String *tempSubstring = substring;
        ARC_String_StripEndsWhitespace(&substring, tempSubstring);
        ARC_String_Destroy(tempSubstring);

        if(substring->length <= 2){
            arc_errno = ARC_ERRNO_DATA;
            ARC_DEBUG_LOG(arc_errno, "HERB_OSXLang_GetInstructionSize(instruction), STRING size of substring %s was less than 2 (the needed space for quotes)", substring->data);

            ARC_String *errorLogString;
            ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] STRINGs size was not big enough to be an acutal string");

            HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
            ARC_String_Destroy(errorLogString);

            return 0;
        }

        //check for quotes
        if(substring->data[0] != '"' || substring->data[substring->length - 1] != '"'){
            //TODO: need to check if string is a valid string
            arc_errno = ARC_ERRNO_DATA;
            ARC_DEBUG_ERR("HERB_OSXLang_GetInstructionSize(instruction), STRING could not find matching \"");

            ARC_String *errorLogString;
            ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] STRING did not have matching \"");

            HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
            ARC_String_Destroy(errorLogString);

            return 0;
        }

        //the -2 is for the quotes, the + 1 is for the '\0'
        uint64_t substringSize = (substring->length - 2) + 1;
        for(uint32_t i = 1; i < substring->length - 2; i++){
            //get rid of the extra character within a escape character
            if(substring->data[i - 1] != '\\' && substring->data[i] == '\\'){
                substringSize--;
            }
        }

        //return size
        return substringSize;
    }

    return HERB_OSXLANG_INSTRUCTION_SIZE;
}

void HERB_OSXLang_SetProgramCounter(HERB_OSXLangAssemblerStorage *storage, ARC_String *instruction, uint32_t line){
    //get program counter
    uint32_t programCounter = HERB_OSXLangAssemblerStorage_GetProgramCounter(storage);

    //if program counter is already set return
    if(programCounter != ~(uint32_t)0){
        return;
    }

    //check if instruction is a WORD directive and if so return
    if(ARC_String_SubstringEqualsCString(instruction, 0, HERB_OSXLANG_DIRECTIVE_STRING_WORD " ", strlen(HERB_OSXLANG_DIRECTIVE_STRING_WORD " "))){
        return;
    }

    //check if instruction is a BYTE directive and if so return
    if(ARC_String_SubstringEqualsCString(instruction, 0, HERB_OSXLANG_DIRECTIVE_STRING_BYTE " ", strlen(HERB_OSXLANG_DIRECTIVE_STRING_BYTE " "))){
        return;
    }

    //check if instruction is a SPACE directive and if so return the number of bytes specified by space
    if(ARC_String_SubstringEqualsCString(instruction, 0, HERB_OSXLANG_DIRECTIVE_STRING_SPACE " ", strlen(HERB_OSXLANG_DIRECTIVE_STRING_SPACE " "))){
        return;
    }

    //check if instruction is a STRING directive and if so return the number of bytes specified by space
    if(ARC_String_SubstringEqualsCString(instruction, 0, HERB_OSXLANG_DIRECTIVE_STRING_STRING " ", strlen(HERB_OSXLANG_DIRECTIVE_STRING_STRING " "))){
        return;
    }

    //set program counter with current line
    HERB_OSXLangAssemblerStorage_SetProgramCounter(storage, line);
}

void HERB_OSXLang_RegisterAssemberInstructionsToHERBRunner(HERB_Runner *runner){
    HERB_Runner_RegisterInstruction(runner, HERB_OSXLANG_INSTRUCTION_STRING_ADD , strlen(HERB_OSXLANG_INSTRUCTION_STRING_ADD) , HERB_OSXLang_AssembleADD );
    HERB_Runner_RegisterInstruction(runner, HERB_OSXLANG_INSTRUCTION_STRING_SUB , strlen(HERB_OSXLANG_INSTRUCTION_STRING_SUB) , HERB_OSXLang_AssembleSUB );
    HERB_Runner_RegisterInstruction(runner, HERB_OSXLANG_INSTRUCTION_STRING_MUL , strlen(HERB_OSXLANG_INSTRUCTION_STRING_MUL) , HERB_OSXLang_AssembleMUL );
    HERB_Runner_RegisterInstruction(runner, HERB_OSXLANG_INSTRUCTION_STRING_DIV , strlen(HERB_OSXLANG_INSTRUCTION_STRING_DIV) , HERB_OSXLang_AssembleDIV );
    HERB_Runner_RegisterInstruction(runner, HERB_OSXLANG_INSTRUCTION_STRING_MOV , strlen(HERB_OSXLANG_INSTRUCTION_STRING_MOV) , HERB_OSXLang_AssembleMOV );
    HERB_Runner_RegisterInstruction(runner, HERB_OSXLANG_INSTRUCTION_STRING_MVI , strlen(HERB_OSXLANG_INSTRUCTION_STRING_MVI) , HERB_OSXLang_AssembleMVI );
    HERB_Runner_RegisterInstruction(runner, HERB_OSXLANG_INSTRUCTION_STRING_ADR , strlen(HERB_OSXLANG_INSTRUCTION_STRING_ADR) , HERB_OSXLang_AssembleADR );
    HERB_Runner_RegisterInstruction(runner, HERB_OSXLANG_INSTRUCTION_STRING_STR , strlen(HERB_OSXLANG_INSTRUCTION_STRING_STR) , HERB_OSXLang_AssembleSTR );
    HERB_Runner_RegisterInstruction(runner, HERB_OSXLANG_INSTRUCTION_STRING_STRB, strlen(HERB_OSXLANG_INSTRUCTION_STRING_STRB), HERB_OSXLang_AssembleSTRB);
    HERB_Runner_RegisterInstruction(runner, HERB_OSXLANG_INSTRUCTION_STRING_LDR , strlen(HERB_OSXLANG_INSTRUCTION_STRING_LDR) , HERB_OSXLang_AssembleLDR );
    HERB_Runner_RegisterInstruction(runner, HERB_OSXLANG_INSTRUCTION_STRING_LDRB, strlen(HERB_OSXLANG_INSTRUCTION_STRING_LDRB), HERB_OSXLang_AssembleLDRB);
    HERB_Runner_RegisterInstruction(runner, HERB_OSXLANG_INSTRUCTION_STRING_B   , strlen(HERB_OSXLANG_INSTRUCTION_STRING_B)   , HERB_OSXLang_AssembleB   );
    HERB_Runner_RegisterInstruction(runner, HERB_OSXLANG_INSTRUCTION_STRING_BL  , strlen(HERB_OSXLANG_INSTRUCTION_STRING_BL)  , HERB_OSXLang_AssembleBL  );
    HERB_Runner_RegisterInstruction(runner, HERB_OSXLANG_INSTRUCTION_STRING_BX  , strlen(HERB_OSXLANG_INSTRUCTION_STRING_BX)  , HERB_OSXLang_AssembleBX  );
    HERB_Runner_RegisterInstruction(runner, HERB_OSXLANG_INSTRUCTION_STRING_BNE , strlen(HERB_OSXLANG_INSTRUCTION_STRING_BNE) , HERB_OSXLang_AssembleBNE );
    HERB_Runner_RegisterInstruction(runner, HERB_OSXLANG_INSTRUCTION_STRING_BGT , strlen(HERB_OSXLANG_INSTRUCTION_STRING_BGT) , HERB_OSXLang_AssembleBGT );
    HERB_Runner_RegisterInstruction(runner, HERB_OSXLANG_INSTRUCTION_STRING_BLT , strlen(HERB_OSXLANG_INSTRUCTION_STRING_BLT) , HERB_OSXLang_AssembleBLT );
    HERB_Runner_RegisterInstruction(runner, HERB_OSXLANG_INSTRUCTION_STRING_BEQ , strlen(HERB_OSXLANG_INSTRUCTION_STRING_BEQ) , HERB_OSXLang_AssembleBEQ );
    HERB_Runner_RegisterInstruction(runner, HERB_OSXLANG_INSTRUCTION_STRING_CMP , strlen(HERB_OSXLANG_INSTRUCTION_STRING_CMP) , HERB_OSXLang_AssembleCMP );
    HERB_Runner_RegisterInstruction(runner, HERB_OSXLANG_INSTRUCTION_STRING_AND , strlen(HERB_OSXLANG_INSTRUCTION_STRING_AND) , HERB_OSXLang_AssembleAND );
    HERB_Runner_RegisterInstruction(runner, HERB_OSXLANG_INSTRUCTION_STRING_ORR , strlen(HERB_OSXLANG_INSTRUCTION_STRING_ORR) , HERB_OSXLang_AssembleORR );
    HERB_Runner_RegisterInstruction(runner, HERB_OSXLANG_INSTRUCTION_STRING_EOR , strlen(HERB_OSXLANG_INSTRUCTION_STRING_EOR) , HERB_OSXLang_AssembleEOR );
    HERB_Runner_RegisterInstruction(runner, HERB_OSXLANG_INSTRUCTION_STRING_SWI , strlen(HERB_OSXLANG_INSTRUCTION_STRING_SWI) , HERB_OSXLang_AssembleSWI );

    HERB_Runner_RegisterInstruction(runner, HERB_OSXLANG_DIRECTIVE_STRING_WORD  , strlen(HERB_OSXLANG_DIRECTIVE_STRING_WORD  ), HERB_OSXLang_AssembleWORD  );
    HERB_Runner_RegisterInstruction(runner, HERB_OSXLANG_DIRECTIVE_STRING_BYTE  , strlen(HERB_OSXLANG_DIRECTIVE_STRING_BYTE  ), HERB_OSXLang_AssembleBYTE  );
    HERB_Runner_RegisterInstruction(runner, HERB_OSXLANG_DIRECTIVE_STRING_SPACE , strlen(HERB_OSXLANG_DIRECTIVE_STRING_SPACE ), HERB_OSXLang_AssembleSPACE );
    HERB_Runner_RegisterInstruction(runner, HERB_OSXLANG_DIRECTIVE_STRING_STRING, strlen(HERB_OSXLANG_DIRECTIVE_STRING_STRING), HERB_OSXLang_AssembleSTRING);
}

void HERB_OSXLang_AssembleADD(void *storage, void *instructionString, uint64_t instructionSize){
    HERB_OSXLang_ReadTagRegisterRegisterRegister((HERB_OSXLangAssemblerStorage *)storage, (ARC_String *)instructionString, HERB_OSXLANG_INSTRUCTION_STRING_ADD, HERB_OSXLANG_INSTRUCTION_ENCODE_ADD);
}

void HERB_OSXLang_AssembleSUB(void *storage, void *instructionString, uint64_t instructionSize){
    HERB_OSXLang_ReadTagRegisterRegisterRegister((HERB_OSXLangAssemblerStorage *)storage, (ARC_String *)instructionString, HERB_OSXLANG_INSTRUCTION_STRING_SUB, HERB_OSXLANG_INSTRUCTION_ENCODE_SUB);
}

void HERB_OSXLang_AssembleMUL(void *storage, void *instructionString, uint64_t instructionSize){
    HERB_OSXLang_ReadTagRegisterRegisterRegister((HERB_OSXLangAssemblerStorage *)storage, (ARC_String *)instructionString, HERB_OSXLANG_INSTRUCTION_STRING_MUL, HERB_OSXLANG_INSTRUCTION_ENCODE_MUL);
}

void HERB_OSXLang_AssembleDIV(void *storage, void *instructionString, uint64_t instructionSize){
    HERB_OSXLang_ReadTagRegisterRegisterRegister((HERB_OSXLangAssemblerStorage *)storage, (ARC_String *)instructionString, HERB_OSXLANG_INSTRUCTION_STRING_DIV, HERB_OSXLANG_INSTRUCTION_ENCODE_DIV);
}

void HERB_OSXLang_AssembleMOV(void *storage, void *instructionString, uint64_t instructionSize){
    HERB_OSXLang_ReadTagRegisterRegister((HERB_OSXLangAssemblerStorage *)storage, (ARC_String *)instructionString, HERB_OSXLANG_INSTRUCTION_STRING_MOV, HERB_OSXLANG_INSTRUCTION_ENCODE_MOV);
}

void HERB_OSXLang_AssembleMVI(void *storage, void *instructionString, uint64_t instructionSize){
    HERB_OSXLang_ReadTagRegisterImmediateValue((HERB_OSXLangAssemblerStorage *)storage, (ARC_String *)instructionString, HERB_OSXLANG_INSTRUCTION_STRING_MVI, HERB_OSXLANG_INSTRUCTION_ENCODE_MVI);
}

void HERB_OSXLang_AssembleADR(void *storage, void *instructionString, uint64_t instructionSize){
    HERB_OSXLang_ReadTagRegisterLabelAddress((HERB_OSXLangAssemblerStorage *)storage, (ARC_String *)instructionString, HERB_OSXLANG_INSTRUCTION_STRING_ADR, HERB_OSXLANG_INSTRUCTION_ENCODE_ADR);
}

void HERB_OSXLang_AssembleSTR(void *storage, void *instructionString, uint64_t instructionSize){
    HERB_OSXLang_ReadTagRegisterRegister((HERB_OSXLangAssemblerStorage *)storage, (ARC_String *)instructionString, HERB_OSXLANG_INSTRUCTION_STRING_STR, HERB_OSXLANG_INSTRUCTION_ENCODE_STR);
}

void HERB_OSXLang_AssembleSTRB(void *storage, void *instructionString, uint64_t instructionSize){
    HERB_OSXLang_ReadTagRegisterRegister((HERB_OSXLangAssemblerStorage *)storage, (ARC_String *)instructionString, HERB_OSXLANG_INSTRUCTION_STRING_STRB, HERB_OSXLANG_INSTRUCTION_ENCODE_STRB);
}

void HERB_OSXLang_AssembleLDR(void *storage, void *instructionString, uint64_t instructionSize){
    HERB_OSXLang_ReadTagRegisterRegister((HERB_OSXLangAssemblerStorage *)storage, (ARC_String *)instructionString, HERB_OSXLANG_INSTRUCTION_STRING_LDR, HERB_OSXLANG_INSTRUCTION_ENCODE_LDR);
}

void HERB_OSXLang_AssembleLDRB(void *storage, void *instructionString, uint64_t instructionSize){
    HERB_OSXLang_ReadTagRegisterRegister((HERB_OSXLangAssemblerStorage *)storage, (ARC_String *)instructionString, HERB_OSXLANG_INSTRUCTION_STRING_LDRB, HERB_OSXLANG_INSTRUCTION_ENCODE_LDRB);
}

void HERB_OSXLang_AssembleB(void *storage, void *instructionString, uint64_t instructionSize){
    HERB_OSXLang_ReadTagLabelAddress((HERB_OSXLangAssemblerStorage *)storage, (ARC_String *)instructionString, HERB_OSXLANG_INSTRUCTION_STRING_B, HERB_OSXLANG_INSTRUCTION_ENCODE_B);
}

void HERB_OSXLang_AssembleBL(void *storage, void *instructionString, uint64_t instructionSize){
    HERB_OSXLang_ReadTagLabelAddress((HERB_OSXLangAssemblerStorage *)storage, (ARC_String *)instructionString, HERB_OSXLANG_INSTRUCTION_STRING_BL, HERB_OSXLANG_INSTRUCTION_ENCODE_BL);
}

void HERB_OSXLang_AssembleBX(void *storage, void *instructionString, uint64_t instructionSize){
    HERB_OSXLang_ReadTagRegister((HERB_OSXLangAssemblerStorage *)storage, (ARC_String *)instructionString, HERB_OSXLANG_INSTRUCTION_STRING_BX, HERB_OSXLANG_INSTRUCTION_ENCODE_BX);
}

void HERB_OSXLang_AssembleBNE(void *storage, void *instructionString, uint64_t instructionSize){
    HERB_OSXLang_ReadTagLabelAddress((HERB_OSXLangAssemblerStorage *)storage, (ARC_String *)instructionString, HERB_OSXLANG_INSTRUCTION_STRING_BNE, HERB_OSXLANG_INSTRUCTION_ENCODE_BNE);
}

void HERB_OSXLang_AssembleBGT(void *storage, void *instructionString, uint64_t instructionSize){
    HERB_OSXLang_ReadTagLabelAddress((HERB_OSXLangAssemblerStorage *)storage, (ARC_String *)instructionString, HERB_OSXLANG_INSTRUCTION_STRING_BGT, HERB_OSXLANG_INSTRUCTION_ENCODE_BGT);
}

void HERB_OSXLang_AssembleBLT(void *storage, void *instructionString, uint64_t instructionSize){
    HERB_OSXLang_ReadTagLabelAddress((HERB_OSXLangAssemblerStorage *)storage, (ARC_String *)instructionString, HERB_OSXLANG_INSTRUCTION_STRING_BLT, HERB_OSXLANG_INSTRUCTION_ENCODE_BLT);
}

void HERB_OSXLang_AssembleBEQ(void *storage, void *instructionString, uint64_t instructionSize){
    HERB_OSXLang_ReadTagLabelAddress((HERB_OSXLangAssemblerStorage *)storage, (ARC_String *)instructionString, HERB_OSXLANG_INSTRUCTION_STRING_BEQ, HERB_OSXLANG_INSTRUCTION_ENCODE_BEQ);
}

void HERB_OSXLang_AssembleCMP(void *storage, void *instructionString, uint64_t instructionSize){
    HERB_OSXLang_ReadTagRegisterRegister((HERB_OSXLangAssemblerStorage *)storage, (ARC_String *)instructionString, HERB_OSXLANG_INSTRUCTION_STRING_CMP, HERB_OSXLANG_INSTRUCTION_ENCODE_CMP);
}

//TODO: this is RegisterRegisterRegister which is different from the documentation, why?
void HERB_OSXLang_AssembleAND(void *storage, void *instructionString, uint64_t instructionSize){
    HERB_OSXLang_ReadTagRegisterRegisterRegister((HERB_OSXLangAssemblerStorage *)storage, (ARC_String *)instructionString, HERB_OSXLANG_INSTRUCTION_STRING_AND, HERB_OSXLANG_INSTRUCTION_ENCODE_AND);
}

//TODO: this is RegisterRegisterRegister which is different from the documentation, why?
void HERB_OSXLang_AssembleORR(void *storage, void *instructionString, uint64_t instructionSize){
    HERB_OSXLang_ReadTagRegisterRegisterRegister((HERB_OSXLangAssemblerStorage *)storage, (ARC_String *)instructionString, HERB_OSXLANG_INSTRUCTION_STRING_ORR, HERB_OSXLANG_INSTRUCTION_ENCODE_ORR);
}

//TODO: this is RegisterRegisterRegister which is different from the documentation, why?
void HERB_OSXLang_AssembleEOR(void *storage, void *instructionString, uint64_t instructionSize){
    HERB_OSXLang_ReadTagRegisterRegisterRegister((HERB_OSXLangAssemblerStorage *)storage, (ARC_String *)instructionString, HERB_OSXLANG_INSTRUCTION_STRING_EOR, HERB_OSXLANG_INSTRUCTION_ENCODE_EOR);
}

void HERB_OSXLang_AssembleSWI(void *storage, void *instructionString, uint64_t instructionSize){
    HERB_OSXLang_ReadTagImmediateValue((HERB_OSXLangAssemblerStorage *)storage, (ARC_String *)instructionString, HERB_OSXLANG_INSTRUCTION_STRING_SWI, HERB_OSXLANG_INSTRUCTION_ENCODE_SWI);
}

void HERB_OSXLang_AssembleWORD(void *storage, void *instructionString, uint64_t instructionSize){
    ARC_String *substring;
    ARC_String_Copy(&substring, (ARC_String *)instructionString);

    //make sure the correct tag is being run
    HERB_OSXLang_CheckAndStripInstructionTagCStringWithStrlen(&substring, HERB_OSXLANG_DIRECTIVE_STRING_WORD);
    if(arc_errno){
        return;
    }

    //create instruction with four bytes
    //TODO: strip + if exists
    HERB_OSXLangInstruction *instruction;
    HERB_OSXLangInstruction_Create(&instruction, 4);

    //get integer of size 32 from instruction
    int32_t tempInteger32;
    HERB_OSXLang_StripAndReadInteger32(&substring, &tempInteger32, NULL);
    if(arc_errno){
        free(instruction);
        return;
    }

    instruction->bytes[0] = 0xff & (tempInteger32 >> 24);
    instruction->bytes[1] = 0xff & (tempInteger32 >> 16);
    instruction->bytes[2] = 0xff & (tempInteger32 >>  8);
    instruction->bytes[3] = 0xff & (tempInteger32 >>  0);

    HERB_OSXLangAssemblerStorage_AddInstruction(storage, instruction);
    free(substring);
}

//TODO: add more error checks to this
void HERB_OSXLang_AssembleBYTE(void *storage, void *instructionString, uint64_t instructionSize){
    ARC_String *substring;
    ARC_String_Copy(&substring, (ARC_String *)instructionString);

    //make sure the correct tag is being run
    HERB_OSXLang_CheckAndStripInstructionTagCStringWithStrlen(&substring, HERB_OSXLANG_DIRECTIVE_STRING_BYTE);
    if(arc_errno){
        return;
    }

    //create instruction with one byte
    HERB_OSXLangInstruction *instruction;
    HERB_OSXLangInstruction_Create(&instruction, 1);

    //check if byte is a char
    if(substring->data[0] == '\'' && substring->data[2] == '\''){
        instruction->bytes[0] = substring->data[1];
        HERB_OSXLangAssemblerStorage_AddInstruction(storage, instruction);

        ARC_String_Destroy(substring);
        return;
    }

    //check if byte is a value
    if(substring->data[0] == '\\'){
        ARC_String *byteString;
        ARC_String_CopySubstring(&byteString, substring, 1, substring->length - 1);

        instruction->bytes[0] = (uint8_t)ARC_String_ToUint64_t(byteString);
        HERB_OSXLangAssemblerStorage_AddInstruction(storage, instruction);

        ARC_String_Destroy(byteString);
        ARC_String_Destroy(substring);
        return;
    }

    arc_errno = ARC_ERRNO_DATA;
    ARC_DEBUG_LOG(arc_errno, "HERB_OSXLang_AssembleBYTE(void *storage, void *instructionString, uint64_t instructionSize), could not read \"%s\"", substring->data);

    ARC_String *errorLogString;
    ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] could not read \"");

    ARC_String *tempErrorLogString = errorLogString;
    ARC_String_Merge(&errorLogString, tempErrorLogString, substring);
    ARC_String_Destroy(tempErrorLogString);

    ARC_String *tempErrorLogStringNext;
    tempErrorLogString = errorLogString;
    ARC_String_CreateWithStrlen(&tempErrorLogStringNext, "\"");
    ARC_String_Merge(&errorLogString, tempErrorLogString, tempErrorLogStringNext);
    ARC_String_Destroy(tempErrorLogString);
    ARC_String_Destroy(tempErrorLogStringNext);

    HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
    ARC_String_Destroy(errorLogString);

    //clean up
    HERB_OSXLangInstruction_Destroy(instruction);
    ARC_String_Destroy(substring);
}

void HERB_OSXLang_AssembleSPACE(void *storage, void *instructionString, uint64_t instructionSize){
    ARC_String *substring;
    ARC_String_Copy(&substring, (ARC_String *)instructionString);

    //make sure the correct tag is being run
    HERB_OSXLang_CheckAndStripInstructionTagCStringWithStrlen(&substring, HERB_OSXLANG_DIRECTIVE_STRING_SPACE);
    if(arc_errno){
        return;
    }

    //get size
    uint32_t size = ARC_String_ToUint64_t(substring);
    if(arc_errno){
        ARC_String_Destroy(substring);
        return;
    }

    //if size is not within bounds throw an error
    if(size == 0 || size > 1024){
        arc_errno = ARC_ERRNO_DATA;
        ARC_DEBUG_LOG(arc_errno, "HERB_OSXLang_AssembleSPACE(storage, instructionString, instructionSize), SPACE value \"%u\" is out of bounds", size);

        ARC_String *errorLogString;
        ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] SPACE is out of bounds");

        HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
        ARC_String_Destroy(errorLogString);

        return;
    }

    //create instruction with the size
    HERB_OSXLangInstruction *instruction;
    HERB_OSXLangInstruction_Create(&instruction, (uint64_t)size);

    //fill instruction with 0s
    for(uint32_t i = 0; i < size; i++){
        instruction->bytes[i] = 0;
    }

    //add space
    HERB_OSXLangAssemblerStorage_AddInstruction(storage, instruction);
}

void HERB_OSXLang_AssembleSTRING(void *storage, void *instructionString, uint64_t instructionSize){
    ARC_String *substring;
    ARC_String_Copy(&substring, (ARC_String *)instructionString);

    //make sure the correct tag is being run
    HERB_OSXLang_CheckAndStripInstructionTagCStringWithStrlen(&substring, HERB_OSXLANG_DIRECTIVE_STRING_STRING);
    if(arc_errno){
        return;
    }

    if(substring->length <= 2){
        arc_errno = ARC_ERRNO_DATA;
        ARC_DEBUG_LOG(arc_errno, "HERB_OSXLang_GetInstructionSize(instruction), STRING size of substring %s was less than 2 (the needed space for quotes)", substring->data);

        ARC_String *errorLogString;
        ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] STRINGs size was not big enough to be an acutal string");

        HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
        ARC_String_Destroy(errorLogString);

        return;
    }

    //check for quotes
    if(substring->data[0] != '"' || substring->data[substring->length - 1] != '"'){
        //TODO: need to check if string is a valid string
        arc_errno = ARC_ERRNO_DATA;
        ARC_DEBUG_ERR("HERB_OSXLang_AssembleSTRING(storage, instruction, instructionSize), STRING could not find matching \"");

        ARC_String *errorLogString;
        ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] STRING did not have matching \"");

        HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
        ARC_String_Destroy(errorLogString);

        return;
    }
    //the -2 is for the quotes, the + 1 is for the '\0'
    uint64_t size = (substring->length - 2);
    for(uint32_t i = 1; i < substring->length - 2; i++){
       //get rid of the extra character within a escape character
        if(substring->data[i - 1] != '\\' && substring->data[i] == '\\'){
            size--;
        }
    }

    //create instruction with the size
    HERB_OSXLangInstruction *instruction;
    HERB_OSXLangInstruction_Create(&instruction, size + 1);

    //fill instruction with string data
    uint32_t substringIndex = 1;
    for(uint64_t i = 0; i < size; i++){
        if(substring->data[substringIndex] != '\\'){
            //not an escape character so assign and continue
            instruction->bytes[i] = substring->data[substringIndex];
            substringIndex++;
            continue;
        }

        substringIndex++;
        switch(substring->data[substringIndex]){
            case '\\':
                instruction->bytes[i] = '\\';
                break;
            case 't':
                instruction->bytes[i] = '\t';
                break;
            case 'n':
                instruction->bytes[i] = '\n';
                break;
            case '0':
                instruction->bytes[i] = '\0';
                break;
            case '\'':
                instruction->bytes[i] = '\'';
                break;
            case '\"':
                instruction->bytes[i] = '"';
                break;
            default:
                arc_errno = ARC_ERRNO_DATA;
                ARC_DEBUG_LOG(arc_errno, "HERB_OSXLang_AssembleSTRING(storage, instruction, instructionSize), excape operator \\%c not recognized", substring->data[substringIndex]);

                ARC_String *errorLogString;
                ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] STRING did not have matching escape operator");

                HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
                ARC_String_Destroy(errorLogString);

                return;
        }

        substringIndex++;
    }

    //add the end of the string
    instruction->bytes[size] = 0x00;

    //add string
    HERB_OSXLangAssemblerStorage_AddInstruction(storage, instruction);
}

//TODO: add more errorlog logs
void HERB_OSXLang_CheckAndStripInstructionTagCStringWithStrlen(ARC_String **instructionString, const char *tag){
    //add space to tag
    uint64_t tagWithSpaceLen = strlen(tag) + 1;
    char tagWithSpace[tagWithSpaceLen + 1];
    strcpy(tagWithSpace, tag);
    tagWithSpace[tagWithSpaceLen - 1] = ' ';
    tagWithSpace[tagWithSpaceLen] = '\0';

    //check if instruction is a tag with a space
    if(!ARC_String_SubstringEqualsCString(*instructionString, 0, tagWithSpace, tagWithSpaceLen)){
        //first instruction was not tag, error and break
        arc_errno = ARC_ERRNO_DATA;
        ARC_DEBUG_LOG(arc_errno, "HERB_OSXLang_CheckAndStripInstructionTagCStringWithStrlen(strippedInstructionString, instructionString, tag), \"%s\" did not start with \"%s\"", (*instructionString)->data, tag);
        return;
    }

    //strip tag instruction and leading space from instructionString
    ARC_String *temp = *instructionString;
    ARC_String_CopySubstring(instructionString, temp, tagWithSpaceLen, temp->length - tagWithSpaceLen);
    ARC_String_Destroy(temp);
}

void HERB_OSXLang_StripAndReadInteger32(ARC_String **instructionString, int32_t *integer32, char *search){
    uint64_t nextSpace = (*instructionString)->length;

    if(search != NULL){
        nextSpace = ARC_String_FindCString(*instructionString, search, strlen(search));
        if(arc_errno){
            return;
        }

        if(nextSpace == ~(uint64_t)0){
            arc_errno = ARC_ERRNO_DATA;
            ARC_DEBUG_LOG(arc_errno, "HERB_OSXLang_StripAndReadImmediateValue(instructionString, registerEncode, search), could not find \"%s\"", search);
            return;
        }

        if(nextSpace > 0){
            nextSpace--;
        }
    }

    ARC_String *integer32String;
    ARC_String_CopySubstring(&integer32String, *instructionString, 0, nextSpace);
    *integer32 = (int32_t)ARC_String_ToInt64_t(integer32String);
    ARC_String_Destroy(integer32String);
    if(arc_errno){
        return;
    }

    if(search == NULL){
        return;
    }

    ARC_String *temp = *instructionString;
    ARC_String_CopySubstring(instructionString, temp, nextSpace + 1, temp->length - (nextSpace + 1));
    ARC_String_Destroy(temp);
}

void HERB_OSXLang_StripAndReadRegister(ARC_String **instructionString, uint8_t *registerEncode, char *search){
    uint64_t nextSpace = (*instructionString)->length;

    if(search != NULL){
        nextSpace = ARC_String_FindCString(*instructionString, search, strlen(search));
        if(arc_errno){
            return;
        }

        if(nextSpace == ~(uint64_t)0){
            arc_errno = ARC_ERRNO_DATA;
            ARC_DEBUG_LOG(arc_errno, "HERB_OSXLang_StripAndReadRegister(instructionString, registerEncode, search), could not find \"%s\"", search);
            return;
        }

        if(nextSpace > 0){
            nextSpace--;
        }
    }

    //strip off brackets if they exist
    //TODO: for readable add back in brackets
    uint64_t start = 0;
    uint64_t length = nextSpace;
    if((*instructionString)->data[start] == '[' && (*instructionString)->data[length - 1] == ']'){
        start++;
        length -= 2;
    }

    ARC_String *registerString;
    ARC_String_CopySubstring(&registerString, *instructionString, start, length);
    *registerEncode = HERB_OSXLangRegister_GetEncode(registerString);
    ARC_String_Destroy(registerString);
    if(arc_errno){
        return;
    }

    if(search == NULL){
        return;
    }

    ARC_String *temp = *instructionString;
    ARC_String_CopySubstring(instructionString, temp, nextSpace + 1, temp->length - (nextSpace + 1));
    ARC_String_Destroy(temp);
}

void HERB_OSXLang_StripAndReadImmediateValue(ARC_String **instructionString, uint32_t *immediateValue, char *search){
    uint64_t nextSpace = (*instructionString)->length;

    if(search != NULL){
        nextSpace = ARC_String_FindCString(*instructionString, search, strlen(search));
        if(arc_errno){
            return;
        }

        if(nextSpace == ~(uint64_t)0){
            arc_errno = ARC_ERRNO_DATA;
            ARC_DEBUG_LOG(arc_errno, "HERB_OSXLang_StripAndReadImmediateValue(instructionString, registerEncode, search), could not find \"%s\"", search);
            return;
        }

        if(nextSpace > 0){
            nextSpace--;
        }
    }

    ARC_String *registerString;
    ARC_String_CopySubstring(&registerString, *instructionString, 0, nextSpace);
    *immediateValue = (uint32_t)ARC_String_ToUint64_t(registerString);
    ARC_String_Destroy(registerString);
    if(arc_errno){
        return;
    }

    if(search == NULL){
        return;
    }

    ARC_String *temp = *instructionString;
    ARC_String_CopySubstring(instructionString, temp, nextSpace + 1, temp->length - (nextSpace + 1));
    ARC_String_Destroy(temp);
}

void HERB_OSXLang_StripAndReadLabelAddress(ARC_String **instructionString, HERB_OSXLangAssemblerStorage *storage, uint32_t *labelAddress, char *search){
    uint64_t nextSpace = (*instructionString)->length;

    if(search != NULL){
        nextSpace = ARC_String_FindCString(*instructionString, search, strlen(search));
        if(arc_errno){
            return;
        }

        if(nextSpace == ~(uint64_t)0){
            arc_errno = ARC_ERRNO_DATA;
            ARC_DEBUG_LOG(arc_errno, "HERB_OSXLang_StripAndReadImmediateValue(instructionString, registerEncode, search), could not find \"%s\"", search);
            return;
        }

        if(nextSpace > 0){
            nextSpace--;
        }
    }

    ARC_String *labelString;
    ARC_String_CopySubstring(&labelString, *instructionString, 0, nextSpace);
    uint32_t *labelAddressPtr = HERB_OSXLangAssemblerStorage_GetLabelAddress(storage, labelString);
    if(labelAddressPtr == NULL){
        arc_errno = ARC_ERRNO_DATA;
        ARC_DEBUG_LOG(arc_errno, "HERB_OSXLang_StripAndReadImmediateValue(instructionString, registerEncode, search), could not find label \"%s\"", labelString->data);
        ARC_String_Destroy(labelString);
        return;
    }

    *labelAddress = *labelAddressPtr;
    ARC_String_Destroy(labelString);
    if(arc_errno){
        return;
    }

    if(search == NULL){
        return;
    }

    ARC_String *temp = *instructionString;
    ARC_String_CopySubstring(instructionString, temp, nextSpace + 1, temp->length - (nextSpace + 1));
    ARC_String_Destroy(temp);
}

void HERB_OSXLang_ReadTagLabelAddress(HERB_OSXLangAssemblerStorage *storage, ARC_String *instructionString, char *tag, uint8_t opCode){
    ARC_String *substring;
    ARC_String_Copy(&substring, instructionString);

    //make sure the correct tag is being run
    HERB_OSXLang_CheckAndStripInstructionTagCStringWithStrlen(&substring, tag);
    if(arc_errno){
        return;
    }

    //create instruction and set instruction op code
    HERB_OSXLangInstruction *instruction;
    HERB_OSXLangInstruction_Create(&instruction, HERB_OSXLANG_INSTRUCTION_SIZE);
    instruction->bytes[0] = opCode;

    //label address
    uint32_t tempLabelAddress;
    HERB_OSXLang_StripAndReadLabelAddress(&substring, storage, &tempLabelAddress, NULL);
    if(arc_errno){
        free(instruction);
        return;
    }

    instruction->bytes[1] = 0xff & (tempLabelAddress >> 24);
    instruction->bytes[2] = 0xff & (tempLabelAddress >> 16);
    instruction->bytes[3] = 0xff & (tempLabelAddress >>  8);
    instruction->bytes[4] = 0xff & (tempLabelAddress >>  0);

    //fill the rest of bytes with 0
    for(uint32_t byteIndex = 0; byteIndex < HERB_OSXLANG_INSTRUCTION_SIZE - 5; byteIndex++){
        instruction->bytes[5 + byteIndex] = 0x00;
    }

    HERB_OSXLangAssemblerStorage_AddInstruction(storage, instruction);
    free(substring);
}

void HERB_OSXLang_ReadTagRegisterLabelAddress(HERB_OSXLangAssemblerStorage *storage, ARC_String *instructionString, char *tag, uint8_t opCode){
    ARC_String *substring;
    ARC_String_Copy(&substring, instructionString);

    //make sure the correct tag is being run
    HERB_OSXLang_CheckAndStripInstructionTagCStringWithStrlen(&substring, tag);
    if(arc_errno){
        return;
    }

    //create instruction and set instruction op code
    HERB_OSXLangInstruction *instruction;
    HERB_OSXLangInstruction_Create(&instruction, HERB_OSXLANG_INSTRUCTION_SIZE);
    instruction->bytes[0] = opCode;

    //register 1
    uint8_t tempReigisterEncode;
    HERB_OSXLang_StripAndReadRegister(&substring, &tempReigisterEncode, (char *)" ");
    if(arc_errno){
        free(instruction);
        return;
    }

    instruction->bytes[1] = tempReigisterEncode;

    //label address
    uint32_t tempLabelAddress;
    HERB_OSXLang_StripAndReadLabelAddress(&substring, storage, &tempLabelAddress, NULL);
    if(arc_errno){
        free(instruction);
        return;
    }

    instruction->bytes[2] = 0xff & (tempLabelAddress >> 24);
    instruction->bytes[3] = 0xff & (tempLabelAddress >> 16);
    instruction->bytes[4] = 0xff & (tempLabelAddress >>  8);
    instruction->bytes[5] = 0xff & (tempLabelAddress >>  0);

    //fill the rest of bytes with 0
    for(uint32_t byteIndex = 0; byteIndex < HERB_OSXLANG_INSTRUCTION_SIZE - 6; byteIndex++){
        instruction->bytes[6 + byteIndex] = 0x00;
    }

    HERB_OSXLangAssemblerStorage_AddInstruction(storage, instruction);
    free(substring);
}

void HERB_OSXLang_ReadTagImmediateValue(HERB_OSXLangAssemblerStorage *storage, ARC_String *instructionString, char *tag, uint8_t opCode){
    ARC_String *substring;
    ARC_String_Copy(&substring, instructionString);

    //make sure the correct tag is being run
    HERB_OSXLang_CheckAndStripInstructionTagCStringWithStrlen(&substring, tag);
    if(arc_errno){
        return;
    }

    //create instruction and set instruction op code
    HERB_OSXLangInstruction *instruction;
    HERB_OSXLangInstruction_Create(&instruction, HERB_OSXLANG_INSTRUCTION_SIZE);
    instruction->bytes[0] = opCode;

    //immediate value
    uint32_t tempImmediateValue;
    HERB_OSXLang_StripAndReadImmediateValue(&substring, &tempImmediateValue, NULL);
    if(arc_errno){
        free(instruction);
        return;
    }

    instruction->bytes[1] = 0xff & (tempImmediateValue >> 24);
    instruction->bytes[2] = 0xff & (tempImmediateValue >> 16);
    instruction->bytes[3] = 0xff & (tempImmediateValue >>  8);
    instruction->bytes[4] = 0xff & (tempImmediateValue >>  0);

    //fill the rest of bytes with 0
    for(uint32_t byteIndex = 0; byteIndex < HERB_OSXLANG_INSTRUCTION_SIZE - 5; byteIndex++){
        instruction->bytes[5 + byteIndex] = 0x00;
    }

    HERB_OSXLangAssemblerStorage_AddInstruction(storage, instruction);
    free(substring);
}

void HERB_OSXLang_ReadTagRegisterImmediateValue(HERB_OSXLangAssemblerStorage *storage, ARC_String *instructionString, char *tag, uint8_t opCode){
    ARC_String *substring;
    ARC_String_Copy(&substring, instructionString);

    //make sure the correct tag is being run
    HERB_OSXLang_CheckAndStripInstructionTagCStringWithStrlen(&substring, tag);
    if(arc_errno){
        return;
    }

    //create instruction and set instruction op code
    HERB_OSXLangInstruction *instruction;
    HERB_OSXLangInstruction_Create(&instruction, HERB_OSXLANG_INSTRUCTION_SIZE);
    instruction->bytes[0] = opCode;

    //register 1
    uint8_t tempReigisterEncode;
    HERB_OSXLang_StripAndReadRegister(&substring, &tempReigisterEncode, (char *)" ");
    if(arc_errno){
        free(instruction);
        return;
    }

    instruction->bytes[1] = tempReigisterEncode;

    //immediate value
    uint32_t tempImmediateValue;
    HERB_OSXLang_StripAndReadImmediateValue(&substring, &tempImmediateValue, NULL);
    if(arc_errno){
        free(instruction);
        return;
    }

    instruction->bytes[2] = 0xff & (tempImmediateValue >> 24);
    instruction->bytes[3] = 0xff & (tempImmediateValue >> 16);
    instruction->bytes[4] = 0xff & (tempImmediateValue >>  8);
    instruction->bytes[5] = 0xff & (tempImmediateValue >>  0);

    //fill the rest of bytes with 0
    for(uint32_t byteIndex = 0; byteIndex < HERB_OSXLANG_INSTRUCTION_SIZE - 6; byteIndex++){
        instruction->bytes[6 + byteIndex] = 0x00;
    }

    HERB_OSXLangAssemblerStorage_AddInstruction(storage, instruction);
    free(substring);
}

void HERB_OSXLang_ReadTagRegister(HERB_OSXLangAssemblerStorage *storage, ARC_String *instructionString, char *tag, uint8_t opCode){
    ARC_String *substring;
    ARC_String_Copy(&substring, instructionString);

    //make sure the correct tag is being run
    HERB_OSXLang_CheckAndStripInstructionTagCStringWithStrlen(&substring, tag);
    if(arc_errno){
        return;
    }

    //create instruction and set instruction op code
    HERB_OSXLangInstruction *instruction;
    HERB_OSXLangInstruction_Create(&instruction, HERB_OSXLANG_INSTRUCTION_SIZE);
    instruction->bytes[0] = opCode;

    //register 1
    uint8_t tempReigisterEncode;
    HERB_OSXLang_StripAndReadRegister(&substring, &tempReigisterEncode, NULL);
    if(arc_errno){
        free(instruction);
        return;
    }

    instruction->bytes[1] = tempReigisterEncode;

    //fill the rest of bytes with 0
    for(uint32_t byteIndex = 0; byteIndex < HERB_OSXLANG_INSTRUCTION_SIZE - 2; byteIndex++){
        instruction->bytes[2 + byteIndex] = 0x00;
    }

    HERB_OSXLangAssemblerStorage_AddInstruction(storage, instruction);

    free(substring);
}

void HERB_OSXLang_ReadTagRegisterRegister(HERB_OSXLangAssemblerStorage *storage, ARC_String *instructionString, char *tag, uint8_t opCode){
    ARC_String *substring;
    ARC_String_Copy(&substring, instructionString);

    //make sure the correct tag is being run
    HERB_OSXLang_CheckAndStripInstructionTagCStringWithStrlen(&substring, tag);
    if(arc_errno){
        return;
    }

    //create instruction and set instruction op code
    HERB_OSXLangInstruction *instruction;
    HERB_OSXLangInstruction_Create(&instruction, HERB_OSXLANG_INSTRUCTION_SIZE);
    instruction->bytes[0] = opCode;

    //register 1
    uint8_t tempReigisterEncode;
    HERB_OSXLang_StripAndReadRegister(&substring, &tempReigisterEncode, (char *)" ");
    if(arc_errno){
        free(instruction);
        return;
    }

    instruction->bytes[1] = tempReigisterEncode;

    //register 2
    HERB_OSXLang_StripAndReadRegister(&substring, &tempReigisterEncode, NULL);
    if(arc_errno){
        free(instruction);
        return;
    }

    instruction->bytes[2] = tempReigisterEncode;

    //fill the rest of bytes with 0
    for(uint32_t byteIndex = 0; byteIndex < HERB_OSXLANG_INSTRUCTION_SIZE - 3; byteIndex++){
        instruction->bytes[3 + byteIndex] = 0x00;
    }

    HERB_OSXLangAssemblerStorage_AddInstruction(storage, instruction);

    free(substring);
}

void HERB_OSXLang_ReadTagRegisterRegisterRegister(HERB_OSXLangAssemblerStorage *storage, ARC_String *instructionString, char *tag, uint8_t opCode){
    ARC_String *substring;
    ARC_String_Copy(&substring, instructionString);

    //make sure the correct tag is being run
    HERB_OSXLang_CheckAndStripInstructionTagCStringWithStrlen(&substring, tag);
    if(arc_errno){
        return;
    }

    //create instruction and set instruction op code
    HERB_OSXLangInstruction *instruction;
    HERB_OSXLangInstruction_Create(&instruction, HERB_OSXLANG_INSTRUCTION_SIZE);
    instruction->bytes[0] = opCode;

    //register 1
    uint8_t tempReigisterEncode;
    HERB_OSXLang_StripAndReadRegister(&substring, &tempReigisterEncode, (char *)" ");
    if(arc_errno){
        free(instruction);
        return;
    }

    instruction->bytes[1] = tempReigisterEncode;

    //register 2
    HERB_OSXLang_StripAndReadRegister(&substring, &tempReigisterEncode, (char *)" ");
    if(arc_errno){
        free(instruction);
        return;
    }

    instruction->bytes[2] = tempReigisterEncode;

    //register 3
    HERB_OSXLang_StripAndReadRegister(&substring, &tempReigisterEncode, NULL);
    if(arc_errno){
        free(instruction);
        return;
    }

    instruction->bytes[3] = tempReigisterEncode;

    //fill the rest of bytes with 0
    for(uint32_t byteIndex = 0; byteIndex < HERB_OSXLANG_INSTRUCTION_SIZE - 4; byteIndex++){
        instruction->bytes[4 + byteIndex] = 0x00;
    }

    HERB_OSXLangAssemblerStorage_AddInstruction(storage, instruction);

    free(substring);
}
