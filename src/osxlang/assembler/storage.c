#include "storage.h"

#include <arc/std/io.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

struct HERB_OSXLangAssemblerStorage {
    ARC_Vector    *instructions;
    ARC_Hashtable *labels;

    uint32_t line;
    uint32_t programCounter;
    uint32_t loaderAddress;
};

//TODO: put this in the header
int8_t HERB_OSXLangAssemblerStorage_InstructionComp(void *instruction1, size_t *instruction1size, void *instruction2, size_t *instruction2size){
    if(*instruction1size - *instruction2size){
        return -1;
    }

    return strncmp((const char *)instruction1, (const char *)instruction2, *instruction1size);
}

void HERB_OSXLangAssemblerStorage_Create(HERB_OSXLangAssemblerStorage **storage){
    *storage = (HERB_OSXLangAssemblerStorage *)malloc(sizeof(HERB_OSXLangAssemblerStorage));

    ARC_Vector_Create(&(*storage)->instructions);
    ARC_Hashtable_Create(&(*storage)->labels, HERB_OSXLANG_ASSEMBLER_STORAGE_BUCKET_SIZE, NULL, HERB_OSXLangAssemblerStorage_InstructionComp);

    (*storage)->line           = 0;
    (*storage)->programCounter = ~(uint32_t)0;
    (*storage)->loaderAddress  = 0;
}

void HERB_OSXLangAssemblerStorage_Destroy(HERB_OSXLangAssemblerStorage *storage){
    //TODO: define external
    ARC_Hashtable_Destroy(storage->labels, NULL, NULL);
    ARC_Vector_Destroy(storage->instructions);
    free(storage);
}

void HERB_OSXLangAssemblerStorage_WriteToFile(HERB_OSXLangAssemblerStorage *storage, ARC_String *path){
    //get size add three headers (4 bytes each for size 12)
    uint32_t size = 12;
    for(uint32_t i = 0; i < ARC_Vector_Size(storage->instructions); i++){
        HERB_OSXLangInstruction *instruction = ARC_Vector_Get(storage->instructions, i);
        size += instruction->bytesSize;
    }

    //create string
    char dataCString[size];
    storage->line = size - 12;

    //add line header
    dataCString[0] = 0xff & (storage->line >> 24);
    dataCString[1] = 0xff & (storage->line >> 16);
    dataCString[2] = 0xff & (storage->line >>  8);
    dataCString[3] = 0xff & (storage->line >>  0);

    //add program counter header
    dataCString[4] = 0xff & (storage->programCounter >> 24);
    dataCString[5] = 0xff & (storage->programCounter >> 16);
    dataCString[6] = 0xff & (storage->programCounter >>  8);
    dataCString[7] = 0xff & (storage->programCounter >>  0);

    //add loader address header
    dataCString[ 8] = 0xff & (storage->loaderAddress >> 24);
    dataCString[ 9] = 0xff & (storage->loaderAddress >> 16);
    dataCString[10] = 0xff & (storage->loaderAddress >>  8);
    dataCString[11] = 0xff & (storage->loaderAddress >>  0);

    //store instructions in string to write
    size = 12;
    for(uint32_t i = 0; i < ARC_Vector_Size(storage->instructions); i++){
        HERB_OSXLangInstruction *instruction = (HERB_OSXLangInstruction *)ARC_Vector_Get(storage->instructions, i);

        for(uint32_t byteIndex = 0; byteIndex < instruction->bytesSize; byteIndex++){
            dataCString[size] = instruction->bytes[byteIndex];
            size ++;
        }
    }

    ARC_String dataString = {
        dataCString,
        size
    };
    ARC_IO_WriteStrToFile(path, &dataString);
}

void HERB_OSXLangAssemblerStorage_AddInstruction(HERB_OSXLangAssemblerStorage *storage, HERB_OSXLangInstruction *instruction){
    ARC_Vector_Add(storage->instructions, (void *)instruction);
}

void HERB_OSXLangAssemblerStorage_PrintInstructions(HERB_OSXLangAssemblerStorage *storage){
    uint32_t instructionsSize = ARC_Vector_Size(storage->instructions);
    for(uint32_t i = 0; i < instructionsSize; i++){
        HERB_OSXLangInstruction *instruction = (HERB_OSXLangInstruction *)ARC_Vector_Get(storage->instructions, i);
        HERB_OSXLangInstruction_PrintWithNewLine(instruction);
    }
}

void HERB_OSXLangAssemblerStorage_AddLabel(HERB_OSXLangAssemblerStorage *storage, ARC_String *label, uint64_t line){
    ARC_String *copy;
    ARC_String_Copy(&copy, label);

    uint64_t *data = (uint64_t *)malloc(sizeof(uint64_t));
    *data = line;

    ARC_Hashtable_Add(storage->labels, copy->data, copy->length, data);
}

uint32_t *HERB_OSXLangAssemblerStorage_GetLabelAddress(HERB_OSXLangAssemblerStorage *storage, ARC_String *label){
    uint32_t *address = NULL;
    ARC_Hashtable_Get(storage->labels, label->data, label->length, (void **)&address);
    return address;
}

uint64_t HERB_OSXLangAssemblerStorage_GetLine(HERB_OSXLangAssemblerStorage *storage){
    return storage->line;
}

void HERB_OSXLangAssemblerStorage_SetLine(HERB_OSXLangAssemblerStorage *storage, uint32_t line){
    storage->line = line;
}

uint32_t HERB_OSXLangAssemblerStorage_GetProgramCounter(HERB_OSXLangAssemblerStorage *storage){
    return storage->programCounter;
}

void HERB_OSXLangAssemblerStorage_SetProgramCounter(HERB_OSXLangAssemblerStorage *storage, uint32_t programCounter){
    storage->programCounter = programCounter;
}

void HERB_OSXLangAssemblerStorage_SetLoaderAddress(HERB_OSXLangAssemblerStorage *storage, uint32_t loaderAddress){
    storage->loaderAddress = loaderAddress;
}