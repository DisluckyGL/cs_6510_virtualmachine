#include "runner.h"

#include "../errorlog.h"
#include "assembler.h"
#include "storage.h"
#include <arc/std/errno.h>
#include <stdlib.h>
#include <string.h>

void HERB_OSXLangRunnerAssembler_InitStorage(void **storage){
    HERB_OSXLangAssemblerStorage_Create((HERB_OSXLangAssemblerStorage **)storage);
}

void HERB_OSXLangRunnerAssembler_DeinitStorage(void *storage){
    HERB_OSXLangAssemblerStorage_Destroy((HERB_OSXLangAssemblerStorage *)storage);
}

int8_t HERB_OSXLangRunnerAssembler_InstructionComp(void *instruction1, size_t *instruction1size, void *instruction2, size_t *instruction2size){
    //if sizes are the same return false (-1)
    if(*instruction1size - *instruction2size){
        return -1;
    }

    //return a string comparison of the data
    return strncmp((const char *)instruction1, (const char *)instruction2, *instruction1size);
}

//TODO: check for tabs as well as spaces
void HERB_OSXLangRunnerAssembler_LabelRun(HERB_Runner *runner, void *data, size_t dataSize){
    if(arc_errno){
        //there is an unresolved error break
        return;
    }

    //copy data into a string so we can string maniuplate
    ARC_String *dataString;
    ARC_String_Create(&dataString, (char *)data, dataSize);

    ARC_String *tempDataString = NULL;

    //check for comment and strip
    uint64_t commentStartIndex = ARC_String_FindCString(dataString, ";", 1);
    if(arc_errno == 0 && commentStartIndex != ~(uint64_t)0){
        //return if comment takes up full string
        if(commentStartIndex <= 1){
            ARC_String_Destroy(dataString);
            return;
        }

        //strip comment
        tempDataString = dataString;
        ARC_String_CopySubstring(&dataString, tempDataString, 0, commentStartIndex - 1);
        ARC_String_Destroy(tempDataString);
    }

    //TODO: might want to remove this reset but I'm not sure why or why not yet
    if(arc_errno){
        arc_errno = 0;
    }

    //if the line is empty return
    if(dataString->length == 1 && dataString->data[0] == '\n'){
        ARC_String_Destroy(dataString);
        return;
    }

    //strip whitespace
    tempDataString = dataString;
    ARC_String_StripEndsWhitespace(&dataString, tempDataString);
    ARC_String_Destroy(tempDataString);

    //check for label
    uint64_t labelOrInstructionSize = ARC_String_FindCString(dataString, " ", 1);
    if(arc_errno){
        ARC_String_Destroy(dataString);
        return;
    }

    if(labelOrInstructionSize == ~(uint64_t)0){
        arc_errno = ARC_ERRNO_DATA;
        ARC_DEBUG_ERR("HERB_OSXLangRunnerAssembler_LabelRun(runner, data, dataSize), could not find space for label or instruction");

        ARC_String *errorLogString;
        ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] could not find space for label or instruction");

        HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
        ARC_String_Destroy(errorLogString);

        ARC_String_Destroy(dataString);

        return;
    }

    //get rid of space after label as part of the string
    labelOrInstructionSize--;

    //check if label or instruction
    ARC_String *labelOrInstruction;
    ARC_String_CopySubstring(&labelOrInstruction, dataString, 0, labelOrInstructionSize);
    HERB_Runner_InstructionFn instructionFn = HERB_Runner_GetInstructionFn(runner, labelOrInstruction->data, labelOrInstruction->length);

    HERB_OSXLangAssemblerStorage *storage = HERB_Runner_GetStorage(runner);
    uint32_t line = HERB_OSXLangAssemblerStorage_GetLine(storage);

    //if it is an instruction add length, cleanup, and return
    if(instructionFn != NULL){
        //set program counter
        HERB_OSXLang_SetProgramCounter(storage, dataString, line);

        //add length
        line += HERB_OSXLang_GetInstructionSize(dataString);
        HERB_OSXLangAssemblerStorage_SetLine(storage, line);

        //cleanup
        ARC_String_Destroy(labelOrInstruction);
        ARC_String_Destroy(dataString);
        return;
    }

    HERB_OSXLangAssemblerStorage_AddLabel(storage, labelOrInstruction, line);
    ARC_String_Destroy(labelOrInstruction);

    //strip label
    tempDataString = dataString;
    ARC_String_CopySubstring(&dataString, tempDataString, labelOrInstructionSize + 1, tempDataString->length - (labelOrInstructionSize + 1));
    ARC_String_Destroy(tempDataString);

    //add length
    line += HERB_OSXLang_GetInstructionSize(dataString);
    HERB_OSXLangAssemblerStorage_SetLine(storage, line);

    //cleanup
    ARC_String_Destroy(dataString);
}

void HERB_OSXLangRunnerAssembler_InstructionRun(HERB_Runner *runner, void *data, size_t dataSize){
    if(arc_errno){
        //there is an unresolved error break
        return;
    }

    //copy data into a string so we can string maniuplate
    ARC_String *dataString;
    ARC_String_Create(&dataString, (char *)data, dataSize);

    ARC_String *tempDataString = NULL;

    //check for comment and strip
    uint64_t commentStartIndex = ARC_String_FindCString(dataString, ";", 1);
    if(arc_errno == 0 && commentStartIndex != ~(uint64_t)0){
        //return if comment takes up full string
        if(commentStartIndex <= 1){
            ARC_String_Destroy(dataString);
            return;
        }

        //strip comment
        tempDataString = dataString;
        ARC_String_CopySubstring(&dataString, tempDataString, 0, commentStartIndex - 1);
        ARC_String_Destroy(tempDataString);
    }

    //TODO: might want to remove this reset but I'm not sure why or why not yet
    if(arc_errno){
        arc_errno = 0;
    }

    //if the line is empty return
    if(dataString->length == 1 && dataString->data[0] == '\n'){
        return;
    }

    //strip whitespace
    tempDataString = dataString;
    ARC_String_StripEndsWhitespace(&dataString, tempDataString);
    ARC_String_Destroy(tempDataString);

    //check for label
    uint64_t labelOrInstructionSize = ARC_String_FindCString(dataString, " ", 1);
    if(arc_errno){
        return;
    }

    if(labelOrInstructionSize == ~(uint64_t)0){
        arc_errno = ARC_ERRNO_DATA;
        ARC_DEBUG_ERR("HERB_OSXLangRunnerAssembler_InstructionRun(runner, data, dataSize), could not find space for label or instruction");

        ARC_String *errorLogString;
        ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] could not find space for label or instruction");

        HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
        ARC_String_Destroy(errorLogString);

        return;
    }

    //get rid of space after label as part of the string
    labelOrInstructionSize--;

    //check if label or instruction
    ARC_String *labelOrInstruction;
    ARC_String_CopySubstring(&labelOrInstruction, dataString, 0, labelOrInstructionSize);
    HERB_Runner_InstructionFn instructionFn = HERB_Runner_GetInstructionFn(runner, labelOrInstruction->data, labelOrInstruction->length);
    ARC_String_Destroy(labelOrInstruction);

    if(instructionFn == NULL){
        //strip label
        tempDataString = dataString;
        ARC_String_CopySubstring(&dataString, tempDataString, labelOrInstructionSize + 1, tempDataString->length - (labelOrInstructionSize + 1));
        ARC_String_Destroy(tempDataString);

        //strip whitespace
        tempDataString = dataString;
        ARC_String_StripEndsWhitespace(&dataString, tempDataString);
        ARC_String_Destroy(tempDataString);

        //get instruction
        labelOrInstructionSize = ARC_String_FindCString(dataString, " ", 1);
        if(arc_errno){
            ARC_String_Destroy(dataString);
            return;
        }

        if(labelOrInstructionSize == ~(uint64_t)0){
            arc_errno = ARC_ERRNO_DATA;
            ARC_DEBUG_ERR("HERB_OSXLangRunnerAssembler_InstructionRun(runner, data, dataSize), could not find space for instruction");

            ARC_String *errorLogString;
            ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] could not find space for instruction");

            HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
            ARC_String_Destroy(errorLogString);

            ARC_String_Destroy(dataString);
            return;
        }

        //get rid of space after label as part of the string
        labelOrInstructionSize--;

        //get instruction function
        ARC_String_CopySubstring(&labelOrInstruction, dataString, 0, labelOrInstructionSize);
        instructionFn = HERB_Runner_GetInstructionFn(runner, labelOrInstruction->data, labelOrInstruction->length);
        ARC_String_Destroy(labelOrInstruction);

        //if there is no instruction throw an error
        if(instructionFn == NULL){
            arc_errno = ARC_ERRNO_DATA;
            ARC_DEBUG_ERR("HERB_OSXLangRunnerAssembler_InstructionRun(runner, data, dataSize), instructionFn is null");

            ARC_String *errorLogString;
            ARC_String_CreateWithStrlen(&errorLogString, "[ERROR] instructionFn is null");

            HERB_OSXLangErrorLog_AddError(herb_errorlog, errorLogString);
            ARC_String_Destroy(errorLogString);

            ARC_String_Destroy(dataString);
            return;
        }
    }

    //run instruction, size is passed through string so we can pass 0 in the size parameter
    instructionFn(HERB_Runner_GetStorage(runner), dataString, 0);

    //cleanup
    ARC_String_Destroy(dataString);
}

void HERB_OSXLangRunnerAssembler_Runner(ARC_String *data, ARC_String *path, uint32_t loaderAddress){
    //init runner
    HERB_Runner *runner;
    HERB_Runner_Create(&runner, HERB_OSXLangRunnerAssembler_InitStorage, HERB_OSXLangRunnerAssembler_DeinitStorage, HERB_OSXLangRunnerAssembler_InstructionComp);

    //create newline string to merge with data
    ARC_String *newLine;
    ARC_String_Create(&newLine, "\n", 1);

    //add new line to end of data so last instruction will be read
    ARC_String *usedData = data;
    ARC_String_Merge(&usedData, data, newLine);
    ARC_String_Destroy(newLine);

    //set instructions for runner
    HERB_OSXLang_RegisterAssemberInstructionsToHERBRunner(runner);

    //define seperator and instruction end
    char *separator = " ";
    uint64_t separatorSize = 1;
    char *instructionEnd = "\n";
    uint64_t instructionEndSize = 1;

    //get labels
    HERB_Runner_Run(runner, usedData->data, usedData->length, separator, &separatorSize, instructionEnd, &instructionEndSize, HERB_OSXLangRunnerAssembler_LabelRun);

    //run commands
    HERB_Runner_Run(runner, usedData->data, usedData->length, separator, &separatorSize, instructionEnd, &instructionEndSize, HERB_OSXLangRunnerAssembler_InstructionRun);

    //set loader address
    HERB_OSXLangAssemblerStorage *storage = HERB_Runner_GetStorage(runner);
    HERB_OSXLangAssemblerStorage_SetLoaderAddress(storage, loaderAddress);

    //output
    HERB_OSXLangAssemblerStorage_WriteToFile(storage, path);

    //cleanup
    ARC_String_Destroy(usedData);
    HERB_Runner_Destroy(runner);
}
