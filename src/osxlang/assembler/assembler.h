#ifndef HERB_OSXLANG_ASSEMBLER_H_
#define HERB_OSXLANG_ASSEMBLER_H_

#include "../../runner.h"
#include "../instruction.h"
#include "storage.h"
#include <arc/std/string.h>
#include <arc/std/vector.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief gets an instruction's length
 *
 * @note this is used for labels to get locations, directives are the reason this function is needed
 *
 * @param instruction instruction to get size from
 *
 * @return instructions size (usually will be the size of an instruction, but might be larger or smaller due to a directive)
*/
uint32_t HERB_OSXLang_GetInstructionSize(ARC_String *instruction);

/**
 * @brief checks for instruction and sets program counter if it hasn't been set yet
 *
 * @note this is used to get starting instruction, we need this function because directives are not instructions but are bundeld within instructions with the runner
 *
 * @param storage     place to store program counter
 * @param instruction instruction to check if it is the starting instruction
 * @param line        current line (byte) the assembler is on
*/
void HERB_OSXLang_SetProgramCounter(HERB_OSXLangAssemblerStorage *storage, ARC_String *instruction, uint32_t line);

/**
 * @brief adds OSX Lang assembly instructions to a HERB_Runner
 *
 * @param runner HERB_Runner to add OSX Lang assembly instructions to
*/
void HERB_OSXLang_RegisterAssemberInstructionsToHERBRunner(HERB_Runner *runner);

/**
 * @brief the ADD instruction for the OSX Lang
 *
 * @note this is used as a HERB_Runner_InstructionFn, this doxygen will mention types used instead of void *
 *
 * @note [<label>] ADD <reg1> <reg2> <reg3> ; Add
 *       Encode: 1 byte op code | 1 byte register | 1 byte register | 1 byte register | 2 bytes unused
 *       ADD = 16
 *       Suggested Usage: <reg1>  <reg2> + <reg3> 
 *
 * @param storage           HERB_OSXLangAssemblerStorage to store instructions to. will add HERB_OSXLangInstruction to the vector on success
 * @param instructionString string that holds the add instruction from an osx lang assembly string
 * @param instructionSize   this will not be used as instructionString holds the string length, this parameter is needed for HERB_Runner
*/
void HERB_OSXLang_AssembleADD(void *storage, void *instructionString, uint64_t instructionSize);

/**
 * @brief the SUB instruction for the OSX Lang
 *
 * @note this is used as a HERB_Runner_InstructionFn, this doxygen will mention types used instead of void *
 *
 * @note [<label>] SUB <reg1> <reg2> <reg3> ; Subtract
 *       Encode: 1 byte op code | 1 byte register | 1 byte register | 1 byte register | 2 bytes unused
 *       SUB = 17
 *       Suggested Usage:  <reg1>  <reg2> - <reg3>
 *
 * @param storage           HERB_OSXLangAssemblerStorage to store instructions to. will add HERB_OSXLangInstruction to the vector on success
 * @param instructionString string that holds the sub instruction from an osx lang assembly string
 * @param instructionSize   this will not be used as instructionString holds the string length, this parameter is needed for HERB_Runner
*/
void HERB_OSXLang_AssembleSUB(void *storage, void *instructionString, uint64_t instructionSize);

/**
 * @brief the MUL instruction for the OSX Lang
 *
 * @note this is used as a HERB_Runner_InstructionFn, this doxygen will mention types used instead of void *
 *
 * @note [<label>] MUL <reg1> <reg2> <reg3> ; Multiple
 *       Encode: 1 byte op code | 1 byte register | 1 byte register | 1 byte register | 2 bytes unused
 *       MUL = 18
 *       Suggested Usage: <reg1>  <reg2> * <reg3>
 *
 * @param storage           HERB_OSXLangAssemblerStorage to store instructions to. will add HERB_OSXLangInstruction to the vector on success
 * @param instructionString string that holds the mul instruction from an osx lang assembly string
 * @param instructionSize   this will not be used as instructionString holds the string length, this parameter is needed for HERB_Runner
*/
void HERB_OSXLang_AssembleMUL(void *storage, void *instructionString, uint64_t instructionSize);

/**
 * @brief the DIV instruction for the OSX Lang
 *
 * @note this is used as a HERB_Runner_InstructionFn, this doxygen will mention types used instead of void *
 *
 * @note [<label>] DIV <reg1> <reg2> <reg3> ; Divide
 *       Encode: 1 byte op code | 1 byte register | 1 byte register | 1 byte register | 2 bytes unused
 *       DIV = 19
 *       Suggested Usage: <reg1>  <reg2> / <reg3>
 *       Move Data
 *
 * @param storage           HERB_OSXLangAssemblerStorage to store instructions to. will add HERB_OSXLangInstruction to the vector on success
 * @param instructionString string that holds the div instruction from an osx lang assembly string
 * @param instructionSize   this will not be used as instructionString holds the string length, this parameter is needed for HERB_Runner
*/
void HERB_OSXLang_AssembleDIV(void *storage, void *instructionString, uint64_t instructionSize);

/**
 * @brief the MOV instruction for the OSX Lang
 *
 * @note this is used as a HERB_Runner_InstructionFn, this doxygen will mention types used instead of void *
 *
 * @note [<label>] MOV <reg1> < reg2> ; Load data from register
 *       Encode: 1 byte op code | 1 byte register | 1 byte register | 3 bytes unused
 *       MOV = 1
 *       Suggested Usage:  <reg1>  < reg2>
 *
 * @param storage           HERB_OSXLangAssemblerStorage to store instructions to. will add HERB_OSXLangInstruction to the vector on success
 * @param instructionString string that holds the mov instruction from an osx lang assembly string
 * @param instructionSize   this will not be used as instructionString holds the string length, this parameter is needed for HERB_Runner
*/
void HERB_OSXLang_AssembleMOV(void *storage, void *instructionString, uint64_t instructionSize);

/**
 * @brief the MVI instruction for the OSX Lang
 *
 * @note this is used as a HERB_Runner_InstructionFn, this doxygen will mention types used instead of void *
 *
 * @note [<label>] MVI <reg1> <imm> ; Load register with immediate value
 *       Encode: 1 byte op code | 1 byte register | 4 byte immediate
 *       MVI = 22
 *       Suggested Usage: <reg1>  <imm>
 *
 * @param storage           HERB_OSXLangAssemblerStorage to store instructions to. will add HERB_OSXLangInstruction to the vector on success
 * @param instructionString string that holds the mvi instruction from an osx lang assembly string
 * @param instructionSize   this will not be used as instructionString holds the string length, this parameter is needed for HERB_Runner
*/
void HERB_OSXLang_AssembleMVI(void *storage, void *instructionString, uint64_t instructionSize);

/**
 * @brief the ADR instruction for the OSX Lang
 *
 * @note [<label>] ADR <reg1> <label> ; Get address of label
 *       Encode: 1 byte op code | 1 byte register | 4 byte address
 *       ADR = 0
 *       Suggested Usage: <reg1>  address(<label>)
 *
 * @param storage           HERB_OSXLangAssemblerStorage to store instructions to. will add HERB_OSXLangInstruction to the vector on success
 * @param instructionString string that holds the adr instruction from an osx lang assembly string
 * @param instructionSize   this will not be used as instructionString holds the string length, this parameter is needed for HERB_Runner
*/
void HERB_OSXLang_AssembleADR(void *storage, void *instructionString, uint64_t instructionSize);

/**
 * @brief the STR instruction for the OSX Lang
 *
 * @note [<label>] STR <reg1> <reg2> ; Store word (int) using register indirect addressing
 *       Encode: 1 byte op code | 1 byte register | 1 byte register | 3 bytes unused
 *       STR=2
 *       Suggested Usage: memory[<reg2>]  <reg1>
 *
 * @param storage           HERB_OSXLangAssemblerStorage to store instructions to. will add HERB_OSXLangInstruction to the vector on success
 * @param instructionString string that holds the str instruction from an osx lang assembly string
 * @param instructionSize   this will not be used as instructionString holds the string length, this parameter is needed for HERB_Runner
*/
void HERB_OSXLang_AssembleSTR(void *storage, void *instructionString, uint64_t instructionSize);

/**
 * @brief the STRB instruction for the OSX Lang
 *
 * @note [<label>] STRB <reg1> <reg2>  ; Store byte using register indirect addressing
 *       Encode: 1 byte op code | 1 byte register | 1 byte register | 3 bytes unused
 *       STRB=3
 *       Suggested Usage: memory[<reg2>]  byte(memory[<reg1>])
 *
 * @param storage           HERB_OSXLangAssemblerStorage to store instructions to. will add HERB_OSXLangInstruction to the vector on success
 * @param instructionString string that holds the strb instruction from an osx lang assembly string
 * @param instructionSize   this will not be used as instructionString holds the string length, this parameter is needed for HERB_Runner
*/
void HERB_OSXLang_AssembleSTRB(void *storage, void *instructionString, uint64_t instructionSize);

/**
 * @brief the LDR instruction for the OSX Lang
 *
 * @note [<label>] LDR <reg1> <reg2>  ; Load word (int) using register indirect addressing
 *       Encode: 1 byte op code | 1 byte register | 1 byte register | 3 bytes unused
 *       LDR=4
 *       Suggested Usage: <reg1>  memory[<reg2>]
 *
 * @param storage           HERB_OSXLangAssemblerStorage to store instructions to. will add HERB_OSXLangInstruction to the vector on success
 * @param instructionString string that holds the ldr instruction from an osx lang assembly string
 * @param instructionSize   this will not be used as instructionString holds the string length, this parameter is needed for HERB_Runner
*/
void HERB_OSXLang_AssembleLDR(void *storage, void *instructionString, uint64_t instructionSize);

/**
 * @brief the LDRB instruction for the OSX Lang
 *
 * @note [<label>] LDRB <reg1> <reg2>  ; Load byte using register indirect addressing
 *       Encode: 1 byte op code | 1 byte register | 1 byte register | 3 bytes unused
 *       LDRB=5
 *       Suggested Usage: <reg1>  byte(memory[<reg2>]
 *
 * @param storage           HERB_OSXLangAssemblerStorage to store instructions to. will add HERB_OSXLangInstruction to the vector on success
 * @param instructionString string that holds the ldrb instruction from an osx lang assembly string
 * @param instructionSize   this will not be used as instructionString holds the string length, this parameter is needed for HERB_Runner
*/
void HERB_OSXLang_AssembleLDRB(void *storage, void *instructionString, uint64_t instructionSize);

/**
 * @brief the B instruction for the OSX Lang
 *
 * @note [<label>] B <label> ;  Jump to label
 *       Encode: 1 byte op code | 4 byte address | 1 byte unused
 *       B = 7
 *       Suggested Usage: PC  address(<label>)
 *
 * @param storage           HERB_OSXLangAssemblerStorage to store instructions to. will add HERB_OSXLangInstruction to the vector on success
 * @param instructionString string that holds the b instruction from an osx lang assembly string
 * @param instructionSize   this will not be used as instructionString holds the string length, this parameter is needed for HERB_Runner
*/
void HERB_OSXLang_AssembleB(void *storage, void *instructionString, uint64_t instructionSize);

/**
 * @brief the BL instruction for the OSX Lang
 *
 * @note [<label>] BL <label> ;  Jump to label and link (place the PC of the next instruction into a register)
 *       Encode: 1 byte op code | 4 byte address | 1 byte unused
 *       BL = 21
 *       Suggested Usage: PC  address(<label>)  R5  PC+6
 *
 * @param storage           HERB_OSXLangAssemblerStorage to store instructions to. will add HERB_OSXLangInstruction to the vector on success
 * @param instructionString string that holds the bl instruction from an osx lang assembly string
 * @param instructionSize   this will not be used as instructionString holds the string length, this parameter is needed for HERB_Runner
*/
void HERB_OSXLang_AssembleBL(void *storage, void *instructionString, uint64_t instructionSize);

/**
 * @brief the BX instruction for the OSX Lang
 *
 * @note [<label>] BX <reg> ; Jump to address in register
 *       Encode: 1 byte op code | 1 byte register | 4 bytes unused
 *       BX = 6
 *       Suggested Usage: PC  <reg>
 *
 * @param storage           HERB_OSXLangAssemblerStorage to store instructions to. will add HERB_OSXLangInstruction to the vector on success
 * @param instructionString string that holds the bx instruction from an osx lang assembly string
 * @param instructionSize   this will not be used as instructionString holds the string length, this parameter is needed for HERB_Runner
*/
void HERB_OSXLang_AssembleBX(void *storage, void *instructionString, uint64_t instructionSize);

/**
 * @brief the BNE instruction for the OSX Lang
 *
 * @note [<label>] BNE <label> ; Jump to label if Z register is not zero
 *       Encode: 1 byte op code | 4 byte address | 1 byte unused
 *       BNE = 8
 *       Suggested Usage: PC  address(<label>) if Z != 0
 *
 * @param storage           HERB_OSXLangAssemblerStorage to store instructions to. will add HERB_OSXLangInstruction to the vector on success
 * @param instructionString string that holds the bne instruction from an osx lang assembly string
 * @param instructionSize   this will not be used as instructionString holds the string length, this parameter is needed for HERB_Runner
*/
void HERB_OSXLang_AssembleBNE(void *storage, void *instructionString, uint64_t instructionSize);

/**
 * @brief the BGT instruction for the OSX Lang
 *
 * @note [<label>] BGT <label> ; Jump to label if Z register is greater than zero
 *       Encode: 1 byte op code | 4 byte address | 1 byte unused
 *       BGT = 9
 *       Suggested Usage: PC  address(<label>) if Z > 0
 *
 * @param storage           HERB_OSXLangAssemblerStorage to store instructions to. will add HERB_OSXLangInstruction to the vector on success
 * @param instructionString string that holds the bgt instruction from an osx lang assembly string
 * @param instructionSize   this will not be used as instructionString holds the string length, this parameter is needed for HERB_Runner
*/
void HERB_OSXLang_AssembleBGT(void *storage, void *instructionString, uint64_t instructionSize);

/**
 * @brief the BLT instruction for the OSX Lang
 *
 * @note [<label>] BLT <label> ; Jump to label if Z register is less than zero
 *       Encode: 1 byte op code | 4 byte address | 1 byte unused
 *       BLT = 10
 *       Suggested Usage: PC  address(<label>) if Z < 0
 *
 * @param storage           HERB_OSXLangAssemblerStorage to store instructions to. will add HERB_OSXLangInstruction to the vector on success
 * @param instructionString string that holds the blt instruction from an osx lang assembly string
 * @param instructionSize   this will not be used as instructionString holds the string length, this parameter is needed for HERB_Runner
*/
void HERB_OSXLang_AssembleBLT(void *storage, void *instructionString, uint64_t instructionSize);

/**
 * @brief the BEQ instruction for the OSX Lang
 *
 * @note [<label>] BEQ <label> ; Jump to label if Z register is zero
 *       Encode: 1 byte op code | 4 byte address | 1 byte unused
 *       BEQ = 11
 *       Suggested Usage: PC  address(<label>) if Z == 0
 *
 * @param storage           HERB_OSXLangAssemblerStorage to store instructions to. will add HERB_OSXLangInstruction to the vector on success
 * @param instructionString string that holds the beq instruction from an osx lang assembly string
 * @param instructionSize   this will not be used as instructionString holds the string length, this parameter is needed for HERB_Runner
*/
void HERB_OSXLang_AssembleBEQ(void *storage, void *instructionString, uint64_t instructionSize);

/**
 * @brief the CMP instruction for the OSX Lang
 *
 * @note [<label>] CMP <reg1> <reg2> ; Compare <reg1> and <reg2> place result in Z register
 *       Encode: 1 byte op code | 1 byte register | 1 byte register | 3 bytes unused
 *       CMP = 12
 *       Suggested Usage: Z  <reg1> - <reg2>
 *
 * @param storage           HERB_OSXLangAssemblerStorage to store instructions to. will add HERB_OSXLangInstruction to the vector on success
 * @param instructionString string that holds the cmp instruction from an osx lang assembly string
 * @param instructionSize   this will not be used as instructionString holds the string length, this parameter is needed for HERB_Runner
*/
void HERB_OSXLang_AssembleCMP(void *storage, void *instructionString, uint64_t instructionSize);

/**
 * @brief the AND instruction for the OSX Lang
 *
 * @note [<label>] AND <reg1> <reg2> ; Perform an AND operation on <reg1> and <reg2> result in Z register
 *       Encode: 1 byte op code | 1 byte register | 1 byte register | 3 bytes unused
 *       AND = 13
 *
 * @note The AND operation can be logical or bitwise either will work
 *       Suggested Usage: Z  <reg1> & <reg2> bitwise AND operation
 *       Suggested Usage: Z  <reg1> && <reg2> logical AND operation
 *
 * @param storage           HERB_OSXLangAssemblerStorage to store instructions to. will add HERB_OSXLangInstruction to the vector on success
 * @param instructionString string that holds the and instruction from an osx lang assembly string
 * @param instructionSize   this will not be used as instructionString holds the string length, this parameter is needed for HERB_Runner
*/
void HERB_OSXLang_AssembleAND(void *storage, void *instructionString, uint64_t instructionSize);

/**
 * @brief the ORR instruction for the OSX Lang
 *
 * @note [<label>] ORR <reg1> <reg2> ; Perform an OR operation on <reg1> and <reg2> result in Z register
 *       Encode: 1 byte op code | 1 byte register | 1 byte register | 3 bytes unused
 *       ORR = 14
 *
 * @note The ORR operation can be logical or bitwise either will work
 *       Suggested Usage: Z  <reg1> | <reg2> bitwise OR operation
 *       Suggested Usage: Z  <reg1> || <reg2> logical OR operation
 *
 * @param storage           HERB_OSXLangAssemblerStorage to store instructions to. will add HERB_OSXLangInstruction to the vector on success
 * @param instructionString string that holds the orr instruction from an osx lang assembly string
 * @param instructionSize   this will not be used as instructionString holds the string length, this parameter is needed for HERB_Runner
*/
void HERB_OSXLang_AssembleORR(void *storage, void *instructionString, uint64_t instructionSize);

/**
 * @brief the EOR instruction for the OSX Lang
 *
 * @note [<label>] EOR <reg1> <reg2> ; Exclusive OR operation on <reg1> and <reg2> result in Z register
 *       Encode: 1 byte op code | 1 byte register | 1 byte register | 3 bytes unused
 *       EOR = 15
 *
 * @note Note: The EOR operation can be logical or bitwise, there is little actual need for this instruction
 *       Suggested Usage: Z  <reg1> ^ <reg2> bitwise Exclusive OR operation
 *       Suggested Usage: Z  (<reg1> ||<reg2>) && !(<reg1> && <reg2>)
 *
 * @param storage           HERB_OSXLangAssemblerStorage to store instructions to. will add HERB_OSXLangInstruction to the vector on success
 * @param instructionString string that holds the eor instruction from an osx lang assembly string
 * @param instructionSize   this will not be used as instructionString holds the string length, this parameter is needed for HERB_Runner
*/
void HERB_OSXLang_AssembleEOR(void *storage, void *instructionString, uint64_t instructionSize);

/**
 * @brief the SWI instruction for the OSX Lang
 *
 * @note SWI <imm> ; Software Interrupt
 *       Encode: 1 byte op code | 4 byte immediate | 1 byte unused
 *       SWI = 20
 *       Suggested Usage: Execute interrupt <imm>
 *
 * @param storage           HERB_OSXLangAssemblerStorage to store instructions to. will add HERB_OSXLangInstruction to the vector on success
 * @param instructionString string that holds the swi instruction from an osx lang assembly string
 * @param instructionSize   this will not be used as instructionString holds the string length, this parameter is needed for HERB_Runner
*/
void HERB_OSXLang_AssembleSWI(void *storage, void *instructionString, uint64_t instructionSize);

/**
 * @brief the WORD directive for the OSX Lang
 *
 * @note [<label>] .WORD <integer> ; Allocate space for a positive or negative 4 byte integer
 *
 * @note this is tied to HERB_OSXLang_GetInstructionSize(instruction) so don't forget to update if you change this
 *
 * @param storage           HERB_OSXLangAssemblerStorage to store instructions to. will add HERB_OSXLangInstruction to the vector on success
 * @param instructionString string that holds the word instruction from an osx lang assembly string
 * @param instructionSize   this will not be used as instructionString holds the string length, this parameter is needed for HERB_Runner
*/
void HERB_OSXLang_AssembleWORD(void *storage, void *instructionString, uint64_t instructionSize);

/**
 * @brief the BYTE directive for the OSX Lang
 *
 * @note both notes below are valid ways of using this directive
 *
 * @note [<label>] .BYTE '<char>'  ; Allocate space for printable ASCII character
 * @note [<label>] .BYTE \\<value>  ; Allocate space for a byte value 0...255
 *
 * @note this is tied to HERB_OSXLang_GetInstructionSize(instruction) so don't forget to update if you change this
 *
 * @param storage           HERB_OSXLangAssemblerStorage to store instructions to. will add HERB_OSXLangInstruction to the vector on success
 * @param instructionString string that holds the byte instruction from an osx lang assembly string
 * @param instructionSize   this will not be used as instructionString holds the string length, this parameter is needed for HERB_Runner
*/
void HERB_OSXLang_AssembleBYTE(void *storage, void *instructionString, uint64_t instructionSize);

/**
 * @brief the SPACE directive for the OSX Lang
 *
 * @note [<label>] .SPACE <value>  ; Allocate 1...1024 bytes of space
 *
 * @note this is tied to HERB_OSXLang_GetInstructionSize(instruction) so don't forget to update if you change this
 *
 * @param storage           HERB_OSXLangAssemblerStorage to store instructions to. will add HERB_OSXLangInstruction to the vector on success
 * @param instructionString string that holds the space instruction from an osx lang assembly string
 * @param instructionSize   this will not be used as instructionString holds the string length, this parameter is needed for HERB_Runner
*/
void HERB_OSXLang_AssembleSPACE(void *storage, void *instructionString, uint64_t instructionSize);

/**
 * @brief the STRING directive for the OSX Lang
 *
 * @note [<label>] .STRING <value>  ; Allocate 1...1024 bytes of space for a string of bytes 0...255
 *
 * @note this is tied to HERB_OSXLang_GetInstructionSize(instruction) so don't forget to update if you change this
 *
 * @param storage           HERB_OSXLangAssemblerStorage to store instructions to. will add HERB_OSXLangInstruction to the vector on success
 * @param instructionString string that holds the space instruction from an osx lang assembly string
 * @param instructionSize   this will not be used as instructionString holds the string length, this parameter is needed for HERB_Runner
*/
void HERB_OSXLang_AssembleSTRING(void *storage, void *instructionString, uint64_t instructionSize);

/**
 * @brief checks for instruction tag and strips it using a cstring and strlen()
 *
 * @param instructionString string to check for tag
 * @param tag               tag that will be checked against the string
*/
void HERB_OSXLang_CheckAndStripInstructionTagCStringWithStrlen(ARC_String **instructionString, const char *tag);

/**
 * @brief strip and reads an integer of 32 bytes
 *
 * @param instructionString string to strip integer from
 * @param integer32         the stripped integer
 * @param search            the seperator for the next part of the instruction, if NULL will use the full instruction string
*/
void HERB_OSXLang_StripAndReadInteger32(ARC_String **instructionString, int32_t *integer32, char *search);

/**
 * @brief strips and reads a register
 *
 * @param instructionString string to strip register from
 * @param registerEncode    the stripped regester as an encode value
 * @param search            the seperator for the next part of the instruction, if NULL will use the full instruction string
*/
void HERB_OSXLang_StripAndReadRegister(ARC_String **instructionString, uint8_t *registerEncode, char *search);

/**
 * @brief strips and reads an immediate value
 *
 * @param instructionString string to strip immediate value from
 * @param immediateValue    the stripped immediate value
 * @param search            the seperator for the next part of the instruction, if NULL will use the full instruction string
*/
void HERB_OSXLang_StripAndReadImmediateValue(ARC_String **instructionString, uint32_t *immediateValue, char *search);

/**
 * @brief strips and reads a label address
 *
 * @param instructionString string to strip label address from
 * @param storage           storage to get label address from
 * @param labelAddress      the stripped label address
 * @param search            the seperator for the next part of the instruction, if NULL will use the full instruction string
*/
void HERB_OSXLang_StripAndReadLabelAddress(ARC_String **instructionString, HERB_OSXLangAssemblerStorage *storage, uint32_t *labelAddress, char *search);

/**
 * @brief reads a tag and a label address
 *
 * @param storage           storage mostly used for getting labels
 * @param instructionString instruction to read a tag and label address
 * @param tag               tag to search for
 * @param opCode            opcode to use
*/
void HERB_OSXLang_ReadTagLabelAddress(HERB_OSXLangAssemblerStorage *storage, ARC_String *instructionString, char *tag, uint8_t opCode);

/**
 * @brief reads a tag, a register, and a label address
 *
 * @param storage           storage mostly used for getting labels
 * @param instructionString instruction to read a tag, register, and label address
 * @param tag               tag to search for
 * @param opCode            opcode to use
*/
void HERB_OSXLang_ReadTagRegisterLabelAddress(HERB_OSXLangAssemblerStorage *storage, ARC_String *instructionString, char *tag, uint8_t opCode);

/**
 * @brief reads a tag and an immediate value
 *
 * @param storage           storage mostly used for getting labels
 * @param instructionString instruction to read a tag and immediate value
 * @param tag               tag to search for
 * @param opCode            opcode to use
*/
void HERB_OSXLang_ReadTagImmediateValue(HERB_OSXLangAssemblerStorage *storage, ARC_String *instructionString, char *tag, uint8_t opCode);

/**
 * @brief reads a tag, one register, and an immediate value
 *
 * @param storage           storage mostly used for getting labels
 * @param instructionString instruction to read a tag, register, and immediate value
 * @param tag               tag to search for
 * @param opCode            opcode to use
*/
void HERB_OSXLang_ReadTagRegisterImmediateValue(HERB_OSXLangAssemblerStorage *storage, ARC_String *instructionString, char *tag, uint8_t opCode);

/**
 * @brief reads a tag and one register
 *
 * @param storage           storage mostly used for getting labels
 * @param instructionString instruction to read a tag and register
 * @param tag               tag to search for
 * @param opCode            opcode to use
*/
void HERB_OSXLang_ReadTagRegister(HERB_OSXLangAssemblerStorage *storage, ARC_String *instructionString, char *tag, uint8_t opCode);

/**
 * @brief reads a tag and two registers
 *
 * @param storage           storage mostly used for getting labels
 * @param instructionString instruction to read a tag and two registers
 * @param tag               tag to search for
 * @param opCode            opcode to use
*/
void HERB_OSXLang_ReadTagRegisterRegister(HERB_OSXLangAssemblerStorage *storage, ARC_String *instructionString, char *tag, uint8_t opCode);

/**
 * @brief reads a tag and three registers
 *
 * @param storage           storage mostly used for getting labels
 * @param instructionString instruction to read a tag and three registers
 * @param tag               tag to search for
 * @param opCode            opcode to use
*/
void HERB_OSXLang_ReadTagRegisterRegisterRegister(HERB_OSXLangAssemblerStorage *storage, ARC_String *instructionString, char *tag, uint8_t opCode);

#ifdef __cplusplus
}
#endif

#endif // !HERB_OSXLANG_ASSEMBLER_H_