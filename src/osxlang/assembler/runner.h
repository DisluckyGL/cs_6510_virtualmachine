#ifndef HERB_OSXLANG_RUNNER_ASSEMBLER_H_
#define HERB_OSXLANG_RUNNER_ASSEMBLER_H_

#include "../../runner.h"
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief creates storege for HERB_Runner used by HERB_OSXLangRunnerAssembler
 *
 * @param storage the storate the HERB_OSXLangRunnerAssembler will use
*/
void HERB_OSXLangRunnerAssembler_InitStorage(void **storage);

/**
 * @brief destroys storege for HERB_Runner used by HERB_OSXLangRunnerAssembler
 *
 * @param storage the storate the HERB_OSXLangRunnerAssembler will destroy
*/
void HERB_OSXLangRunnerAssembler_DeinitStorage(void *storage);

/**
 * @brief compares instructions used in storage, passed as a callback to HERB_Runner
 *
 * @note this is the same as ARC_Hashtable_KeyCompare, just rewritten here to make it easier on the user
 *
 * @note TODO: create ARC_Bool and start using it instead of returning int8_t
 *             also reformat hashtable to only require instructions and not their sizes for comparison
 *
 * @param instruction1     data for instruction1
 * @param instruction1size size of instruction1
 * @param instruction2     data for instruction2
 * @param instruction2size size of instruction2
 *
 * @return 0 when keys match
*/
int8_t HERB_OSXLangRunnerAssembler_InstructionComp(void *instruction1, size_t *instruction1size, void *instruction2, size_t *instruction2size);

/**
 * @brief used as a callback for HERB_Runner_Run
 *
 * @note this is run after every line (or what is specified for HERB_RUnner_Run), checks each substring for a label and updates line numbers
 *
 * @param runner   current runnter that is running the program, can be used to get the storage
 * @param data     this void * is a char * substring
 * @param dataSize this is the size of the substring
*/
void HERB_OSXLangRunnerAssembler_LabelRun(HERB_Runner *runner, void *data, size_t dataSize);

/**
 * @brief used as a callback for HERB_Runner_Run
 *
 * @note this is run after every line (or what is specified for HERB_RUnner_Run), skips labels and adds instructions to storage
 *
 * @param runner   current runnter that is running the program, can be used to get the storage
 * @param data     this void * is a char * substring
 * @param dataSize this is the size of the substring
*/
void HERB_OSXLangRunnerAssembler_InstructionRun(HERB_Runner *runner, void *data, size_t dataSize);

/**
 * @brief assembles and outputs the assembly to a given file
 *
 * @param data          string of assembly to assemble
 * @param path          path to write file to
 * @param loaderAddress a header value for where to start loading
*/
void HERB_OSXLangRunnerAssembler_Runner(ARC_String *data, ARC_String *path, uint32_t loaderAddress);

#ifdef __cplusplus
}
#endif

#endif // !HERB_OSXLANG_RUNNER_ASSEMBLER_H_