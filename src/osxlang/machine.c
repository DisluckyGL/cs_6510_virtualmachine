#include "machine.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <arc/std/bool.h>

void HERB_OSXLangMachine_Create(HERB_OSXLangMachine **machine, uint64_t randomAccessMemorySize, uint64_t softDriveSize){
    *machine = (HERB_OSXLangMachine *)malloc(sizeof(HERB_OSXLangMachine));

    HERB_OSXLangMemory_Create(&((*machine)->randomAccessMemory), randomAccessMemorySize);
    HERB_OSXLangMemory_Create(&((*machine)->softDrive)         , softDriveSize         );

    HERB_OSXLangBinaryExecutor_Create(&((*machine)->binaryExecutor));

    HERB_OSXLangClock_Create(&((*machine)->clock), NULL);

    for(uint32_t i = 0; i < HERB_OSXLANG_MACHINE_LOCK_NUM; i++){
        (*machine)->locks[i] = ARC_False;
    }
}

void HERB_OSXLangMachine_Destroy(HERB_OSXLangMachine *machine){
    HERB_OSXLangClock_Destroy(machine->clock);

    HERB_OSXLangBinaryExecutor_Destroy(machine->binaryExecutor);

    HERB_OSXLangMemory_Destroy(machine->randomAccessMemory);
    HERB_OSXLangMemory_Destroy(machine->softDrive         );

    free(machine);
}

void HERB_OSXLangMachine_Print(HERB_OSXLangMachine *machine, uint8_t rowSize, uint8_t groupedBytes){
    printf("Registers:\n");
    for(uint8_t i = 0; i < HERB_OSXLANG_REGISTER_MAX_ENCODE; i++){
        const char *cString = HERB_OSXLangRegister_GetConstCString(i);
        if(cString != NULL){
            printf("%s: %08x\t", cString, machine->binaryExecutor->registers[i]);
        }
    }
    printf("\n\n");

    printf("Random Access Memory:\n");
    HERB_OSXLangMemory_Print(machine->randomAccessMemory, rowSize, groupedBytes);
    printf("\n");

    printf("Soft Drive:\n");
    HERB_OSXLangMemory_Print(machine->softDrive, rowSize, groupedBytes);
}

//TODO: add the new changes here to HERB_OSXLangMachine_Print(...)
void HERB_OSXLangMachine_PrintToBuffer(HERB_OSXLangMachine *machine, uint8_t rowSize, uint8_t groupedBytes, ARC_ConsoleBuffer *buffer){
    ARC_ConsoleBuffer_AddCStringWithStrlen(buffer, "Registers:\n");
    for(uint8_t i = 0; i < HERB_OSXLANG_REGISTER_MAX_ENCODE; i++){
        const char *cString = HERB_OSXLangRegister_GetConstCString(i);
        if(cString != NULL){
            char registersCString[strlen(cString) + 13];
            sprintf(registersCString,"%s: %08x\t", cString, machine->binaryExecutor->registers[i]);
            ARC_ConsoleBuffer_AddCStringWithStrlen(buffer, registersCString);
        }
    }
    ARC_ConsoleBuffer_AddChar(buffer, '\n');
    ARC_ConsoleBuffer_AddChar(buffer, '\n');
    ARC_ConsoleBuffer_AddChar(buffer, '\n');

    //I'm too burn't out to put a better value here, 256 is overkill
    char blocksSizeCStr[256];

    ARC_ConsoleBuffer_AddCStringWithStrlen(buffer, "Random Access Memory Blocks: ");
    sprintf(blocksSizeCStr, "%u", HERB_OSXLangMemory_GetAvailableBlockSize(machine->randomAccessMemory));
    ARC_ConsoleBuffer_AddCStringWithStrlen(buffer, blocksSizeCStr);
    ARC_ConsoleBuffer_AddChar(buffer, '/');
    sprintf(blocksSizeCStr, "%u", HERB_OSXLangMemory_GetTotalBlockSize(machine->randomAccessMemory));
    ARC_ConsoleBuffer_AddCStringWithStrlen(buffer, blocksSizeCStr);
    ARC_ConsoleBuffer_AddChar(buffer, '\n');

    ARC_ConsoleBuffer_AddCStringWithStrlen(buffer, "Soft Drive Blocks: ");
    sprintf(blocksSizeCStr, "%u", HERB_OSXLangMemory_GetAvailableBlockSize(machine->softDrive));
    ARC_ConsoleBuffer_AddCStringWithStrlen(buffer, blocksSizeCStr);
    ARC_ConsoleBuffer_AddChar(buffer, '/');
    sprintf(blocksSizeCStr, "%u", HERB_OSXLangMemory_GetTotalBlockSize(machine->softDrive));
    ARC_ConsoleBuffer_AddCStringWithStrlen(buffer, blocksSizeCStr);
    ARC_ConsoleBuffer_AddChar(buffer, '\n');
    ARC_ConsoleBuffer_AddChar(buffer, '\n');

    ARC_ConsoleBuffer_AddCStringWithStrlen(buffer, "Random Access Memory:\n");
    HERB_OSXLangMemory_PrintToBuffer(machine->randomAccessMemory, rowSize, groupedBytes, buffer);
    ARC_ConsoleBuffer_AddChar(buffer, '\n');

    ARC_ConsoleBuffer_AddCStringWithStrlen(buffer, "Soft Drive:\n");
    HERB_OSXLangMemory_PrintToBuffer(machine->softDrive, rowSize, groupedBytes, buffer);
    ARC_ConsoleBuffer_AddChar(buffer, '\n');
}
