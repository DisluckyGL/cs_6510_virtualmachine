#include "gantt.h"

#include <arc/console/buffer.h>
#include <arc/std/vector.h>
#include <arc/std/io.h>
#include <stdio.h>
#include <stdlib.h>

struct HERB_OSXLangGanttChart {
    ARC_Vector *quantums;
};


void HERB_OSXLangGanttChart_Create(HERB_OSXLangGanttChart **ganttchart, uint32_t quantums){
    *ganttchart = (HERB_OSXLangGanttChart *)malloc(sizeof(HERB_OSXLangGanttChart));

    ARC_Vector_Create(&((*ganttchart)->quantums));
    for(uint32_t i = 0; i < quantums; i++){
        ARC_Vector *quantum;
        ARC_Vector_Create(&quantum);
        ARC_Vector_Add((*ganttchart)->quantums, (void *)quantum);
    }
}

void HERB_OSXLangGanttChart_Destroy(HERB_OSXLangGanttChart *ganttchart){
    for(uint32_t i = 0; i < ARC_Vector_Size(ganttchart->quantums); i++){
        ARC_Vector *quantum = ARC_Vector_Get(ganttchart->quantums, i);

        for(uint32_t j = 0; j < ARC_Vector_Size(quantum); j++){
            uint32_t *id = ARC_Vector_Get(quantum, j);

            free(id);
        }

        ARC_Vector_Destroy(quantum);
    }

    ARC_Vector_Destroy(ganttchart->quantums);
}

void HERB_OSXLangGanttChart_Clear(HERB_OSXLangGanttChart *ganttchart){
    uint32_t quantums = ARC_Vector_Size(ganttchart->quantums);

    HERB_OSXLangGanttChart_Destroy(ganttchart);

    ARC_Vector_Create(&((ganttchart)->quantums));
    for(uint32_t i = 0; i < quantums; i++){
        ARC_Vector *quantum;
        ARC_Vector_Create(&quantum);
        ARC_Vector_Add((ganttchart)->quantums, (void *)quantum);
    }
}

void HERB_OSXLangGanttChart_AddId(HERB_OSXLangGanttChart *ganttchart, uint32_t id, uint32_t quantum){
    if(id != ~(uint32_t)0 && quantum >= ARC_Vector_Size(ganttchart->quantums)){
        //out of bounds
        //TODO: throw an errror
        return;
    }

    for(uint32_t i = 0; i < ARC_Vector_Size(ganttchart->quantums); i++){
        ARC_Vector *quantumVector = ARC_Vector_Get(ganttchart->quantums, i);

        uint32_t *idCopy = (uint32_t *)malloc(sizeof(uint32_t));
        *idCopy = ~(uint32_t)0;

        if(i == quantum){
            *idCopy = id;
        }

        ARC_Vector_Add(quantumVector, (void *)idCopy);
    }
}

void HERB_OSXLangGanttChart_Print(HERB_OSXLangGanttChart *ganttchart){
    for(uint32_t i = 0; i < ARC_Vector_Size(ganttchart->quantums); i++){
        ARC_Vector *quantum = ARC_Vector_Get(ganttchart->quantums, i);

        for(uint32_t j = 0; j < ARC_Vector_Size(quantum); j++){
            uint32_t *id = ARC_Vector_Get(quantum, j);

            printf("%u", *id);
        }

        printf("\n");
    }
}

void HERB_OSXLangGanttChart_PrintToBuffer(HERB_OSXLangGanttChart *ganttchart, ARC_ConsoleBuffer *buffer){
    for(uint32_t i = 0; i < ARC_Vector_Size(ganttchart->quantums); i++){
        ARC_Vector *quantum = ARC_Vector_Get(ganttchart->quantums, i);

        for(uint32_t j = 0; j < ARC_Vector_Size(quantum); j++){
            uint32_t *id = ARC_Vector_Get(quantum, j);

            char idCString[11];
            sprintf(idCString, "%08x ", *id);
            ARC_ConsoleBuffer_AddCStringWithStrlen(buffer, idCString);
        }

        ARC_ConsoleBuffer_AddChar(buffer, '\n');
    }
}

void HERB_OSXLangGanttChart_PrintToFile(HERB_OSXLangGanttChart *ganttchart, ARC_String *path){
    ARC_String *outputString = NULL;

    for(uint32_t i = 0; i < ARC_Vector_Size(ganttchart->quantums); i++){
        ARC_Vector *quantum = ARC_Vector_Get(ganttchart->quantums, i);

        for(uint32_t j = 0; j < ARC_Vector_Size(quantum); j++){
            uint32_t *id = ARC_Vector_Get(quantum, j);

            char idCString[11];
            sprintf(idCString, "%08x ", *id);
            if(outputString == NULL){
                ARC_String_CreateWithStrlen(&outputString, idCString);
                continue;
            }

            ARC_String *idString;
            ARC_String_CreateWithStrlen(&idString, idCString);

            ARC_String *tempOutputString = outputString;
            ARC_String_Merge(&outputString, tempOutputString, idString);
            ARC_String_Destroy(tempOutputString);

            ARC_String_Destroy(idString);
        }

        ARC_String *newlineString;
        ARC_String_Create(&newlineString, "\n", 1);

        ARC_String *tempOutputString = outputString;
        ARC_String_Merge(&outputString, tempOutputString, newlineString);
        ARC_String_Destroy(tempOutputString);

        ARC_String_Destroy(newlineString);
    }

    ARC_IO_WriteStrToFile(path, outputString);
}

void HERB_OSXLangGanttChart_SetQuantums(HERB_OSXLangGanttChart *ganttchart, uint32_t quantums){
    HERB_OSXLangGanttChart_Destroy(ganttchart);

    ARC_Vector_Create(&((ganttchart)->quantums));
    for(uint32_t i = 0; i < quantums; i++){
        ARC_Vector *quantum;
        ARC_Vector_Create(&quantum);
        ARC_Vector_Add((ganttchart)->quantums, (void *)quantum);
    }
}

HERB_OSXLangGanttChart *herb_ganttchart = NULL;

ARC_Bool herb_useGanttchart = ARC_False;