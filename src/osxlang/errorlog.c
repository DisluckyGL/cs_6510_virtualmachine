#include "errorlog.h"

#include <arc/console/buffer.h>
#include <arc/std/vector.h>
#include <stdio.h>
#include <stdlib.h>

struct HERB_OSXLangErrorLog {
    ARC_Vector *errorVector;
};

void HERB_OSXLangErrorLog_Create(HERB_OSXLangErrorLog **errorLog){
    *errorLog = (HERB_OSXLangErrorLog *)malloc(sizeof(HERB_OSXLangErrorLog));

    ARC_Vector_Create(&((*errorLog)->errorVector));
}

void HERB_OSXLangErrorLog_Destroy(HERB_OSXLangErrorLog *errorLog){
    ARC_Vector_Destroy(errorLog->errorVector);
}

void HERB_OSXLangErrorLog_AddError(HERB_OSXLangErrorLog *errorLog, ARC_String *log){
    ARC_String *string;
    ARC_String_Copy(&string, log);

    ARC_Vector_Add(errorLog->errorVector, string);
}

void HERB_OSXLangErrorLog_AddErrorCString(HERB_OSXLangErrorLog *errorLog, char *log, uint64_t logSize){
    ARC_String *string;
    ARC_String_Create(&string, log, logSize);
}

void HERB_OSXLangErrorLog_Print(HERB_OSXLangErrorLog *errorLog){
    for(uint32_t i = 0; i < ARC_Vector_Size(errorLog->errorVector); i++){
        printf("%s\n", ((ARC_String *)ARC_Vector_Get(errorLog->errorVector, i))->data);
    }
}

void HERB_OSXLangErrorLog_PrintToBuffer(HERB_OSXLangErrorLog *errorLog, ARC_ConsoleBuffer *buffer){
    for(uint32_t i = 0; i < ARC_Vector_Size(errorLog->errorVector); i++){
        ARC_ConsoleBuffer_AddString(buffer, ((ARC_String *)ARC_Vector_Get(errorLog->errorVector, i)));
        ARC_ConsoleBuffer_AddChar(buffer, '\n');
    }
}

HERB_OSXLangErrorLog *herb_errorlog = NULL;