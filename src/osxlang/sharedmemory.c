#include "sharedmemory.h"

#include "memory.h"
#include <arc/std/errno.h>
#include <arc/std/bool.h>
#include <stdlib.h>

ARC_Bool HERB_OSXLangMemory_ReserveSharedSection(HERB_OSXLangMemory *memory, uint32_t id, uint64_t size){
    //TODO: check if id is already in use

    //get total size of what is being stored, start with one byte for the block type
    uint32_t storageSize = 1;

    //add id size
    storageSize += 4;

    //add size of blockSize
    storageSize += 4;

    //add shared memory size
    storageSize += size;

    //get number of blocks needed
    uint32_t blocksSize = storageSize / memory->blockSize;

    //add block num and each block to the header minus the initial block along with an extra byte for the block's header
    storageSize += (blocksSize - 1) * (4 + 1);

    //get new block size
    uint32_t newBlocksSize = storageSize / memory->blockSize;

    //add remainding blocks (if any)
    storageSize += (newBlocksSize - blocksSize) * (4 + 1);

    //set block size to the new block size
    blocksSize = newBlocksSize;

    //add extra block if needed
    if(storageSize % memory->blockSize != 0){
        blocksSize++;
        //this will never create a new block because memory->blockSize has to be bigger than 5
        storageSize += 4 + 1;
    }

    //create blocks and fill with max value for error checks
    uint32_t blocks[blocksSize];
    for(uint32_t i = 0; i < blocksSize; i++){
        blocks[i] = ~(uint32_t)0;
    }

    //find empty blocks
    uint32_t blockIndex = 0;
    for(uint32_t i = 0; i < memory->dataSize / memory->blockSize; i++){
        if(((uint8_t *)memory->data)[i * memory->blockSize] == HERB_OSXLANG_MEMORY_BLOCK_NONE){
            blocks[blockIndex] = i * memory->blockSize;
            blockIndex++;
            if(blockIndex >= blocksSize){
                break;
            }
        }
    }

    //return false if not all the blocks were found
    if(blockIndex != blocksSize){
        return ARC_False;
    }

    //get the starting index
    uint32_t dataIndex = 0;

    //set block type header
    memory->data[blocks[dataIndex]] = HERB_OSXLANG_MEMORY_BLOCK_SHARED;

    //reserve the data blocks
    for(uint32_t i = 1; i < blocksSize; i++){
        memory->data[blocks[i]] = HERB_OSXLANG_MEMORY_BLOCK_DATA;
    }

    //set blocks into header
    HERB_OSXLangMemory_SetBlockHeaderAtIndex(&dataIndex, memory, blocks, blocksSize);

    //set the id into the header
    HERB_OSXLangMemory_SetDataBytesToUint32InBlocks(&dataIndex, memory, blocks, blocksSize, id);

    //memory was reserved, return true
    return ARC_True;
}

void HERB_OSXLangMemory_ClearSharedSection(HERB_OSXLangMemory *memory, uint32_t id){
    uint32_t *blocks          = NULL;
    uint32_t  blocksSize      = 0;
    uint32_t  blockStartIndex = 0;
    uint32_t  currentIndex    = 0;

    for(uint32_t i = 0; i < memory->dataSize / memory->blockSize; i++){
        //skip if the current block is not shared
        if(((uint8_t *)memory->data)[i * memory->blockSize] != HERB_OSXLANG_MEMORY_BLOCK_SHARED){
            continue;
        }

        //reset the current index
        currentIndex = 0;

        //get the current block index
        blockStartIndex = i * memory->blockSize;

        //get the blocks
        HERB_OSXLangMemory_GetBlocksFromIndex(&currentIndex, &blocks, &blocksSize, memory, blockStartIndex);

        //check the shared memory id
        uint32_t blockId = HERB_OSXLangMemory_GetDataBytesAsUint32FromBlocks(&currentIndex, memory, blocks, blocksSize);
        if(id == blockId){
            break;
        }

        free(blocks);
        blocks = NULL;
        blocksSize = 0;
    }

    //clear the shared block type
    memory->data[blockStartIndex] = HERB_OSXLANG_MEMORY_BLOCK_NONE;

    //clear all data blocks
    for(uint32_t i = 0; i < blocksSize; i++){
        memory->data[blocks[i]] = HERB_OSXLANG_MEMORY_BLOCK_NONE;
    }

    if(blocks != NULL){
        free(blocks);
    }
}

void HERB_OSXLangMemory_GetSharedBlocksFromId(uint32_t id, uint32_t **blocks, uint32_t *blocksSize, HERB_OSXLangMemory *memory, uint32_t *index){
    for(uint32_t i = 0; i < memory->dataSize / memory->blockSize; i++){
        //skip if the current block is not shared
        if(((uint8_t *)memory->data)[i * memory->blockSize] != HERB_OSXLANG_MEMORY_BLOCK_SHARED){
            continue;
        }

        //set the current index to 0
        uint32_t currentIndex = *index;

        //get the current block index
        uint32_t blockStartIndex = i * memory->blockSize;

        //get the blocks
        HERB_OSXLangMemory_GetBlocksFromIndex(&currentIndex, blocks, blocksSize, memory, blockStartIndex);

        //check the shared memory id
        uint32_t blockId = HERB_OSXLangMemory_GetDataBytesAsUint32FromBlocks(&currentIndex, memory, *blocks, *blocksSize);
        if(id == blockId){
            *index = currentIndex;
            return;
        }

        free(*blocks);
        *blocks = NULL;
        *blocksSize = 0;
    }
}