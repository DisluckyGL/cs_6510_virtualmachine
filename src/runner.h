#ifndef HERB_RUNNER_H_
#define HERB_RUNNER_H_

#include <arc/std/string.h>
#include <arc/std/vector.h>
#include <arc/std/hashtable.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief the bucket size used within a runner for the hashtable
*/
#define HERB_RUNNER_BUCKET_SIZE 0x20

/**
 * @brief init function for storage within a HERB_Runner type
 *
 * @param storage function to setup needed storage type for the HERB_Runner type
*/
typedef void (* HERB_Runner_InitStorageFn)(void **storage);

/**
 * @brief deinit function for storage within a HERB_Runner type
 *
 * @param storage function to clean up storage type for the HERB_Runner type
*/
typedef void (* HERB_Runner_DeinitStorageFn)(void *storage);

/**
 * @brief function to compare instructions
 *
 * @note this is the same as ARC_Hashtable_KeyCompare, just rewritten here to make it easier on the user
 *
 * @note TODO: create ARC_Bool and start using it instead of returning int8_t
 *             also reformat hashtable to only require instructions and not their sizes for comparison
 *
 * @param instruction1     data for instruction1
 * @param instruction1size size of instruction1
 * @param instruction2     data for instruction2
 * @param instruction2size size of instruction2
 *
 * @return 0 when keys match
*/
typedef int8_t (* HERB_Runner_InstructionCompFn)(void *instruction1, size_t *instruction1size, void *instruction2, size_t *instruction2size);

/**
 * @brief instruction function for runner, used as a type for registering functions
 *
 * @note storage's type will be defined when creating a HERB_Runner
 *
 * @param storage         variable where to store what happens within an instruction function
 * @param instruction     an instruction passed as some kind of data
 * @param instructionSize size of the instruction passed
*/
typedef void (* HERB_Runner_InstructionFn)(void *storage, void *instruction, uint64_t instructionSize);

/**
 * @brief HERB_Runner type, defined in the c file to keep its contents private
*/
typedef struct HERB_Runner HERB_Runner;

/**
 * @brief instruction function for runner, used to override or edit data before running an instruction
 *
 * @param runner   gives the runner so the callback can get the current instruction function or storage
 * @param data     current instruction data
 * @param dataSize size of current instruction data
*/
typedef void (* HERB_Runner_InstructionRunFn)(HERB_Runner *runner, void *data, size_t dataSize);

/**
 * @brief creates a HERB_Runner type
 *
 * @param runner          HERB_Runner type to create
 * @param initStorageFn   init function callback for storage type
 * @param deinitStorageFn deinit function callback for storage type
*/
void HERB_Runner_Create(HERB_Runner **runner, HERB_Runner_InitStorageFn initStorageFn, HERB_Runner_DeinitStorageFn deinitStorageFn, HERB_Runner_InstructionCompFn instructionCompFn);

/**
 * @brief destroys a HERB_Runner type
 *
 * @param runner HERB_Runner type to destory
*/
void HERB_Runner_Destroy(HERB_Runner *runner);

/**
 * @brief registers an instruction to a HERB_Runner
 *
 * @param runner        runner to register instruction to
 * @param keyword       data that when found will get the instructionFN
 * @param keywordSize   size of keyword data to check
 * @param instructionFn function to run if keyword is found
*/
void HERB_Runner_RegisterInstruction(HERB_Runner *runner, void *keyword, size_t keywordSize, HERB_Runner_InstructionFn instructionFn);

/**
 * @brief runs through given data using a HERB_Runner
 *
 * @param runner               HERB_Runner to use when running
 * @param data                 data that HERB_Runner will use to run
 * @param dataSize             size of data that HERB_Runner will use to run
 * @param keywordSeparator     data that signifies the end of a keyword
 * @param keywordSeparatorSize size of keyword separator data
 * @param instructionEnd       data that signifies the end of an instruction
 * @param instructionEndSize   size of instruction end data
 * @param instructionRunFn     function that is ran in place of found HERB_Runner_InstructionFn, when set to NULL the default HERB_Runner_InstructionFn will run
*/
void HERB_Runner_Run(HERB_Runner *runner, void *data, uint64_t dataSize, void *keywordSeparator, void *keywordSeparatorSize, void *instructionEnd, size_t *instructionEndSize, HERB_Runner_InstructionRunFn instructionRunFn);

/**
 * @brief runs through given instruction data using a HERB_Runner
 *
 * @param runner               HERB_Runner to use when running
 * @param data                 data that HERB_Runner will use to run
 * @param dataSize             size of data that HERB_Runner will use to run
 * @param keywordSeparator     data that signifies the end of a keyword
 * @param keywordSeparatorSize size of keyword separator data
 * @param instructionRunFn     function that is ran in place of found HERB_Runner_InstructionFn, when set to NULL the default HERB_Runner_InstructionFn will run
*/
void HERB_Runner_RunSingle(HERB_Runner *runner, void *data, uint64_t dataSize, void *keywordSeparator, void *keywordSeparatorSize, HERB_Runner_InstructionRunFn instructionRunFn);

/**
 * @brief gets the storage from a HERB_Runner
 *
 * @param runner HERB_Runner to get storage from
 *
 * @return the storage a HERB_Runner has as a void *
*/
void *HERB_Runner_GetStorage(HERB_Runner *runner);

/**
 * @brief gets a HERB_Runner_InstructionFn from a HERB_Runner with a given keyword
 *
 * @note this is very useful if you use instructionRunFn when running
 *
 * @param runner      runner to get HERB_Runner_InstructionFn from
 * @param keyword     keyword to get matching HERB_Runner_InststructionFn from
 * @param keywordSize size of keyword
 *
 * @return a HERB_Runner_InstructionFn if keyword was found, NULL on failure
*/
HERB_Runner_InstructionFn HERB_Runner_GetInstructionFn(HERB_Runner *runner, void *keyword, size_t keywordSize);

#ifdef __cplusplus
}
#endif

#endif // !HERB_OSXLANG_H_