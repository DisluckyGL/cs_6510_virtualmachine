#include "bufferpoll.h"

#include <arc/console/view.h>
#include <arc/console/buffer.h>
#include <arc/console/key.h>
#include <arc/math/point.h>
#include <arc/math/rectangle.h>
#include <arc/std/bool.h>
#include <arc/std/errno.h>
#include <pthread.h>
#include <stdint.h>
#include <unistd.h>

typedef struct HERB_ConsoleBuffer_RunPollParams {
    uint32_t *pollTime;
    HERB_ConsoleBufferPollFn *pollFn;
    void *userdata;
    ARC_Bool *running;
    pthread_mutex_t *pollMutex;
    ARC_Point *cursorPos;
    ARC_ConsoleView *view;
    ARC_ConsoleBuffer *buffer;
} HERB_ConsoleBuffer_RunPollParams;

void *HERB_ConsoleBuffer_RunPoll(void *voidPollParams){
    HERB_ConsoleBuffer_RunPollParams *pollParams = (HERB_ConsoleBuffer_RunPollParams *)voidPollParams;

    pthread_mutex_lock(pollParams->pollMutex);
    ARC_Rect viewBounds = ARC_ConsoleView_GetBounds(pollParams->view);
    pthread_mutex_unlock(pollParams->pollMutex);

    while(*(pollParams->running)){
        if(pollParams->pollFn != NULL){
            pthread_mutex_lock(pollParams->pollMutex);
            (*(pollParams->pollFn))(pollParams->userdata);
            pthread_mutex_unlock(pollParams->pollMutex);
        }

        pthread_mutex_lock(pollParams->pollMutex);
        ARC_ConsoleView_Clear(pollParams->view);
        ARC_ConsoleBuffer_RenderSection(pollParams->buffer, pollParams->view, pollParams->cursorPos->y, viewBounds.h - 1);
        pthread_mutex_unlock(pollParams->pollMutex);

        if(pollParams->pollTime != NULL){
            sleep(*(pollParams->pollTime));
        }
    }

    return NULL;
}

void HERB_ConsoleBuffer_Poll(ARC_ConsoleView *view, ARC_ConsoleBuffer *buffer, ARC_Point *cursorPos, HERB_ConsoleBufferInputFn inputFn, uint32_t *pollTime, HERB_ConsoleBufferPollFn *pollFn, void *userdata){
    pthread_t pollThread;
    pthread_mutex_t pollMutex;

    //create the mutex
    if(pthread_mutex_init(&pollMutex, NULL) != 0){
        arc_errno = ARC_ERRNO_INIT;
        ARC_DEBUG_ERR("HERB_ConsoleBuffer_Poll(view, buffer, pollTime, pollFn, userdata), could not init poll mutex");
        return;
    }

    ARC_Rect viewBounds = ARC_ConsoleView_GetBounds(view);

    //create the poll threads params
    ARC_Bool pollRunning = ARC_True;
    HERB_ConsoleBuffer_RunPollParams pollParams = {
        pollTime,
        pollFn,
        userdata,
        &pollRunning,
        &pollMutex,
        cursorPos,
        view,
        buffer
    };

    //create the poll thread
    int32_t pollThreadReturnValue = pthread_create(&pollThread, NULL, HERB_ConsoleBuffer_RunPoll, (void *)&pollParams);
    if(pollThreadReturnValue != 0){
        arc_errno = ARC_ERRNO_INIT;
        ARC_DEBUG_ERR("HERB_ConsoleBuffer_Poll(view, buffer, pollTime, pollFn, userdata), could not init poll pthread");
        return;
    }

    //get the input and call the users input function
    ARC_Bool running = ARC_True;
    while(running){
        //get the users input
        ARC_ConsoleKey *key = ARC_ConsoleView_GetCreateConsoleKeyAt(view, *cursorPos);

        //redraw the buffer at next position
        pthread_mutex_lock(&pollMutex);
        running = inputFn(key, userdata);

        ARC_ConsoleView_Clear(view);
        ARC_ConsoleBuffer_RenderSection(buffer, view, cursorPos->y, viewBounds.h - 1);
        pthread_mutex_unlock(&pollMutex);
    }

    pollRunning = ARC_False;
    pthread_join(pollThread, NULL);
}

void HERB_ConsoleBuffer_PollAndRunScroll(ARC_ConsoleView *view, ARC_ConsoleBuffer *buffer, uint32_t *pollTime, HERB_ConsoleBufferPollFn *pollFn, void *userdata){
    pthread_t pollThread;
    pthread_mutex_t pollMutex;

    //create the mutex
    if(pthread_mutex_init(&pollMutex, NULL) != 0){
        arc_errno = ARC_ERRNO_INIT;
        ARC_DEBUG_ERR("HERB_ConsoleBuffer_PullAndRunScroll(view, buffer, pollTime, pollFn, userdata), could not init poll mutex");
        return;
    }

    //get cursor information for the buffer
    //TODO: get the cursor pos.y as size - view.height
    ARC_Point cursorPos = { 0, 0 };
    ARC_Rect viewBounds = ARC_ConsoleView_GetBounds(view);

    //create the poll threads params
    ARC_Bool pollRunning = ARC_True;
    HERB_ConsoleBuffer_RunPollParams pollParams = {
        pollTime,
        pollFn,
        userdata,
        &pollRunning,
        &pollMutex,
        &cursorPos,
        view,
        buffer
    };

    //create the poll thread
    int32_t pollThreadReturnValue = pthread_create(&pollThread, NULL, HERB_ConsoleBuffer_RunPoll, (void *)&pollParams);
    if(pollThreadReturnValue != 0){
        arc_errno = ARC_ERRNO_INIT;
        ARC_DEBUG_ERR("HERB_ConsoleBuffer_PullAndRunScroll(view, buffer, pollTime, pollFn, userdata), could not init poll pthread");
        return;
    }

    //get the input and move the buffer output
    //TODO: move the buffer output
    ARC_Bool running = ARC_True;
    while(running){
        //get the users input
        ARC_ConsoleKey *key = ARC_ConsoleView_GetCreateConsoleKeyAt(view, (ARC_Point){ 0, 0 });

        //check to see if it should quit
        if(ARC_ConsoleKey_EqualsPointer(key, ARC_KEY_ESC)){
            running = ARC_False;
            pollRunning = ARC_False;
            break;
        }

        //check to see if it should go up
        else if(ARC_ConsoleKey_EqualsPointer(key, ARC_KEY_UP) || ARC_ConsoleKey_EqualsPointer(key, ARC_KEY_K)){
            if(cursorPos.y > 0){
                cursorPos.y--;
            }
        }

        //check to see if it should go down
        else if(ARC_ConsoleKey_EqualsPointer(key, ARC_KEY_DOWN) || ARC_ConsoleKey_EqualsPointer(key, ARC_KEY_J)){
            if(cursorPos.y < ARC_ConsoleBuffer_GetLineNumbers(buffer) - viewBounds.h){
                cursorPos.y++;
            }
        }

//        //check to see if it should go left
//        else if(ARC_ConsoleKey_EqualsPointer(key, ARC_KEY_LEFT)){
//            if(cursorPos.x > 0){
//                cursorPos.x--;
//            }
//        }
//
//        //check to see if it should go left
//        else if(ARC_ConsoleKey_EqualsPointer(key, ARC_KEY_RIGHT)){
//            if(cursorPos.x < viewBounds.w - 1){
//                cursorPos.x++;
//            }
//        }

        //redraw the buffer at next position
        pthread_mutex_lock(&pollMutex);
        ARC_ConsoleView_Clear(view);
        ARC_ConsoleBuffer_RenderSection(buffer, view, cursorPos.y, viewBounds.h);
        pthread_mutex_unlock(&pollMutex);

        ARC_ConsoleKey_Destroy(key);
    }

    pthread_join(pollThread, NULL);
}
