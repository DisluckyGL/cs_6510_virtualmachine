#ifndef HERB_STATE_H_
#define HERB_STATE_H_

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief predefines HERB_State so it can be used in its own callback
*/
typedef struct HERB_State HERB_State;

/**
 * @brief main function callback for HERB_State type, used to pass through other states to create a state machine
*/
typedef void (* HERB_State_MainFn)(void *userdata);

/**
 * @brief HERB_State type which is used as a state machine. Each state has a main function callback and data
*/
struct HERB_State {
    HERB_State_MainFn mainFn;

    void *data;
};

#ifdef __cplusplus
}
#endif

#endif // !HERB_STATE_H_