#ifndef HERB_VIRTUAL_MACHINE_H_
#define HERB_VIRTUAL_MACHINE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "osxlang/machine.h"
#include "osxlang/process/states.h"
#include <pthread.h>
#include <arc/console/view.h>

/**
 * @brief time to sleep the processState when it is idle
*/
#define HERB_VIRTUAL_MACHINE_IDLE_SLEEP_TIME 1

/**
 * @brief the HERB_VirtualMachine type, holds everything needed to run a virtual machine with multiple threads
*/
typedef struct HERB_VirtualMachine {
    HERB_OSXLangMachine *machine;

    HERB_OSXLangProcessState *processState;

    ARC_ConsoleView *consoleView;

    pthread_t shell;

    uint32_t shellsRunning;

    pthread_mutex_t machineMutex;
    pthread_mutex_t processStateMutex;
    pthread_mutex_t shellsRunningMutex;
} HERB_VirtualMachine;

/**
 * @brief creates a HERB_VirtualMachine type
 *
 * @param virtualMachine the HERB_VirtualMachine to create
*/
void HERB_VirtualMachine_Create(HERB_VirtualMachine **virtualMachine);

/**
 * @brief destroys a HERB_VirtualMachine type
 *
 * @param virtualMachine the HERB_VirtualMachine to destroy
*/
void HERB_VirtualMachine_Destroy(HERB_VirtualMachine *virtualMachine);

/**
 * @brief runs a HERB_VirtualMachine
 *
 * @note the virtual machine will run on two threads, one for the prosse state machine, and one for the shell
 *
 * @param virtualMachine the HERB_VirtualMachine to run
*/
void HERB_VirtualMachine_Run(HERB_VirtualMachine *virtualMachine);

#ifdef __cplusplus
}
#endif

#endif // !HERB_VIRTUAL_MACHINE_H_