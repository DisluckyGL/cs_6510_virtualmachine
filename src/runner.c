#include "runner.h"

#include <arc/std/errno.h>
#include <stdlib.h>

struct HERB_Runner {
    void          *storage;
    ARC_Hashtable *instructions;

    HERB_Runner_DeinitStorageFn   deinitStorageFn;
    HERB_Runner_InstructionCompFn instructionCompFn;
};

void HERB_Runner_Create(HERB_Runner **runner, HERB_Runner_InitStorageFn initStorageFn, HERB_Runner_DeinitStorageFn deinitStorageFn, HERB_Runner_InstructionCompFn instructionCompFn){
    *runner = (HERB_Runner *)malloc(sizeof(HERB_Runner));

    initStorageFn(&((*runner)->storage));
    (*runner)->deinitStorageFn = deinitStorageFn;
    (*runner)->instructionCompFn = instructionCompFn;
    ARC_Hashtable_Create(&((*runner)->instructions), HERB_RUNNER_BUCKET_SIZE, NULL, instructionCompFn);
}

void HERB_Runner_Destroy(HERB_Runner *runner){
    //TODO: define external
    ARC_Hashtable_Destroy(runner->instructions, NULL, NULL);
    runner->deinitStorageFn(runner->storage);
    free(runner);
}

void HERB_Runner_RegisterInstruction(HERB_Runner *runner, void *keyword, size_t keywordSize, HERB_Runner_InstructionFn instructionFn){
    ARC_Hashtable_Add(runner->instructions, keyword, keywordSize, instructionFn);
}

void HERB_Runner_Run(HERB_Runner *runner, void *data, uint64_t dataSize, void *keywordSeparator, void *keywordSeparatorSize, void *instructionEnd, size_t *instructionEndSize, HERB_Runner_InstructionRunFn instructionRunFn){
    void *instructionData = data;
    void *keyword = data;
    uint64_t instructionDataSize = 0;
    uint64_t keywordSize = 0;
    uint64_t alreadySearchedInstructionDataSize = 0;

    HERB_Runner_InstructionFn instructionFn = NULL;

    for(int i = 0; i < dataSize; i++){
        //TODO: check with separtor for instructionFn
        if(instructionRunFn == NULL && instructionFn == NULL){
            keywordSize = i - alreadySearchedInstructionDataSize;

            instructionFn = HERB_Runner_GetInstructionFn(runner, keyword, keywordSize);
        }

        instructionDataSize = i - alreadySearchedInstructionDataSize;
        //TODO: 0 means they are equal... yeah I know I f***ed this up, but I don't have time to fix rn
        if(runner->instructionCompFn(instructionData + instructionDataSize, instructionEndSize, instructionEnd, instructionEndSize) == 0){
            if(instructionRunFn == NULL && instructionFn == NULL){
                arc_errno = ARC_ERRNO_NULL;
                ARC_DEBUG_ERR("HERB_Runner_Run(runner, data, dataSize, keywordSeparator, keywordSeparatorSize, instructionEnd, instructionEndSize, instructionRunFn), instructionFn and instructionRunFn were NULL so instruction cannot be run");
                return;
            }

            if(instructionRunFn == NULL){
                void *instruction = NULL;
                instructionFn(runner->storage, instruction, instructionDataSize);
            }
            else {
                instructionRunFn(runner, instructionData, instructionDataSize);
            }

            keyword = data + i + *instructionEndSize;
            instructionData = data + i + *instructionEndSize;
            alreadySearchedInstructionDataSize = i;
            instructionFn = NULL;
        }
    }
}

void HERB_Runner_RunSingle(HERB_Runner *runner, void *data, uint64_t dataSize, void *keywordSeparator, void *keywordSeparatorSize, HERB_Runner_InstructionRunFn instructionRunFn){
    void *instructionData = data;
    HERB_Runner_InstructionFn instructionFn = NULL;

    //TODO: check with separtor for instructionFn
    for(int i = 0; i < dataSize; i++){
        if(instructionRunFn == NULL && instructionFn == NULL){
            instructionFn = HERB_Runner_GetInstructionFn(runner, data, i);
            break;
        }
    }

    if(instructionRunFn == NULL && instructionFn == NULL){
        arc_errno = ARC_ERRNO_NULL;
        ARC_DEBUG_ERR("HERB_Runner_RunSingle(runner, data, dataSize, keywordSeparator, keywordSeparatorSize, instructionRunFn), instructionFn and instructionRunFn were NULL so instruction cannot be run");
        return;
    }

    if(instructionRunFn == NULL){
        void *instruction = NULL;
        instructionFn(runner->storage, instruction, dataSize);
        return;
    }

    instructionRunFn(runner, instructionData, dataSize);
}

void *HERB_Runner_GetStorage(HERB_Runner *runner){
    return runner->storage;
}

HERB_Runner_InstructionFn HERB_Runner_GetInstructionFn(HERB_Runner *runner, void *keyword, size_t keywordSize){
    HERB_Runner_InstructionFn instructionFn = NULL;

    ARC_Hashtable_Get(runner->instructions, keyword, keywordSize, (void **)&instructionFn);
    if(arc_errno){
        arc_errno = 0;
        instructionFn = NULL;
    }

    return instructionFn;
}