#include "virtualmachine.h"
#include "osxlang/errorlog.h"
#include "osxlang/gantt.h"
#include "osxlang/assembler/runner.h"
#include <arc/std/io.h>
#include <arc/std/string.h>

int main(int32_t argc, char **argv){
    HERB_OSXLangErrorLog_Create(&herb_errorlog);

    if(argc == 3){
        ARC_String *path;
        ARC_String_CreateWithStrlen(&path, argv[1]);
        // ARC_String_CreateWithStrlen(&path, "temp.asm");

        ARC_String *tempString;
        ARC_IO_FileToStr(path, &tempString);
        ARC_String_Destroy(path);

        ARC_String *outPath;
        ARC_String_CreateWithStrlen(&outPath, "temp.osx");

        ARC_String *loaderAddressStr;
        ARC_String_CreateWithStrlen(&loaderAddressStr, argv[2]);
        // ARC_String_CreateWithStrlen(&loaderAddressStr, "0");
        uint32_t loaderAddress = (uint32_t) ARC_String_ToUint64_t(loaderAddressStr);
        ARC_String_Destroy(loaderAddressStr);

        HERB_OSXLangRunnerAssembler_Runner(tempString, outPath, loaderAddress);

        ARC_String_Destroy(outPath);
        ARC_String_Destroy(tempString);
    }
    else {
        HERB_OSXLangGanttChart_Create(&herb_ganttchart, 1);

        HERB_VirtualMachine *virtualMachine;
        HERB_VirtualMachine_Create(&virtualMachine);

        HERB_VirtualMachine_Run(virtualMachine);

        HERB_VirtualMachine_Destroy(virtualMachine);

        HERB_OSXLangGanttChart_Destroy(herb_ganttchart);
    }

    HERB_OSXLangErrorLog_Destroy(herb_errorlog);
    return 0;
}