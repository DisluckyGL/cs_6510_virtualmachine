#ifndef HERB_BUFFER_POLL_H_
#define HERB_BUFFER_POLL_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <arc/console/buffer.h>
#include <arc/console/view.h>
#include <arc/console/key.h>
#include <stdint.h>

/**
 * @brief a callback for polling
 *
 * @param userdata data the user of the callback provides and can use
*/
typedef void (* HERB_ConsoleBufferPollFn)(void *userdata);

/**
 * @brief a callback for polling
 *
 * @param userdata   data the user of the callback provides and can use
 * @param consoleKey the current console key input by the user
 *
 * @return ARC_False when the program is done needing input (usually will end a poll function if used in a poll function)
*/
typedef ARC_Bool (* HERB_ConsoleBufferInputFn)(ARC_ConsoleKey *consoleKey, void *userdata);

/**
 * @brief runs a buffer in a poll mode (the buffer is updated with polling)
 *
 * @note will exit this function when HERB_ConsoleBufferInputFn returns ARC_False
 *
 * @param view      the view to output the buffer to
 * @param buffer    the buffer to poll
 * @param cursorPos the posistion to place to cursor
 * @param input     the input function that controlls key input and when the program ends
 * @param pollTime  the time in seconds between each poll of the buffer, set to NULL to not have a timeout
 * @param pollFn    the poll function that runs when pollTime is hit, can be NULL
*/
void HERB_ConsoleBuffer_Poll(ARC_ConsoleView *view, ARC_ConsoleBuffer *buffer, ARC_Point *cursorPos, HERB_ConsoleBufferInputFn inputFn, uint32_t *pollTime, HERB_ConsoleBufferPollFn *pollFn, void *userdata);

/**
 * @brief runs a buffer in a scroll mode (move buffer with arrow keys)
 *
 * @param view     the view to output the buffer to
 * @param buffer   the buffer to scroll
 * @param pollTime the time in seconds between each poll of the buffer, set to NULL to not have a timeout
 * @param pollFn   the poll function that runs when pollTime is hit, can be NULL
*/
void HERB_ConsoleBuffer_PollAndRunScroll(ARC_ConsoleView *view, ARC_ConsoleBuffer *buffer, uint32_t *pollTime, HERB_ConsoleBufferPollFn *pollFn, void *userdata);

#ifdef __cplusplus
}
#endif

#endif // !HERB_BUFFER_POLL_H_