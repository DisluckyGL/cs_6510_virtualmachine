#include "virtualmachine.h"

#include "osxlang/shell/runner.h"
#include <arc/math/rectangle.h>
#include <arc/std/errno.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>

void HERB_VirtualMachine_Create(HERB_VirtualMachine **virtualMachine){
    *virtualMachine = (HERB_VirtualMachine *)malloc(sizeof(HERB_VirtualMachine));

    HERB_OSXLangMachine_Create(&((*virtualMachine)->machine), 5120, 5120);

    HERB_OSXLangProcessState_Create(&((*virtualMachine)->processState), (*virtualMachine)->machine);

    ARC_ConsoleView_Create(&((*virtualMachine)->consoleView), (ARC_Rect){ 0, 0, 0, 0});

    (*virtualMachine)->shellsRunning = 0;

    if(pthread_mutex_init(&((*virtualMachine)->machineMutex), NULL) != 0){
        ARC_DEBUG_ERR("HERB_VirtualMachine_Run(virtualMachine), could not init machine mutex");

        HERB_OSXLangProcessState_Destroy((*virtualMachine)->processState);
        HERB_OSXLangMachine_Destroy((*virtualMachine)->machine);
        free(*virtualMachine);
        *virtualMachine = NULL;
        return;
    }

    if(pthread_mutex_init(&((*virtualMachine)->processStateMutex), NULL) != 0){
        ARC_DEBUG_ERR("HERB_VirtualMachine_Run(virtualMachine), could not init process state mutex");

        pthread_mutex_destroy(&((*virtualMachine)->machineMutex));

        HERB_OSXLangProcessState_Destroy((*virtualMachine)->processState);
        HERB_OSXLangMachine_Destroy((*virtualMachine)->machine);
        free(*virtualMachine);
        *virtualMachine = NULL;
        return;
    }

    if(pthread_mutex_init(&((*virtualMachine)->shellsRunningMutex), NULL) != 0){
        ARC_DEBUG_ERR("HERB_VirtualMachine_Run(virtualMachine), could not init shells running mutex");

        pthread_mutex_destroy(&((*virtualMachine)->machineMutex));
        pthread_mutex_destroy(&((*virtualMachine)->processStateMutex));

        HERB_OSXLangProcessState_Destroy((*virtualMachine)->processState);
        HERB_OSXLangMachine_Destroy((*virtualMachine)->machine);
        free(*virtualMachine);
        *virtualMachine = NULL;
        return;
    }
}

void HERB_VirtualMachine_Destroy(HERB_VirtualMachine *virtualMachine){
    pthread_mutex_destroy(&(virtualMachine->shellsRunningMutex));
    pthread_mutex_destroy(&(virtualMachine->processStateMutex));
    pthread_mutex_destroy(&(virtualMachine->machineMutex));

    ARC_ConsoleView_Destroy(virtualMachine->consoleView);

    HERB_OSXLangProcessState_Destroy(virtualMachine->processState);
    HERB_OSXLangMachine_Destroy(virtualMachine->machine);
    free(virtualMachine);
}

void HERB_VirtualMachine_Run(HERB_VirtualMachine *virtualMachine){
    virtualMachine->shellsRunning++;

    //create prams for the shell
    HERB_OSXLangRunnerShellParams shellParams = {
        virtualMachine->machine,
        virtualMachine->processState,
        virtualMachine->consoleView,
        "SHELL > ",
        &(virtualMachine->shellsRunning),
        &(virtualMachine->machineMutex),
        &(virtualMachine->shellsRunningMutex)
    };

    //create and check the shell thread
    int32_t shellThreadReturnValue = pthread_create(&(virtualMachine->shell), NULL, HERB_OSXLangRunnerShell_Start, (void *)&shellParams);
    if(shellThreadReturnValue != 0){
        arc_errno = ARC_ERRNO_INIT;
        ARC_DEBUG_ERR("HERB_VirtualMachine_Run(virtualMachine), could not init shell pthread");
        return;
    }

    //get value of shells running
    pthread_mutex_lock(&(virtualMachine->shellsRunningMutex));
    uint32_t shellsRunning = virtualMachine->shellsRunning;
    pthread_mutex_unlock(&(virtualMachine->shellsRunningMutex));

    //run the process
    while(shellsRunning != 0){
        //run the current state
        pthread_mutex_lock(&(virtualMachine->machineMutex));
        HERB_OSXLangProcessState_RunCurrentState(virtualMachine->processState);
        uint32_t processSize = HERB_OSXLangProcessState_GetProcessSize(virtualMachine->processState);
        pthread_mutex_unlock(&(virtualMachine->machineMutex));

        if(processSize == 0){
            sleep(HERB_VIRTUAL_MACHINE_IDLE_SLEEP_TIME);
        }

        //get the value of shells running
        pthread_mutex_lock(&(virtualMachine->shellsRunningMutex));
        shellsRunning = virtualMachine->shellsRunning;
        pthread_mutex_unlock(&(virtualMachine->shellsRunningMutex));
    }

    //join the threads on end
    pthread_join(virtualMachine->shell, NULL);
}
