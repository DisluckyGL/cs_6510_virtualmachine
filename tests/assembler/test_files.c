#include "../test.h"

ARC_TEST(newTest){
    int foo = 12;
    ARC_CHECK(12 == foo);
}

ARC_TEST(number2Test){
    char foo = 'a';
    ARC_CHECK('b' == foo);
    ARC_CHECK('a' == foo);
    ARC_CHECK('c' == foo);
}