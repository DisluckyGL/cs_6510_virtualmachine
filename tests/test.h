#ifndef ARC_TEST_H_
#define ARC_TEST_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdio.h>

extern uint32_t *temp_arc_test_num_checks_run__ARC_TEST__;
extern uint32_t *temp_arc_test_num_checks_passed__ARC_TEST__;

#define ARC_TEST_START_MESSAGE(MESSAGE) void ARC_TEST_START_MESSAGE__ ## MESSAGE() __attribute__ ((constructor));\
    uint32_t arc_test_num_checks_run__ARC_TEST__ ## MESSAGE = 0;\
    uint32_t arc_test_num_checks_passed__ARC_TEST__ ## MESSAGE = 0;\
    void ARC_TEST_START_MESSAGE__ ## MESSAGE(){\
        printf("[ARC TEST] Running: %s\n", #MESSAGE);\
        temp_arc_test_num_checks_run__ARC_TEST__ = &arc_test_num_checks_run__ARC_TEST__ ## MESSAGE; \
        temp_arc_test_num_checks_passed__ARC_TEST__ = &arc_test_num_checks_passed__ARC_TEST__ ## MESSAGE; \
    } 

#define ARC_TEST_END_MESSAGE(MESSAGE) void ARC_TEST_END_MESSAGE__ ## MESSAGE() __attribute__ ((destructor)); void ARC_TEST_END_MESSAGE__ ## MESSAGE(){\
    printf("[ARC TEST] RUN: %u, PASSED: %u, Completed: %s\n", arc_test_num_checks_run__ARC_TEST__ ## MESSAGE, arc_test_num_checks_passed__ARC_TEST__ ## MESSAGE, #MESSAGE); } 

#define ARC_TEST(MESSAGE) ARC_TEST_START_MESSAGE(MESSAGE) ARC_TEST_END_MESSAGE(MESSAGE) void ARC_TEST__ ## MESSAGE() __attribute__ ((constructor)); void ARC_TEST__ ## MESSAGE ()

#define ARC_CHECK(TEST)\
    if(temp_arc_test_num_checks_run__ARC_TEST__ == NULL || temp_arc_test_num_checks_passed__ARC_TEST__ == NULL){ return; }\
    printf("\t%u) ", *temp_arc_test_num_checks_run__ARC_TEST__);\
    ++*temp_arc_test_num_checks_run__ARC_TEST__;\
    if(TEST){ printf("PASS\n"); ++*temp_arc_test_num_checks_passed__ARC_TEST__; }\
    else { printf("FAIL\n"); }

#ifdef __cplusplus
}
#endif

#endif // !ARC_TEST_H_