; this file is part of a series written by Brendan Smith to help testing the Linux-compatible assembler writen by Heber.
; Are there any invalid amounts of white space to have? Let's try several.
; EXPECTED OUTPUT: assembles, runs.
MVI R1 0 ; accumulator
MVI R2 1 ; step size
 ADD R1 R1 R2
  ADD R1 R1 R2
   ADD R1 R1 R2
    ADD R1 R1 R2
     ADD R1 R1 R2
      ADD R1 R1 R2
       ADD R1 R1 R2
















               ADD R1 R1 R2
ADD R1 R1 R2
       ADD R1 R1 R2
                                                                                                    ;ahoy there
                                                                                SWI 0