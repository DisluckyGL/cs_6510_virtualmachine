; this file is part of a series written by Brendan Smith to help testing the Linux-compatible assembler writen by Heber.
; If a label is provided but not defined, is a meainingful error displayed to the user?
; EXPECTED OUTPUT: fails to assemble, meaningful error is presented to the user
MVI R1 0
MVI R2 1
B UNDEFINIED_LABEL
ADD R1 R1 R2 ; this line should _not_ be reached
SWI 0