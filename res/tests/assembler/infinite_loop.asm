; this file is part of a series written by Brendan Smith to help testing the Linux-compatible assembler writen by Heber.
; Should loop infinitely
; EXPECTED OUTPUT: builds and runs. If left to run for several minutes, continues running. R1 has different values depending on how long the program runs
MVI R1 0 ; accumulator
MVI R2 1 ; step size
LOOP
ADD R1 R1 R2
B LOOP
SWI 0