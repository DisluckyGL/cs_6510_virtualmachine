; this file is part of a series written by Brendan Smith to help testing the Linux-compatible assembler writen by Heber.
; this file will attempt to break things via comments
; EXPECTED OUTPUT: builds and runs without errors, R1 contains 10,000
    ; indented comment

 ;leading space
;
 ; ^empty comment
MVI R1 0 ; comment following command
; comment containing semicolon; how mischievous
                                                    ; lots of leading space
;!@#$%^&*()_+

SWI 0