; this file is part of a series written by Brendan Smith to help testing the Linux-compatible assembler writen by Heber.
; this file attempts to use each documented command once.
; EXPECTED OUTPUT: builds and runs without errors
START ADD R1 R2 R3
SUB R1 R2 R3
MUL R1 R2 R3
DIV R1 R2 R3

MOV R1 R2
MVI R1 43981 ; 0xBADBAD
ADR R1 START

STR R2 R1
STRB R2 R1
LDR R2 R1
LDRB R2 R1

B BRANCH
BRANCH ; label
BL BRANCH_LINK
BRANCH_LINK ; label
ADR R1 BRANCH_REG
BX R1
BRANCH_REG ; label
BNE NE
NE ; label
BGT GT
GT ; label
BLT LT
LT ; label
BEQ EQ
EQ ; label

CMP R1 R1
AND R1 R2
ORR R1 R2
EOR R1 R2
SWI 0
