; this file is part of a series written by Brendan Smith to help testing the Linux-compatible assembler writen by Heber.
; this file contains simple assembly code that should probably pass. 
; EXPECTED OUTPUT: builds and runs. R1 contains 50 at end of execution.
MVI R1 0
MVI R2 100
ADD R1 R1 R2 ; R1: 100

MVI R2 25
SUB R1 R1 R2 ; R1: 75

MVI R2 3
DIV R1 R1 R2 ; R1: 25

MVI R2 2
MUL R1 R1 R2 ; 50

SWI 0