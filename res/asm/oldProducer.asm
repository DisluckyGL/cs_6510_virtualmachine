FILE_ADDRESS .STRING "test_data/consumer.osx"
; TODO: init shmem
MVI R0 9 ; shmem_init mode
SWI 0 ; syscall. TThe location of that shmem is written to R3.
MOV R6 R3 ; copy shmem_loc to R4 for safe keeping

; TODO: init semaphore
MVI R0 11 ; semaphore init mode
MVI R1 5 ; semaphore max value
SWI 0 ; syscall. Semaphore ID is written to R3
MOV R5 R3 ; copy Semaphore ID to R5 for safe keeping

; whoami, to later determine whether this is the parent
MVI R0 4 ; whoami mode
SWI 0 ; syscall. PID is written to R3.

; fork
MVI R0 3 ; fork
SWI 0 ;syscall. PID is written to R0.

; compare, to determine whether this is the parent
CMP R0 R3 ; compare PID (r0) with pre-fork pid (r3)
; if child, jump to an exec at the end
BNE CONSUMER

; if parent, increment semaphore in loop
MVI R0 12 ; increment semaphore mode
MOV R1 R5 ; copy semaphore ID to R1, where it's required
MVI R2 0 ; accumulator
MVI R4 1; step size
MVI R5 10 ; target

; loop body. Count up, filling shared mem, until ten loops.
LOOP SWI 0 ; Increment semaphore. Writes semaphore value to R3.
ADD R2 R2 R4 ; increment accumulator
ADD R3 R3 R6 ; calculate target location
STRB R3 R2 ; write byte [R2] to location R3
CMP R2 R5 ; compare
BNE LOOP; return to head

; if parent, wait
MVI R0 6 ; wait mode
SWI 0 ;syscall

; exit
MVI R0 0; exit mode
SWI 0 ; syscall

; exec(consumer.osx)
CONSUMER MVI R0 5 ; exec mode
MVI R1 FILE_ADDRESS 
MVI R2 22 ; num chars in filepath
SWI 0 ; syscall