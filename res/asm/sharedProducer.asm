SHARED_MEMORY_CREATED .STRING "shared memory created!"

MVI R0 1  ; set the shared memory id to 1
MVI R1 72 ; set the shared memory size to 72
SWI 5     ; create shared memory

MVI R2 0 ; set the location to store byte to 0
MVI R1 65; set the byte value to 'A'
SWI 7    ; store to shared memory

ADR R0 SHARED_MEMORY_CREATED ; move the string to R0
SWI 2                        ; print the string
SWI 3                        ; wait for some user input

SWI 0 ;end