SHARED_MEMORY_GOT .STRING "got value from shared memory\n"
SHARED_MEMORY_CLEARED .STRING "shared memory cleared!"

MVI R2 0; set the location to get byte to 0
MVI R0 1; set the shared memory id to 1
SWI 8   ; get from shared memory

MOV R0 R1 ; move the shared value to R0 to print
SWI 2     ; print the shared value
SWI 3     ; wait for some user input

MVI R0 1  ; set the shared memory id to 1
SWI 6     ; clear shared memory

ADR R0 SHARED_MEMORY_CLEARED ; move the string to R0
SWI 2                        ; print the string
SWI 3                        ; wait for some user input


SWI 0 ;end