HELLO_WORLD .STRING "Hello World!"
TEST_CHAR .BYTE 'A'

ADR R0 HELLO_WORLD ;move the hello world string to R0
SWI 2 ;print the HELLO_WORLD
SWI 3 ;wait for some user input

ADR R0 TEST_CHAR ;move the test char to R0
SWI 1 ;print the TEST_CHAR
SWI 3 ;wait for some user input

ADR R0 HELLO_WORLD ;move the hello world string to R0
SWI 2 ;print the HELLO_WORLD
SWI 3 ;wait for some user input

SWI 0 ;end
