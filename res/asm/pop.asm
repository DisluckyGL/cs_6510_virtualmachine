MayBe .SPACE 100
Zombie .WORD 666
.SPACE 4
NO .WORD -345
WEE .WORD +245
YES .BYTE \255
SOS .BYTE \137
    .BYTE 'c'
NEXT .BYTE \23

; if (NO == WEE) Zombie = NO + WEE ELSE Zombie = NO - WEE
          ADR R1 NO
          ADR R2 WEE
          LDR R3 [R1] ; NO
          LDR R4 [R2] ; WEE
; if (NO == WEE)
          CMP R3 R4
          BNE ELSE
          ADD R5 R1 R2 ; NO + WEE
          ADR R1 Zombie
          STR R5 [R1]    ; Zombie = NO + WEE
          B SKIP_ELSE
ELSE      SUB R5 R1 R2 ; NO - WEE
          ADR R1 Zombie
          STR R5 [R1]    ; Zombie = NO - WEE
SKIP_ELSE SUB R1 R1 R1 ; Put the next instruction here
; while (WEE > NO) WEE = WEE - 1
          ADR R1 NO
          ADR R2 WEE
          LDR R3 [R1] ; NO
          LDR R4 [R2] ; WEE
WHILE     CMP R4 R3
          BEQ END_WHILE
          BLT END_WHILE
          MVI R5 1
          SUB R4 R4 R1 ; WEE = WEE - 1
B         WHILE
END_WHILE ADD R1 R1 R1 ; Put the next instruction here
Jumble MOV R0 R4 ; Move it
CMP R5 R1 ;
LDR R1 [R2]
LDRB R1 [R5]
STR R2 [R3]
STRB R2 [R2]
AND R1 R2 R3
ORR R4 R5 FP
MVI R2 -77 ; Comment
Zulu ADD R1 R2 R3
ADR R5 Zulu
SUB R1 R2 R3
MUL R1 R2 R5
DIV R1 R2 R5
ORR R1 R2 R5
EOR R1 R2 R5
ADR R5 Zulu
BX R4
BX R5
BL Jumble
B NEXT
BNE NXT
BGT NEXT
BLT Zulu

SWI 01
NXT SWI 07
