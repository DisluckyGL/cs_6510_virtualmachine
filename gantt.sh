#!/bin/bash

base_path=$(pwd)
osx=$base_path/build/osx

add_max_range=16
generate_osx_file () {
    echo "CHAR .BYTE 'A'" > $2
    echo "ADR R0 CHAR" >> $2
    echo "" >> $2

    local blockIndex=0
    for (( blockIndex=0; blockIndex<$1; blockIndex++ )) do
        local add_random_val=$(($RANDOM % $add_max_range))

        local randomForIndex=0
        for (( randomForIndex=0; randomForIndex<$add_random_val; randomForIndex++ )) do
            echo "ADD R0 R1 R2" >> $2
        done

        echo "SWI 1" >> $2
        echo "" >> $2
    done

    echo "SWI 0" >> $2
}

run_osx () {
    local osx_file_path=$1
    local osx_quantum=$2

/usr/bin/expect<<EOF
    set timeout 360
    spawn $osx

    expect "SHELL > "
    send -- "setquantum $osx_quantum\r"

    expect "SHELL > "
    send -- "load $osx_file_path/gantt0.osx\r"

    expect "SHELL > "
    send -- "load $osx_file_path/gantt1.osx\r"

    expect "SHELL > "
    send -- "load $osx_file_path/gantt2.osx\r"

    expect "SHELL > "
    send -- "gantt -s\r"

    expect "SHELL > "
    send -- "runlocal gantt0.osx gantt1.osx gantt2.osx\r"

    sleep 1

    expect "SHELL > "
    send -- "gantt -o\r"

    expect "SHELL > "
    send "exit\r"

    expect eof
EOF

    echo ""
}

run_all_gantt () {
    rm -rf temp_gantt || true
    mkdir -p temp_gantt/asm
    mkdir -p temp_gantt/osx
    mkdir -p temp_gantt/gantt

    local ganttIndex=0
    for (( ganttIndex=0; ganttIndex<$1; ganttIndex++ )) do
        generate_osx_file 16 temp_gantt/asm/gantt$ganttIndex.asm
        $osx temp_gantt/asm/gantt$ganttIndex.asm 0
        mv temp.osx temp_gantt/osx/gantt$ganttIndex.osx
    done

    local magnitudeIndex=0
    for (( magnitudeIndex=1; magnitudeIndex<$2; magnitudeIndex++ )) do
        local quantumIndex=0
        for (( quantumIndex=2; quantumIndex<$3; quantumIndex++ )) do
            run_osx temp_gantt/osx "3 $magnitudeIndex $(( $magnitudeIndex * $quantumIndex )) 1024"
            mv gantt.txt temp_gantt/gantt/gantt_$(( $magnitudeIndex ))_1_$(( $quantumIndex )).txt
        done
    done
}

run_all_gantt 3 8 9
#run_all_gantt 3 3 4
