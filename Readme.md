# CS 6510 OSX Assembler, Virtual Machine, and Operating System
This project is a school assignment, though I'll probably do more than what is required. If something is not documented, then it is still in the process of being developed. This project will be fully documented by the end

# Building
I'm only going to lay out the steps for building on linux. if you have questions for other operating systems, ping me on discord, my discord username is herbglitch

Building with linux, you shouldn't need any libraries. Clone the repo (probably with https unless you want to contribute) then:
```
git submodule update --init --recursive
mkdir build
cd build
cmake ..
make
```
if you run into any errors, please let me know

# Running
## Running the Assembler
to run the assembler add the file path and loader address, this will change soon
```
./build/osx path/to/osx.asm 0
```
it should kick out a temp.osx

## Running a Virtual Machine Shell
to run the virtual machine shell just run
```
./build/osx
```

# Notes
THERE IS A MEMORY LEAK, I will fix then when not stressing for time. it has to do with the ARC_Hashtable implementation I use. I need to free the nodes when done running and haven't done that yet. This should not cause an issue as the memory will be freed when the program ends, and it doesn't keep allocating during runtime. There might be other memory leaks, I'll make sure to clean them all up when I get time