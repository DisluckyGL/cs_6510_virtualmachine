cmake_minimum_required(VERSION 3.10)

project(osx)

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

set(CMAKE_C_FLAGS "-Wall -Werror -g -ggdb -DARC_DEBUG ")

set(ARCHEUS_STD_CONSOLE_BACKEND "NCURSES" CACHE STRING "use ncurses")

add_subdirectory(lib/arc)

set(OSX_SOURCES
    src/bufferpoll.c
    src/runner.c
    src/virtualmachine.c

    src/osxlang/binary.c
    src/osxlang/clock.c
    src/osxlang/errorlog.c
    src/osxlang/gantt.c
    src/osxlang/instruction.c
    src/osxlang/machine.c
    src/osxlang/memory.c
    src/osxlang/process.c
    src/osxlang/registers.c
    src/osxlang/sharedmemory.c

    src/osxlang/assembler/assembler.c
    src/osxlang/assembler/runner.c
    src/osxlang/assembler/storage.c

    src/osxlang/binary/executor.c

    src/osxlang/process/states.c
    src/osxlang/process/states/new.c
    src/osxlang/process/states/ready.c
    src/osxlang/process/states/running.c
    src/osxlang/process/states/terminate.c
    src/osxlang/process/states/waiting.c

    src/osxlang/shell/commands.c
    src/osxlang/shell/runner.c
    src/osxlang/shell/storage.c
)

add_executable(osx
    src/main.c

    ${OSX_SOURCES}
)

add_executable(test_files
    tests/test.c

    tests/assembler/test_files.c

    ${OSX_SOURCES}
)

target_include_directories(osx
    PUBLIC src/
)

target_include_directories(test_files
    PUBLIC src/
)

if(WIN32 AND NOT MSVC)
    target_link_libraries(osx PUBLIC archeus_std mingw32)
    target_link_libraries(test_files PUBLIC archeus_std mingw32)
else()
    target_link_libraries(osx PUBLIC archeus_std)
    target_link_libraries(test_files PUBLIC archeus_std)
endif()